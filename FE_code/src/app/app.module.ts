import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { LoginComponent } from "../components/login/login.component";
import { ForgotpasswordComponent } from "../components/forgotpassword/forgotpassword.component";

import { AppRoutingModule } from "./app-routing.module";
import { DataService } from "../common/services/data.service";
import { HttpDataInterceptor } from '../common/services/data.interceptor';
import { FormatSitesResponse } from "../common/services/format-sites-response.service";
import { SharedModule } from "../modules/shared/shared.module";

import { AuthenticationGuard } from "../common/guards/authentication.guard";
import { AdminGuard } from '../common/guards/admin.guard';

import { RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  declarations: [
    AppComponent, 
    LoginComponent,
    ForgotpasswordComponent
  ],
  imports: [
    AppRoutingModule, 
    SharedModule, 
    BrowserModule, 
    HttpClientModule, 
    BrowserAnimationsModule,
    RecaptchaModule.forRoot()
  ],
  providers: [
    DataService, 
    FormatSitesResponse, 
    AuthenticationGuard, 
    AdminGuard,
    { provide: HTTP_INTERCEPTORS, useClass: HttpDataInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
