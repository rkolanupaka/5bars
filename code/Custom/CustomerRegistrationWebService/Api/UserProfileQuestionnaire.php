<?php

namespace Custom\CustomerRegistrationWebService\Api;

use Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface;

class UserProfileQuestionnaire implements UserProfileQuestionnaireInterface{
	
	
	/**
	 *
	 * @var Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[]
	 */
	protected $userPreferences;
	
	/**
	 *
	 * @var Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterface
	 */
	protected $questions;
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[]|null
	 */
	public function getPreferences(){
		return $this->userPreferences;
	}
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[] $userPreferences
	 * @return void
	 */
	public function setPreferences($userPreferences){
		$this->userPreferences = $userPreferences;
	}
	
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterface|null
	 */
	public function getQuestions(){
		return $this->questions;
	}
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterface $questions
	 * @return void
	 */
	public function setQuestions($questions){
		$this->questions = $questions;
	}
	
}