<?php
namespace Custom\DataSync\Model\CronJob;

class DataSyncProcess extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var \Custom\DataSync\Model\Connection\ConnectionFactory
     */
    protected $connectionFactory;
    
    /**
     * @var \Custom\DataSync\Model\MasterProduct\MasterDataRepositoryFactory
     */
    protected $masterDataRepositoryFactory;
    
    /**
     * @var \Custom\DataSync\Model\ImpexBatch\ImpexBatchFactory
     */
    protected $impexBatchFactory;
    
    /**
     * @var \Custom\DataSync\Model\SalesProductDump\SalesProductDumpFactory
     */
    protected $salesProductDumpFactory;
    
    /**
     * @var \Custom\DataSync\Model\ReservedProducts\ReservedProductsFactory
     */
    protected $reservedProductsFactory;
    
    /**
     * @var \Custom\DataSync\Model\ProductConflict\ProductConflictFactory
     */
    protected $productConflictFactory;
    
    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    protected $transactionFactory;
    
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;
    
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;
    
    /**
     * @var \Magento\Catalog\Api\Data\ProductInterfaceFactory
     */
    protected $productFactory;
    
    /**
     * @param \Psr\Log\LoggerInterface LoggerInterface
     * @param \Custom\DataSync\Model\Connection\ConnectionFactory $connectionFactory
     * @param \Custom\DataSync\Model\MasterProduct\MasterDataRepositoryFactory  $masterDataRepositoryFactory
     * @param \Custom\DataSync\Model\ImpexBatch\ImpexBatchFactory $impexBatchFactory
     * @param \Custom\DataSync\Model\SalesProductDump\SalesProductDumpFactory $salesProductDumpFactory
     * @param \Custom\DataSync\Model\ReservedProducts\ReservedProductsFactory $reservedProductsFactory
     * @param \Custom\DataSync\Model\ProductConflict\ProductConflictFactory $productConflictFactory
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
     */ 
    
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Custom\DataSync\Model\Connection\ConnectionFactory $connectionFactory,
        \Custom\DataSync\Model\MasterProduct\MasterDataRepositoryFactory  $masterDataRepositoryFactory,
        \Custom\DataSync\Model\ImpexBatch\ImpexBatchFactory $impexBatchFactory,
        \Custom\DataSync\Model\SalesProductDump\SalesProductDumpFactory $salesProductDumpFactory,
        \Custom\DataSync\Model\ReservedProducts\ReservedProductsFactory $reservedProductsFactory,
        \Custom\DataSync\Model\ProductConflict\ProductConflictFactory $productConflictFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
        )
    {
        $this->logger = $logger;
        $this->connectionFactory = $connectionFactory;
        $this->masterDataRepositoryFactory = $masterDataRepositoryFactory;
        $this->impexBatchFactory = $impexBatchFactory;
        $this->salesProductDumpFactory = $salesProductDumpFactory;
        $this->reservedProductsFactory = $reservedProductsFactory;
        $this->productConflictFactory = $productConflictFactory;
        $this->resourceConnection = $resourceConnection;
        $this->transactionFactory = $transactionFactory;
        $this->_storeManager = $storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->productRepository = $productRepository;
        $this->registry = $registry;
        $this->productFactory = $productFactory;
    }
    
    /**
     * Execute Cronjob
     *
     * @return void
     */
    public function execute()
    {
        $this->logger->info("DataSyncProcess | execute | ******************Cronjob Starts******************");
        try
        {
            $connection = $this->connectionFactory->create();
            
            $masterDBConnection = $connection->getMasterDBConnection();
            $salesConnection = $connection->getSalesConnection();
            $this->logger->info("DataSyncProcess | execute | Got Master DB and Magento DB Connections");
            $transaction = $this->transactionFactory->create();
            $this->logger->info("DataSyncProcess | execute | Got Transaction");
            
            $impexBatch = $this->impexBatchFactory->create();
            $timeStamp = $impexBatch->getLastExecutionTime($salesConnection, $transaction);
            $this->logger->info("DataSyncProcess | execute | Got TimeStamp Of Last Successful Cronjob");
            $batchId = $impexBatch->getBatchIdOfLastExecutionTime($salesConnection, $transaction);
            $this->logger->info("DataSyncProcess | execute | Got Batch Id Of Last Successful Cronjob");
            
            $masterData = $this->masterDataRepositoryFactory->create();
            $results = $masterData->getDetailsFromMaster($masterDBConnection, $transaction, $timeStamp);
            $this->logger->info("DataSyncProcess | execute | Got Records From Master DB Which Are Modified Since Last Successful Cronjob");
            
            $salesProductDump = $this->salesProductDumpFactory->create();
            $salesProductDump->saveSalesProductDetails($salesConnection, $transaction, $results);
            $this->logger->info("DataSyncProcess | execute | Saved Records In Temporary Table");
            
            $reservedProducts = $this->reservedProductsFactory->create();
            $reservedProductsList = $reservedProducts->getReservedProducts($salesConnection, $transaction, $timeStamp);
            $this->logger->info("DataSyncProcess | execute | Got The List Of Products Which Are Placed As Part Of Order ::: ".sizeof($reservedProductsList));
            
            $processedRecords = $this->processRecords($results, $reservedProductsList, $batchId);
            $this->logger->info("DataSyncProcess | execute | Got processedRecords");

            $conflictRecords = $processedRecords[0];
            $inactiveRecords = $processedRecords[1];
            $modifyRecords = $processedRecords[2];
            $newRecords = $processedRecords[3];
            $newOrders = $processedRecords[4];
            
            $this->logger->info("DataSyncProcess | execute | Size of conflictRecords::::" .sizeof($conflictRecords));
            $this->logger->info("DataSyncProcess | execute | Size of inactiveRecords::::" .sizeof($inactiveRecords));
            $this->logger->info("DataSyncProcess | execute | Size of modifyRecords::::" .sizeof($modifyRecords));
            $this->logger->info("DataSyncProcess | execute | Size of newRecords::::" .sizeof($newRecords));
            $this->logger->info("DataSyncProcess | execute | Size of newOrders::::" .sizeof($newOrders));
            
            if(sizeof($conflictRecords) > 0)
            {
                $this->logger->info("DataSyncProcess | execute | conflictRecords > 0");
                $productConflict = $this->productConflictFactory->create();
                $productConflict->saveConflictProductDetails($salesConnection, $transaction, $conflictRecords);
                $this->sendCustomEmail($conflictRecords);
            }
            $this->logger->info("DataSyncProcess | execute | After conflict records");
            
            if(sizeof($inactiveRecords) > 0)
            {
                $this->logger->info("DataSyncProcess | execute | inactiveRecords > 0");
                foreach($inactiveRecords as $sku)
                {
                    try
                    {
                        $product = $this->productFactory->create();
                        $product->load($product->getIdBySku($sku)); //loading the existing product
                        $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
                        $this->productRepository->save($product);
                        $this->logger->info("DataSyncProcess | execute | Disabled the product - SKU - ".$sku);
                        //$this->registry->register('isSecureArea', true);
                        //$this->productRepository->deleteById($sku);
                    }
                    catch(\Exception $e)
                    {
                        $this->logger->info("DataSyncProcess | execute | Exception While Saving Inactive Product - SKU - ".$sku." ::: ".$e->getMessage());
                    }
                }
            }
            $this->logger->info("DataSyncProcess | execute | After inactive records");
            
            if(sizeof($modifyRecords) > 0)
            {
                $this->logger->info("DataSyncProcess | execute | modifyRecords > 0");
                for($count = 0; $count < sizeof($modifyRecords); $count++)
                {
                    $values = explode(",", $modifyRecords[$count]);
                    //$this->logger->info("DataSyncProcess | execute | Got Modified Record::: ".$modifyRecords[$count]);
                    try
                    {
                        $product = $this->productFactory->create();
                        $product->load($product->getIdBySku($values[1])); //loading the existing product
                        $this->setProductDetails($product, $values);
                    }
                    catch(\Exception $e)
                    {
                        $this->logger->info("DataSyncProcess | execute | Exception While Saving Modified Product - SKU - ".$values[1]." ::: ".$e->getMessage());
                    }
                }
                $this->logger->info("DataSyncProcess | execute | Product Details Are Saved To Magento For Modified Products");
            }
            $this->logger->info("DataSyncProcess | execute | After modify records");
            
            if(sizeof($newRecords) > 0)
            {
                $this->logger->info("DataSyncProcess | execute | newRecords > 0");
                for($count = 0; $count < sizeof($newRecords); $count++)
                {
                    $values = explode(",", $newRecords[$count]);
                    //$this->logger->info("DataSyncProcess | execute | Got New Record::: ".$newRecords[$count]);
                    try
                    {
                        $product = $this->productFactory->create();
                        $this->setProductDetails($product, $values);
                    }
                    catch(\Exception $e)
                    {
                        $this->logger->info("DataSyncProcess | execute | Exception While Saving New Product ::: ".$e->getMessage());
                    }
                }
                $this->logger->info("DataSyncProcess | execute | Product Details Are Saved To Magento For New Products");
                
                $masterData->updateMasterProduct($masterDBConnection, $transaction, $newRecords);
                $this->logger->info("DataSyncProcess | execute | Master Product Table Is Updated For New Records");
            }
            $this->logger->info("DataSyncProcess | execute | After new records");
            
            if(sizeof($newOrders) > 0)
            {
                $this->logger->info("DataSyncProcess | execute | newOrders > 0");
                $masterData->updateOrderRecordsMasterProduct($masterDBConnection, $transaction, $newOrders);
                $this->logger->info("DataSyncProcess | execute | Master Product Table Is Updated For New Orders");
            }
            $this->logger->info("DataSyncProcess | execute | After new orders");
            
            $impexBatch->updateCronjobTime($salesConnection, $transaction);
            $this->logger->info("DataSyncProcess | execute | Impex Batch Table Is Updated With Time");
            
            $salesConnection->truncateTable('master_product_dump');
            $this->logger->info("DataSyncProcess | execute | Master Product Dump Table Is Truncated");
            
            $transaction->save();
        }
        catch(\Exception $e)
        {
            $salesConnection->rollBack();
            $masterDBConnection->rollBack();
            $this->logger->info("DataSyncProcess | execute | Exception from cronjob::: ".$e->getMessage());
        }
        
        $this->logger->info("DataSyncProcess | execute | ******************Cronjob Ends******************");
    }
    
    public function setProductDetails($product, $values)
    {
        $this->logger->info("DataSyncProcess | setProductDetails");
        //8,SOU000001,1,1,0,,,-119.89357328600,36.82259024040,150,750,3000,Other,Steel,Fresno,California,2017-11-10 05:57:25,2017-11-23 06:59:14,admin
        $product->setSku($values[1]);
        $product->setName($values[1]);
        $product->setAttributeSetId(4);
        $product->setStatus($values[2]);
        $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
        $product->setTypeId('virtual'); // type of product (simple/virtual/downloadable/configurable)
        $product->setReserved($values[4]);
        $product->setLongitude($values[7]);
        $product->setLatitude($values[8]);
        $product->setMonthlyCharges($values[9]);
        $product->setRentLeaseFee($values[9]);
        $product->setSiteSelectionFee($values[10]);
        $product->setSiteSurveyLeaseFee($values[11]);
        $product->setCityArea($values[12]);
        $product->setPoleClass($values[13]);
        $product->setCity($values[14]);
        $product->setState($values[15]);
        $product->setPrice($values[9]); // price of product
        $product->setStockData(array('use_config_manage_stock' => 0,'manage_stock' => 1,'min_sale_qty' => 1,'max_sale_qty' => 1,'is_in_stock' => 1,'qty' => 1));
        $product->setQuantityAndStockStatus(['qty' => 1, 'is_in_stock' => 1]);
        $this->productRepository->save($product);
    }
    
    public function processRecords($results, $reservedProductsList, $batchId)
    {
        $this->logger->info("DataSyncProcess | processRecords");
        $conflictRecords = array();
        $inactiveRecords = array();
        $modifyRecords = array();
        $newRecords = array();
        $newOrders = array();
        $conflictReservedProductsList = array();
        $processedRecords = array();
        
        try
        {
            if(null != $results)
            {
                foreach($results as $result)
                {
                    $isConflictFound = false;
                    $isNewOrder = false;
                    $resultArray = array();
                    $resultArray = $result;
                    $strOfResults = implode(",", $resultArray) ;
                    //$this->logger->info("DataSyncProcess | processRecords | result:::::::::".$strOfResults);
                    $valuesOfResult = explode(",", $strOfResults);
                    
                    if(null != $reservedProductsList)
                    {
                        foreach($reservedProductsList as $reservedProduct)
                        {
                            $reservedItemArray = array();
                            $reservedItemArray = $reservedProduct;
                            $strOfReservedProducts = implode(",", $reservedItemArray) ;
                            //$this->logger->info("DataSyncProcess | processRecords | reservedProduct::::::::".$strOfReservedProducts);
                            $valuesOfReservedProductList = explode(",", $strOfReservedProducts);
                            
                            if($valuesOfResult[1] == $valuesOfReservedProductList[1]) //checking if the sku is present in both the arrays
                            {
                                $this->logger->info("DataSyncProcess | processRecords | Found Conflict - SKU ::: ".$valuesOfResult[1]);
                                $string = "'".$valuesOfResult[1]."','".$valuesOfReservedProductList[2]."'";
                                //$this->logger->info("DataSyncProcess | processRecords | checkConflicts::::".$string);
                                $conflictRecords[] = $string;
                                $conflictReservedProductsList[] = $reservedProduct;
                                $isConflictFound = true;
                                break;
                            }
                        }
                    }
                    if(!$isConflictFound)
                    {
                        if($valuesOfResult[2] == 0) //$valuesOfResult[2] indicates status of result - active/inactive
                        {
                            $this->logger->info("DataSyncProcess | processRecords | Found Inactive Product - SKU ::: ".$valuesOfResult[1]);
                            $inactiveRecords[] = $valuesOfResult[1];
                        }
                        else if($valuesOfResult[3] == 1) //$valuesOfResult[3] indicates new product - boolean - 1/0 = yes/no
                        {
                            $this->logger->info("DataSyncProcess | processRecords | Found New Product - SKU ::: ".$valuesOfResult[1]);
                            $newRecords[] = $strOfResults;
                        }
                        else //if none of the above, then some other attribute has been modified
                        {
                            $this->logger->info("DataSyncProcess | processRecords | Found Modified Record - SKU ::: ".$valuesOfResult[1]);
                            $modifyRecords[] = $strOfResults;
                        }
                    }
                }
            }
            
            //$newOrders = array_diff($reservedProductsList, $conflictReservedProductsList);
            
            /* try
            {
                $newOrders = array_diff_uassoc($reservedProductsList, $conflictReservedProductsList, 
                    function ($obj_a, $obj_b)
                    {
                        $reservedItemArray_a = array();
                        $reservedItemArray_a = $obj_a;
                        $strOfReservedItems_a = implode(",", $reservedItemArray_a) ;
                        $valuesOfReservedItemList_a = explode(",", $strOfReservedItems_a);
                        
                        $reservedItemArray_b = array();
                        $reservedItemArray_b = $obj_b;
                        $strOfReservedItems_b = implode(",", $reservedItemArray_b) ;
                        $valuesOfReservedItemList_b = explode(",", $strOfReservedItems_b);
                        
                        if($valuesOfReservedItemList_a[1] == $valuesOfReservedItemList_b[1])
                        {
                            return 0;
                        }
                        return -1;
                });
                //$this->logger->info("DataSyncProcess | processRecords | Size of newOrders :::" .sizeof($newOrders));
            }
            catch(\Exception $e)
            {
                $this->logger->info("DataSyncProcess | execute | Exception from udiff::: ".$e->getMessage());
            } */
            
            //$this->logger->info("DataSyncProcess | processRecords | Size of results :::" .sizeof($results));
            //$this->logger->info("DataSyncProcess | processRecords | Size of reservedProductsList :::" .sizeof($reservedProductsList));
            //$this->logger->info("DataSyncProcess | processRecords | Size of conflictRecords :::" .sizeof($conflictRecords));
            
            if(null != $reservedProductsList && sizeof($reservedProductsList) > 0)
            {
                //$this->logger->info("DataSyncProcess | processRecords | null != reservedProductsList && sizeof(reservedProductsList) > 0");
                foreach ($reservedProductsList  as $reservedItem)
                {
                    $conflictCount = 1;
                    $reservedItemArray = array();
                    $reservedItemArray = $reservedItem;
                    $strOfReservedItems = implode(",", $reservedItemArray) ;
                    //$this->logger->info("DataSyncProcess | processRecords | reservedItem ::::::::".$strOfReservedItems);
                    $valuesOfReservedItemList = explode(",", $strOfReservedItems);
                    
                    if(sizeof($conflictRecords) == 0)
                    {
                        //$this->logger->info("DataSyncProcess | processRecords | sizeof(conflictRecords) == 0 ::: ".sizeof($conflictRecords));
                        $reservedItemArray = array();
                        $reservedItemArray = $reservedItem;
                        $strOfReservedItems = implode(",", $reservedItemArray) ;
                        $valuesOfReservedItemList = explode(",", $strOfReservedItems);
                        
                        $this->logger->info("DataSyncProcess | processRecords | Found New Order - SKU ::: ".$valuesOfReservedItemList[1]);
                        $newOrders[] = $valuesOfReservedItemList[1];
                    }
                    else
                    {
                        for($count = 0; $count < sizeof($conflictRecords); $count++)
                        {
                            try
                            {
                                //$this->logger->info("DataSyncProcess | processRecords | conflictItem ::::::::".$conflictRecords[$count]);
                                $valuesOfConflictItemsList = explode(",", $conflictRecords[$count]);
                                
                                //$this->logger->info("DataSyncProcess | processRecords | valuesOfReservedItemList[1] ::: ".$valuesOfReservedItemList[1]);
                                //$this->logger->info("DataSyncProcess | processRecords | valuesOfConflictItemsList[0] ::: ".substr($valuesOfConflictItemsList[0],1,9));
                                //$this->logger->info("DataSyncProcess | processRecords | compare ::: ".strcmp($valuesOfReservedItemList[1], substr($valuesOfConflictItemsList[0],1,9)));
                                //if not equal then add to $newOrders
                                if(strcmp($valuesOfReservedItemList[1], substr($valuesOfConflictItemsList[0],1,9)) == 0)
                                {
                                    //$this->logger->info("DataSyncProcess | processRecords | Found Conflicted Record - SKU ::: ".$valuesOfReservedItemList[1]);
                                    break;
                                }
                                if(strcmp($valuesOfReservedItemList[1], substr($valuesOfConflictItemsList[0],1,9)) !== 0 && $conflictCount == sizeof($conflictRecords))
                                {
                                    $this->logger->info("DataSyncProcess | processRecords | Found New Order - SKU ::: ".$valuesOfReservedItemList[1]);
                                    $newOrders[] = $valuesOfReservedItemList[1];
                                    break;
                                }
                                //$this->logger->info("DataSyncProcess | processRecords | Did not satisfy if conditions - SKU ::: ".$valuesOfReservedItemList[1]);
                            }
                            catch(\Exception $e)
                            {
                                $this->logger->info("DataSyncProcess | execute | Exception from cronjob::: ".$e->getMessage());
                            }
                            //$this->logger->info("DataSyncProcess | execute | conflictCount ::: ".$conflictCount);
                            $conflictCount++;
                        }
                    }
                }
            }
        }
        catch(\Exception $e)
        {
            $this->logger->info("DataSyncProcess | execute | Exception from cronjob::: ".$e->getMessage());
        }
        
        //adding all the arrays to $processedRecords which is an array or arrays
        $processedRecords[] = $conflictRecords;
        $processedRecords[] = $inactiveRecords;
        $processedRecords[] = $modifyRecords;
        $processedRecords[] = $newRecords;
        $processedRecords[] = $newOrders;
        return $processedRecords;
    }
    
    public function sendCustomEmail($conflictRecords)
    {
        $this->logger->info("DataSyncProcess | sendCustomEmail | Start");
        $store = $this->_storeManager->getStore()->getId();
        $transport = $this->_transportBuilder->setTemplateIdentifier('datasyncprocess_email_template')
            ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
            ->setTemplateVars($conflictRecords)
            ->setFrom('general')
            ->addTo('testfivebars@gmail.com', 'Five Bars')
            ->getTransport();
        $transport->sendMessage();
        $this->logger->info("DataSyncProcess | sendCustomEmail | End");
        return $this;
    }
}