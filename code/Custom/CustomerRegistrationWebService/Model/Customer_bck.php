<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomerRegistrationWebService\Model;

use Custom\CustomerRegistrationWebService\Api\CustomerInterface;

/**
 * Defines the implementation class of the calculator service contract.
 */
class Customer implements CustomerInterface
{
    
    /**
     * Compute mid-point between two points.
     *
     * @api
     * @param $email The Email.
     * @param $firstName The First Name
     * @param $lastName The Last Name
     * @return $fullName The Full Name.
     */
    public function register($email, $firstName, $lastName) {
        echo "start";
        $userData = array("username" => "admin", "password" => "Adm1n123");
        $ch = curl_init("http://".$_SERVER['SERVER_NAME'].":8888/magento/index.php/rest/V1/integration/admin/token");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Lenght: " . strlen(json_encode($userData))));

        $token = curl_exec($ch);
        echo "Admin Token".$token;

        $customerData = [
            'customer' => [
                "email" => $email,
                "firstname" => $firstName,
                "lastname" => $lastName,
                "storeId" => 1,
                "websiteId" => 1
            ],
            "password" => "Demo1234"
        ];

        
        $ch = curl_init("http://".$_SERVER['SERVER_NAME'].":8888/magento/index.php/rest/V1/customers");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

        $result = curl_exec($ch);

        $result = json_decode($result, 1);
        return $result;
        
    }
    
    /**
     * Compute mid-point between two points.
     *
     * @api

     * @return $success The Full Name.
     */
    public function details(){
        $success="Success";
        $customerData = [
            'customer' => [
                'id' => 10,
                "email" => "user@example.com",
                "firstname" => "John2",
                "lastname" => "Doe2",
                "storeId" => 1,
                "websiteId" => 1
            ],
            "password" => "Demo1234"
        ];

        $ch = curl_init("http://magento213/index.php/rest/V1/customers/10");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

        $result = curl_exec($ch);

        $result = json_decode($result, 1);
        echo '<pre>';print_r($result);
        return $success;
    }
}