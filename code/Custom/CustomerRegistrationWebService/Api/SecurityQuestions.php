<?php

namespace Custom\CustomerRegistrationWebService\Api;

use Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface;

class SecurityQuestions implements SecurityQuestionsInterface{
	/**
	 *
	 * @var int
	 */
	protected $setId;
	
	/**
	 *
	 * @var int
	 */
	protected $qId;
	
	/**
	 *
	 * @var string
	 */
	protected $question;
	
	public function jsonSerialize(){
		return array(
				'setId' => $this->setId,
				'qId' => $this->qId,
				'question' => $this->question,
				
		);
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getId(){
		return $this->setId;
	}
	
	/**
	 * @api
	 * @param int $setId
	 * @return void
	 */
	public function setId($setId){
		$this->setId = $setId;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getQId(){
		return $this->qId;
	}
	
	/**
	 * @api
	 * @param int $qId
	 * @return void
	 */
	public function setQId($qId){
		$this->qId = $qId;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getQuestion(){
		return $this->question;
	}
	
	/**
	 * @api
	 * @param string $question
	 * @return void
	 */
	public function setQuestion($question){
		$this->question = $question;
	}
}