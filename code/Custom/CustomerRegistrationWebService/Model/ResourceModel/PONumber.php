<?php
namespace Custom\CustomerRegistrationWebService\Model\ResourceModel;




class PONumber extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * constructor
     * 
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_entity_ponumber', 'po_id');
    }

    /**
     * Retrieves Post Name from DB by passed id.
     *
     * @param string $id
     * @return array
     */
    /* public function getPONumberByCustomerId($id)
    {
    	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/debug.log');
    	$logger = new \Zend\Log\Logger();
    	$logger->addWriter($writer);
        $adapter = $this->getConnection();
        $logger->info("connection started");
        $select = $adapter->select()
            ->from($this->getMainTable(), '*')
            ->where('customer_id = :customer_id');
        $binds = ['customer_id' => (int)$id];
        return $adapter->fetchAll($select, $binds, null);
    } */
    
}