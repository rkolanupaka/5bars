<?php
namespace Custom\CustomerRegistrationWebService\Api;

interface UserProfileQuestionnaireInterface{
	
	
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[]|null
	 */
	public function getPreferences();
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[] $userPreferences
	 * @return void
	 */
	public function setPreferences($userPreferences);
	
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterface|null
	 */
	public function getQuestions();
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterface $questions
	 * @return void
	 */
	public function setQuestions($questions);
}