<?php
namespace Custom\DataSync\Api\Data;

/**
 * Interface ProductConflictInterface.php
 * @api
 */
interface ProductConflictInterface
{
    /**
     * Saves conflicted products
     *
     * @return void
     */
    public function saveConflictProductDetails($connection, $transaction, $conflictRecords);
    
}