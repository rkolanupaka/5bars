<?php

namespace Custom\DataSync\Model\ImpexBatch;

use Custom\DataSync\Api\Data\ImpexBatchInterface;

class ImpexBatch extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,ImpexBatchInterface
{
    
    const CACHE_TAG = 'impex_batch';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var TimezoneInterface
     */
    private $timezone;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
        ) 
    {
            $this->logger = $logger;
            $this->date = $date;
            $this->timezone = $timezone;
    }
    
    protected function _construct()
    {
        $this->_init('\Custom\DataSync\Model\ResourceModel\ImpexBatch');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        
        return $values;
    }
    
    
    public function getSysDate()
    {
        $this->logger->info("ImpexBatch | getSysDate");
        // for get current time according to time zone
        $time = $this->timezone->scopeTimeStamp();
        $date1 = new \Zend_Date($time, \Zend_Date::TIMESTAMP);
        $date2 = date_create($date1);
        $date = date_format($date2, 'Y-m-d H:i:s');
        return $date;
    }
    
    public function getYesterdayDate()
    {
        $this->logger->info("ImpexBatch | getYesterdayDate");
        // for get current time according to time zone
        $time = $this->timezone->scopeTimeStamp();
        $date1 = new \Zend_Date($time, \Zend_Date::TIMESTAMP);
        $date2 = date_create($date1);
        $date3 = date_format($date2, 'Y-m-d H:i:s');
        $date = date('Y-m-d H:i:s', strtotime($date3 .' -1 day'));
        return $date;
    }
    
    public function updateCronjobTime($connection, $transaction)
    {
        try
        {
            $this->logger->info("ImpexBatch | updateCronjobTime");
            //$date = $this->getSysDate();
            $sql = "INSERT INTO magento.impex_batch (start_time) VALUES (CURRENT_TIMESTAMP)";
            $this->logger->info("ImpexBatch | updateCronjobTime | ::: ".$sql);
            $connection->query($sql);
        }
        catch (\Exception $e)
        {
            $this->logger->info("ImpexBatch | updateCronjobTime | Exception While Updating Cronjob Execution Time::: ".$e->getMessage());
        }
    }
    
    public function getLastStartTime($connection)
    {
        $this->logger->info("ImpexBatch | getLastStartTime");
        $sql = "SELECT MAX(start_time) FROM magento.impex_batch";
        return $connection->fetchAll($sql);
    }
    
    public function getBatchIdOfLastStartTime($connection)
    {
        $this->logger->info("ImpexBatch | getBatchIdOfLastStartTime");
        $sql = "SELECT batch_id FROM magento.impex_batch WHERE start_time = (SELECT MAX(start_time) FROM magento.impex_batch)";
        return $connection->fetchAll($sql);
    }
    
    public function getLastExecutionTime($connection, $transaction)
    {
        $this->logger->info("ImpexBatch | getLastExecutionTime");
        $results = $this->getLastStartTime($connection);
        $str = null;
        if(null != $results)
        {
            foreach($results as $result)
            {
                $resultArray = array();
                $resultArray = $result;
                $str = implode(",", $resultArray) ;
                if(strlen($str) > 0)
                {
                    return $str;
                }
                else
                {
                    return $this->getYesterdayDate();
                }
            }
        }
        return null;
    }
    
    public function getBatchIdOfLastExecutionTime($connection, $transaction)
    {
        $this->logger->info("ImpexBatch | getBatchIdOfLastExecutionTime");
        $results = $this->getBatchIdOfLastStartTime($connection);
        $str = null;
        if(null != $results)
        {
             foreach($results as $result)
             {
                 $resultArray = array();
                 $resultArray = $result;
                 $str = implode(",", $resultArray) ;
             }
             return $str;
        }
        return null;
    }
    
}