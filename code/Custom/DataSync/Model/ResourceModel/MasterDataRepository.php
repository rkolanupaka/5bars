<?php
namespace Custom\DataSync\Model\ResourceModel;

class MasterDataRepository extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * constructor
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
        )
    {
        parent::__construct($context);
    }
    
    protected function _construct()
    {
        
    }
    
    
}