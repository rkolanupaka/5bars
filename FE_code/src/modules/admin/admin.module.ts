import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { SiteManagementComponent } from '../../components/site-management/site-management.component';
import { ReportsComponent } from '../../components/reports/reports.component';
import { AdminRouterModule } from './admin.module.routing';
import { SiteRequestsComponent } from '../../components/site-requests/site-requests.component';

@NgModule({
    imports: [SharedModule, AdminRouterModule],
    declarations: [ReportsComponent, SiteManagementComponent, SiteRequestsComponent],
    exports: [ReportsComponent, SiteManagementComponent, SiteRequestsComponent]
})

export class AdminModule {
    constructor (){}
}