<?php
namespace Custom\SiteManagement\Api\Data;
interface CitiesInterface
{
	/**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
	const CITY_ID = 'city_id';
	const CITY = 'city';
	const STATE_CODE = 'state_code';
	const ENABLED = 'enabled';
	const COMING_SOON = 'coming_soon';
	const ADMIN_EMAILID = 'admin_emailID';
	
	/**#@-*/
	
	
}