<?php

namespace Custom\SiteManagement\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface StatesResultInterface
{
	/**
	 * Get attributes list.
	 *
	 * @return \Custom\SiteManagement\Api\Data\StatesInterface[]
	 */
	public function getItems();
	
	/**
	 * Set attributes list.
	 *
	 * @param \Custom\SiteManagement\Api\Data\StatesInterface[] $items
	 * @return $this
	 */
	public function setItems(array $items);
	
}
