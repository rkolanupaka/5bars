<?php


namespace Custom\CustomerRegistrationWebService\Api;

use Magento\Framework\Api\AbstractSimpleObject;
use Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterface;
use Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterface;

/**
 * SearchResults Service Data Object used for the search service requests
 */
class SetUpProfileResult extends AbstractSimpleObject implements SetUpProfileResultInterface
{
    const PO_NUMBER = 'poNumber';
    const PROJECT_CONTACT = 'projectContact';

    /**
     * Get attribute.
     *
     * @return \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterface
     */
    public function getProjectContact()
    {
    	return $this->_get(self::PROJECT_CONTACT);
    }
    
    /**
     * Set attribute.
     *
     * @param \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterface $projectContact
     * @return $this
     */
    public function setProjectContact($projectContact)
    {
    	return $this->setData(self::PROJECT_CONTACT, $projectContact);
    }
    
    /**
     * Get attribute.
     *
     * @return \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterface
     */
    public function getPoNumber()
    {
    	return $this->_get(self::PO_NUMBER);
    }
    
    /**
     * Set attribute.
     *
     * @param \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterface $poNumber
     * @return $this
     */
    public function setPoNumber($poNumber)
    {
    	return $this->setData(self::PO_NUMBER, $poNumber);
    }
    
}
