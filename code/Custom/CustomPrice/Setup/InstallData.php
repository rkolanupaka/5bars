<?php
namespace Custom\CustomPrice\Setup;
 
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
 
    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;
 
    /**
     * @var SalesSetup
     */
    private $salesSetupFactory;
 
    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory
    )
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }
 
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		
        /** @var QuoteSetup $quoteSetup */
        $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
 
        /** @var SalesSetup $salesSetup */
        $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
		
		/**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'survey_lease_selected',
            [
                'type'                    => 'int',
                'label'                   => 'Survey Lease Selected',
                'input'                   => 'select',
                'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible'                 => true,
                'required'                => false,
                'user_defined'            => true,
                'default'                 => 'No',
                'searchable'              => false,
                'filterable'              => false,
                'comparable'              => false,
                'visible_on_front'        => false,
                'used_in_product_listing' => false,
                'unique'                  => false,
                'option'                  => [
                    'values' => [
                        'Yes',
                        'No'
                    ],
                ]
            ]
        );
 
        $attributeOptions = [
            'type' => 'varchar',
            'label' => 'Survey Lease Selected',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true
        ];
        $quoteSetup->addAttribute('quote_item', 'survey_lease_selected', $attributeOptions);
        $salesSetup->addAttribute('order_item', 'survey_lease_selected', $attributeOptions);
        
        
        /**
         * Adding attribute has_conduit_requested to the eav/attribute
         */
        $eavSetup->addAttribute(
        		\Magento\Catalog\Model\Product::ENTITY,
        		'has_conduit_requested',
        		[
        				'type'                    => 'int',
        				'label'                   => 'Has Conduit Requested',
        				'input'                   => 'select',
        				'global'                  => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
        				'visible'                 => true,
        				'required'                => false,
        				'user_defined'            => true,
        				'default'                 => 'No',
        				'searchable'              => false,
        				'filterable'              => false,
        				'comparable'              => false,
        				'visible_on_front'        => false,
        				'used_in_product_listing' => false,
        				'unique'                  => false,
        				'option'                  => [
        						'values' => [
        								'Yes',
        								'No'
        						],
        				]
        		]
        		);
        
        $attributeOptions = [
        		'type' => 'varchar',
        		'label' => 'Has Conduit Requested',
        		'input' => 'text',
        		'required' => false,
        		'visible' => true,
        		'user_defined' => true
        ];
        $quoteSetup->addAttribute('quote_item', 'has_conduit_requested', $attributeOptions);
        $salesSetup->addAttribute('order_item', 'has_conduit_requested', $attributeOptions);
    
    }
}