<?php

namespace Custom\CustomOrder\Plugin;

class CustomSetAdminMailInOrders{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
	
	/**
	 * @param \Psr\Log\LoggerInterface $logger
	 */
	public function __construct(
			\Psr\Log\LoggerInterface $logger
			) {
				$this->logger = $logger;
	}
	
	public function aroundSend(\Magento\Sales\Model\Order\Email\Sender $subject,
			\Closure $proceed,
			\Magento\Sales\Model\Order $order
			){
		$this->logger->info("^^^^^^^^Into Around Plugin****");
	}
}