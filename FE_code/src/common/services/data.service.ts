import { Injectable, Inject } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { HttpHeaders } from '@angular/common/http';

import { HttpClient } from '@angular/common/http';
import {DOCUMENT} from '@angular/common';

import { environment } from '../../environments/environment';

@Injectable()
export class DataService {
  baseHost:string;
  loggenUserData:any;
  token:string;
  // baseLocalHost;

  constructor(public http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseHost = 'http://'+ this.document.location.hostname;
    // this.baseLocalHost = environment.baseLocalHost;
  }

  fetchData(url, headerOpt?){
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.get(url, httpOptions);
  }

  fetchPlainData(url, headerOpt?) {
    return this.http.get(url, {responseType: 'text'});
  }

  fetchPostData(url, body, headerOpt?){
    let bodyString = JSON.stringify(body); // Stringify payload
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.post(url,body, httpOptions);
  }

  fetchPutData(url, body, headerOpt?){
    let bodyString = JSON.stringify(body); // Stringify payload
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.put(url,body, httpOptions);
  }

  fetchDeleteData(url, headerOpt?){
    const httpOptions = {
      headers: new HttpHeaders(headerOpt)
    };
    return this.http.delete(url, httpOptions);
  }

  getHostName() {
    return this.baseHost;
  }

  // getLocalHostName() {
  //   return this.baseLocalHost;
  // }
}