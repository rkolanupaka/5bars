<?php

namespace Custom\CustomPrice\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface CartItemsResultInterface
{
	/**
	 * Get Unsaved Cart Items
	 *
	 * @return array
	 */
	public function getUnsavedItems();
	
	/**
	 * Set Unsaved Cart Items.
	 *
	 * @param array $unsavedItems
	 * @return $this
	 */
	public function setUnsavedItems($unsavedItems);
	
	/**
	 * Get Saved Cart Items.
	 *
	 * @return \Magento\Quote\Api\Data\CartItemInterface[]
	 */
	public function getSavedItems();
	
	/**
	 * Set Saved Cart Items.
	 *
	 * @param \Magento\Quote\Api\Data\CartItemInterface[] $savedItems
	 * @return $this
	 */
	public function setSavedItems($savedItems);
	
}
