<?php 
namespace Custom\CustomerRegistrationWebService\Model;

class ProjectContact extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,
\Custom\CustomerRegistrationWebService\Api\Data\ProjectContactInterface
{
    const CACHE_TAG = 'customer_entity_project_contact';

    protected $_cacheTag = 'customer_entity_project_contact';

    protected $_eventPrefix = 'customer_entity_project_contact';

    protected function _construct()
    {
        $this->_init('Custom\CustomerRegistrationWebService\Model\ResourceModel\ProjectContact');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
    
}