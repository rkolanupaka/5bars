import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ComingSoonComponent } from '../../components/coming-soon/coming-soon.component';
import { ComingSoonModuleRouterModule } from './coming-soon.module.routing';

@NgModule({
    imports: [SharedModule, ComingSoonModuleRouterModule],
    declarations: [ComingSoonComponent],
    exports: [ComingSoonComponent]
})

export class ComingSoonModule {
    constructor (){}
}