import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router} from '@angular/router';

import { DataService } from '../../common/services/data.service';

@Component({
  selector: 'site-selex-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  eyeOpen:boolean = false;
  hostValue:string;
  passwordMatched:boolean = false;
  paramId:string;
  paramToken:string;
  pass8charVali:boolean = false;
  passucaseVali:boolean = false;
	passlcaseVali:boolean = false;
	passnumVali:boolean = false;
  passscharVali:boolean = false;
  resetPswrdFormFullvalid:boolean = false;
  resetPasswordSuccess:boolean = false;
  showLoader:boolean = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  validUrl:boolean = false;
  
  ucase = new RegExp("[A-Z]+");
	lcase = new RegExp("[a-z]+");
	num = new RegExp("[0-9]+");
  schar = new RegExp("[!@#\$%\^&\*]+");

  @ViewChild('resetConfirmPassword') resetConfirmPassword;
  @ViewChild('resetPassword') resetPassword;

  constructor(private activatedRoute: ActivatedRoute, private dataservice: DataService, private router: Router) { }

  ngOnInit() {
    this.hostValue = this.dataservice.getHostName();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.paramId = params['id'];
      this.paramToken = params['token'];
      var url = this.hostValue+'/magento/index.php/rest/V1/customers/'+this.paramId+'/password/resetLinkToken/'+this.paramToken;
      const headerDict = {
        'Content-Type': 'application/json'
      }
      this.showLoader = true;
      this.dataservice.fetchData(url, headerDict).subscribe((data)=>{
        this.showLoader = false;
        this.validUrl = true;
      }, (err)=> {
        this.showLoader = false;
        this.showPopUpMessage = "The link you used to sign in has expired. Please reset your password and restart the process.";
        this.showPopUpTrue = true;
      })
    });
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  // full reset form validations
  checkPasswordMatchValid(resetPasswordForm) {
    let passValue = resetPasswordForm.value.password;
    if(this.ucase.test(passValue)) {
      this.passucaseVali = true;
    } else {
      this.passucaseVali = false;
    }
    if(this.lcase.test(passValue)) {
      this.passlcaseVali = true;
    } else {
      this.passlcaseVali = false;
    }
    if(this.num.test(passValue)) {
      this.passnumVali = true;
    } else {
      this.passnumVali = false;
    }
    if(this.schar.test(passValue)) {
      this.passscharVali = true;
    } else {
      this.passscharVali = false;
    }
    if(passValue.length >= 8) {
      this.pass8charVali = true;
    } else {
      this.pass8charVali = false;
    }
    if(passValue == resetPasswordForm.value.confirmPassword) {
      this.passwordMatched = false;
    } else {
      this.passwordMatched = true;
    }

    if(this.passucaseVali && this.passlcaseVali && this.passnumVali && this.passscharVali && this.pass8charVali && !this.passwordMatched) {
      this.resetPswrdFormFullvalid = true;
    } else {
      this.resetPswrdFormFullvalid = false;
    }
  }

  showHidePassword() {
    this.eyeOpen = !this.eyeOpen;
    if(this.eyeOpen) {
      this.resetConfirmPassword.nativeElement.type = "text";
      this.resetPassword.nativeElement.type = "text";
    } else {
      this.resetConfirmPassword.nativeElement.type = "password";
      this.resetPassword.nativeElement.type = "password";
    }
  }

  resetPswrdFormSubmit(resetPasswordForm) {
    let headerDict = {
      "Content-Type": "application/json"
    }
    let userData = {
      "customerId": this.paramId,
      "resetToken": this.paramToken,
      "newPassword": resetPasswordForm.value.password
    }
    let url = this.hostValue+'/magento/index.php/rest/V1/customer/password/reset';
    this.showLoader = true;
    this.dataservice.fetchPutData(url, userData, headerDict).subscribe((data)=>{
      this.showLoader = false;
      this.resetPasswordSuccess = true;
      setTimeout(() => {
        this.router.navigate(['']);
      }, 1000);
    }, (err)=>{
      this.showLoader = false;
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "Something went wrong";
      }
      this.showPopUpTrue = true;
    })
  }

}
