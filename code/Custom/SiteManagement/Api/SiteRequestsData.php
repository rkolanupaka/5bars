<?php
namespace Custom\SiteManagement\Api;
use Custom\SiteManagement\Api\SiteRequestsDataInterface;

class SiteRequestsData implements SiteRequestsDataInterface{
	
	/**
	 *
	 * @var int
	 */
	protected $id;
	
	/**
	 *
	 * @var string
	 */
	protected $sku;
	
	/**
	 *
	 * @var int
	 */
	protected $customerId;
	
	/**
	 *
	 * @var string
	 */
	protected $customerName;
	
	/**
	 *
	 * @var string
	 */
	protected $customerEmail;
	
	/**
	 *
	 * @var string
	 */
	protected $requestId;
	
	/**
	 *
	 * @var string
	 */
	protected $cityArea;
	
	/**
	 *
	 * @var float
	 */
	protected $latitude;
	
	/**
	 *
	 * @var float
	 */
	protected $longitude;
	
	/**
	 *
	 * @var string
	 */
	protected $poleClass;
	
	/**
	 *
	 * @var string
	 */
	protected $state;
	
	/**
	 *
	 * @var string
	 */
	protected $city;
	
	/**
	 *
	 * @var string
	 */
	protected $status;
	
		
	/**
	 *
	 * @var string
	 */
	protected $rejectedReason;
	
	/**
	 *
	 * @var int
	 */
	protected $siteCustomerId;
	
	/**
	 *
	 * @var \DateTime
	 */
	protected $siteAssignedDate;
	
	/**
	 *
	 * @var int
	 */
	protected $siteHoldDaysCount;
	
	/**
	 *
	 * @var int
	 */
	protected $siteRequested;
	
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getId(){
		return $this->id;
	}
	
	/**
	 * @api
	 * @param int $id
	 * @return void
	 */
	public function setId($id){
		$this->id = $id;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSku(){
		return $this->sku;
	}
	
	/**
	 * @api
	 * @param string $sku
	 * @return void
	 */
	public function setSku($sku){
		$this->sku = $sku;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getCustomerId(){
		return $this->customerId;
	}
	
	/**
	 * @api
	 * @param int $customerId
	 * @return void
	 */
	public function setCustomerId($customerId){
		$this->customerId = $customerId;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCustomerName(){
		return $this->customerName;
	}
	
	/**
	 * @api
	 * @param string $customerName
	 * @return void
	 */
	public function setCustomerName($customerName){
		$this->customerName = $customerName;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCustomerEmail(){
		return $this->customerEmail;
	}
	
	/**
	 * @api
	 * @param string $customerEmail
	 * @return void
	 */
	public function setCustomerEmail($customerEmail){
		$this->customerEmail = $customerEmail;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRequestId(){
		return $this->requestId;
	}
	
	/**
	 * @api
	 * @param string $requestId
	 * @return void
	 */
	public function setRequestId($requestId){
		$this->requestId = $requestId;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCityArea(){
		return $this->cityArea;
	}
	
	/**
	 * @api
	 * @param string $cityArea
	 * @return void
	 */
	public function setCityArea($cityArea){
		$this->cityArea = $cityArea;
	}
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLatitude(){
		return $this->latitude;
	}
	
	/**
	 * @api
	 * @param float $latitude
	 * @return void
	 */
	public function setLatitude($latitude){
		$this->latitude = $latitude;
	}
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLongitude(){
		return $this->longitude;
	}
	
	/**
	 * @api
	 * @param float $longitude
	 * @return void
	 */
	public function setLongitude($longitude){
		$this->longitude = $longitude;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getPoleClass(){
		return $this->poleClass;
	}
	
	/**
	 * @api
	 * @param string $poleClass
	 * @return void
	 */
	public function setPoleClass($poleClass){
		$this->poleClass = $poleClass;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getState(){
		return $this->state;
	}
	
	/**
	 * @api
	 * @param string $state
	 * @return void
	 */
	public function setState($state){
		$this->state = $state;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCity(){
		return $this->city;
	}
	
	/**
	 * @api
	 * @param string $city
	 * @return void
	 */
	public function setCity($city){
		$this->city = $city;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getStatus(){
		return $this->status;
	}
	
	/**
	 * @api
	 * @param string $status
	 * @return void
	 */
	public function setStatus($status){
		$this->status = $status;
	}
	

	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRejectedReason(){
		return $this->rejectedReason;
	}
	
	/**
	 * @api
	 * @param string $rejectedReason
	 * @return void
	 */
	public function setRejectedReason($rejectedReason){
		$this->rejectedReason = $rejectedReason;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSiteAssignedTo(){
		return $this->siteCustomerId;
	}
	
	/**
	 * @api
	 * @param int $siteCustomerId
	 * @return void
	 */
	public function setSiteAssignedTo($siteCustomerId){
		$this->siteCustomerId = $siteCustomerId;
	}
	
	/**
	 * @api
	 * @return \DateTime|null
	 */
	public function getSiteAssignedDate(){
		return $this->siteAssignedDate;
	}
	
	/**
	 * @api
	 * @param \DateTime $siteAssignedDate
	 * @return void
	 */
	public function setSiteAssignedDate($siteAssignedDate){
		$this->siteAssignedDate = $siteAssignedDate;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSiteHoldDaysCount(){
		return $this->siteHoldDaysCount;
	}
	
	/**
	 * @api
	 * @param int $siteHoldDaysCount
	 * @return void
	 */
	public function setSiteHoldDaysCount($siteHoldDaysCount){
		$this->siteHoldDaysCount = $siteHoldDaysCount;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSiteRequested(){
		return $this->siteRequested;
	}
	
	/**
	 * @api
	 * @param int $siteRequested
	 * @return void
	 */
	public function setSiteRequested($siteRequested){
		$this->siteRequested = $siteRequested;
	}
}