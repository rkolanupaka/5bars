<?php 
namespace Custom\CustomerRegistrationWebService\Model;

class PONumber extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,
\Custom\CustomerRegistrationWebService\Api\Data\PONumberInterface
{
    const CACHE_TAG = 'customer_entity_ponumber';

    protected $_cacheTag = 'customer_entity_ponumber';

    protected $_eventPrefix = 'customer_entity_ponumber';

    protected function _construct()
    {
        $this->_init('Custom\CustomerRegistrationWebService\Model\ResourceModel\PONumber');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
    
}