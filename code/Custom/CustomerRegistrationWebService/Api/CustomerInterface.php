<?php



namespace Custom\CustomerRegistrationWebService\Api;

use Custom\CustomerRegistrationWebService\Api\Data\PONumberInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Defines Sevice Contract for Customer class
 */
interface CustomerInterface
{
    
    /**
     * Reset customer password.
     * 
     * @api
     * @param int $customerId
     * @param string $resetToken
     * @param string $newPassword
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resetPassword($customerId, $resetToken, $newPassword);
    
    /**
     * Loads a specific order
     * 
     * @param int $id The order ID.
     * @return \Magento\Sales\Api\Data\OrderInterface Order interface.
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getOrderDetails($id);
    
    /**
     * Loads a collection of PO Numbers
     * 
     * @param int $id
     * @return \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterface
     * @throws \Magento\Framework\Exception\InputException
     */
    public function getPONumberByCustomerId($id);
    
    /**
     * 
     * @param int $customerId
     * @param string $ponumber
     * @param string $comment
     * @param int|null $poId
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function savePONumber($customerId,$ponumber,$comment,$poId=null);
    
    /**
     * Loads a collection of PO Numbers
     * 
     * @param int $id
     * @return \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterface
     * @throws \Magento\Framework\Exception\InputException
     */
    public function getProjectContactByCustomerId($id);
    
    /**
     * 
     * @param int $customerId
     * @param string $name
     * @param string $email
     * @param string $phone
     * @param string $company
     * @param int|null $contactId
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function saveProjectContact($customerId,$name,$email,$phone,$company,$contactId=null);
    
    /**
     * Revoke token by customer id
     * 
     * @param int $id
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function revokeCustomerAccessToken($id);
    
    /**
     * Create access token for admin given the customer credentials.
     *
     * @param string $username
     * @param string $password
     * @return string Token created
     * @throws \Magento\Framework\Exception\AuthenticationException
     */
    public function createCustomerAccessToken($username, $password);
    
    /**
     * Get Products List based on sku id
     * 
     * @param string $skuId
     * @param string $city
     * @param string $state
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     */
   /*  public function getList($skuId,$city=null,$state=null); */
    
    /**
     * Delete PO Number based on ID
     * 
     * @param int $id
     * @return bool
     */
    public function deletePONumber($id);
    
    /**
     * Delete Project Contact based on ID
     *
     * @param int $id
     * @return bool
     */
    public function deleteProjectContact($id);
    
    /**
     * 
     * @param int $id
     * @return \Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterface
     */
    public function getSetUpProfileDetails($id);
    
    /**
     * @return void
     */
    public function searchProducts();
    
    /**
     * @param int $customerId
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function userProfileQuestionnaire($customerId);
    
   /**
     * To Save the Security Questions Submitted by Customer
     *
     * @param \Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[] $answers
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function saveProfileQuestionnaire($answers);
    
    
    /**
     * To Update the Security Questions Submitted by Customer
     *
     * @param \Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[] $answers
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function updateProfileQuestionnaire($answers);
    
    /**
     * API to get the Security Questions basing on CustomerEMail
     * 
     * @param string $customerEmail
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function securityQuestionsInfo($customerEmail);
    
    
    /**
     * API to check if the Customer Exists with SiteSelex or not
     * 
     * @param string $emailId
     * @return bool
     */
    public function customerValidation($emailId);
    
    
    /**
     * API to check if the entered Customer info is valid or Not
     *
     * @param string $emailId
     * @param string $fName
     * @param string $lName
     * @param string $postCode
     * @param mixed $questions
     * @return \Custom\CustomerRegistrationWebService\Api\ValidationResponseInterface
     */
    public function customerInfoValidation($emailId,$fName,$lName,$postCode,$questions);
    
}