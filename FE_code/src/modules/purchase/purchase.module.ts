import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { HomeComponent } from "../../components/home/home.component";
import { SiteSearchComponent } from "../../components/site-search/site-search.component";

import { PaymentComponent } from "../../components/payment/payment.component";
import { ConfirmationComponent } from "../../components/confirmation/confirmation.component";
import { PurchaseRouterModule } from "./purchase.module.routing";
import { WaitListSitesComponent } from "../../components/wait-list-sites/wait-list-sites.component";
// import { filter } from "rxjs/operator/filter";

@NgModule({
  imports: [SharedModule, PurchaseRouterModule],
  declarations: [
    PaymentComponent,
    ConfirmationComponent,
    SiteSearchComponent,
    HomeComponent,
    WaitListSitesComponent
  ],
  exports: [
    PaymentComponent,
    ConfirmationComponent,
    SiteSearchComponent,
    HomeComponent,
    WaitListSitesComponent
  ]
})
export class PurchaseModule {
  constructor() {}
}
