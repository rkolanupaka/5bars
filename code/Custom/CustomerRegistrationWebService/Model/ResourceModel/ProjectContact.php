<?php
namespace Custom\CustomerRegistrationWebService\Model\ResourceModel;




class ProjectContact extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * constructor
     * 
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('customer_entity_project_contact', 'contact_id');
    }

}