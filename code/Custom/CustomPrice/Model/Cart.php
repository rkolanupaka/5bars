<?php 


namespace Custom\CustomPrice\Model;
use Custom\CustomerRegistrationWebService\Api\CustomerInterface;
use Custom\CustomPrice\Api\CartInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;

/**
 * Defines the implementation class of the Customer service contract.
 */
class Cart extends \Magento\Framework\App\Action\Action implements CartInterface
{
    
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    
    /**
     * @var \Magento\Quote\Api\CartItemRepositoryInterface
     */
    protected $cartItemRepository;
    
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     *
     */
    protected $_productRepo;
    
    /**
     * @var \Custom\CustomPrice\Api\Data\CartItemsResultInterfaceFactory
     */
    protected $cartItemsResultFactory;
    
    /**
     * 
     * @var \Magento\Quote\Api\Data\CartItemInterfaceFactory
     */
    protected $cartItemInterfaceFactory;
    
    /**
     * @var Magento\Quote\Api\Data\CartItemExtensionFactory
     */
    private $cartItemExtensionFactory;
    
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Quote\Api\CartItemRepositoryInterface $cartItemRepository
     * @param \Magento\Catalog\Model\ProductRepository $productRepo
     * @param \Custom\CustomPrice\Api\Data\CartItemsResultInterfaceFactory $cartItemsResultFactory
     * @param \Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemInterfaceFactory
     * @param \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
    							\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
    							\Magento\Quote\Api\CartItemRepositoryInterface $cartItemRepository,
    							\Magento\Catalog\Model\ProductRepository $productRepo,
    							\Custom\CustomPrice\Api\Data\CartItemsResultInterfaceFactory $cartItemsResultFactory,
    							\Magento\Quote\Api\Data\CartItemInterfaceFactory $cartItemInterfaceFactory,
    							\Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
    	) {
        $this->quoteRepository = $quoteRepository;
        $this->cartItemRepository = $cartItemRepository;
        $this->_productRepo = $productRepo;
        $this->cartItemsResultFactory = $cartItemsResultFactory;
        $this->cartItemInterfaceFactory = $cartItemInterfaceFactory;
        $this->cartItemExtensionFactory = $cartItemExtensionFactory ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Quote\Api\Data\CartItemExtensionFactory::class);
        parent::__construct($context);
    }
    
    
    
    
    /**
    * Override method. //TODO: Clean Up
    */
    public function execute()
    {
        
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::updateCart()
     */
    public function updateCart($surveyLeaseFlag,$hasConduitRequested,$cartItem)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	//$quoteItem = $this->cartItemRepository->save($cartItem);
    	$quoteItem = null;
    	$cartId = $cartItem->getQuoteId();
    	$quote = $this->quoteRepository->getActive($cartId);
    	$cartItems = $quote->getItems();
    	foreach ($cartItems as $item){
    		if($item->getItemId() == $cartItem->getItemId()){
    			
    			$item->setSurveyLeaseSelected($surveyLeaseFlag);
    			$item->setHasConduitRequested($hasConduitRequested);
    			$this->updatePrice($item,$surveyLeaseFlag);
    			
    			$CartProduct = $item->getProduct();
    			$product = $this->_productRepo->get($CartProduct->getSku());
    			
    			$itemExtensionAttributes = $item->getExtensionAttributes();
    			if ($itemExtensionAttributes === null) {
    				$itemExtensionAttributes = $this->cartItemExtensionFactory->create();
    			}
    			
    			$itemExtensionAttributes->setMonthlyCharges($product->getCustomAttribute('monthly_charges')->getValue());
    			 //$itemExtensionAttributes->setCamFee($product->getCustomAttribute('cam_fee')->getValue());
    			 $itemExtensionAttributes->setSiteSelectionFee($product->getCustomAttribute('site_selection_fee')->getValue());
    			 $itemExtensionAttributes->setSiteSurveyLeaseFee($product->getCustomAttribute('site_survey_lease_fee')->getValue());
    			 $itemExtensionAttributes->setRentLeaseFee($product->getCustomAttribute('rent_lease_fee')->getValue());
    			 $itemExtensionAttributes->setCityArea($product->getCustomAttribute('city_area')->getValue());
    			 $itemExtensionAttributes->setPoleClass($product->getCustomAttribute('pole_class')->getValue());
    			 $itemExtensionAttributes->setLatitude($product->getCustomAttribute('latitude')->getValue());
    			 $itemExtensionAttributes->setLongitude($product->getCustomAttribute('longitude')->getValue());
    			 $itemExtensionAttributes->setState($product->getCustomAttribute('state')->getValue());
    			 $itemExtensionAttributes->setCity($product->getCustomAttribute('city')->getValue());
    			 
    			 $item->setExtensionAttributes($itemExtensionAttributes);
    			
    			$logger->info('Survey Lease fee flag'.$surveyLeaseFlag);
    			$logger->info('Conduit Requested Flag'.$hasConduitRequested.'....'.$item->getHasConduitRequested());
    			$quoteItem = $item;
				//$quoteItem->save();
    			$logger->info("Cart | update | CITY:".$quoteItem->getCity());
    			break;
    		
    		}
    	}
    	//$quote->save();
    	$this->quoteRepository->save($quote);
    	return $quoteItem;
    }
    
    protected function updatePrice($item,$surveyLeaseFlag)
    {
    	$sku = $item->getSku();
    	$surveyLeasePrice = $this->_productRepo->get($sku)->getData('site_survey_lease_fee');
    	$currentItemPrice = $item->getOriginalCustomPrice();
    	if($surveyLeaseFlag == 1 && $currentItemPrice < $surveyLeasePrice){
    		$item->setOriginalCustomPrice($currentItemPrice + $surveyLeasePrice);
    	}
    	else if($surveyLeaseFlag == 0 && $currentItemPrice > $surveyLeasePrice){
    		$item->setOriginalCustomPrice($currentItemPrice - $surveyLeasePrice);
    	}
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomPrice\Api\CartInterface::save()
     */
    public function save($quoteId,$items)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	$savedItems = array();
    	$unsavedItems = array();
    	$cartItemsResult = $this->cartItemsResultFactory->create();
    	foreach ($items as $item){
    		$cartItem = $this->cartItemInterfaceFactory->create();
    		$cartItem->setSku($item->getSku());
    		$cartItem->setQty(1);
    		$cartItem->setQuoteId($quoteId);
    		$this->updatePrice($cartItem, $item->getSurveyLeaseFlag());
    		try{
    			$savedItems[] = $this->cartItemRepository->save($cartItem);
    		}catch(NoSuchEntityException $ex){
    			$unsavedItems[] = $cartItem->getSku();
    			$logger->info('No Such Entity Exception '.$cartItem->getSku());
    		}catch(CouldNotSaveException $ex){
    			$unsavedItems[] = $cartItem->getSku();
    			$logger->info('Could Not Save Exception '.$cartItem->getSku());
    		}catch(InputException $ex){
    			$unsavedItems[] = $cartItem->getSku();
    			$logger->info('Input Exception'.$cartItem->getSku());
    		}catch(\Exception $ex){
    			$unsavedItems[] = $cartItem->getSku();
    			$logger->info('Generic Exception'.$cartItem->getSku());
    		}
    	}
    	$cartItemsResult->setSavedItems($savedItems);
    	$cartItemsResult->setUnsavedItems($unsavedItems);
    	return $cartItemsResult;
    }
    
    
    /**
     * Returns information of the cart for a specified customer.
     *
     * @param int $customerId The customer ID.
     * @return \Magento\Quote\Api\Data\CartInterface Cart object.
     */
    public function getCart($customerId){
    	
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$logger = $objectManager->get('\Psr\Log\LoggerInterface');
    	$cartInterface = $objectManager->get('\Magento\Quote\Api\CartManagementInterface')->getCartForCustomer($customerId);
    	/* $cartId = $cartInterface->getId();
    	$cart = $this->quoteRepository->getActive($cartId);
    	$cartItems = $cart->getItems(); */
    	$cartItems = $cartInterface->getItems();
    	foreach ($cartItems as $item){
    		
    		$itemSku = $item->getSku();
    		$product = $this->_productRepo->get($itemSku);
    		$itemExtensionAttributes = $item->getExtensionAttributes();
    		if ($itemExtensionAttributes === null) {
    			$itemExtensionAttributes = $this->cartItemExtensionFactory->create();
    		}
    		
    		$logger->info("Monthly Charges before: ".$item->getMonthlyCharges());
    		$itemExtensionAttributes->setMonthlyCharges($item->getMonthlyCharges());
    		//$itemExtensionAttributes->setCamFee($product->getCustomAttribute('cam_fee')->getValue());
    		$itemExtensionAttributes->setSiteSelectionFee($item->getSiteSelectionFee());
    		$itemExtensionAttributes->setSiteSurveyLeaseFee($item->getSiteSurveyLeaseFee());
    		$itemExtensionAttributes->setRentLeaseFee($item->getRentLeaseFee());
    		$itemExtensionAttributes->setCityArea($item->getCityArea());
    		$itemExtensionAttributes->setPoleClass($item->getPoleClass());
    		$itemExtensionAttributes->setLatitude($item->getLatitude());
    		$itemExtensionAttributes->setLongitude($item->getLongitude());
    		$itemExtensionAttributes->setState($item->getState());
    		$itemExtensionAttributes->setCity($item->getCity());
    		$itemExtensionAttributes->setHasConduitRequested($item->getHasConduitRequested());
    		$itemExtensionAttributes->setSurveyLeaseSelected($item->getSurveyLeaseSelected());
    		
    		$item->setExtensionAttributes($itemExtensionAttributes);
    		
    		$logger->info("Monthly Charges After: ".$itemExtensionAttributes->getMonthlyCharges());
    	}
    	return $cartInterface;
    }
    
}