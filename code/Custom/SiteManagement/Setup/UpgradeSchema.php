<?php

namespace Custom\SiteManagement\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){
		$installer = $setup;
		$installer->startSetup();
		
		// Creation of master product dump table
		
		if (!$installer->tableExists('site_requests')) {
			$table = $installer->getConnection()->newTable(
					$installer->getTable('site_requests')
					)
					->addColumn(
							'id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[
									'identity' => true,
									'nullable' => false,
									'primary'  => true,
									'unsigned' => true,
							],
							'ID'
							)
									->addColumn(
											'requestid',
											\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
											255,
											['nullable => false'],
											'Request ID'
											)
											->addColumn(
															'longitude',
															\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
															45,
															['nullable => false'],
															'Longitude'
															)
															->addColumn(
																	'latitude',
																	\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																	45,
																	['nullable => false'],
																	'Latitude'
																	)
																	->addColumn(
																			'city',
																			\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																			45,
																			['nullable => false'],
																			'City'
																			)
																			->addColumn(
																					'state',
																					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																					45,
																					['nullable => false'],
																					'State'
																					)
																					->addColumn(
																							'city_area',
																							\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																							45,
																							['nullable => false'],
																							'City Area'
																							)
																							->addColumn(
																									'pole_class',
																									\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																									45,
																									['nullable => false'],
																									'Pole Class'
																									)
																									->addColumn(
																											'created_timestamp',
																											\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
																											null,
																											['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
																											'Created Timestamp'
																											)
																											->addColumn(
																													'updated_timestamp',
																													\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
																													null,
																													['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
																													'Created Timestamp'
																													)
																													->addColumn(
																															'status',
																															\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																															45,
																															['nullable => false'],
																															'status'
																															)
																															->addIndex(
																																	$setup->getIdxName(
																																			$installer->getTable('site_requests'),
																																			['id', 'requestid'],
																																			\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
																																			),
																																	['id','requestid'],
																																	['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
																																	)
																																	->setComment('Site Requests');
																																	
																																	$installer->getConnection()->createTable($table);
																																	
		}
		
		if ($installer->tableExists('cities')){
			$setup->getConnection()->addColumn(
					$setup->getTable('cities'),
					'admin_emailID',
					[
							'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
							'length' => 255,
							'comment' =>'Admin Email ID'
					]
					);
		}
		$setup->getConnection()->addColumn(
				$setup->getTable('site_requests'),
				'sku',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Sku'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable('site_requests'),
				'rejected_reason',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Rejected Reason'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable('site_requests'),
				'customerid',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
						'length' => null,
						'unsigned' => true,
						'nullable' => false,
						'comment' =>'Rejected Reason'
				]
				);
		$setup->getConnection()->addForeignKey(
				$setup->getFkName('site_requests','customerid','customer_entity','entity_id'),
				$setup->getTable('site_requests'),
				'customerid',
				$setup->getTable('customer_entity'),
				'entity_id'
				);
		//$setup->endSetup();
	}
}