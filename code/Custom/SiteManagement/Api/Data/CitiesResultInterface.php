<?php

namespace Custom\SiteManagement\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface CitiesResultInterface
{
	/**
	 * Get attributes list.
	 *
	 * @return \Custom\SiteManagement\Api\Data\CitiesInterface[]
	 */
	public function getItems();
	
	/**
	 * Set attributes list.
	 *
	 * @param \Custom\SiteManagement\Api\Data\CitiesInterface[] $items
	 * @return $this
	 */
	public function setItems(array $items);
	
}
