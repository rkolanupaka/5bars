import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestSiteComponent } from '../../components/request-site/request-site.component';
import { MySiteRequestsComponent } from '../../components/my-site-requests/my-site-requests.component';

const routes:Routes = [
    {
        path: '',
        component: RequestSiteComponent
    },
    {
        path: 'my-site-requests',
        component: MySiteRequestsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class RequestSiteRouterModule {

}