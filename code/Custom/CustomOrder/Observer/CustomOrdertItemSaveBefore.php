<?php
namespace Custom\CustomOrder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;

class CustomOrdertItemSaveBefore implements ObserverInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;
    
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $serializer;
    
    /**
     * @var OrderItemExtensionFactory
     */
    private $orderItemExtensionFactory;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     * @param RequestInterface $request
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @param \Magento\Sales\Api\Data\OrderItemExtensionFactory|null $orderItemExtensionFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory = null
        ){
            $this->logger = $logger;
            $this->_request = $request;
            $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
            $this->orderItemExtensionFactory = $orderItemExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderItemExtensionFactory::class);
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->logger->info('CustomOrdertItemSaveBefore | execute | entered');
        
        // Gets the order item which is being saved.
        $item  = $observer->getEvent()->getItem();
        if($item){
            $this->logger->info('CustomOrdertItemSaveBefore | execute | orderItem found');
            $itemExtensionAttributes = $item->getExtensionAttributes();
            if ($itemExtensionAttributes === null) {
                $itemExtensionAttributes = $this->orderItemExtensionFactory->create();
            }
            //set survey lease
            $surveyLeaseSelected = $item->getSurveyLeaseSelected();
            $this->logger->info('CustomOrdertItemSaveBefore | execute | orderItem surveyLeaseSelected :'.$surveyLeaseSelected);
            $itemExtensionAttributes->setSurveyLeaseSelected($surveyLeaseSelected);
            $this->logger->info('CustomOrdertItemSaveBefore | execute | surveyLeaseSelected extenion attribute : '.$itemExtensionAttributes->getSurveyLeaseSelected());
            
            //set hasConduitRequested
            $hasConduitRequested = $item->getHasConduitRequested();
            $this->logger->info('CustomOrdertItemSaveBefore | execute | orderItem ConduitRequested :'.$hasConduitRequested);
            if($hasConduitRequested != null){
           	 	$itemExtensionAttributes->setHasConduitRequested($hasConduitRequested);
            }
            $this->logger->info('CustomOrdertItemSaveBefore | execute | orderItem ConduitRequested :'.$itemExtensionAttributes->getHasConduitRequested());
            
            $item->setExtensionAttributes($itemExtensionAttributes);
        } else {
            $this->logger->info('CustomOrdertItemSaveBefore | execute | item not found');
        }
        $this->logger->info('CustomOrdertItemSaveBefore | execute | leaving');
        return $this;
    }
    
}