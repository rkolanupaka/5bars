<?php
    namespace Custom\CustomPrice\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\RequestInterface;
		
    class CustomPrice implements ObserverInterface
    {
		/**
		* @var \Magento\Framework\App\RequestInterface
		*/
		protected $_request;
		
		/**
		* @var \Magento\Catalog\Model\ProductRepository
		*
		*/
		protected $_productRepo;
		
		/**
		* @var \Magento\Framework\Serialize\Serializer\Json
		*/
		protected $serializer;
		
		/**
		* @param RequestInterface $request
		* @param \Magento\Catalog\Model\ProductRepository $productRepo
		* @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
		*/
		public function __construct(
			\Magento\Framework\App\RequestInterface $request,
			\Magento\Catalog\Model\ProductRepository $productRepo,
			\Magento\Framework\Serialize\Serializer\Json $serializer = null
		){
			$this->_request = $request;
			$this->_productRepo = $productRepo;
			$this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
		} 
		
        public function execute(\Magento\Framework\Event\Observer $observer) 
		{
            $logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
			
			$quote_item = $observer->getQuoteItem();
			$productId = $quote_item->getProduct()->getId();
			$logger->info("Product ID from Quote Item:::::::::::::" .$productId);
			
			if($productId)
			{
				$productModel = $this->_productRepo->getById($productId);
				
				/* $isSurveyLeaseSelected = 'false';
				$isConduitRequested = 'false'; */
				
				//get all the params from request
				$params = [];
				$content = $this->_request->getContent();
				if ($content) {
					$params = $this->serializer->unserialize($content);
				}
				
				//only for Add To Cart API call
				if (array_key_exists('is_build_selected', $params)) 
				{
					$isBuildSelected = $params['is_build_selected'];
					$logger->info("isBuildSelected::::::::::::" .$isBuildSelected);
					
					$productModel->getSampleAttr(); 
					$isBuildSelected == 'true' ? $price = $productModel->getData('build_price') : $price = $productModel->getData('reserved_price');
					$logger->info("Modified Price::::::::::::" .$price);
					
					$quote_item->setOriginalCustomPrice($price);
					$quote_item->save();
				}
				
				if (array_key_exists('survey_lease_selected', $params)) {
					$isSurveyLeaseSelected = $params['survey_lease_selected'];
					$logger->info("CustomPrice | execute | isSurveyLeaseSelected::::::::::::".$isSurveyLeaseSelected);
					$price = $productModel->getPrice();
					$logger->info("CustomPrice | execute | Actual Product Price : ".$price);
					if ($isSurveyLeaseSelected == 'true') {
						$logger->info("CustomPrice | execute | set survey_lease_selected as true to quote");
						$quote_item->setSurveyLeaseSelected(true);
						$logger->info("CustomPrice | execute | survey_lease_selected to add : ".$productModel->getData('site_survey_lease_fee'));
						$price = $price + $productModel->getData('site_survey_lease_fee');
						$logger->info("CustomPrice | execute | Modified Price with site_survey_lease_fee ::::::::::::" .$price);
						$quote_item->save();
					} else {
						$quote_item->setSurveyLeaseSelected(false);
						$logger->info("CustomPrice | execute | Price without modification : ".$price);
					}
					
					$quote_item->setOriginalCustomPrice($price);
					$logger->info("CustomPrice | execute | leaving with set survey_lease_selected as :".$quote_item->getSurveyLeaseSelected());
				}
				
				if(array_key_exists('has_conduit_requested', $params)){
					$logger->info("CustomPrice | execute | isConduitRequested found in request::::::::::::");
					$isConduitRequested = $params['has_conduit_requested'];
					$logger->info("CustomPrice | execute | isConduitRequested::::::::::::".$isConduitRequested);
					if ($isConduitRequested!=null && $isConduitRequested == 'true'){
						$quote_item->setHasConduitRequested(true);
						$logger->info("CustomPrice | execute | isConduitRequested::::::::::::".$quote_item->getHasConduitRequested());
						$quote_item->save();
					}
					else{
						$quote_item->setHasConduitRequested(false);
					}
				}
				
				/* if($quote_item->getSurveyLeaseSelected() | $quote_item->getHasConduitRequested()){
					$quote_item->save();
				} */
			}
			return $this;
        }
 
    }