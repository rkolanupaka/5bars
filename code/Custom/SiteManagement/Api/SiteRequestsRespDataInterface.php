<?php

namespace Custom\SiteManagement\Api;

interface SiteRequestsRespDataInterface{
	
	/**
	 * Method to save the Original Site Details requested by User
	 *
	 * @api
	 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $requestedSites
	 * @return void
	 */
	public function setRequestedSites($requestedSites);
	
	
	/**
	 * @api
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getRequestedSites();
	
	
	/**
	 *  Method to save the Duplicate Site Details requested by User
	 *
	 * @api
	 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $duplicateSites
	 * @return void
	 */
	public function setDuplicateSites($duplicateSites);
	
	
	/**
	 * @api
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getDuplicateSites();
	
	
	/**
	 *  Method to save the Request Pending Site Details requested by User
	 *
	 * @api
	 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $requestPendingSites
	 * @return void
	 */
	public function setRequestPendingSites($requestPendingSites);
	
	
	/**
	 * @api
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getRequestPendingSites();
	
	
}