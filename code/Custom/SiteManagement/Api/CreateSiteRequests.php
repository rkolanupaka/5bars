<?php

namespace Custom\SiteManagement\Api;

use Custom\SiteManagement\Api\CreateSiteRequestsInterface;

class CreateSiteRequests implements CreateSiteRequestsInterface{
	
	
	/**
	 *
	 * @var string
	 */
	protected $customerId;
	
	/**
	 *
	 * @var string
	 */
	protected $requestId;
	
	
	/**
	 *
	 * @var \Magento\Catalog\Api\Data\ProductInterface
	 */
	protected $product;
	
	/**
	 *
	 * @var string
	 */
	protected $rejectedReason;
	
	/**
	 * @api
	 * @param string $requestId
	 * @return void
	 */
	public function setRequestId($requestId){
		$this->requestId = $requestId;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRequestId(){
		return $this->requestId;
	}
	
	/**
	 * @api
	 * @param string $customerId
	 * @return void
	 */
	public function setCustomerId($customerId){
		$this->customerId = $customerId;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCustomerId(){
		return $this->customerId;
	}
	
	
	/**
	 * @api
	 * @param \Magento\Catalog\Api\Data\ProductInterface $product
	 * @return string|null
	 */
	public function setProduct($product){
		$this->product = $product;
	}
	
	
	/**
	 * @api
	 * @return \Magento\Catalog\Api\Data\ProductInterface|null
	 */
	public function getProduct(){
		return $this->product;
	}
	
	/**
	 * @api
	 * @param string $rejectedReason
	 * @return void
	 */
	public function setRejectedReason($rejectedReason){
		$this->rejectedReason = $rejectedReason;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRejectedReason(){
		return $this->rejectedReason;
	}
}