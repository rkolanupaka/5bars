<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomerRegistrationWebService\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
	/**
	 * @used-by \Custom\CustomerRegistrationWebService\Setup::install()
	 * @used-by \Custom\CustomerRegistrationWebService\Setup\InstallData::install()
	 */
	const COMPANY_NAME = 'company_name';
	
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$installer = $setup;
    	$installer->startSetup();
    	if (!$installer->tableExists('customer_entity_ponumber')) {
    		$table = $installer->getConnection()->newTable(
    				$installer->getTable('customer_entity_ponumber')
    				)
    				->addColumn(
    						'po_id',
    						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    						null,
    						[
    								'identity' => true,
    								'nullable' => false,
    								'primary'  => true,
    								'unsigned' => true,
    						],
    						'PO ID'
    				)
	    			->addColumn(
	    					'customer_id',
	    					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
	    					1,
	    					[],
	    					'Customer ID'
	    			)
    				->addColumn(
    					'po_number',
    					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    					255,
    					['nullable => false'],
    					'PO Number'
    				)
    				->addColumn(
    					'comment',
    					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    					255,
    					['nullable => false'],
    					'Comment'
    				)
    				->setComment('PO Number Table');
    		
    				$installer->getConnection()->createTable($table);
    								
    				$installer->getConnection()->addIndex(
    					$installer->getTable('customer_entity_ponumber'),
    						$setup->getIdxName(
    							$installer->getTable('customer_entity_ponumber'),
    								['comment','po_number'],
    								\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
    						),
    						['comment','po_number'],
    						\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
    				);
    	}
    	
    	if (!$installer->tableExists('customer_entity_project_contact')) {
    		$table = $installer->getConnection()->newTable(
    				$installer->getTable('customer_entity_project_contact')
    				)
    				->addColumn(
    						'contact_id',
    						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    						null,
    						[
    								'identity' => true,
    								'nullable' => false,
    								'primary'  => true,
    								'unsigned' => true,
    						],
    						'Contact ID'
    						)
    						->addColumn(
    								'customer_id',
    								\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    								1,
    								[],
    								'Customer ID'
    								)
    								->addColumn(
    										'name',
    										\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    										255,
    										['nullable => false'],
    										'Name'
    										)
    										->addColumn(
    												'email',
    												\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    												255,
    												['nullable => false'],
    												'Email'
    												)
    												->addColumn(
    														'phone',
    														\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    														255,
    														['nullable => false'],
    														'Phone Number'
    														)
    														->addColumn(
    																'company',
    																\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    																255,
    																['nullable => false'],
    																'Company'
    																)
    												->setComment('Project Contact Table');
    												
    												$installer->getConnection()->createTable($table);
    												
    												$installer->getConnection()->addIndex(
    														$installer->getTable('customer_entity_project_contact'),
    														$setup->getIdxName(
    																$installer->getTable('customer_entity_project_contact'),
    																['name','phone','email'],
    																\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
    																),
    														['name','phone','email'],
    														\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
    														);
    	}
    	$installer->endSetup();
    }
}
