import { Component, OnInit } from '@angular/core';

import { DataService } from '../../common/services/data.service';
import { CookieService } from 'ngx-cookie';
// import { Jsonp } from '@angular/http';

@Component({
  selector: 'app-my-site-requests',
  templateUrl: './my-site-requests.component.html',
  styleUrls: ['./my-site-requests.component.scss']
})
export class MySiteRequestsComponent implements OnInit {
  siteFetched = false;
  siteRequests = [];
  siteApproved = [];
  siteRejected = [];
  userData:any;
  hostValue:string;
  fiveBarsToken:string;

  showLoader:boolean = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;

  constructor(
    private dataservice: DataService,
    private _cookieService:CookieService
  ) { }

  ngOnInit() {
    this.hostValue = this.dataservice.getHostName();
    this.fiveBarsToken = this._cookieService.get('5barsToken');
    this.userData = JSON.parse(localStorage.getItem('fivebarsUserData'));
    this.getSiteRequests();
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  getSiteRequests() {
    let url = this.hostValue + "/magento/index.php/rest/V1/customer/siteRequestItems/"+ this.userData.id;
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data:any)=>{
      this.showLoader = false;
      let tempData = data.filter(item => item.customer_email === this.userData.email);
      this.siteRequests = tempData.filter(item=> item.status === 'REQUESTED');
      this.siteApproved = tempData.filter(item=> item.status === 'APPROVED');
      this.siteRejected = tempData.filter(item=> item.status === 'REJECTED');
      this.siteFetched = true;
    }, (err)=> {
      this.siteFetched = true;
      this.showLoader = false;      
      this.showPopUpMessage = "Something went wrong while loading site requests. Please try again later.";
      this.showPopUpTrue = true;
    })
  }
}

