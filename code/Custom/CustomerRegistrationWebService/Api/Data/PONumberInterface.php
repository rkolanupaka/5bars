<?php
namespace Custom\CustomerRegistrationWebService\Api\Data;
interface PONumberInterface
{
	/**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
	const PO_NUMBER = 'po_number';
	const COMMENT = 'comment';
	const CUSTOMER_ID = 'customer_id';
	
	/**#@-*/
	
	
}