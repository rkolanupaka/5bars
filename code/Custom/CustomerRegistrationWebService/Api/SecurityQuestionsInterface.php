<?php

namespace Custom\CustomerRegistrationWebService\Api;

interface SecurityQuestionsInterface{
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getId();
	
	/**
	 * @api
	 * @param int $setId
	 * @return void
	 */
	public function setId($setId);
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getQId();
	
	/**
	 * @api
	 * @param int $qId
	 * @return void
	 */
	public function setQId($qId);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getQuestion();
	
	/**
	 * @api
	 * @param string $question
	 * @return void
	 */
	public function setQuestion($question);
	
}