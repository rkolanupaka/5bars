<?php



namespace Custom\Reports\Api;


/**
 * Defines Sevice Contract for Reports class
 */
interface ReportsInterface
{
    
    /**
     * @param string $startDate
     * @param string $endDate
     * @return string
     */
    public function getFinServReport($startDate,$endDate);
    
    /**
     * @param string $startDate
     * @param string $endDate
     * @return string
     */
    public function getEngineeringReport($startDate,$endDate);
    
    /**
     * API to download Site location data csv
     * 
     * @param string $state
     * @param string $city
     * @return string
     */
    public function getSiteLocationData($state,$city);
    
    
    /**
     * API to download Site location data for Map View
     *
     * @param string $state
     * @param string $city
     * @return \Custom\Reports\Api\MapViewDataInterface[]
     */
    public function getSiteLocationDataforMapView($state,$city);
    
    
    /**
     * API to download Site location data for Map View Time EnhanceMent 
     *
     * @param string $state
     * @param string $city
     * @return \Custom\Reports\Api\MapViewDataInterface[]
     */
    public function getSiteLocationDataforMaps($state,$city);
    
}