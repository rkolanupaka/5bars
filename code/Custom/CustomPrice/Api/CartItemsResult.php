<?php


namespace Custom\CustomPrice\Api;
use Custom\CustomPrice\Api\Data\CartItemsResultInterface;
use Magento\Framework\Api\AbstractSimpleObject;

/**
 * SearchResults Service Data Object used for the add to cart service requests
 */
class CartItemsResult extends AbstractSimpleObject implements CartItemsResultInterface
{
	const SAVED_ITEMS = 'saved_items';
	const UNSAVED_ITEMS= 'unsaved_items';
	
	
	/**
	 * Get Unsaved Cart Items
	 *
	 * @return array
	 */
	public function getUnsavedItems()
	{
		return $this->_get(self::UNSAVED_ITEMS);
	}
	
	/**
	 * Set Unsaved Cart Items.
	 *
	 * @param array $unsavedItems
	 * @return $this
	 */
	public function setUnsavedItems($unsavedItems)
	{
		return $this->setData(self::UNSAVED_ITEMS, $unsavedItems);
	}
	
	/**
	 * Get Saved Cart Items.
	 *
	 * @return \Magento\Quote\Api\Data\CartItemInterface[]
	 */
	public function getSavedItems()
	{
		return $this->_get(self::SAVED_ITEMS);
	}
	
	/**
	 * Set Saved Cart Items.
	 *
	 * @param \Magento\Quote\Api\Data\CartItemInterface[] $savedItems
	 * @return $this
	 */
	public function setSavedItems($savedItems)
	{
		return $this->setData(self::SAVED_ITEMS, $savedItems);
	}
	
}