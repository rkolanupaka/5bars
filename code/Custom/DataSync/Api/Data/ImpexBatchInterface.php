<?php
namespace Custom\DataSync\Api\Data;

/**
 * Interface ImpexBatchInterface.php
 * @api
 */
interface ImpexBatchInterface
{
    /**
     * Get Last execution timestamp for batch.
     *
     * @return string|null
     */
    public function getLastExecutionTime($connection, $transaction);
    
    /**
     * Get ID of Last execution timestamp for batch.
     *
     * @return string|null
     */
    public function getBatchIdOfLastExecutionTime($connection, $transaction);
    
}
