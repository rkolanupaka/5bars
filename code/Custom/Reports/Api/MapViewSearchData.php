<?php

namespace Custom\Reports\Api;

use Custom\Reports\Api\MapViewSearchDataInterface;

class MapViewSearchData implements MapViewSearchDataInterface{
	
	
	/**
	 *
	 * @var string
	 */
	protected $sku;
	
	/**
	 *
	 * @var float
	 */
	protected $lat;
	
	/**
	 *
	 * @var float
	 */
	protected $long;
	
	/**
	 *
	 * @var string
	 */
	protected $sChk;
	
	/**
	 *
	 * @var string
	 */
	protected $pCls;
	
	/**
	 *
	 * @var string
	 */
	protected $cArea;
	
	public function jsonSerialize(){
		return array(
				'sku' => $this->sku,
				'lat' => $this->lat,
				'long' => $this->long,
				'sChk'=> $this->sChk,
				'pCls' => $this->pCls,
				'cArea' => $this->cArea
		);
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSku(){
		return $this->sku;
	}
	
	/**
	 * @api
	 * @param string $sku
	 * @return void
	 */
	public function setSku($sku){
		$this->sku = $sku;
	}
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLat(){
		return $this->lat;
	}
	
	/**
	 * @api
	 * @param float $lat
	 * @return void
	 */
	public function setLat($lat){
		$this->lat = $lat;
	}
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLong(){
		return $this->long;
	}
	
	/**
	 * @api
	 * @param float $long
	 * @return void
	 */
	public function setLong($long){
		$this->long = $long;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getsChk(){
		return $this->sChk;
	}
	
	/**
	 * @api
	 * @param string $sChk
	 * @return void
	 */
	public function setsChk($sChk){
		$this->sChk = $sChk;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getpType(){
		return $this->pCls;
	}
	
	/**
	 * @api
	 * @param string $pType
	 * @return void
	 */
	public function setpType($pType){
		$this->pCls = $pType;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getlType(){
		return $this->cArea;
	}
	
	/**
	 * @api
	 * @param string $lType
	 * @return void
	 */
	public function setlType($lType){
		$this->cArea = $lType;
	}
}