<?php
namespace Custom\WaitList\Model;

use Exception;
//use Custom\WaitList\Api\WaitlistManagementInterface;
use Magento\Wishlist\Controller\WishlistProvider;
use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory;
use Magento\Wishlist\Model\WishlistFactory;
use Magento\Wishlist\Model\ItemFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Customer;
use Psr\Log\LoggerInterface;
use \Custom\SiteManagement\Api\SiteManagementInterface;

class CronJobForWaitList{
	
	/**
	 * @var CollectionFactory
	 */
	protected $_wishlistCollectionFactory;
	
	/**
	 * Wishlist item collection
	 * @var \Magento\Wishlist\Model\ResourceModel\Item\Collection
	 */
	protected $_itemCollection;
	
	/**
	 * @var WishlistRepository
	 */
	protected $_wishlistRepository;
	
	/**
	 * @var \Magento\Catalog\Model\ProductRepository
	 */
	protected $_productRepository;
	
	/**
	 * @var WishlistFactory
	 */
	protected $_wishlistFactory;
	
	/**
	 * @var ItemFactory
	 */
	protected $_itemFactory;
	
	/**
	 * @var Customer
	 */
	protected $customers;
	
	/**
	 *
	 * @var LoggerInterface
	 */
	protected $logger;
	
	/**
	 * @var \Magento\Customer\Api\CustomerRepositoryInterface
	 */
	protected $customerRepository;
	
	/**
	 * @var \Magento\Framework\Mail\Template\TransportBuilder
	 */
	protected $_transportBuilder;
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;
	
	/**
	 * @var \Magento\Framework\Translate\Inline\StateInterface
	 */
	protected $inlineTranslation;
	
	/**
	 * @var \Custom\SiteManagement\Api\SiteManagementInterface
	 */
	protected $siteManagementInterface;
	
	/**
	 * @param CollectionFactory $wishlistCollectionFactory
	 * @param \Magento\Catalog\Model\ProductFactory $productFactory
	 * @param \Magento\Framework\Math\Random $mathRandom
	 * @param \Psr\Log\LoggerInterface $logger
	 * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
	 * @param \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
	 * @param \Magento\Framework\Stdlib\DateTime $dateTime
	 * @param \Magento\Customer\Model\Customer $customers
	 * @param ProductRepositoryInterface $productRepository
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder
	 * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
	 * @param \Magento\Catalog\Model\ProductRepository $_productRepository
	 * @param \Custom\SiteManagement\Api\SiteManagementInterface $siteManagementInterface
	 */
	public function __construct(
			\Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory $wishlistCollectionFactory,
			\Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
			\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
			\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
			\Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
			\Psr\Log\LoggerInterface $logger,
			\Magento\Wishlist\Model\ItemFactory $itemFactory,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder,
			\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
			\Magento\Customer\Model\Customer $customers,
			\Magento\Catalog\Model\ProductRepository $_productRepository,
			\Custom\SiteManagement\Api\SiteManagementInterface $siteManagementInterface
			) {
				$this->_wishlistCollectionFactory = $wishlistCollectionFactory;
				$this->_productRepository = $_productRepository;
				$this->customerRepositoryInterface = $customerRepositoryInterface;
				$this->customerRepository = $customerRepositoryFactory->create();
				$this->logger = $logger;
				$this->_wishlistFactory = $wishlistFactory;
				$this->_itemFactory = $itemFactory;
				$this->customers = $customers;
				$this->storeManager = $storeManager;
				$this->_transportBuilder = $_transportBuilder;
				$this->inlineTranslation = $inlineTranslation;
				$this->siteManagementInterface = $siteManagementInterface;
	}
	
	
	/**
	 * Execute Cronjob
	 *
	 * @return void
	 */
	public function execute(){
		$this->logger->info('CronJobForWaiList |execute |Wait List Cron is STARTED.....');
		$this->waitList();
		$this->logger->info('CronJobForWaiList |execute |Wait List Cron is ENDED.....');
		$this->logger->info('CronJobForSiteRequests |execute |SiteRequests Cron is STARTED.....');
		$this->siteRequests();
		$this->logger->info('CronJobForSiteRequests |execute |SiteRequests Cron is ENDED.....');
	}
	
	/**
	 * Method to call the WaitList Functionality
	 *
	 * @return void
	 */
	public function waitList(){
		$arrayForWaitListProducts = $this->getWaitListEnabledDetails();
		foreach ($arrayForWaitListProducts as $waitEnabledProduct){
			$this->getCustomersforProduct($waitEnabledProduct['entity_id']);
		}
	}
	
	/**
	 * Method to call the SiteRequests Functionality
	 *
	 * @return void
	 */
	public function siteRequests(){
		$arrayForSiteRequestedProducts = $this->getSiteRequestsEnabledDetails();
		$this->logger->info("Array Size....".sizeof($arrayForSiteRequestedProducts));
		foreach ($arrayForSiteRequestedProducts as $siteEnabledProduct){
			$this->blockSiteRequestsForCustomer($siteEnabledProduct['entity_id']);
		}
	}
	
	/**
	 * Method to return array of SiteRequests enabled Product Details
	 *
	 * @return array
	 */
	public function getSiteRequestsEnabledDetails(){
		$this->logger->info("Entered to get SitesRequestes Details");
		$resource = $this->getResourceConnection();
		$connection = $resource->getConnection();
		$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
		$catalogProductEntity = $resource->getTableName('catalog_product_entity');
		$eavAttribute = $resource->getTableName('eav_attribute');
		$eavEntityType = $resource->getTableName('eav_entity_type');
		$sql = "select cpe.entity_id from ".$catalogProductEntity." cpe
				JOIN ".$catalogProductEntityInt." v3 ON cpe.entity_id = v3.entity_id
				AND v3.store_id = 0
				AND v3.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_requested'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
				where v3.value = 1";
		$arrayForSiteRequestedProducts = $connection->query($sql)->fetchAll();
		return $arrayForSiteRequestedProducts;
	}
	
	/**
	 * Method to return array of WaitList enabled Product Details
	 *
	 * @return array
	 */
	public function getWaitListEnabledDetails(){
		$resource = $this->getResourceConnection();
		$connection = $resource->getConnection();
		$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
		$catalogProductEntity = $resource->getTableName('catalog_product_entity');
		$eavAttribute = $resource->getTableName('eav_attribute');
		$eavEntityType = $resource->getTableName('eav_entity_type');
		$sql = "select cpe.entity_id from ".$catalogProductEntity." cpe
				JOIN ".$catalogProductEntityInt." v3 ON cpe.entity_id = v3.entity_id
				AND v3.store_id = 0
				AND v3.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'wait_list_enabled'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
				where v3.value = 1";
		$arrayForWaitListProducts = $connection->query($sql)->fetchAll();
		return $arrayForWaitListProducts;
	}
	
	public function getCustomersforProduct($productId){
		$this->logger->info('Wait List Cron is running');
		
		//Code for getting the customer who wished for the product first basing on the product Id
		$items = $this->_itemFactory->create()->getCollection()->getData();
		$wishlists = $this->_wishlistFactory->create()->getCollection();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productObj = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
		$productSku=$productObj->getSku();
		//$siteSurveyLeaseFee = $productObj->getData('site_survey_lease_fee');
		$this->logger->info('Sku from Product Id ------'.$productObj->getSku());
		
		//getting custom Site Url
		 $variable = $objectManager->create('Magento\Variable\Model\Variable');
		 $baseHostUrl = $variable->loadByCode('baseHostUrl')->getPlainValue();
		 $this->logger->info("SiteSelex Url :: ".$baseHostUrl);
		
		$resource = $this->getResourceConnection();
		$connection = $resource->getConnection();
		$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
		$catalogProductEntity = $resource->getTableName('catalog_product_entity');
		$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
		$catalogProductEntityDateTime = $resource->getTableName('catalog_product_entity_datetime');
		$eavAttribute = $resource->getTableName('eav_attribute');
		$eavEntityType = $resource->getTableName('eav_entity_type');
		$sql1 = "select e.sku,v4.value as latitude,v5.value as longitude,v1.value as state,v2.value as city,v6.value as city_area,v7.value as pole_class,v8.value as monthly_charges,v9.value as wait_list_date FROM ".$catalogProductEntity." e LEFT JOIN ".$catalogProductEntityVarchar." v1 ON e.entity_id = v1.entity_id
				AND v1.store_id = 0
				AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'state'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id
				AND v2.store_id = 0
				AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityInt." v3 ON e.entity_id = v3.entity_id
				AND v3.store_id = 0
				AND v3.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'reserved'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v4 ON e.entity_id = v4.entity_id
				AND v4.store_id = 0
				AND v4.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'latitude'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v5 ON e.entity_id = v5.entity_id
				AND v5.store_id = 0
				AND v5.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'longitude'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
												
				LEFT JOIN ".$catalogProductEntityVarchar." v6 ON e.entity_id = v6.entity_id
				AND v6.store_id = 0
				AND v6.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city_area'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
												
				LEFT JOIN ".$catalogProductEntityVarchar." v7 ON e.entity_id = v7.entity_id
				AND v7.store_id = 0
				AND v7.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'pole_class'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
												
				LEFT JOIN ".$catalogProductEntityVarchar." v8 ON e.entity_id = v8.entity_id
				AND v8.store_id = 0
				AND v8.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'monthly_charges'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
												
				LEFT JOIN ".$catalogProductEntityDateTime." v9 ON e.entity_id = v9.entity_id
				AND v9.store_id = 0
				AND v9.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'wait_list_date'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product'))
												
                WHERE e.sku='".$productSku."'";
		$productArray = $connection->query($sql1)->fetch();
		$this->logger->info('Product Sku from Array SQL ------'.$productArray['sku']);
		if(sizeof($productArray)<1){
			$this->logger->info('No Info on the Product with Sku:'.$productSku.' exists');
		}
		
		$customerEmail = array();
		$wishListItem = $resource->getTableName('wishlist_item');
		$wishList = $resource->getTableName('wishlist');
		$sql = "select customer_id from ".$wishList." WHERE wishlist_id=(
				select wishlist_id from ".$wishListItem." WHERE product_id='".$productId."' and added_at =(
						select MIN(added_at) from ".$wishListItem." where product_id='".$productId."'))";
		$array = $connection->query($sql)->fetch();
		$this->logger->info('CustomerID from Array SQL ------'.$array['customer_id']);
		if(is_null($array['customer_id'])){
			$this->logger->info('No Product with '.$productId.' exists in WaitList');
			return;
		}
		
		$customerid = $array['customer_id'];
		$this->logger->info('Customer Id who requested the Site First ------'.$customerid);
		$customer = $this->customerRepository->getById($customerid);
		$customerEmail[]=$customer->getEmail();
		$this->logger->info('customerEmail[]  ------'.$customerEmail[0]);
		
		$agentDate = $productArray['wait_list_date'];
		$this->logger->info('Agents Date  ------'.$agentDate);
		$agentDate = strtotime($agentDate);
		$tillDate = strtotime("+7 day",$agentDate);
		//$tillDate = strtotime("2018-05-07 07:10:05");
		$agentUpdatedDate=date("Y-m-d H:i:s",$tillDate);
		$this->logger->info('End Date for the customer to reserve the Product  ------'.$agentUpdatedDate);
		$today = date("Y-m-d H:i:s");
		$this->logger->info('Todays Date  ------'.$today);
		$today = strtotime($today);
		$this->logger->info('Todays Date  ------'.$today);
		
		if($today>=$agentDate && $today<=$tillDate){
			
			$emailCheckSql="select wlt.email_check from ".$wishListItem." wlt join ".$wishList." wl on wlt.wishlist_id=wl.wishlist_id
							where wlt.product_id='".$productId."' and wl.customer_id='".$customerid."'";
			$emailCheckData = $connection->query($emailCheckSql)->fetch();
			$emailCheck=$emailCheckData['email_check'];
			if($emailCheck==1){
				$this->logger->info('Email Sent Previously');
			}
			else{
				$this->sendCustomEmail($customerEmail,$productSku,$productArray,$baseHostUrl);
				$updateSql = "update ".$wishListItem." wlt join ".$wishList." wl on wlt.wishlist_id=wl.wishlist_id
				set wlt.email_check=1,wlt.ready_to_reserve=1,wlt.reserve_by_date='".$agentUpdatedDate."' where wlt.product_id='".$productId."' and wl.customer_id='".$customerid."'";
				$connection->query($updateSql);
				$this->logger->info('Email Sent Now');
				
				$productforSiteBlock = $this->_productRepository->get($productSku);
				$productforSiteBlock->setCustomAttribute('site_assigned_to',$customerid);
				$this->logger->info("*********");
				$this->siteManagementInterface->editSite($productforSiteBlock);
				//$this->logger->info($productforSiteBlock->getCustomAttribute('site_requested_by')->getValue());
			}
		}
		else {
			$sql2="select wishlist_item_id from ".$wishListItem." WHERE product_id='".$productId."' and added_at =(
						select MIN(added_at) from ".$wishListItem." where product_id='".$productId."')";
			$arrayforwishListItemId = $connection->query($sql2)->fetch();
			$wishListItmId=$arrayforwishListItemId['wishlist_item_id'];
			$this->logger->info('WishListItemId of the Site requested on the min date ------'.$wishListItmId);
			
			//$this->deleteWaitlistForCustomer($customerid, $wishListItmId);
			$wlm = $objectManager->get('Custom\WaitList\Model\WaitlistManagement');
			$wlm->deleteWaitlistForCustomer($customerid, $wishListItmId);
			$this->logger->info('Deleted the Customer who did not reseve product in Specified Duartion');
			
			$productforSiteBlockResetSql= "update ".$catalogProductEntityVarchar." cpdt join ".$catalogProductEntity." cpe on cpdt.entity_id=cpe.entity_id
			and cpdt.store_id=0
			and cpdt.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_assigned_to'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product')) set cpdt.value=0 where cpe.sku='".$productSku."'";
			$connection->query($productforSiteBlockResetSql);
			
			$agentDateSql = "update ".$catalogProductEntityDateTime." cpdt join ".$catalogProductEntity." cpe on cpdt.entity_id=cpe.entity_id
			and cpdt.store_id=0
			and cpdt.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'wait_list_date'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product')) set cpdt.value='".$agentUpdatedDate."' where cpe.sku='".$productSku."'";
			$connection->query($agentDateSql);
			$this->logger->info('Customer who wished first was deleted and assigned to next customer in Queue');
			
			//***Recursive Logic***
			$checkSql = "select * from ".$wishListItem." where product_id='".$productId."'";
			$ProductCheckArray = $connection->query($checkSql)->fetchAll();
			if(!empty($ProductCheckArray)){
				$this->getCustomersforProduct($productId);
			}
			else{
				$this->logger->info('No Customer in the WaitList Queue w.r.t Sku:'.$productSku);
				return false;
			}
		}
		
	}
	
	
	public function blockSiteRequestsForCustomer($productId){
		$siteRequestedProduct = $this->_productRepository->getById($productId);
		$siteRequestedSku = $siteRequestedProduct->getSku();
		$this->logger->info("Site Requested SKU: ".$siteRequestedSku);
		
		$siteRequestedDate = $siteRequestedProduct->getCustomAttribute('site_requested_date')->getValue();
		$this->logger->info("SiteRequestedDate: ".$siteRequestedDate);
		$siteRequestedDate= strtotime($siteRequestedDate);
		$tillDate = strtotime("+7 day",$siteRequestedDate);
		//$tillDate = strtotime("2018-05-07 07:10:05");
		$siteRequestedFinishDate=date("Y-m-d H:i:s",$tillDate);
		$this->logger->info('End Date for the customer to reserve the Product  ------'.$siteRequestedFinishDate);
		$today = date("Y-m-d H:i:s");
		$this->logger->info('Todays Date  ------'.$today);
		$today = strtotime($today);
		$this->logger->info('Todays Date  ------'.$today);
		if($today>=$siteRequestedDate && $today<=$tillDate){
			
		}
		else{
			//$siteRequestedProduct->setCustomAttribute('site_requested',0);
			$resource = $this->getResourceConnection();
			$connection = $resource->getConnection();
			$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
			$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
			$catalogProductEntity = $resource->getTableName('catalog_product_entity');
			$eavAttribute = $resource->getTableName('eav_attribute');
			$eavEntityType = $resource->getTableName('eav_entity_type');
			$blockSiteSqlforBool = "update ".$catalogProductEntityInt." cpdt join ".$catalogProductEntity." cpe on cpdt.entity_id=cpe.entity_id
			and cpdt.store_id=0
			and cpdt.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_requested'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product')) set cpdt.value=0 where cpe.sku='".$siteRequestedSku."'";
			$connection->query($blockSiteSqlforBool);
			
			$blockSiteSqlforId = "update ".$catalogProductEntityVarchar." cpdt join ".$catalogProductEntity." cpe on cpdt.entity_id=cpe.entity_id
			and cpdt.store_id=0
			and cpdt.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_assigned_to'
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																WHERE entity_type_code = 'catalog_product')) set cpdt.value=0 where cpe.sku='".$siteRequestedSku."'";
			$connection->query($blockSiteSqlforId);
		}
	}
	
	public function sendCustomEmail($customerEmail,$productSku,$productArray,$baseHostUrl)
	 {
		 $logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		 $logger->info("WishListManagement | send WishList Alertmail | Start");
		 
		 $this->inlineTranslation->suspend();
		 $store = $this->storeManager->getStore()->getId();
		 $logger->info("after Storemanager :: ");
		 $transport = $this->_transportBuilder->setTemplateIdentifier('wait_list_product_email_cron')
		 ->setTemplateOptions(['area' => 'frontend', 'store' => $store])
		 ->setTemplateVars(['sites' => $productArray,'urls' => $baseHostUrl,'id' => $productSku])
		/* ->setTemplateVars(
		 [
		 'store' => $this->_storeManager->getStore(),
		 ]
		 ) */
		->setFrom('general')
		 // you can config general email address in Store -> Configuration -> General -> Store Email Addresses
		 ->addTo($customerEmail, 'Five Bars')
		 ->getTransport();
		 $transport->sendMessage();
		 
		 $logger->info("WishListManagement | send WishList Alertmail | End");
	 }
	
	
	/**
	 *
	 * @return \Magento\Framework\App\ResourceConnection
	 */
	public function getResourceConnection(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->get('Magento\Framework\App\ResourceConnection');
	}
}