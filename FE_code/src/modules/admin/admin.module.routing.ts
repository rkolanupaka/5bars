import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportsComponent } from '../../components/reports/reports.component';
import { SiteManagementComponent } from '../../components/site-management/site-management.component';
import { SiteRequestsComponent } from '../../components/site-requests/site-requests.component';

const routes:Routes = [
    {
        path: 'reports',
        component: ReportsComponent
    },
    {
        path: 'sitemanagement',
        component: SiteManagementComponent
    },
    {
        path: 'siterequests',
        component: SiteRequestsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AdminRouterModule {

}