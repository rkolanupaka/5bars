<?php

namespace Custom\CustomerRegistrationWebService\Model;

class SecurityCheckUserMapping extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	
	const CACHE_TAG = 'security_check_user_mapping';
	
	protected $_cacheTag = 'security_check_user_mapping';
	
	protected $_eventPrefix = 'security_check_user_mapping';
	
	protected function _construct()
	{
		$this->_init('Custom\CustomerRegistrationWebService\Model\ResourceModel\SecurityCheckUserMapping');
	}
	
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	
	public function getDefaultValues()
	{
		$values = [];
		
		return $values;
	}
}