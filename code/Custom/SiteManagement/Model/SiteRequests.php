<?php

namespace Custom\SiteManagement\Model;
use Custom\SiteManagement\Api\SiteRequestsInterface;
use Custom\SiteManagement\Api\SiteRequestsDataInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTime;

class SiteRequests implements SiteRequestsInterface{
	
	
	/**
	 * @var \Custom\SiteManagement\Model\SiteRequestsTableFactory
	 */
	protected $siteRequestsTableFactory;
	
	/**
	 * @var \Magento\Framework\Mail\Template\TransportBuilder
	 */
	protected $_transportBuilder;
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;
	
	/**
	 * @var \Magento\Framework\Translate\Inline\StateInterface
	 */
	protected $inlineTranslation;
	
	/**
	 * @var \Magento\Framework\ObjectManagerInterface
	 */
	protected $_objectManager;
	
	/**
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $logger;
	
	/**
	 * @var \Custom\SiteManagement\Api\SiteRequestsRespDataInterfaceFactory
	 */
	protected $siteRequestsRespDataInterfaceFactory;
	
	/**
	 * @var \Magento\Catalog\Model\ProductRepository
	 */
	protected $_productRepository;
	
	/**
	 * @var \Magento\Customer\Api\CustomerRepositoryInterface
	 */
	protected $customerRepository;
	
	/**
	 * @var \Custom\SiteManagement\Api\SiteRequestsDataInterfaceFactory
	 */
	protected $siteRequestsDataInterfaceFactory;
	
	
	
	
	
	/**
	 * 
	 * @param \Custom\SiteManagement\Model\SiteRequestsTableFactory $siteRequestsTableFactory
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
	 * @param \Magento\Framework\ObjectManagerInterface $_objectManager
	 * @param \Psr\Log\LoggerInterface $logger
	 * @param \Custom\SiteManagement\Api\SiteRequestsRespDataInterfaceFactory $siteRequestsRespDataInterfaceFactory
	 * @param \Magento\Catalog\Model\ProductRepository $_productRepository
	 * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
	 * @param \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
	 * @param \Custom\SiteManagement\Api\SiteRequestsDataInterfaceFactory $siteRequestsDataInterfaceFactory
	 */
	
	public function __construct(\Custom\SiteManagement\Model\SiteRequestsTableFactory $siteRequestsTableFactory,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder,
			\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
			\Magento\Framework\ObjectManagerInterface $_objectManager,
			\Psr\Log\LoggerInterface $logger,
			\Custom\SiteManagement\Api\SiteRequestsRespDataInterfaceFactory $siteRequestsRespDataInterfaceFactory,
			\Magento\Catalog\Model\ProductRepository $_productRepository,
			\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
			\Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
			\Custom\SiteManagement\Api\SiteRequestsDataInterfaceFactory $siteRequestsDataInterfaceFactory
			) {
				$this->siteRequestsTableFactory=$siteRequestsTableFactory;
				$this->storeManager = $storeManager;
				$this->_transportBuilder = $_transportBuilder;
				$this->inlineTranslation = $inlineTranslation;
				$this->_objectManager = $_objectManager;
				$this->logger = $logger;
				$this->siteRequestsRespDataInterfaceFactory = $siteRequestsRespDataInterfaceFactory;
				$this->_productRepository = $_productRepository;
				$this->customerRepositoryInterface = $customerRepositoryInterface;
				$this->customerRepository = $customerRepositoryFactory->create();
				$this->siteRequestsDataInterfaceFactory = $siteRequestsDataInterfaceFactory;
	}
	
	
	/**
	 * @api
	 * {@inheritDoc}
	 * @see \Custom\SiteManagement\Api\SiteRequestsInterface::saveSiteRequets()
	 * @param \Custom\SiteManagement\Api\SiteRequestsDataInterface[] $sites
	 */
	public function saveSiteRequets($sites){
		
		
		$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		
		//$randomStr=uniqid('SR');
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$tableName = $resource->getTableName('site_requests');
		$catalogProductEntity = $resource->getTableName('catalog_product_entity');
		$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
		$eavAttribute = $resource->getTableName('eav_attribute');
		$eavEntityType = $resource->getTableName('eav_entity_type');
		$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
		$catalogProductEntityDateTime = $resource->getTableName('catalog_product_entity_datetime');
		
		//$sql = "SELECT requestid FROM " . $tableName. " order by requestid Desc limit 1";
		$sql = "SELECT max(requestid) FROM " . $tableName;
		$result = $connection->fetchOne($sql);
		if(empty($result)){
			$result=100001;	
		}
		else{
			$result++;	
		}
		$logger->info('SQL reqult -'.$result);
			
		$details=array();
		$duplicateRequests = array();
		$requestPendingSites = array();
		try{
			foreach($sites as $eSites){
				
				$duplicateCheckSql = "select e.sku,v4.value as latitude,v5.value as longitude,v1.value as state,v2.value as city,
				v6.value as city_area,v7.value as pole_class FROM ".$catalogProductEntity." e LEFT JOIN ".$catalogProductEntityVarchar."
				 v1 ON e.entity_id = v1.entity_id 
				AND v1.store_id = 0 
				AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'state' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id 
				AND v2.store_id = 0 
				AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v4 ON e.entity_id = v4.entity_id 
				AND v4.store_id = 0 
				AND v4.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'latitude' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v5 ON e.entity_id = v5.entity_id 
				AND v5.store_id = 0 
				AND v5.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'longitude' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				
				LEFT JOIN ".$catalogProductEntityVarchar." v6 ON e.entity_id = v6.entity_id 
				AND v6.store_id = 0 
				AND v6.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city_area' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))

				LEFT JOIN ".$catalogProductEntityVarchar." v7 ON e.entity_id = v7.entity_id 
				AND v7.store_id = 0 
				AND v7.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'pole_class' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                WHERE v4.value = '".$eSites->getLatitude()."' AND v5.value = '".$eSites->getLongitude()."'";
			
				$customerInfo = $this->customerRepository->getById($eSites->getCustomerId());
				$customerEmail = $customerInfo->getEmail();
				$customerName = $customerInfo->getFirstname()." ".$customerInfo->getLastname();
				
				$skuArray = $connection->query($duplicateCheckSql)->fetch();
				
				$this->logger->info("Null check Flag: ".is_null($skuArray['latitude']));
				$logger->info("SizeofSkuArray ".$skuArray['latitude']);
				if(is_null($skuArray['latitude'])){
					
					$requestCheckSql = "SELECT * from ".$tableName." sr WHERE sr.latitude='".$eSites->getLatitude()."' and sr.longitude='".$eSites->getLongitude()."' and sr.status='REQUESTED'";
					$requestCheckArray = $connection->query($requestCheckSql)->fetch();
					if(is_null($requestCheckArray['latitude'])){
					
							$eSites->setRequestId($result);
							
							$logger->info('Site Request CustomerId -'.$eSites->getCustomerId());
							$logger->info('Site Request State -'.$eSites->getState());
							$logger->info('Site Request City -'.$eSites->getCity());
							
							$siteRequestsTable=$this->siteRequestsTableFactory->create();
							$siteRequestsTable->setData('customerid',$eSites->getCustomerId())
							->setData('requestid',$eSites->getRequestId())
							->setData('latitude',$eSites->getLatitude())
							->setData('longitude',$eSites->getLongitude())
							->setData('pole_class',$eSites->getPoleClass())
							->setData('city_area',$eSites->getCityArea())
							->setData('state',$eSites->getState())
							->setData('city',$eSites->getCity())
							//->setData('created_timestamp',$createdAt)
							//->setData('updated_timestamp',$updatedAt)
							->setData('status','REQUESTED')
							->save();
							
							$siteRequestId=$siteRequestsTable->getData('id');
							$eSites->setId($siteRequestId);
							$logger->info('Site Request Id *****-'.$eSites->getId());
							$logger->info('Site Request Id *****-'.$siteRequestId);
							$logger->info('***** Actual Auto Site Request Id *****-'.$eSites->getRequestId());
							
							
							//$details[]=$siteRequestsTable->getData();
							$details[] = $eSites;
					}
					else{
						$eSites->setSku($requestCheckArray['sku']);
						$eSites->setLatitude($requestCheckArray['latitude']);
						$eSites->setLongitude($requestCheckArray['longitude']);
						$eSites->setCity($requestCheckArray['city']);
						$eSites->setState($requestCheckArray['state']);
						$eSites->setCityArea($requestCheckArray['city_area']);
						$eSites->setPoleClass($requestCheckArray['pole_class']);
						$requestPendingSites[] = $eSites;
					}
				}
				else{
					
					$siteHoldsql = "select v1.value as site_requested,v2.value as site_assigned_to,v3.value as site_requested_date from ".$catalogProductEntity." e LEFT JOIN ".$catalogProductEntityInt." v1 ON e.entity_id = v1.entity_id
					AND v1.store_id = 0
					AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_requested'
											AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																	WHERE entity_type_code = 'catalog_product'))
	                LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id
					AND v2.store_id = 0
					AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_assigned_to'
											AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																	WHERE entity_type_code = 'catalog_product'))
	                LEFT JOIN ".$catalogProductEntityDateTime." v3 ON e.entity_id = v3.entity_id
					AND v3.store_id = 0
					AND v3.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'site_requested_date'
											AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
					WHERE entity_type_code = 'catalog_product'))
												
               		 WHERE e.sku='".$skuArray['sku']."'";
					
					$siteHoldArray = $connection->query($siteHoldsql)->fetch();
					
					if(is_null($siteHoldArray['site_requested_date'])){
						$productSiteRequestedDate = null;
						$productCustomerId = null;
						$siteRequestedFlag = null;
					}
					else{
						$productSiteRequestedDate = $siteHoldArray['site_requested_date'];
						$productCustomerId = $siteHoldArray['site_assigned_to'];
						$siteRequestedFlag = $siteHoldArray['site_requested'];
						
						$productSiteRequestedDate1= strtotime($productSiteRequestedDate);
						$tillDate = strtotime("+7 day",$productSiteRequestedDate1);
						$siteTillDate=date("Y-m-d H:i:s",$tillDate);
						
						$today = date("Y-m-d",mktime(0,0,0));
						$diff = abs($tillDate - strtotime($today));
						$years   = floor($diff / (365*60*60*24));
						$months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
						$days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
					}
					
					
					
					
					$eSites->setSiteAssignedDate($productSiteRequestedDate);
					if(is_null($siteHoldArray['site_requested_date'])){
						$eSites->setSiteHoldDaysCount(null);
					}
					else{
						$eSites->setSiteHoldDaysCount($days);
					}
					$eSites->setSiteAssignedTo($productCustomerId);
					$eSites->setSiteRequested($siteRequestedFlag);
					$eSites->setSku($skuArray['sku']);
					$eSites->setCity($skuArray['city']);
					$eSites->setState($skuArray['state']);
					$eSites->setCityArea($skuArray['city_area']);
					$eSites->setPoleClass($skuArray['pole_class']);
					$duplicateRequests[] = $eSites;
				}
			}
			
			//$randomId++;
			
			$siteReqRespDataInterface = $this->siteRequestsRespDataInterfaceFactory->create();
			$siteReqRespDataInterface->setRequestedSites($details);
			$siteReqRespDataInterface->setRequestPendingSites($requestPendingSites);
			$siteReqRespDataInterface->setDuplicateSites($duplicateRequests);
			if(($siteReqRespDataInterface->getRequestedSites())!=null){
				$this->sendCustomEmail($details,$customerEmail,$customerName,$result);
			}
			return $siteReqRespDataInterface;
		
		}
		catch (\Exception $e){
			
			$logger->info("SiteRequests| Saving SiteDetails| Exception While saving Site Requests Details::: ".$e->getMessage());
			throw new CouldNotSaveException(__('Problem While saving Customer Site Requests'));
		}
		
	}
	
	/**
	 * @api
	 * {@inheritDoc}
	 * @param string $startDate
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getSiteRequests($startDate){
		
		$siteRequestsDetails = array();
		$siteRequestsDetails = $this->getCustomerInfo($startDate);
		$this->logger->info("size of Array: ".sizeof($siteRequestsDetails));
		$siteRequestsItems = array();
		for($j=0; $j<sizeof($siteRequestsDetails); $j++){
			//$siteRequestsItems[] = $siteRequestsItem->getData();
			$siteRequestsDataInterface = $this->siteRequestsDataInterfaceFactory->create();
			$siteRequestsDataInterface->setSku($siteRequestsDetails[$j]['sku']);
			$name = $siteRequestsDetails[$j]['firstname'].' '.$siteRequestsDetails[$j]['lastname'];
			$siteRequestsDataInterface->setCustomerName($name);
			$siteRequestsDataInterface->setCustomerId($siteRequestsDetails[$j]['customerid']);
			$siteRequestsDataInterface->setCustomerEmail($siteRequestsDetails[$j]['email']);
			$siteRequestsDataInterface->setRequestId($siteRequestsDetails[$j]['requestid']);
			$siteRequestsDataInterface->setSku($siteRequestsDetails[$j]['sku']);
			$siteRequestsDataInterface->setCityArea($siteRequestsDetails[$j]['city_area']);
			$siteRequestsDataInterface->setLatitude($siteRequestsDetails[$j]['latitude']);
			$siteRequestsDataInterface->setLongitude($siteRequestsDetails[$j]['longitude']);
			$siteRequestsDataInterface->setPoleClass($siteRequestsDetails[$j]['pole_class']);
			$siteRequestsDataInterface->setState($siteRequestsDetails[$j]['state']);
			$siteRequestsDataInterface->setCity($siteRequestsDetails[$j]['city']);
			$siteRequestsDataInterface->setStatus($siteRequestsDetails[$j]['status']);
			$siteRequestsDataInterface->setRejectedReason($siteRequestsDetails[$j]['rejected_reason']);
			$siteRequestsItems[] = $siteRequestsDataInterface; 
		}
		return $siteRequestsItems;
	}
	
	
	  /**
		* Method to Query on SiteRequests and Customer Entity to give the SiteRequests Details
		*
		* @param string $startDate
		* @return array
		*/
	public function getCustomerInfo($startDate){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		//$this->logger->info("customer Id:".$customerId);
		$connection = $resource->getConnection();
		$customerEntity = $resource->getTableName('customer_entity');
		$siteRequests = $resource->getTableName('site_requests');
		$customerSql = "select sr.customerid,sr.requestid,sr.longitude,sr.latitude,sr.city,sr.state,sr.city_area,sr.pole_class,sr.status,sr.sku,
						sr.rejected_reason,ce.firstname,ce.lastname,ce.email from ".$siteRequests." sr join ".$customerEntity." ce on sr.customerid=ce.entity_id
						 and sr.created_timestamp>='".$startDate."'";
		$customerArray = $connection->query($customerSql)->fetchAll();
		//$this->logger->info("Customer Latitude".$customerArray[0]['latitude']);
		//$this->logger->info("Customer Latitude".$customerArray[1]['latitude']);
		return $customerArray;
	}
	
	
	/**
	 * Method to get the siteDetails of the Sites requested w.r.t that customer
	 *
	 * @api
	 * @param int $customerId
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getRequestedSitesforCustomer($customerId){
		$RequestedSiteDetails = array();
		$RequestedSiteDetails = $this->getRequestedSitesInfo($customerId);
		$this->logger->info("size of Array: ".sizeof($RequestedSiteDetails));
		$siteRequestsItems = array();
		for($j=0; $j<sizeof($RequestedSiteDetails); $j++){
			//$siteRequestsItems[] = $siteRequestsItem->getData();
			$siteRequestsDataInterface = $this->siteRequestsDataInterfaceFactory->create();
			$siteRequestsDataInterface->setSku($RequestedSiteDetails[$j]['sku']);
			$name = $RequestedSiteDetails[$j]['firstname'].' '.$RequestedSiteDetails[$j]['lastname'];
			$siteRequestsDataInterface->setCustomerName($name);
			$siteRequestsDataInterface->setCustomerId($RequestedSiteDetails[$j]['customerid']);
			$siteRequestsDataInterface->setCustomerEmail($RequestedSiteDetails[$j]['email']);
			$siteRequestsDataInterface->setRequestId($RequestedSiteDetails[$j]['requestid']);
			$siteRequestsDataInterface->setSku($RequestedSiteDetails[$j]['sku']);
			$siteRequestsDataInterface->setCityArea($RequestedSiteDetails[$j]['city_area']);
			$siteRequestsDataInterface->setLatitude($RequestedSiteDetails[$j]['latitude']);
			$siteRequestsDataInterface->setLongitude($RequestedSiteDetails[$j]['longitude']);
			$siteRequestsDataInterface->setPoleClass($RequestedSiteDetails[$j]['pole_class']);
			$siteRequestsDataInterface->setState($RequestedSiteDetails[$j]['state']);
			$siteRequestsDataInterface->setCity($RequestedSiteDetails[$j]['city']);
			$siteRequestsDataInterface->setStatus($RequestedSiteDetails[$j]['status']);
			$siteRequestsDataInterface->setRejectedReason($RequestedSiteDetails[$j]['rejected_reason']);
			$siteRequestsItems[] = $siteRequestsDataInterface;
		}
		return $siteRequestsItems;
	}
	
	
	/**
	 * Method to Query on SiteRequests and Customer Entity to give the SiteRequests Details w.r.t. customer
	 *
	 * @param int $customerId
	 * @return array
	 */
	public function getRequestedSitesInfo($customerId){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		//$this->logger->info("customer Id:".$customerId);
		$connection = $resource->getConnection();
		$customerEntity = $resource->getTableName('customer_entity');
		$siteRequests = $resource->getTableName('site_requests');
		$requestedSitesSql = "select sr.customerid,sr.requestid,sr.longitude,sr.latitude,sr.city,sr.state,sr.city_area,sr.pole_class,sr.status,sr.sku,
						sr.rejected_reason,ce.firstname,ce.lastname,ce.email from ".$siteRequests." sr join ".$customerEntity." ce on sr.customerid=ce.entity_id
						 and sr.customerid='".$customerId."'";
		$requestedSitesArray = $connection->query($requestedSitesSql)->fetchAll();
		return $requestedSitesArray;
	}
	
		/**
		* Method to generate Sku automatically basing on the State and City
		*
		* @api
		* @param string $city
		* @param string $state
		* @return string
		* @throws \Magento\Framework\Exception\LocalizedException
		*/
	public function generateSku($state,$city){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$catalogProductEntity = $resource->getTableName('catalog_product_entity');
		$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
		$eavAttribute = $resource->getTableName('eav_attribute');
		$eavEntityType = $resource->getTableName('eav_entity_type');
		$sql = "select max(substring(cpe.sku,4,6)) max_sku,substring(cpe.sku,1,3) prefix from ".$catalogProductEntity." cpe 
				
				join ".$catalogProductEntityVarchar." cpeIP1 on cpeIP1.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrInput1 on evAttrInput1.attribute_id = cpeIP1.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeIP2 on cpeIP2.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrInput2 on evAttrInput2.attribute_id = cpeIP2.attribute_id

				join ".$eavEntityType." eet
				              on  eet.entity_type_id = evAttrInput1.entity_type_id
				              and eet.entity_type_id = evAttrInput2.entity_type_id
				
				where eet.entity_type_code = 'catalog_product'
				              and evAttrInput1.attribute_code = 'state'
				              and evAttrInput2.attribute_code = 'city'
								and cpeIP1.value='".$state."'
				              	and cpeIP2.value='".$city."'";
		$skuArray = $connection->query($sql)->fetch();
		$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		$logger->info("Max Sku Value: ".$skuArray['max_sku']);
		
		if($skuArray['max_sku']>=999999){
			throw new LocalizedException(__("Maximum Sku Sequence Reached for ".$city));
			//return "Maximum Sku Sequence Reached for ".$city;
		}
		else{
			$newSkuValue = str_pad($skuArray['max_sku']+1,6,0,STR_PAD_LEFT);
			$newSku = $skuArray['prefix'].$newSkuValue;
			$logger->info("New Sku Value: ".$newSku);
			return $newSku;
		}
		
	}
	
	
	/**
	 * Method to call the AutoSku function and Saving the Product to Inventory after Admin Approval requested by Customer
	 *
	 * @api
	 * @param \Custom\SiteManagement\Api\CreateSiteRequestsInterface $siteRequestedInfo
	 */
	public function approveSiteRequests($siteRequestedInfo){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		$sProduct = $siteRequestedInfo->getProduct();
		$today = date("Y-m-d H:i:s");
		$sProduct->setCustomAttribute('site_requested_date', $today);
		$sProduct->setCustomAttribute('site_requested', 1);
		$sProduct->setCustomAttribute('site_assigned_to',$siteRequestedInfo->getCustomerId());
		
		$product = $objectManager->get('Custom\SiteManagement\Model\SiteManagement')->saveSitesToInventory($sProduct);
		$sku = $product->getSku();
		//$product = $this->siteManagementInterface->saveSites($siteRequestedInfo->getProduct());
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$siteRequestsTable = $resource->getTableName('site_requests');
		$sql = "update ".$siteRequestsTable." sr set sr.status='APPROVED',sr.sku='".$sku."' where sr.requestid='".$siteRequestedInfo->getRequestId()."'
				and sr.customerid='".$siteRequestedInfo->getCustomerId()."' and sr.latitude='".$siteRequestedInfo->getProduct()->getCustomAttribute('latitude')->getValue()."'
				and sr.longitude='".$siteRequestedInfo->getProduct()->getCustomAttribute('longitude')->getValue()."'";
		$connection->query($sql);
		$this->logger->info("Updated SiteRequests Table and Sending the mail Notification as Approved to Customer");
		
		$productInfo = $this->_productRepository->get($sku);
		$reqId = $siteRequestedInfo->getRequestId();
		$customerId = $siteRequestedInfo->getCustomerId();
		$customer = $this->customerRepository->getById($customerId);
		$customerEmail=$customer->getEmail();
		$customerName =$customer->getFirstname().' '.$customer->getLastname();
		//getting custom Site Url
		$variable = $objectManager->create('Magento\Variable\Model\Variable');
		$baseHostUrl = $variable->loadByCode('baseHostUrl')->getPlainValue();
		$this->logger->info("SiteSelex Url :: ".$baseHostUrl);
		
		$this->siteApprovedMail($productInfo,$reqId,$customerEmail,$customerName,$baseHostUrl);
	}
	
	
	/**
	 * Method to Reject the Site Requested by Customer and send the Notification Mail to Customer
	 *
	 * @api
	 * @param \Custom\SiteManagement\Api\CreateSiteRequestsInterface $siteRequestedInfo
	 * @return null
	 */
	public function rejectSiteRequests($siteRequestedInfo){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$rejectionMessage = $siteRequestedInfo->getRejectedReason();
		$this->logger->info($rejectionMessage);
		$siteRequestsTable = $resource->getTableName('site_requests');
		$sql = "update ".$siteRequestsTable." sr set sr.status='REJECTED',sr.rejected_reason='".$rejectionMessage."' where sr.requestid='".$siteRequestedInfo->getRequestId()."'
				and sr.customerid='".$siteRequestedInfo->getCustomerId()."' and sr.latitude='".$siteRequestedInfo->getProduct()->getCustomAttribute('latitude')->getValue()."'
				and sr.longitude='".$siteRequestedInfo->getProduct()->getCustomAttribute('longitude')->getValue()."'";
		$connection->query($sql);
		$this->logger->info("Sending Notification Mail to customer after Admin Rejecting the Site Request");
		$reqId = $siteRequestedInfo->getRequestId();
		$customerId = $siteRequestedInfo->getCustomerId();
		$customer = $this->customerRepository->getById($customerId);
		$customerEmail=$customer->getEmail();
		$customerName =$customer->getFirstname().' '.$customer->getLastname();
		$this->logger->info($customerName);
		$product = $siteRequestedInfo->getProduct();
		$this->siteRejectionMail($product, $reqId, $customerEmail,$customerName,$rejectionMessage);
	}
	
	public function siteRejectionMail($product,$reqId,$customerEmail,$customerName,$rejectionMessage){
		$this->logger->info("RejectionSiteRequestsDetails | sendCustomEmail | Start");
		$this->inlineTranslation->suspend();
		$store = $this->storeManager->getStore()->getId();
		$this->logger->info("after Storemanager :: ");
		$transport = $this->_transportBuilder->setTemplateIdentifier('site_rejection_admin_email')
		->setTemplateOptions(['area' => 'frontend', 'store' => $store])
		->setTemplateVars(['sites' => $product,'msgs' => $rejectionMessage,'customerName' =>$customerName,'id' => $reqId])
		/* ->setTemplateVars(
		 [
		 'store' => $this->_storeManager->getStore(),
		 ]
		 ) */
		->setFrom('general')
		// you can config general email address in Store -> Configuration -> General -> Store Email Addresses
		->addTo($customerEmail, 'Five Bars')
		->getTransport();
		$transport->sendMessage();
		$this->logger->info("RejectionSiteRequestsDetails | sendCustomEmail | End");
	}
	
	public function siteApprovedMail($productInfo,$reqId,$customerEmail,$customerName,$baseHostUrl){
		$this->logger->info("ApproveSiteRequestsDetails | sendCustomEmail | Start");
		$this->inlineTranslation->suspend();
		$store = $this->storeManager->getStore()->getId();
		$this->logger->info("after Storemanager :: ");
		$transport = $this->_transportBuilder->setTemplateIdentifier('site_approval_admin_email')
		->setTemplateOptions(['area' => 'frontend', 'store' => $store])
		->setTemplateVars(['sites' => $productInfo,'urls' => $baseHostUrl ,'customerName' =>$customerName,'id' => $reqId])
		/* ->setTemplateVars(
		 [
		 'store' => $this->_storeManager->getStore(),
		 ]
		 ) */
		->setFrom('general')
		// you can config general email address in Store -> Configuration -> General -> Store Email Addresses
		->addTo($customerEmail, 'Five Bars')
		->getTransport();
		$transport->sendMessage();
		$this->logger->info("ApproveSiteRequestsDetails | sendCustomEmail | End");
	}
	
	public function sendCustomEmail($sites,$customerEmail,$customerName,$result)
	{
		$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		$logger->info("SiteRequestsDetails | sendCustomEmail | Start");
		
		//getting custom AdminEmailId
		$variable = $this->_objectManager->create('Magento\Variable\Model\Variable');
		$adminEmail = $variable->loadByCode('adminEmailid')->getPlainValue();
		$logger->info("Admin Email Id :: ".$adminEmail);
		
		$this->inlineTranslation->suspend();
		$store = $this->storeManager->getStore()->getId();
		$logger->info("after Storemanager :: ");	
		$transport = $this->_transportBuilder->setTemplateIdentifier('sites_request_admin_email')
		->setTemplateOptions(['area' => 'frontend', 'store' => $store])
		->setTemplateVars(['sites' => $sites, 'email' => $customerEmail,'name' => $customerName,'id' => $result])
		/* ->setTemplateVars(
		 [
		 'store' => $this->_storeManager->getStore(),
		 ]
		 ) */
		->setFrom('general')
		// you can config general email address in Store -> Configuration -> General -> Store Email Addresses
		->addTo($adminEmail, 'Five Bars')
		->getTransport();
		$transport->sendMessage();
		
		$logger->info("SiteRequestsDetails | sendCustomEmail | End");
	}
}