<?php

namespace Custom\WaitList\Model;

use Exception;
use Custom\WaitList\Api\WaitlistManagementInterface;
use Magento\Wishlist\Controller\WishlistProvider;
use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory;
use Magento\Wishlist\Model\WishlistFactory;
use Magento\Wishlist\Model\ItemFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Customer;
use Psr\Log\LoggerInterface;

/**
 * Defines the implementaiton class of the \Magento\Wishlist\Api\WishlistManagementInterface
 */
class WaitlistManagement implements WaitlistManagementInterface
{
	
	/**
	 * @var CollectionFactory
	 */
	protected $_wishlistCollectionFactory;
	
	/**
	 * Wishlist item collection
	 * @var \Magento\Wishlist\Model\ResourceModel\Item\Collection
	 */
	protected $_itemCollection;
	
	/**
	 * @var WishlistRepository
	 */
	protected $_wishlistRepository;
	
	/**
	 * @var ProductRepository
	 */
	protected $_productRepository;
	
	/**
	 * @var WishlistFactory
	 */
	protected $_wishlistFactory;
	
	/**
	 * @var ItemFactory
	 */
	protected $_itemFactory;
	
	/**
	 * @var Customer
	 */
	protected $customers;
	
	/**
	 *
	 * @var LoggerInterface
	 */
	protected $logger;
	
	 /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;
	
	/**
	 * @param CollectionFactory $wishlistCollectionFactory
	 * @param \Magento\Catalog\Model\ProductFactory $productFactory
	 * @param \Magento\Framework\Math\Random $mathRandom
	 * @param \Psr\Log\LoggerInterface $logger
	 * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
	 * @param \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
	 * @param \Magento\Framework\Stdlib\DateTime $dateTime
	 * @param \Magento\Customer\Model\Customer $customers
	 * @param ProductRepositoryInterface $productRepository
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
	 */
	public function __construct(
			\Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory $wishlistCollectionFactory,
			\Magento\Wishlist\Model\WishlistFactory $wishlistFactory,
			\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
			\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
			\Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
			\Psr\Log\LoggerInterface $logger,
			\Magento\Wishlist\Model\ItemFactory $itemFactory,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			\Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder,
			\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
			\Magento\Customer\Model\Customer $customers
			) {
				$this->_wishlistCollectionFactory = $wishlistCollectionFactory;
				$this->_productRepository = $productRepository;
				$this->customerRepositoryInterface = $customerRepositoryInterface;
				$this->customerRepository = $customerRepositoryFactory->create();
				$this->logger = $logger;
				$this->_wishlistFactory = $wishlistFactory;
				$this->_itemFactory = $itemFactory;
				$this->customers = $customers;
				$this->storeManager = $storeManager;
				$this->_transportBuilder = $_transportBuilder;
				$this->inlineTranslation = $inlineTranslation;
	}
	
	/**
	 * Get waitlist collection
	 * @param int $customerId
	 * @return array WishlistData
	 */
	public function getWaitlistForCustomer($customerId)
	{
		if (empty($customerId) || !isset($customerId) || $customerId == "") {
			$message = __('Id required');
			$status = false;
			$response[] = [
					"message" => $message,
					"status"  => $status
			];
			return $response;
		} else {
			$collection =
			$this->_wishlistCollectionFactory->create()
			->addCustomerIdFilter($customerId);
			
			$wishlistData = [];
			foreach ($collection as $item) {
				$productInfo = $item->getProduct()->toArray();
				$data = [
						"wishlist_item_id" => $item->getWishlistItemId(),
						"wishlist_id"      => $item->getWishlistId(),
						"product_id"       => $item->getProductId(),
						"store_id"         => $item->getStoreId(),
						"added_at"         => $item->getAddedAt(),
						"description"      => $item->getDescription(),
						"qty"              => round($item->getQty()),
						"reserveByDate"    => $item->getReserveByDate(),
						"readyToReserveFlag"=> $item->getReadyToReserve(),
						"product"          => $productInfo
				];
				$wishlistData[] = $data;
			}
			return $wishlistData;
		}
	}
	
	/**
	 * Add waitlist item for the customer
	 * @param int $customerId
	 * @param string $productId
	 * @return array|bool
	 *
	 */
	public function addWaitlistForCustomer($customerId, $productId)
	{
		if ($productId == null) {
			$message = __('Invalid product, Please select a valid product');
			$status = false;
			$response[] = [
					"message" => $message,
					"status"  => $status
			];
			return $response;
		}
		try {
			$product = $this->_productRepository->get($productId);
			$productid = $product->getId();
			$this->logger->info("Product ID :".$productid);
		} catch (Exception $e) {
			return false;
		}
		try {
			$wishlist = $this->_wishlistFactory->create()
			->loadByCustomerId($customerId, true);
			$wishlist->addNewItem($product);
			$wishlist->save();
			
			
			/* $resource = $this->getResourceConnection();
			$connection = $resource->getConnection();
			$wishListItem = $resource->getTableName('wishlist_item');
			$wishList = $resource->getTableName('wishlist');
			$sql = "select wlt.wishlist_item_id from ".$wishListItem." wlt join ".$wishList." wl on wlt.wishlist_id=wl.wishlist_id 
					where wlt.product_id='".$productid."' and wl.customer_id='".$customerId."'";
			$array = $connection->query($sql)->fetch(); */
		} catch (Exception $e) {
			return false;
		}
		$message = __('Item added to wishlist.');
		$status = true;
		$response[] = [
				"message" => $message,
				"status"  => $status
		];
		return $response;
	}
	
	/**
	 * Delete waitlist item for customer
	 * @param int $customerId
	 * @param int $productIdId
	 * @return array
	 *
	 */
	public function deleteWaitlistForCustomer($customerId, $wishlistItemId)
	{
		
		$message = null;
		$status = null;
		if ($wishlistItemId == null) {
			$message = __('Invalid wishlist item, Please select a valid item');
			$status = false;
			$response[] = [
					"message" => $message,
					"status"  => $status
			];
			return $response;
		}
		$item = $this->_itemFactory->create()->load($wishlistItemId);
		if (!$item->getId()) {
			$message = __('The requested Wish List Item doesn\'t exist .');
			$status = false;
			
			$response[] = [
					"message" => $message,
					"status"  => $status
			];
			return $response;
		}
		$wishlistId = $item->getWishlistId();
		$wishlist = $this->_wishlistFactory->create();
		
		if ($wishlistId) {
			$wishlist->load($wishlistId);
		} elseif ($customerId) {
			$wishlist->loadByCustomerId($customerId, true);
		}
		if (!$wishlist) {
			$message = __('The requested Wish List Item doesn\'t exist .');
			$status = false;
			$response[] = [
					"message" => $message,
					"status"  => $status
			];
			return $response;
		}
		if (!$wishlist->getId() || $wishlist->getCustomerId() != $customerId) {
			$message = __('The requested Wish List Item doesn\'t exist .');
			$status = false;
			$response[] = [
					"message" => $message,
					"status"  => $status
			];
			return $response;
		}
		try {
			$item->delete();
			$wishlist->save();
		} catch (Exception $e) {
			return false;
		}
		
		$message = __(' Item has been removed from wishlist .');
		$status = true;
		$response[] = [
				"message" => $message,
				"status"  => $status
		];
		return $response;
	}
	
	/**
	 * delete waitlist item.
	 *
	 * @param int $customerId
	 * @param string $productSku
	 * @return array|string
	 *
	 */
	public function deleteWaitlistProductForCustomer($customerId, $productSku){
		$product = $this->_productRepository->get($productSku);
		$productid = $product->getId();
		$this->logger->info("Product ID :".$productid);
		$this->logger->info("Customer ID :".$customerId);
		$resource = $this->getResourceConnection();
		$connection = $resource->getConnection();
		$wishListItem = $resource->getTableName('wishlist_item');
		$wishList = $resource->getTableName('wishlist');
		try {
			$deleteSql = "DELETE wlt from ".$wishListItem." wlt join ".$wishList." wl on wl.wishlist_id=wlt.wishlist_id 
						  where wlt.product_id='".$productid."' and wl.customer_id='".$customerId."'";
			$connection->query($deleteSql);
		}
		catch (Exception $e){
			return "Sku ".$productSku." doesn't Exists";
		}
	}
	
	
	
	/**
	*
	* @return \Magento\Framework\App\ResourceConnection
	*/
	public function getResourceConnection(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->get('Magento\Framework\App\ResourceConnection');
	}
	
}