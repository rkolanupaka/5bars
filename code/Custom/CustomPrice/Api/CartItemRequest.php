<?php


namespace Custom\CustomPrice\Api;
use Magento\Framework\Api\AbstractSimpleObject;
use Custom\CustomPrice\Api\Data\CartItemRequestInterface;

/**
 * SearchResults Service Data Object used for the add to cart service requests
 */
class CartItemRequest extends AbstractSimpleObject implements CartItemRequestInterface
{
	const SURVEY_LEASE_FLAG = 'surveyLeaseFlag';
	const QUOTE_ID = 'quote_id';
	const SKU= 'sku';
	
	
	/**
	 * Get Survey Lease Flag
	 *
	 * @return int
	 */
	public function getSurveyLeaseFlag()
	{
		return $this->_get(self::SURVEY_LEASE_FLAG);
	}
	
	/**
	 * Set Survey Lease Flag
	 *
	 * @param int $surveyLeaseFlag
	 * @return $this
	 */
	public function setSurveyLeaseFlag($surveyLeaseFlag)
	{
		return $this->setData(self::SURVEY_LEASE_FLAG, $surveyLeaseFlag);
	}
	
	/**
	 * Returns Quote id.
	 *
	 * @return string
	 */
	public function getQuoteId()
	{
		return $this->_get(self::QUOTE_ID);
	}
	
	/**
	 * Sets Quote id.
	 *
	 * @param string $quoteId
	 * @return $this
	 */
	public function setQuoteId($quoteId)
	{
		return $this->setData(self::QUOTE_ID, $quoteId);
	}
	
	/**
	 * Get Sku Id.
	 *
	 * @return string
	 */
	public function getSku()
	{
		return $this->_get(self::SKU);
	}
	
	/**
	 * Set Sku Id.
	 *
	 * @param string
	 * @return $this
	 */
	public function setSku($sku)
	{
		return $this->setData(self::SKU, $sku);
	}
	
}