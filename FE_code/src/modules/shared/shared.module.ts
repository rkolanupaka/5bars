import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HeaderNewComponent } from '../../components/header-new/header-new.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { CustomPopupComponent } from '../../components/custom-popup/custom-popup.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CookieModule } from 'ngx-cookie';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { DragNDropDirective } from '../../common/directives/dragndrop.directive';
import { HoverEffectDirective } from '../../common/directives/hover-effect.directive';
import { OrderFilterPipe, CityFilterPipe, filterCounterPipe, searchFilterPipe } from '../../common/pipes/order-filter.pipe';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatNativeDateModule } from '@angular/material';
import {MatListModule} from '@angular/material/list';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AgmCoreModule } from '@agm/core';


@NgModule({
    imports: [ 
        CommonModule, 
        FormsModule, 
        RouterModule,
        NgxPaginationModule,
        InfiniteScrollModule,
        // Ng2AutoCompleteModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        CookieModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyAKIRNlT44mTTMRaL0Wm3npkMf-Deyasjw'
        })
    ],
    declarations: [ 
        HeaderNewComponent,
        CustomPopupComponent, 
        FooterComponent,
        DragNDropDirective,
        HoverEffectDirective,
        OrderFilterPipe, 
        CityFilterPipe, 
        filterCounterPipe, 
        searchFilterPipe
    ],
    exports: [
        AgmCoreModule,
        HeaderNewComponent,
        FormsModule, 
        CommonModule, 
        CustomPopupComponent, 
        FooterComponent,
        NgxPaginationModule,
        InfiniteScrollModule,
        DragNDropDirective,
        HoverEffectDirective,
        OrderFilterPipe, 
        CityFilterPipe,
        filterCounterPipe, 
        searchFilterPipe,
        MatAutocompleteModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        MatSelectModule,
        MatCheckboxModule,
        CookieModule
    ]
})

export class SharedModule {}