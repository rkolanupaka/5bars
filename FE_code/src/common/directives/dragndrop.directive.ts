import { Directive, HostListener, HostBinding, EventEmitter, Output } from "@angular/core";

@Directive({
    selector : '[fiveBarsDnd]'
})

export class DragNDropDirective {
    @Output() public fileChangedEmitter : EventEmitter<FileList> = new EventEmitter();

    // @HostBinding('style.background') public background = '#8c8b8d';
    // @HostBinding('style.cursor') private cursor = 'default';

    constructor() { }

    @HostListener('dragover', ['$event']) ondragover(evt){
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#565656';
        let files = evt.dataTransfer.items[0];
        // if(files.type != 'text/plain') {
        //     this.cursor = 'no-drop';
        // }
    }

    @HostListener('dragleave', ['$event']) public onDragLeave(evt){
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#8c8b8d';
        // this.cursor = 'default';
    }

    @HostListener('drop', ['$event']) public onDrop(evt){
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#8c8b8d';
        // this.cursor = 'default';
        let files = evt.dataTransfer.files;
        if(files.length > 0 && files[0].type == 'application/vnd.ms-excel'){
          this.fileChangedEmitter.emit(files);
        }
    }
}