<?php

namespace Custom\SiteDrawings\Model;

use Custom\SiteDrawings\Api\SiteDrawingsInterface;

class SiteDrawings implements SiteDrawingsInterface{
	
	/**
     * Create new gallery entry
     *
     * @param string $sku
     * @param \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[] $entrys
     * @return \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[]
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
	public function saveDrawings($sku,$entrys){
		
		$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$logger->info('SKU from SiteDrawings -'.$sku);
		$entryIds =array();
		foreach ($entrys as $drawingEntry){
			$entryId = $objectManager->get('Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface')->create($sku,$drawingEntry);
			$logger->info('SiteDrawing EntryID -'.$entryId);
			$entryIds[] = $entryId;
		}
		
		//return $entryIds;
		return $objectManager->get('Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface')->getList($sku);
	}
	
	
	/**
	 * Create new gallery entry
	 *
	 * @param string $sku
	 * @param int $entryId
	 * @return bool
	 */
	public function removeDrawings($sku,$entryId){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		return $objectManager->get('Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface')->remove($sku, $entryId);
	}
}
