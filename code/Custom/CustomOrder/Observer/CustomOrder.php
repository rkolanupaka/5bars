<?php
    namespace Custom\CustomOrder\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\ObjectManager;
    	
    class CustomOrder implements ObserverInterface
    {
        /**
         * @var \Psr\Log\LoggerInterface
         */
        protected $logger;
        
		/**
		* @var \Magento\Framework\App\RequestInterface
		*/
		protected $_request;
		
		/**
		* @var \Magento\Framework\Serialize\Serializer\Json
		*/
		protected $serializer;
		
		/**
		 * @var OrderExtensionFactory
		 */
		private $orderExtensionFactory;
		
		/**
		 * @var OrderItemExtensionFactory
		 */
		private $orderItemExtensionFactory;

		/**
		 * @var ResourceConnection
		 */
		private $resourceConnection;
		
		/**
		* Constructor
		* 
		* @param \Psr\Log\LoggerInterface LoggerInterface
		* @param RequestInterface $request
		* @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
		* @param \Magento\Sales\Api\Data\OrderExtensionFactory|null $orderExtensionFactory
		* @param \Magento\Sales\Api\Data\OrderItemExtensionFactory|null $orderItemExtensionFactory
		* @param \Magento\Framework\App\ResourceConnection $resourceConnection
        */
		public function __construct(
		    \Psr\Log\LoggerInterface $logger,
			\Magento\Framework\App\RequestInterface $request,
			\Magento\Framework\Serialize\Serializer\Json $serializer = null,
		    \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory = null,
		    \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory = null,
		    \Magento\Framework\App\ResourceConnection $resourceConnection
		)
		{
		    $this->logger = $logger;
			$this->_request = $request;
			$this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
			$this->orderExtensionFactory = $orderExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderExtensionFactory::class);
			$this->orderItemExtensionFactory = $orderItemExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderItemExtensionFactory::class);
			$this->resourceConnection = $resourceConnection;
		} 
		
        public function execute(\Magento\Framework\Event\Observer $observer) 
		{
        	$this->logger->info('Executing Custom Order');
        	
			// Gets the order which is being saved.
            $order  = $observer->getOrder();
            $items = $order->getAllVisibleItems();
            
            foreach($items as $item) 
            {
                $this->saveReserveProduct($item);
            }
			return $this;
        }
        
        public function saveReserveProduct($item)
        {
            $connection = $this->getConnection();
            $tableName = $this->resourceConnection->getTableName('reserved_products');
            //Insert Data into table
            $sql = "Insert Into " .$tableName. " (sku, order_number, reserved_date) Values ('".$item->getSku()."','".$item->getOrderId()."','".$item->getCreatedAt()."')";
            $this->logger->info($sql);
            $connection->query($sql);
        }
        
        public function getConnection()
        {
            /** @var Magento\Framework\App\ResourceConnection $this->resourceConnection **/
            return $this->resourceConnection->getConnection('default');
        }
    }