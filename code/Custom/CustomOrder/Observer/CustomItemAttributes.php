<?php
namespace Custom\CustomOrder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ObjectManager;

class CustomItemAttributes implements ObserverInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;
    
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $serializer;
    
    /**
     * @var OrderExtensionFactory
     */
    private $orderExtensionFactory;
    
    /**
     * @var OrderItemExtensionFactory
     */
    private $orderItemExtensionFactory;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     * @param RequestInterface $request
     * @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
     * @param \Magento\Sales\Api\Data\OrderExtensionFactory|null $orderExtensionFactory
     * @param \Magento\Sales\Api\Data\OrderItemExtensionFactory|null $orderItemExtensionFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null,
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory = null,
        \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory = null
        ){
            $this->logger = $logger;
            $this->_request = $request;
            $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
            $this->orderExtensionFactory = $orderExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderExtensionFactory::class);
            $this->orderItemExtensionFactory = $orderItemExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderItemExtensionFactory::class);
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$this->logger->info('Executing Custom Item Attributes');
    	
        // Gets the order which is being saved.
        $order  = $observer->getOrder();
        $items = $order->getAllVisibleItems();
        
        $monthlyChargesTotal = 0;
        
        foreach($items as $item)
        {
            $product = $item->getProduct();
            if($product){
				$product->setStoreId(0);
                $product->setData('reserved', '1');
                $product->save();
            }
            
            //$this->logger->info("SKU:".$product->getSku().",Latitude:".$product->getLatitude().",Longitude:".$product->getLongitude());
            //$this->logger->info("CustomItemAttribute | execute | PRODUCT StoreID and Reserve FLAG: ".$product->getStoreId()." ".$product->getData('reserved'));
            $item->setMonthlyCharges($product->getMonthlyCharges());
            $item->setCamFee($product->getCamFee());
            $item->setCityArea($product->getCityArea());
            $item->setOneTimeFee($product->getOneTimeFee());
            $item->setPoleClass($product->getPoleClass());
            $item->setRentLeaseFee($product->getRentLeaseFee());
            $item->setSiteSelectionFee($product->getSiteSelectionFee());
            $item->setSiteSurveyLeaseFee($product->getSiteSurveyLeaseFee());
            $item->setLatitude($product->getLatitude());
            $item->setLongitude($product->getLongitude());
            $item->setState($product->getState());
            $item->setCity($product->getCity());
            $monthlyChargesTotal = $monthlyChargesTotal + $item->getMonthlyCharges();
			
		
        }
        $order->setAllVisibleItems($items);
       
        //get all the params from request
        $params = [];
        $content = $this->_request->getContent();
        if ($content) {
            $params = $this->serializer->unserialize($content); 
        }
        
        //save all custom attributes
        
        if (array_key_exists('site_contact_name', $params)) {
        	$order->setData('site_contact_name', $params['site_contact_name']);
        }
        
        if (array_key_exists('site_contact_email', $params)) {
        	$order->setData('site_contact_email', $params['site_contact_email']);
        }
        
        if (array_key_exists('site_contact_phone', $params)) {
        	$order->setData('site_contact_phone', $params['site_contact_phone']);
        }
        
        if (array_key_exists('site_contact_company', $params)) {
        	$order->setData('site_contact_company', $params['site_contact_company']);
        }
        
        
        $order->setData('monthly_charges_total', $monthlyChargesTotal);
        
        //po_number might not be there for E-Procurement
        if (array_key_exists('po_number', $params)) {
            $order->setData('po_number', $params['po_number']);
        }
        
        return $this;
    }
    
}