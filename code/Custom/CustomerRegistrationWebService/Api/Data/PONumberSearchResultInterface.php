<?php

namespace Custom\CustomerRegistrationWebService\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface PONumberSearchResultInterface
{
    /**
     * Get attributes list.
     *
     * @return \Custom\CustomerRegistrationWebService\Api\Data\PONumberInterface[]
     */
    public function getItems();
    
    /**
     * Set attributes list.
     *
     * @param \Custom\CustomerRegistrationWebService\Api\Data\PONumberInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

}
