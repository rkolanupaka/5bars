import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ProfileComponent } from '../../components/profile/profile.component';
import { ProfileRouterModule } from './profile.module.routing';

@NgModule({
    imports: [SharedModule, ProfileRouterModule],
    declarations: [ProfileComponent],
    exports: [ProfileComponent]
})

export class ProfileModule {
    constructor (){}
}