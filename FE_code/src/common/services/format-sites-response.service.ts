import { Injectable } from "@angular/core";

@Injectable()
export class FormatSitesResponse {
  userData: any;
  constructor() {}

  getSitesFormatted(responseItems, type) {
    this.userData = JSON.parse(localStorage.getItem("fivebarsUserData"));
    let tempArray: any = [];
    for (let site of responseItems) {
      if (site.status && site.status == 1) {
        var x,
          y,
          reserved,
          cityTemp,
          stateTemp,
          rpTemp,
          bpTemp,
          desc,
          addon_aneservice_price,
          rent_lease_fee,
          site_selection_fee,
          one_time_fee_price,
          site_survey_lease_fee,
          siteCA = site.custom_attributes,
          addon_aneservice,
          description,
          locationType,
          site_requested,
          site_assigned_to,
          poleType;
        for (let b of siteCA) {
          if (b.attribute_code == "latitude") {
            x = Number(b.value);
          } else if (b.attribute_code == "longitude") {
            y = Number(b.value);
          } else if (b.attribute_code == "reserved") {
            if (b.value == 1) {
              reserved = true;
            } else {
              reserved = false;
            }
          } else if (b.attribute_code == "city") {
            cityTemp = b.value;
          } else if (b.attribute_code == "state") {
            stateTemp = b.value;
          } else if (b.attribute_code == "rent_lease_fee") {
            rent_lease_fee = b.value;
          } else if (b.attribute_code == "site_selection_fee") {
            one_time_fee_price = b.value;
            site_selection_fee = b.value;
          } else if (b.attribute_code == "site_survey_lease_fee") {
            site_survey_lease_fee = b.value;
          } else if (b.attribute_code == "AddOn_AnEService_Enabled") {
            addon_aneservice = b.value;
          } else if (b.attribute_code == "addon_aneservice_price") {
            addon_aneservice_price = b.value;
          } else if (b.attribute_code == "description") {
            description = b.value;
          } else if (b.attribute_code == "city_area") {
            if (b.value == "DT") {
              locationType = "Downtown";
            } else {
              locationType = b.value;
            }
          } else if (b.attribute_code == "pole_class") {
            poleType = b.value;
          } else if (b.attribute_code == "site_requested") {
            site_requested = b.value;
          } else if (b.attribute_code == "site_assigned_to") {
            site_assigned_to = b.value;
          }
        }

        let tempSiteDataResponse = {
          x: x,
          y: y,
          uniqueId: site.sku,
          reserved: reserved,
          city: cityTemp,
          state: stateTemp,
          rent_lease_fee: rent_lease_fee,
          site_selection_fee: site_selection_fee,
          one_time_fee_price: one_time_fee_price,
          site_survey_lease_fee: site_survey_lease_fee,
          addon_aneservice: addon_aneservice,
          addon_aneservice_price: addon_aneservice_price,
          description: description,
          location_type: locationType,
          pole_type: poleType,
          optionalFeeStatus: false,
          optionalConduitStatus: false
        };
        if (type == "all") {
          if (site_requested && site_requested === "1") {
            if (site_assigned_to == this.userData.id) {
              tempArray.push(tempSiteDataResponse);
            }
          } else if (!site_requested || site_requested === "0") {
            tempArray.push(tempSiteDataResponse);
          }
        } else if (type == "unreserved") {
          if (!tempSiteDataResponse.reserved) {
            if (site_requested && site_requested === "1") {
              if (site_assigned_to == this.userData.id) {
                tempArray.push(tempSiteDataResponse);
              }
            } else if (!site_requested || site_requested === "0") {
              tempArray.push(tempSiteDataResponse);
            }
          }
        }
      }
    }
    return tempArray;
  }
}
