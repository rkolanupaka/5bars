<?php

namespace Custom\Reports\Api;

use Custom\Reports\Api\MapViewDataInterface;
use Magento\Catalog\Model\ResourceModel\Layer\Filter\Decimal;
use Zend\Stdlib\JsonSerializable;

class MapViewData implements MapViewDataInterface,JsonSerializable{
	
	/**
	 *
	 * @var string
	 */
	protected $sku;
	
	/**
	 *
	 * @var bool
	 */
	protected $res;
	
	/**
	 *
	 * @var float
	 */
	protected $lat;
	
	/**
	 *
	 * @var float
	 */
	protected $long;
	
	/**
	 *
	 * @var string
	 */
	protected $stat;
	
	/**
	 *
	 * @var bool
	 */
	protected $sChk;
	
	/**
	 *
	 * @var string
	 */
	protected $pCls;
	
	/**
	 *
	 * @var string
	 */
	protected $lType;
	
	
	/**
	 *
	 * @var string
	 */
	protected $rlFee;
	
	/**
	 *
	 * @var string
	 */
	protected $ssFee;
	
	/**
	 *
	 * @var string
	 */
	protected $sslFee;
	
	
	public function jsonSerialize(){
		return array(
		 'sku' => $this->sku,
		 'res' => $this->res,
		 'lat' => $this->lat,
		 'long' => $this->long,
		 'stat'    => $this->stat,
		 'sChk'=> $this->sChk,
		 'pCls' => $this->pCls,
		 'lType' => $this->lType,
		 'rlFee' => $this->rlFee,
		 'ssFee' => $this->ssFee,
		 'sslFee' => $this->sslFee
		 );
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSku(){
		return $this->sku;
	}
	
	/**
	 * @api
	 * @param string $sku
	 * @return void
	 */
	public function setSku($sku){
		$this->sku = $sku;
	}
	
	/**
	 * @api
	 * @return bool|null
	 */
	public function getRes(){
		return $this->res;
	}
	
	/**
	 * @api
	 * @param bool $res
	 * @return void
	 */
	public function setRes($res){
		$this->res = $res;
	}
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLat(){
		return $this->lat;
	}
	
	/**
	 * @api
	 * @param float $lat
	 * @return void
	 */
	public function setLat($lat){
		$this->lat = $lat;
	}
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLong(){
		return $this->long;
	}
	
	/**
	 * @api
	 * @param float $long
	 * @return void
	 */
	public function setLong($long){
		$this->long = $long;
	}
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getStat(){
		return $this->stat;
	}
	
	/**
	 * @api
	 * @param string $stat
	 * @return void
	 */
	public function setStat($stat){
		$this->stat = $stat;
	}
	
	/**
	 * @api
	 * @return bool|null
	 */
	public function getSChk(){
		return $this->sChk;
	}
	
	/**
	 * @api
	 * @param bool $sChk
	 * @return void
	 */
	public function setSChk($sChk){
		$this->sChk = $sChk;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getPCls(){
		return $this->pCls;
	}
	
	/**
	 * @api
	 * @param string $pCls
	 * @return void
	 */
	public function setPCls($pCls){
		$this->pCls = $pCls;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getLType(){
		return $this->lType;
	}
	
	/**
	 * @api
	 * @param string $lType
	 * @return void
	 */
	public function setLType($lType){
		$this->lType = $lType;
	}
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRlFee(){
		return $this->rlFee;
	}
	
	/**
	 * @api
	 * @param string $rlFee
	 * @return void
	 */
	public function setRlFee($rlFee){
		$this->rlFee = $rlFee;
	}
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSsFee(){
		return $this->ssFee;
	}
	
	/**
	 * @api
	 * @param string $ssFee
	 * @return void
	 */
	public function setSsFee($ssFee){
		$this->ssFee = $ssFee;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSslFee(){
		return $this->sslFee;
	}
	
	/**
	 * @api
	 * @param string $sslFee
	 * @return void
	 */
	public function setSslFee($sslFee){
		$this->sslFee = $sslFee;
	}
}