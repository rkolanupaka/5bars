<?php 


namespace Custom\Reports\Model;
use Custom\CustomerRegistrationWebService\Api\CustomerInterface;
use Custom\Reports\Api\ReportsInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Psr\Log\LoggerInterface;

/**
 * Defines the implementation class of the Customer service contract.
 */
class Reports extends \Magento\Framework\App\Action\Action implements ReportsInterface
{
    
     /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    
    /**
     * 
     * @var \DateTime
     */
    protected $date;
    
    /**
     * 
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timeZone;
    
    /**
     * 
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \DateTime $date;
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
    							\DateTime $date,
    							\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
    							\Psr\Log\LoggerInterface $logger
    							) {
        $this->storeManager     = $storeManager;
        $this->date = $date;
        $this->timeZone = $timeZone;
        $this->logger = $logger;
        parent::__construct($context);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getOrders()
     */
    public function getFinServReport($startDate,$endDate)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$salesOrder = $resource->getTableName('sales_order');
    	$salesOrderItem = $resource->getTableName('sales_order_item');
    	$salesOrderAddress = $resource->getTableName('sales_order_address');
    	$salesOrderPayment = $resource->getTableName('sales_order_payment');
    	$sql = "SELECT o.entity_id, GROUP_CONCAT(DISTINCT oi.sku) sku, o.site_contact_name,o.site_contact_phone,
			o.site_contact_email, concat(o.customer_firstname,' ',o.customer_lastname) customer_name,
			oa.telephone,o.customer_email, o.created_at,o.monthly_charges_total,sop.method,o.po_number,
			concat(oa.street,', ',oa.region,', ',oa.country_id,', ',oa.postcode) billing_address 
			FROM ".$salesOrder." as o JOIN ".$salesOrderItem." as oi ON o.entity_id=oi.order_id 
			join ".$salesOrderAddress." as oa ON o.billing_address_id=oa.entity_id 
			join ".$salesOrderPayment." as sop ON sop.parent_id = o.entity_id
			WHERE (o.created_at BETWEEN '".$startDate."' AND '".$endDate."') 
			and oa.address_type like 'billing' GROUP BY o.entity_id";
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size ------'.sizeof($array));
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	$fileName = "Orders". round(microtime(true) * 1000).".csv";
    	$this->prepareFile($array,$this->createFinServReportHeading(),$fileName);
    }
    
    /**
     * 
     * @return \Magento\Framework\Phrase[]
     */
    protected function createFinServReportHeading(){
    	return [
    			__('Order Number'),
    			__('Sites Reserved'),
    			__('Project Contact Name'),
    			__('Project Contact Number'),
    			__('Project Contact eMail'),
    			__('Order Contact Name'),
    			__('Order Contact Number'),
    			__('Order Contact eMail'),
    			__('Date & Time'),
    			__('Total Monthly Fees'),
    			__('PO Number'),
    			__('Billing Address')
    	];
    }
    /**
     *
     * @return \Magento\Framework\App\ResourceConnection
     */
    public function getResourceConnection(){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\ResourceConnection');
    }
    
    /**
     * 
     * @param array $array
     */
    public function prepareFile($array,$heading,$outputFile)
    {
    	$handle = fopen($outputFile, 'w');
    	fputcsv($handle, $heading);
    	
    	foreach ($array as $resultSet)
    	{
    		$row = array();
    		foreach ($resultSet as $result){
    			$row[] = $result;
    		}
    		
    		fputcsv($handle, $row);
    	}
    	$this->downloadCsv($outputFile);
    }
    
    public function downloadCsv($file)
    {
    	if (file_exists($file)) {
    		//set appropriate headers
    		header('Content-Description: File Transfer');
    		header('Content-Type: application/csv');
    		header('Content-Disposition: attachment; filename='.basename($file));
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate');
    		header('Pragma: public');
    		header('Content-Length: ' . filesize($file));
    		ob_clean();
    		flush();
    		readfile($file);
    	}
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getEngineeringReport()
     */
    public function getEngineeringReport($startDate,$endDate)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$salesOrder = $resource->getTableName('sales_order');
    	$salesOrderItem = $resource->getTableName('sales_order_item');
    	$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    	$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    	$eavAttribute = $resource->getTableName('eav_attribute');
    	$eavEntityType = $resource->getTableName('eav_entity_type');
    	$sql = "select oi.sku,oi.latitude,oi.longitude,oi.pole_class,o.entity_id,o.created_at,v1.value as state,
				v2.value as city,o.site_contact_name,o.site_contact_phone,o.site_contact_email
				FROM ".$salesOrder." as o JOIN ".$salesOrderItem." as oi ON o.entity_id=oi.order_id 
				JOIN ".$catalogProductEntity." e ON oi.sku=e.sku 
				LEFT JOIN ".$catalogProductEntityVarchar." v1 ON e.entity_id = v1.entity_id 
				AND v1.store_id = 0 
				AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'state' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product')) 
				LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id 
				AND v2.store_id = 0 
				AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product')) 
				where (o.created_at BETWEEN '".$startDate."' AND '".$endDate."')";
    			
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size --------'.sizeof($array));
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	$fileName = "Products". round(microtime(true) * 1000).".csv";
    	$this->prepareFile($array,$this->createEnggReportHeading(),$fileName);
    }
    
    /**
     *
     * @return \Magento\Framework\Phrase[]
     */
    protected function createEnggReportHeading(){
    	return [
    			__('Site ID'),
    			__('Latitude'),
    			__('Longitude'),
    			__('Pole-Type'),
    			__('Order Number'),
    			__('Order Date'),
    			__('State'),
    			__('City'),
    			__('Project Contact Name'),
    			__('Project Contact Number'),
    			__('Project Contact eMail')
    	];
    }
    
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getSiteLocationData()
     */
    public function getSiteLocationData($state,$city)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
    	$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    	$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    	$eavAttribute = $resource->getTableName('eav_attribute');
    	$eavEntityType = $resource->getTableName('eav_entity_type');
    	$sql = "select v4.value as latitude,v5.value as longitude,e.sku FROM ".$catalogProductEntity." e LEFT JOIN ".$catalogProductEntityVarchar." v1 ON e.entity_id = v1.entity_id 
				AND v1.store_id = 0 
				AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'state' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id 
				AND v2.store_id = 0 
				AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityInt." v3 ON e.entity_id = v3.entity_id 
				AND v3.store_id = 0 
				AND v3.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'reserved' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v4 ON e.entity_id = v4.entity_id 
				AND v4.store_id = 0 
				AND v4.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'latitude' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v5 ON e.entity_id = v5.entity_id 
				AND v5.store_id = 0 
				AND v5.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'longitude' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                WHERE v1.value = '".$state."' AND v2.value = '".$city."' and v3.value = 0";
    	
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size --------'.sizeof($array));
    	foreach ($array as $test){
    		foreach ($test as $t){
    			$this->logger->info('ELements in array---'.$t);
    		}
    		
    	}
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	$fileName = $state."_".$city.".csv";
    	$this->prepareFile($array,$this->createSiteLocationReportHeading(),$fileName);
    }
    
    /**
     *
     * @return \Magento\Framework\Phrase[]
     */
    protected function createSiteLocationReportHeading(){
    	return [
    			__('y'),
    			__('x'),
    			__('XGID')
    	];
    }
    
    /**
     * Override method.
     */
    public function execute()
    {
    	$this->logger->info('Reports Class being called');
    }
    
}