<?php


namespace Custom\SiteManagement\Api;

use Magento\Framework\Api\AbstractSimpleObject;
use Custom\SiteManagement\Api\Data\StateCitiesInterface;

/**
 * SearchResults Service Data Object used for the search service requests
 */
class StateCities extends AbstractSimpleObject implements StateCitiesInterface
{
	const STATE = 'state';
	const STATE_CODE = 'state_code';
	const CITIES = 'cities';
	const CITIES_WITHOUT_INVENTORY = 'cities_without_inventory';
	
	/**
	 * Get attribute.
	 *
	 * @return string
	 */
	public function getState()
	{
		return $this->_get(self::STATE);
	}
	
	/**
	 * Set attribute.
	 *
	 * @param string $state
	 * @return $this
	 */
	public function setState($state)
	{
		return $this->setData(self::STATE, $state);
	}
	
	/**
	 * Get attribute.
	 *
	 * @return string
	 */
	public function getStateCode()
	{
		return $this->_get(self::STATE_CODE);
	}
	
	/**
	 * Set attribute.
	 *
	 * @param string $stateCode
	 * @return $this
	 */
	public function setStateCode($stateCode)
	{
		return $this->setData(self::STATE_CODE, $stateCode);
	}
	
	/**
	 * Get attribute.
	 *
	 * @return array
	 */
	public function getCities()
	{
		return $this->_get(self::CITIES);
	}
	
	/**
	 * Set attribute.
	 *
	 * @param array $cities
	 * @return $this
	 */
	public function setCities($cities)
	{
		return $this->setData(self::CITIES, $cities);
	}
	
	/**
	 * Get attribute.
	 *
	 * @return array
	 */
	public function getCitiesWithoutInventory()
	{
		return $this->_get(self::CITIES_WITHOUT_INVENTORY);
	}
	
	/**
	 * Set attribute.
	 *
	 * @param array $citiesWithoutInventory
	 * @return $this
	 */
	public function setCitiesWithoutInventory($citiesWithoutInventory)
	{
		return $this->setData(self::CITIES_WITHOUT_INVENTORY, $citiesWithoutInventory);
	}
}
