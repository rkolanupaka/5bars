<?php

namespace Custom\DataSync\Api\Data;

/**
 * Interface MasterDataRepositoryInterface.php
 * @api
 */
interface MasterDataRepositoryInterface
{
    /**
     * Return the Details of Products which has updated timestamp greater than the timestamp passed as parameter
     *
     * @param $timeStamp
     * @return 
     */
    public function getDetailsFromMaster($connection, $transaction, $timeStamp);
    
}
