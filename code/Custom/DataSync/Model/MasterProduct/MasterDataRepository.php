<?php

namespace Custom\DataSync\Model\MasterProduct;

use Custom\DataSync\Api\Data\MasterDataRepositoryInterface;

class MasterDataRepository extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,MasterDataRepositoryInterface
{
    const CACHE_TAG = 'product';
   
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_request;
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager
        ) 
    {
            $this->logger = $logger;
            $this->_request = $request;
            $this->_transportBuilder = $transportBuilder;
            $this->_storeManager = $storeManager;
    }
    
    protected function _construct()
    {
        $this->_init('\Custom\DataSync\Model\ResourceModel\MasterDataRepository');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        
        return $values;
    }
    
    public function getProducts($connection, $timeStamp)
    {
        $this->logger->info("MasterDataRepository | getProducts");
        $result = 0;
        try
        {
            $sql = "SELECT * FROM masterdb.product WHERE last_updated_timestamp > '".$timeStamp."' AND order_channel != 'sales'";
            $this->logger->info("MasterDataRepository | getProducts | Query To Get Records From Master DB::: ".$sql);
            $result = $connection->fetchAll($sql);
        }
        catch(\Exception $e)
        {
            $this->logger->info("MasterDataRepository | getProducts | Exception While Getting Products From Master DB::: ".$e->getMessage());
        }
        return $result;
    }
    
    public function getDetailsFromMaster($connection, $transaction, $timeStamp)
    {
        $this->logger->info("MasterDataRepository | getDetailsFromMaster");
        $results = $this->getProducts($connection, $timeStamp);
        $this->logger->info("MasterDataRepository | getDetailsFromMaster | Number Of Results From Master DB::: ".sizeof($results));
        return $results;
    }
    
    public function createQuery($sku, $transaction)
    {
        $this->logger->info("MasterDataRepository | createQuery");
        $sql = "UPDATE masterdb.product SET `is_new` = 0 WHERE `sku` = '";
        $sql = $sql.$sku."'";
        $this->logger->info("MasterDataRepository | createQuery | ::: ".$sql);
        return $sql;
    }
    
    public function createQueryForOrders($sku, $transaction)
    {
        $this->logger->info("MasterDataRepository | createQueryForOrders");
        $sql = "UPDATE masterdb.product SET `is_new` = 0, `reserved` = 1, `order_channel` = 'sales' WHERE `sku` = '";
        $sql = $sql.$sku."'";
        $this->logger->info("MasterDataRepository | createQueryForOrders | ::: ".$sql);
        return $sql;
    }
    
    public function updateOrderRecordsMasterProduct($connection, $transaction, $newOrders)
    {
        $this->logger->info("MasterDataRepository | updateOrderRecordsMasterProduct");
        try
        {
            for($count = 0; $count < sizeof($newOrders); $count++)
            {
                $sql = $this->createQueryForOrders($newOrders[$count], $transaction);
                $connection->query($sql);
            }
        }
        catch (\Exception $e)
        {
            $this->logger->info("MasterDataRepository | updateOrderRecordsMasterProduct | Exception While Updating Master DB For Orders::: ".$e->getMessage());
        }
    }
    
    public function updateMasterProduct($connection, $transaction, $newRecords)
    {
        $this->logger->info("MasterDataRepository | updateMasterProduct");
        //$this->logger->info("MasterDataRepository | updateMasterProduct | Size Of newRecords:: ".sizeof($newRecords));
        try
        {
            for($count = 0; $count < sizeof($newRecords); $count++)
            {
                //$this->logger->info("MasterDataRepository | updateMasterProduct | ".$newRecords[$count]);
                $values = explode(",", $newRecords[$count]);
                $sql = $this->createQuery($values[1], $transaction);
                $connection->query($sql);
            }
        }
        catch (\Exception $e)
        {
            $this->logger->info("MasterDataRepository | updateMasterProduct | Exception While Updating Master DB::: ".$e->getMessage());
        }
    }
    
}