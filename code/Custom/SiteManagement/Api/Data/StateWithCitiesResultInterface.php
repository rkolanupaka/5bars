<?php

namespace Custom\SiteManagement\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface StateWithCitiesResultInterface
{
	/**
	 * Get attributes list.
	 *
	 * @return \Custom\SiteManagement\Api\Data\StateCitiesInterface[]
	 */
	public function getItems();
	
	/**
	 * Set attributes list.
	 *
	 * @param \Custom\SiteManagement\Api\Data\StateCitiesInterface[] $items
	 * @return $this
	 */
	public function setItems(array $items);
	
}
