<?php

namespace Custom\WaitList\Api;


/**
 * Interface WaitlistManagementInterface
 * @api
 */
interface WaitlistManagementInterface
{
	
	/**
	 * Return Waitlist items.
	 *
	 * @param int $customerId
	 * @return array
	 */
	public function getWaitlistForCustomer($customerId);
	
	/**
	 * Return Added waitlist item.
	 *
	 * @param int $customerId
	 * @param string $productId
	 * @return array
	 *
	 */
	public function addWaitlistForCustomer($customerId, $productId);
	
	/**
	 * delete waitlist item.
	 *
	 * @param int $customerId
	 * @param int $wishlistId
	 * @return array
	 *
	 */
	public function deleteWaitlistForCustomer($customerId, $wishlistItemId);
	
	/**
	 * delete waitlist item.
	 *
	 * @param int $customerId
	 * @param string $productSku
	 * @return array|string
	 *
	 */
	public function deleteWaitlistProductForCustomer($customerId, $productSku);
	
	
}