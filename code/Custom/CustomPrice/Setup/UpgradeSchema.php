<?php

namespace  Custom\CustomPrice\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface{
	
	/**
	 * Upgrades DB schema for a module
	 *
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$setup->startSetup();
		
		//Quote Item Table
		$quoteItemTable = 'quote_item';
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'monthly_charges',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Monthly Charges'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'cam_fee',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Cam Fee'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'city_area',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'City Area'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'one_time_fee',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'One Time Fee'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'pole_class',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Pole Class'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'rent_lease_fee',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Rent Lease Fee'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'site_selection_fee',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Site Selection Fee'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'site_survey_lease_fee',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Site Survey Lease Fee'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'latitude',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Latitude'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'longitude',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Longitude'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'state',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'State'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($quoteItemTable),
				'city',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'City'
				]
				);
		
		$setup->endSetup();
	}
}