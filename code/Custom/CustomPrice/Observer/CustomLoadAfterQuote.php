<?php
    namespace Custom\CustomPrice\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\RequestInterface;
		
    class CustomLoadAfterQuote implements ObserverInterface
    {
		/**
		* @var \Magento\Framework\App\RequestInterface
		*/
		protected $_request;
		
		/**
		* @var \Magento\Catalog\Model\ProductRepository
		*
		*/
		protected $_productRepo;
		
		/**
		* @var \Magento\Framework\Serialize\Serializer\Json
		*/
		protected $serializer;
		
		/**
		* @var Magento\Quote\Api\Data\CartItemExtensionFactory
		*/		
		private $cartItemExtensionFactory;
		
		/**
		* @param RequestInterface $request
		* @param \Magento\Catalog\Model\ProductRepository $productRepo
		* @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
		* @param \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
		*/
		public function __construct(
			\Magento\Framework\App\RequestInterface $request,
			\Magento\Catalog\Model\ProductRepository $productRepo,
			\Magento\Framework\Serialize\Serializer\Json $serializer = null,
			\Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
		){
			$this->_request = $request;
			$this->_productRepo = $productRepo;
			$this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
			$this->cartItemExtensionFactory = $cartItemExtensionFactory ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Quote\Api\Data\CartItemExtensionFactory::class);
		} 
		
        public function execute(\Magento\Framework\Event\Observer $observer) 
		{
			$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
			$logger->info("CustomLoadAfterQuote | execute | entered");
			$quote= $observer->getEvent()->getQuote();
			$logger->info("CustomLoadAfterQuote | execute | quote found");
			//$quote_items = $quote->getItems();
			$quote_items = $quote->getAllVisibleItems();
			$logger->info("CustomLoadAfterQuote | execute | QuoteItemsCOUNT: ".count($quote_items));
			if($quote_items)
			{
				$logger->info("CustomLoadAfterQuote | execute | quote_items found");
				foreach ($quote_items as $quote_item){
					$product = $quote_item->getProduct();
					$logger->info("CustomLoadAfterQuote | execute | ProductID: ".$product->getSku());
					//$product = $this->_productRepo->get($product->getSku());
					$itemExtensionAttributes = $quote_item->getExtensionAttributes();
					if ($itemExtensionAttributes === null) {
						$itemExtensionAttributes = $this->cartItemExtensionFactory->create();
					}
					
					$logger->info("CustomLoadAfterQuote | execute | MontlhyCharges: ".$product->getMonthlyCharges());
					$itemExtensionAttributes->setMonthlyCharges($product->getMonthlyCharges());
					$itemExtensionAttributes->setCamFee($product->getCamFee());
					$itemExtensionAttributes->setSiteSelectionFee($product->getSiteSelectionFee());
					$itemExtensionAttributes->setSiteSurveyLeaseFee($product->getSiteSurveyLeaseFee());
					$itemExtensionAttributes->setRentLeaseFee($product->getRentLeaseFee());
					$itemExtensionAttributes->setCityArea($product->getCityArea());
					$itemExtensionAttributes->setPoleClass($product->getPoleClass());
					$itemExtensionAttributes->setLatitude($product->getLatitude());
					$itemExtensionAttributes->setLongitude($product->getLongitude());
					$itemExtensionAttributes->setState($product->getState());
					$itemExtensionAttributes->setCity($product->getCity());
					
					$quote_item->setExtensionAttributes($itemExtensionAttributes);
					$logger->info("CustomLoadAfterQuote | execute | MontlhyCharges from EXTN : ".$itemExtensionAttributes->getMonthlyCharges());
					$logger->info("CustomLoadAfterQuote | execute | set survey_lease_selected as :".$quote_item->getSurveyLeaseSelected());
					$logger->info("CustomLoadAfterQuote | execute | set has_conduit_requested as :".$quote_item->getHasConduitRequested());
					$quote_item->save();
				}
				$quote->save();
			}
			$logger->info("CustomLoadAfterQuote | execute | leaving");
        }
    }