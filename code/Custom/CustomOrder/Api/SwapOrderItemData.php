<?php

namespace Custom\CustomOrder\Api;

use Custom\CustomOrder\Api\SwapOrderItemDataInterface;

class SwapOrderItemData implements SwapOrderItemDataInterface{
	
	/**
	 *
	 * @var int
	 */
	protected $orderNumber;
	
	/**
	 *
	 * @var string
	 */
	protected $oldSku;
	
	/**
	 *
	 * @var string
	 */
	protected $newSku;
	
	/**
	 *
	 * @var int
	 */
	protected $surveyLeaseFlag;
	
	/**
	 *
	 * @var int
	 */
	protected $hasConduitRequested;
	
	/**
	 *
	 * @var string
	 */
	protected $reasonCode;
	
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getOrderNumber(){
		return $this->orderNumber;
	}
	
	/**
	 * @api
	 * @param int $orderNumber
	 * @return void
	 */
	public function setOrderNumber($orderNumber){
		$this->orderNumber = $orderNumber;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getOldSku(){
		return $this->oldSku;
	}
	
	/**
	 * @api
	 * @param string $oldSku
	 * @return void
	 */
	public function setOldSku($oldSku){
		$this->oldSku =$oldSku;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getNewSku(){
		return $this->newSku;
	}
	
	/**
	 * @api
	 * @param string $newSku
	 * @return void
	 */
	public function setNewSku($newSku){
		$this->newSku = $newSku;
	}
	
	
	/**
	 * Get Survey Lease Flag
	 *
	 * @return int
	 */
	public function getSurveyLeaseFlag(){
		return $this->surveyLeaseFlag;
	}
	
	/**
	 * Set Survey Lease Flag
	 *
	 * @param int $surveyLeaseFlag
	 * @return $this
	 */
	public function setSurveyLeaseFlag($surveyLeaseFlag){
		$this->surveyLeaseFlag = $surveyLeaseFlag;
	}
	
	
	/**
	 * Get HasConduitRequested Flag
	 *
	 * @return int
	 */
	public function getHasConduitRequested(){
		return $this->hasConduitRequested;
	}
	
	/**
	 * Set HasConduitRequested Flag
	 *
	 * @param int $hasConduitRequested
	 * @return $this
	 */
	public function setHasConduitRequested($hasConduitRequested){
		$this->hasConduitRequested = $hasConduitRequested;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getReasonCode(){
		return $this->reasonCode;
	}
	
	/**
	 * @api
	 * @param string $reasonCode
	 * @return void
	 */
	public function setReasonCode($reasonCode){
		$this->reasonCode = $reasonCode;
	}
}