<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomOrder\Api;


/**
 * Sends Order confirmation for an order to given email.
 */
interface CustomOrderConfirmationEmailInterface
{
    /**
     * @api
     * @param string $orderid.
     * @param string $emailID.
     * @return void
     */
    public function send($orderID, $emailID);
}