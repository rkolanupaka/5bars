<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomOrder\Model;

use Custom\CustomOrder\Api\CustomOrderRepositoryInterface;

/**
 * Defines the implementation class of the CustomOrderRepositoryInterface service contract.
 */
class CustomOrderRepository implements CustomOrderRepositoryInterface
{
	/**
	* @var \Magento\Framework\Serialize\Serializer\Json
	*/
	protected $serializer;
	
	/**
	 * @var \Magento\Sales\Api\OrderRepositoryInterface
	 */
	protected $orderRepositoryInterface;
	
	/**
	 *
	 * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
	 */
	protected $timeZone;
	
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;
	
	/**
	 * @var OrderItemExtensionFactory
	 */
	protected $orderItemExtensionFactory;
	
	/**
	 * @var \Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface
	 */
	protected $productAttributeMediaGalleryManagementInterface;
	
	
	/**
	 * @var \Magento\Sales\Model\Order
	 */
	protected $order;
	
	protected $orderRepository;
	
	/**
	 * Quote repository.
	 *
	 * @var \Magento\Quote\Model\Quote
	 */
	protected $quote;
	
	/**
	 * @var Magento\Quote\Api\Data\CartItemExtensionFactory
	 */
	protected $cartItemExtensionFactory;
	
	
	protected $logger;
	
	protected $customRepository;
	
	protected $_productRepository;
	
	protected $quoteToOrder;
	
	protected $quoteInterface;
	
	/**
	 * @var \Magento\Sales\Api\Data\OrderItemInterface[]
	 */
	public $updateOrderitems;  
	
	public function __construct(
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
		\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory = null,
		\Magento\Catalog\Api\ProductAttributeMediaGalleryManagementInterface $productAttributeMediaGalleryManagementInterface,
		\Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
	    \Magento\Sales\Model\Order $order,
			\Magento\Quote\Model\Quote $quote,
			\Magento\Quote\Api\CartRepositoryInterface $quoteInterface,
		\Psr\Log\LoggerInterface $loggerInterface,
		\Magento\Catalog\Model\ProductRepository $_productRepository,
		\Custom\CustomPrice\Model\Quote\Item\CustomRepository $customRepository,
			\Magento\Quote\Model\Quote\Item\ToOrderItem $quoteToOrder,
			\Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
	){
		$this->orderRepositoryInterface = $orderRepositoryInterface;
		$this->timeZone = $timeZone;
		$this->storeManager = $storeManager;
		$this->orderItemExtensionFactory = $orderItemExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderItemExtensionFactory::class);
		$this->productAttributeMediaGalleryManagementInterface = $productAttributeMediaGalleryManagementInterface;
		$this->order = $order;
		$this->orderRepository = $orderRepository;
		$this->quote = $quote;
		$this->logger = $loggerInterface;
		$this->customRepository = $customRepository;
		$this->_productRepository = $_productRepository;
		$this->quoteToOrder = $quoteToOrder;
		$this->cartItemExtensionFactory = $cartItemExtensionFactory ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Quote\Api\Data\CartItemExtensionFactory::class);
		
	}
	
	/**
	 * Routing call to out of the box getLIst implementation 
	 *
	 * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
	 * @return \Magento\Sales\Api\Data\OrderSearchResultInterface
	 */
	public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$logger = $objectManager->get('\Psr\Log\LoggerInterface');
		$ordersList = $this->orderRepositoryInterface->getList($searchCriteria);
		$orderInterfaceList = $ordersList->getItems();
		foreach ($orderInterfaceList as $order){
			$logger->info('date before update----'.$order->getCreatedAt());
			$order->setCreatedAt($this->getCreatedAtFormatted(1,$order));
			$logger->info('date after update----'.$order->getCreatedAt());
			
			$orderItems = $order->getItems();
			foreach ($orderItems as $item){
				$sku = $item->getSku();
				$extensionAttributes=$item->getExtensionAttributes();
				if ($extensionAttributes === null) {
					$extensionAttributes = $this->orderItemExtensionFactory->create();
				}
				$extensionAttributes->setSiteDrawings($this->productAttributeMediaGalleryManagementInterface->getList($sku));
				$item->setExtensionAttributes($extensionAttributes);
			}
			
		}
		return $ordersList;
	}
	
	/**
	 * Get formatted order created date in store timezone
	 *
	 * @param   string $format date format type (short|medium|long|full)
	 * @return  string
	 */
	public function getCreatedAtFormatted($format,$orderDetails)
	{
		return $this->timeZone->formatDateTime(
				new \DateTime($orderDetails->getCreatedAt()),
				$format,
				$format,
				null,
				$this->timeZone->getConfigTimezone('store', $this->storeManager->getStore())
				);
	}
	
   /**
    * @api to swap an item in the Existing Order
    *
    * @param \Custom\CustomOrder\Api\SwapOrderItemDataInterface $swapData
    * @return null
    */
	public function swapOrderItems($swapData){
		
		$order = $this->orderRepositoryInterface->get($swapData->getOrderNumber());
		$orderItems = $order->getItems();
		
		/**
		 *
		 * @var \Magento\Sales\Api\Data\OrderItemInterface[]
		 */
		$updatedOrderItems;
		$updatedOrderItems = array();
		
		$oldProduct = null;
		$newSwapProduct = null;
		
		foreach ($orderItems as $orderItem){
			
			$product = $orderItem->getProduct();
			$sku = $product->getSku();
			$this->logger->info("Product ID Check in Items:".$sku);
			if(strcmp($sku,$swapData->getOldSku())==0){
				
				$oldProduct = $orderItem->getProduct();
				$newSwapProduct = $this->_productRepository->get($swapData->getNewSku());
				$this->logger->info("Before SWAP: ProductCode from OrderItem".$orderItem->getProductCode());
				
				$this->logger->info("NEW SKU:".$newSwapProduct->getSku());
				$this->logger->info("NEW Product ID:".$newSwapProduct->getId());
				$this->logger->info("NEW Product STORE ID:".$newSwapProduct->getStoreId());
				
				$orderItem->setProduct($newSwapProduct);
				
				//setting New ProductID in place of Old ProductID for Existing Order
				$orderItem->setProductId($newSwapProduct->getId());
				
				$orderItem->setSku($newSwapProduct->getSku());
				
				$orderItem->setName($newSwapProduct->getSku());
				
				$orderItem->setMonthlyCharges($newSwapProduct->getMonthlyCharges());
				$orderItem->setCamFee($newSwapProduct->getCamFee());
				$orderItem->setCityArea($newSwapProduct->getCityArea());
				$orderItem->setOneTimeFee($newSwapProduct->getOneTimeFee());
				$orderItem->setPoleClass($newSwapProduct->getPoleClass());
				$orderItem->setRentLeaseFee($newSwapProduct->getRentLeaseFee());
				$orderItem->setSiteSelectionFee($newSwapProduct->getSiteSelectionFee());
				$orderItem->setSiteSurveyLeaseFee($newSwapProduct->getSiteSurveyLeaseFee());
				$orderItem->setLatitude($newSwapProduct->getLatitude());
				$orderItem->setLongitude($newSwapProduct->getLongitude());
				$orderItem->setState($newSwapProduct->getState());
				$orderItem->setCity($newSwapProduct->getCity());
				$orderItem->setReasonCode($swapData->getReasonCode());
				//$monthlyChargesTotal = $monthlyChargesTotal + $orderItem->getMonthlyCharges();
				
				//$orderItem->save();
				$this->logger->info("After SWAP: Order ITEM Product ID ".$orderItem->getProductId());
				$this->logger->info("After SWAP: Order ITEM Product SKU ".$orderItem->getSku());
				$this->logger->info("After SWAP: ProductCode from OrderItem".$orderItem->getProductCode());
			}
			$updatedOrderItems[] = $orderItem;
		}
		
		//save Order Items
		$order->setItems($updatedOrderItems);
		
		$order->save();
		//$this->orderRepositoryInterface->save($order);
		
		//Save Old product stock and Reserve flag details
		if($oldProduct){
			$this->logger->info("SWAP CHECK StoreID logger OLD Product Before:".$oldProduct->getStoreId());
			$oldProduct->setStoreId(0);
			$oldProduct->setData('reserved', '0');
			$oldProduct->setStockData(array(
					'is_in_stock' => 1,
					'qty' => 1
			));
			$oldProduct->save();
			//$oldProductInfo = $this->_productRepository->save($oldProduct);
			$this->logger->info("SWAP CHECK StoreID logger OLD Product:".$oldProduct->getStoreId());
		}
		
		//Save New product stock and Reserve flag details
		if($newSwapProduct){
			$this->logger->info("SWAP CHECK StoreID logger NEW Product Before:".$newSwapProduct->getStoreId());
			$newSwapProduct->setStoreId(0);
			$newSwapProduct->setData('reserved', '1');
			$newSwapProduct->setStockData(array(
					'is_in_stock' => 0,
					'qty' => 0
			));
			$newSwapProduct->save();
			//$newProductInfo = $this->_productRepository->save($newSwapProduct);
			$this->logger->info("SWAP CHECK StoreID logger NEW Product:".$newSwapProduct->getStoreId());
		}
	}
}