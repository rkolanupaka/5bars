<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomOrder\Api;


/**
 * Sends Order confirmation for an order to given email.
 */
interface CustomOrderRepositoryInterface
{
    /**
     * Lists orders that match specified search criteria.
     *
     * This will route request to OrderRepositoryInterface 
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria The search criteria.
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface Order search result interface.
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);
    
    
    /**
    * @api to swap an item in the Existing Order
    *
    * @param \Custom\CustomOrder\Api\SwapOrderItemDataInterface $swapData
    * @return null
    */
    public function swapOrderItems($swapData);
}