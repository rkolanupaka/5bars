<?php


namespace Custom\SiteManagement\Api;

use Magento\Framework\Api\AbstractSimpleObject;
use Custom\SiteManagement\Api\Data\StateWithCitiesResultInterface;

/**
 * SearchResults Service Data Object used for the search service requests
 */
class StateWithCitiesResult extends AbstractSimpleObject implements StateWithCitiesResultInterface
{
	const KEY_ITEMS = 'items';
	
	/**
	 * Get items
	 *
	 * @return \Custom\SiteManagement\Api\Data\StateCitiesInterface[]
	 */
	public function getItems()
	{
		return $this->_get(self::KEY_ITEMS) === null ? [] : $this->_get(self::KEY_ITEMS);
	}
	
	/**
	 * Set items
	 *
	 * @param \Custom\SiteManagement\Api\Data\StateCitiesInterface[] $items
	 * @return $this
	 */
	public function setItems(array $items)
	{
		return $this->setData(self::KEY_ITEMS, $items);
	}
	
}
