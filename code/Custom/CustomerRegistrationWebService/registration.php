<?php
/**
* 
*
* @category MyOwn_CustomAPi
*
* @author Phani
* 
*/
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Custom_CustomerRegistrationWebService',
    __DIR__
);