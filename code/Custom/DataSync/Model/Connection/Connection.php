<?php

namespace Custom\DataSync\Model\Connection;

use Custom\DataSync\Api\Data\ConnectionInterface;

class Connection extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,ConnectionInterface
{
    
    const CACHE_TAG = 'impex_batch';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\ResourceConnection $resourceConnection
        )
    {
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
    }
    
    protected function _construct()
    {
        $this->_init('\Custom\DataSync\Model\ResourceModel\Connection');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }
    
    public function getMasterDBConnection()
    {
        $connection = null;
        try 
        {
            $this->logger->info("Getting MasterDBConnection from Connection");
            /** @var Magento\Framework\App\ResourceConnection $this->resourceConnection **/
            $connection = $this->resourceConnection->getConnection('custom');
            $this->logger->info("Master db connection retrieved");
        }
        catch(\Exception $e)
        {
            $this->logger->info("Exception occured while getting Master DB Connection:::".$e->getMessage());
        }
        return $connection;
    }

    public function getSalesConnection()
    {
        $connection = null;
        try
        {
            $this->logger->info("Getting SalesDBConnection from Connection");
            /** @var Magento\Framework\App\ResourceConnection $this->resourceConnection **/
            $connection = $this->resourceConnection->getConnection('default');
            $this->logger->info("Sales db connection retrieved");
        }
        catch(\Exception $e)
        {
            $this->logger->info("Exception occured while getting Sales DB Connection:::".$e->getMessage());
        }
        return $connection;
    }
}