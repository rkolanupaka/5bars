<?php

namespace Custom\CustomerRegistrationWebService\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface ProjectContactSearchResultInterface
{
    /**
     * Get attributes list.
     *
     * @return \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactInterface[]
     */
    public function getItems();
    
    /**
     * Set attributes list.
     *
     * @param \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactInterface[] $items
     * @return $this
     */
    public function setItems(array $items);

}
