import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ContactUsComponent } from '../../components/contact-us/contact-us.component';
import { ContactUsRouterModule } from './contact-us.module.routing';

@NgModule({
    imports: [SharedModule, ContactUsRouterModule],
    declarations: [ContactUsComponent],
    exports: [ContactUsComponent]
})

export class ContactUsModule {
    constructor (){}
}