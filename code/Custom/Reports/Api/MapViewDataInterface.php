<?php

namespace Custom\Reports\Api;

use Magento\Catalog\Model\ResourceModel\Layer\Filter\Decimal;

interface MapViewDataInterface{
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSku();
	
	/**
	 * @api
	 * @param string $sku
	 * @return void
	 */
	public function setSku($sku);
	
	
	/**
	 * @api
	 * @return bool|null
	 */
	public function getRes();
	
	/**
	 * @api
	 * @param bool $res
	 * @return void
	 */
	public function setRes($res);
	
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLat();
	
	/**
	 * @api
	 * @param float $lat
	 * @return void
	 */
	public function setLat($lat);
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLong();
	
	/**
	 * @api
	 * @param float $long
	 * @return void
	 */
	public function setLong($long);
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getStat();
	
	/**
	 * @api
	 * @param string $stat
	 * @return void
	 */
	public function setStat($stat);
	
	/**
	 * @api
	 * @return bool|null
	 */
	public function getSChk();
	
	/**
	 * @api
	 * @param bool $sChk
	 * @return void
	 */
	public function setSChk($sChk);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getPCls();
	
	/**
	 * @api
	 * @param string $pCls
	 * @return void
	 */
	public function setPCls($pCls);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getLType();
	
	/**
	 * @api
	 * @param string $lType
	 * @return void
	 */
	public function setLType($lType);
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRlFee();
	
	/**
	 * @api
	 * @param string $rlFee
	 * @return void
	 */
	public function setRlFee($rlFee);
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSsFee();
	
	/**
	 * @api
	 * @param string $ssFee
	 * @return void
	 */
	public function setSsFee($ssFee);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSslFee();
	
	/**
	 * @api
	 * @param string $sslFee
	 * @return void
	 */
	public function setSslFee($sslFee);
	
}