<?php

namespace Custom\DataSync\Api\Data;

/**
 * Interface ReservedProductsInterface.php
 * @api
 */
interface ReservedProductsInterface
{
    public function getReservedProducts($connection, $transaction, $timeStamp);
}