<?php
namespace Custom\SiteManagement\Model;

class Cities extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,
\Custom\SiteManagement\Api\Data\CitiesInterface
{
	const CACHE_TAG = 'cities';
	
	protected $_cacheTag = 'cities';
	
	protected $_eventPrefix = 'cities';
	
	protected function _construct()
	{
		$this->_init('Custom\SiteManagement\Model\ResourceModel\Cities');
	}
	
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	
	public function getDefaultValues()
	{
		$values = [];
		
		return $values;
	}
	
}