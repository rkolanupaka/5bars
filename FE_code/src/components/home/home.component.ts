import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(
    private router: Router
  ) { }

  navigateToReserve(type) {
    if(type == 'input') {
      this.router.navigate(['site/site-search'], { queryParams: {type:"findSiteByInput"}});
    } else {
      this.router.navigate(['site/site-search'],{ queryParams: {type:"uploadSites"}});
    }
  }
}
