<?php

namespace Custom\Reports\Model;

class CronJobForCache{
	
	/**
	 * @var \Custom\Reports\Api\ReportsInterface
	 */
	protected $repositoryInterface;
	
	
	/**
	 * @var \Custom\SiteManagement\Api\SiteManagementInterface
	 */
	protected $siteManagementInterface;
	
	/**
	 * @param \Custom\Reports\Api\ReportsInterface $repositoryInterface
	 * @param \Custom\SiteManagement\Api\SiteManagementInterface $siteManagementInterface
	 */
	public function __construct(
			\Custom\Reports\Api\ReportsInterface $repositoryInterface,
			\Custom\SiteManagement\Api\SiteManagementInterface $siteManagementInterface
			){
		$this->repositoryInterface = $repositoryInterface;
		$this->siteManagementInterface = $siteManagementInterface;
	}
	/**
	 * Execute Cronjob
	 *
	 * @return void
	 */
	public function execute(){
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$logger = $objectManager->get('\Psr\Log\LoggerInterface');
		$logger->info('CronJobForCache | execute | Started');
		$stateCitiesData = $this->siteManagementInterface->getStateCities();
		$stateCities = $stateCitiesData->getItems();
		$state = null;
		foreach ($stateCities as $stateCity){
			$state = $stateCity->getState();
			$cities = $stateCity->getCities();
			foreach ($cities as $city){
				$reports = $this->repositoryInterface->getSiteLocationDataforMaps($state,$city);
				$fileName = $state."_".$city.".txt";
				$handle = fopen("/var/www/html/assets/search-data/".$fileName, 'w');
				$dataArray = array();
				foreach ($reports as $report){
					$dataArray[] = $report;
				}
				$data = json_encode($dataArray);
				fwrite($handle,$data);
				fclose($handle);
			}
		}
		$logger->info('CronJobForCache | execute | Ended');
	}
	
}