<?php
namespace Custom\DataSync\Api\Data;

/**
 * Interface ConnectionInterface.php
 * @api
 */
interface ConnectionInterface
{
    /**
     * Get Sales Connection.
     *
     * @return string|null
     */
    public function getSalesConnection();
    
    /**
     * Get Master DB Connection.
     *
     * @return string|null
     */
    public function getMasterDBConnection();
    
}
