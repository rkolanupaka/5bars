<?php
namespace Custom\SiteManagement\Model\ResourceModel;




class States extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * constructor
     * 
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('states', 'state_code');
    }

}