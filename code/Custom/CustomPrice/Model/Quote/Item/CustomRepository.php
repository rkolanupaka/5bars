<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Custom\CustomPrice\Model\Quote\Item;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;

class CustomRepository extends \Magento\Quote\Model\Quote\Item\Repository
{
	
	protected $logger;
	protected $_session;
	
	/**
	 * Quote repository.
	 *
	 * @var \Magento\Quote\Api\CartRepositoryInterface
	 */
	protected $quoteRepository;
	
	/**
	 * @var \Magento\Catalog\Model\ProductRepository
	 */
	protected $_productRepository;
	
	
	/**
	 * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
	 * @param \Magento\Customer\Model\Session $session
	 * @param \Psr\Log\LoggerInterface $loggerInterface
	 * @param \Magento\Catalog\Model\ProductRepository $_productRepository
	 */
	public function __construct(
			\Magento\Customer\Model\Session $session,
			\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
			\Magento\Catalog\Model\ProductRepository $_productRepository,
			\Psr\Log\LoggerInterface $loggerInterface
			) {
				$this->logger = $loggerInterface;
				$this->quoteRepository = $quoteRepository;
				$this->_session = $session;
				$this->_productRepository = $_productRepository;
	}
	
	
    /** Custo logic to override price
     * {@inheritdoc}
     */
    public function save(\Magento\Quote\Api\Data\CartItemInterface $cartItem)
    {
        /** @var \Magento\Quote\Model\Quote $quote */
    	$this->logger->info("CustomReposirtory | Execute | Before");
        $cartId = $cartItem->getQuoteId();
        $this->logger->info("CustomReposirtory | Execute | After");
        $quote = $this->quoteRepository->getActive($cartId);

        $quoteItems = $quote->getItems();
        
        $customerId = $quote->getCustomer()->getId();
		
		/** custom logic */
		/* $price = 100; //set your price here
        $cartItem->setCustomPrice($price);
        $cartItem->setOriginalCustomPrice($price); */
		/** end of custom logic */
        
        $quoteItems[] = $cartItem;
		$this->logger->info("CUSTOM Size of Quote Items:".sizeof($quoteItems));
		
		//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		/* $sessionId = $this->_session->getSessionId();
		$this->logger->info("Session ID : ".$sessionId);
		$customer = $this->_session->getCustomer();
		$customerId = $customer->getId();
		$this->logger->info("Customer Logged IN : ".$this->_session->isLoggedIn()); */
		$this->logger->info("Customer ID : ".$customerId);
		$this->logger->info("CUSTOM Size of Quote Items:".sizeof($quoteItems));
		for($j=0; $j<sizeof($quoteItems); $j++){
			$sku = $quoteItems[$j]->getSku();
			$this->logger->info("CUSTOM - SKU in PLUGIN:".$sku);
			$product = $this->_productRepository->get($sku);
			$siteRequested = $product->getCustomAttribute('site_requested');
			$siteAssignedTo = $product->getCustomAttribute('site_assigned_to');
			if($siteRequested && $siteAssignedTo){
				$siteReqValue = $siteRequested->getValue();
				$siteAssignedToValue = $siteAssignedTo->getValue();
				$this->logger->info("CUSTOM - SiteRequested Flag in Plugin:".$siteReqValue);
				$this->logger->info("CUSTOM - SiteAssignedTo Flag in Plugin:".$siteAssignedToValue);
				if(($siteReqValue==1) && ($siteAssignedToValue!=$customerId)){
					throw new LocalizedException(__(" Product ".$sku." is Not Available"));
				}
			}
			else{
				$siteReqValue = 0;
				$siteAssignedToValue = 0;
			}
		}
		
        $quote->setItems($quoteItems);
        $this->logger->info("Custom - Before Save Setting Items");
        $this->quoteRepository->save($quote);
        $this->logger->info("Custom - After Save");
        $quote->collectTotals();
        return $quote->getLastAddedItem();
    }

}
