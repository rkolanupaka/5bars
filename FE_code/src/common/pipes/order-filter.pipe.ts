import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "orderFilter"
})
export class OrderFilterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2): any {
    let filter = arg1;
    let filter2 = arg2;
    if (filter2.length == 0) {
      return value;
    } else {
      filter2.forEach(element => {
        if (element.name == "Order Number") {
          value = value.filter(orderItem => orderItem.increment_id.toLocaleLowerCase().indexOf(element.value) != -1);
        }
        if (element.name == "Site ID") {
          let tempVal = element.value;
          value = value.filter(orderItem => {
            let tempCheck: boolean = false;
            orderItem.items.forEach(element => {
              if (element.sku.toLocaleLowerCase().indexOf(tempVal) != -1) {
                tempCheck = true;
              }
            });
            return tempCheck;
          });
        }
      });
      return value;
    }
  }
}

@Pipe({
  name: "cityFilter"
})
export class CityFilterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2): any {
    let filter = "";
    let filter2 = arg2;
    console.log(filter);
    console.log(filter2);
    if (typeof arg1 == "object") {
      filter = arg1.city.toLocaleLowerCase();
    } else {
      filter = arg1.toLocaleLowerCase();
    }
    if (filter == "" && filter2 == "") {
      return value;
    } else {
      value = value.filter(cityItem => cityItem.city.toLocaleLowerCase().indexOf(filter) != -1);
      return value.filter(cityItem => cityItem.cityStatus.toLocaleLowerCase().indexOf(filter2) != -1);
    }
  }
}

@Pipe({
  name: "filterCounter"
})
export class filterCounterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2: any): any {
    let counter = arg1;
    counter.count = value.length;
    if (counter.count == 0) {
      counter.start = 0;
      counter.end = 0;
    } else if (counter.end > counter.count) {
      counter.start = 1;
      counter.end = counter.count;
    } else {
      counter.start = 1;
      counter.end = 1 + arg2 - 1;
    }
    return value;
  }
}

@Pipe({
  name: "searchFilter"
})
export class searchFilterPipe implements PipeTransform {
  transform(value: any, arg1: any, arg2: any): any {
    let filter = arg1.toLocaleLowerCase();
    let filter2 = arg2.toLocaleLowerCase();
    if (filter == "" &&filter2 == "") {
      return value;
    } else {
      value = value.filter(item => item.sku.toLocaleLowerCase().indexOf(filter) != -1);
      return value.filter(item => item.stat.toLocaleLowerCase().indexOf(filter2) != -1);
    }
  }
}

// @Pipe({
//     name: 'infiniteScrollSize'
// })
// export class infiniteScrollSizePipe implements PipeTransform {
//     transform(value: any, arg1: any): any {
//         let filter = arg1;
//         console.log(arg1);
//         return value;
//         if(filter == "") {
//             return value;
//         } else {
//                 return value.filter(item=>item.uniqueId.toLocaleLowerCase().indexOf(filter) != -1)
//         }
//     }
// }
