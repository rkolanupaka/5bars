<?php



namespace Custom\CustomPrice\Api;

/**
 * Defines Sevice Contract for Customer class
 */
interface CartInterface
{
    
    /**
     * @param int $surveyLeaseFlag
     * @param int $hasConduitRequested
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem $cartItem
     * @return \Magento\Quote\Api\Data\CartItemInterface
     */
	public function updateCart($surveyLeaseFlag,$hasConduitRequested,$cartItem);
    
    /**
     * Add/update the specified cart item.
     *
     * @param string $quoteId
     * @param \Custom\CustomPrice\Api\Data\CartItemRequestInterface[] $items The item.
     * @return \Custom\CustomPrice\Api\Data\CartItemsResultInterface Items.
     */
    public function save($quoteId,$items);
    
    
    /**
     * Returns information of the cart for a specified customer.
     *
     * @param int $customerId The customer ID.
     * @return \Magento\Quote\Api\Data\CartInterface Cart object.
     */
    public function getCart($customerId);
    
}