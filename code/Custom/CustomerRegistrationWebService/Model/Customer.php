<?php 


namespace Custom\CustomerRegistrationWebService\Model;
use Custom\CustomerRegistrationWebService\Api\CustomerInterface;
use Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface;
use Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface;
use Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Webapi\Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random;

/**
 * Defines the implementation class of the Customer service contract.
 */
class Customer extends \Magento\Framework\App\Action\Action implements CustomerInterface
{
    
     /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    
    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $addressFactory;
    
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagementInterface;
    
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepositoryInterface;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Model\PONumberFactory
     */
    protected $poNumberFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Model\ProjectContactFactory
     */
    protected $projectContactFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterfaceFactory
     */
    protected $poNumberResultFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterfaceFactory
     */
    protected $projectContactResultFactory;
    
    /**
     * @var \Magento\Integration\Api\CustomerTokenServiceInterface
     */
    protected $customerTokenServiceInterface;
    
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    
    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagementInterface;
    
    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterfaceFactory
     */
    protected $setUpProfileResultFactory;
    
    /**
     * 
     * @var \DateTime
     */
    protected $date;
    
    /**
     * 
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timeZone;
    
    /**
     * 
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $requestIterface;
    
    
    /**
     *
     * @var \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterfaceFactory
     */
    protected $userProfileQuestinnaireFactory;
    
    /**
     *
     * @var \Custom\CustomerRegistrationWebService\Model\SecurityCheckUserMappingFactory
     */
    protected $securityCheckUserMappingFactory;
    
    protected $jsonHelper;
    
    /**
     *
     * @var \Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterfaceFactory
     */
    protected $securityQuestionsDataInterfaceFactory;
    
    /**
     * @var Random
     */
    private $mathRandom;
    
    /**
     *
     * @var \Custom\CustomerRegistrationWebService\Api\ValidationResponseInterfaceFactory
     */
    protected $validationResponseInterface;
    
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
    
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;
    
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagementInterface
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface
     * @param \Custom\CustomerRegistrationWebService\Model\PONumberFactory $poNumberFactory
     * @param \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterfaceFactory $poNumberResultFactory
     * @param \Custom\CustomerRegistrationWebService\Model\ProjectContactFactory $projectContactFactory
     * @param \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterfaceFactory $projectContactResultFactory
     * @param \Magento\Integration\Api\CustomerTokenServiceInterface
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagementInterface
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterfaceFactory $setUpProfileResultFactory
     * @param \DateTime $date;
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone
     * @param \Magento\Framework\App\RequestInterface $requestIterface
     * @param \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterfaceFactory $userProfileQuestinnaireFactory
     * @param \Custom\CustomerRegistrationWebService\Model\SecurityCheckUserMappingFactory $securityCheckUserMappingFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterfaceFactory $securityQuestionsDataInterfaceFactory
     * @param Random $mathRandom
     * @param \Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Custom\CustomerRegistrationWebService\Api\ValidationResponseInterfaceFactory $validationResponseInterface
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
                                \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
                                \Magento\Customer\Model\AddressFactory $addressFactory,
    							\Magento\Customer\Api\AccountManagementInterface $accountManagementInterface,
    							\Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
    							\Custom\CustomerRegistrationWebService\Model\PONumberFactory $poNumberFactory,
    							\Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterfaceFactory $poNumberResultFactory,
    							\Custom\CustomerRegistrationWebService\Model\ProjectContactFactory $projectContactFactory,
    							\Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterfaceFactory $projectContactResultFactory,
    							\Magento\Integration\Api\CustomerTokenServiceInterface $customerTokenServiceInterface,
    							\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
    							\Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
    							\Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory,
    							\Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterfaceFactory $setUpProfileResultFactory,
    							\DateTime $date,
    							\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
					    		\Magento\Framework\App\Request\Http $httpRequest,
    							\Magento\Framework\App\RequestInterface $requestIterface,
    							\Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterfaceFactory $userProfileQuestinnaireFactory,
    							\Custom\CustomerRegistrationWebService\Model\SecurityCheckUserMappingFactory $securityCheckUserMappingFactory,
    							\Magento\Framework\Json\Helper\Data $jsonHelper,
    							\Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterfaceFactory $securityQuestionsDataInterfaceFactory,
    							Random $mathRandom,
    							\Magento\Framework\Mail\Template\TransportBuilder $_transportBuilder,
    							\Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
    							\Custom\CustomerRegistrationWebService\Api\ValidationResponseInterfaceFactory $validationResponseInterface
    							) {
        $this->storeManager     = $storeManager;
        $this->customerRepositoryInterface     = $customerRepositoryInterface;
        $this->customerRepository = $customerRepositoryFactory->create();
        $this->addressFactory = $addressFactory->create();
        $this->accountManagementInterface= $accountManagementInterface;
        $this->orderRepositoryInterface = $orderRepositoryInterface;
        $this->poNumberFactory = $poNumberFactory;
        $this->poNumberResultFactory = $poNumberResultFactory;
        $this->projectContactFactory = $projectContactFactory;
        $this->productContactResultFactory = $projectContactResultFactory;
        $this->customerTokenServiceInterface = $customerTokenServiceInterface;
        $this->quoteRepository = $quoteRepository;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->setUpProfileResultFactory = $setUpProfileResultFactory;
        $this->date = $date;
        $this->timeZone = $timeZone;
        $this->requestIterface = $requestIterface;
        $this->userProfileQuestinnaireFactory = $userProfileQuestinnaireFactory;
        $this->securityCheckUserMappingFactory = $securityCheckUserMappingFactory;
        $this->jsonHelper = $jsonHelper;
        $this->securityQuestionsDataInterfaceFactory = $securityQuestionsDataInterfaceFactory;
        $this->mathRandom = $mathRandom;
        $this->_transportBuilder = $_transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->validationResponseInterface = $validationResponseInterface;
        parent::__construct($context);
    }
    
    /**
     * Reset customer password.
     *
     * @api
     * @param int $customerId
     * @param string $resetToken
     * @param string $newPassword
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function resetPassword($customerId, $resetToken, $newPassword){
    	try {
    		$customer = $this->customerRepository->getById($customerId);
    	} catch (NoSuchEntityException $e) {
    		throw $e;
    	}
    	$customerEmail = $customer->getEmail();
    	$customerName = $customer->getFirstname()." ".$customer->getLastname();
    	$flag = null;
    	$flag =  $this->accountManagementInterface->resetPassword($customerEmail, $resetToken, $newPassword);
    	if($flag == true){
    		$this->resetSuccessMail($customerEmail, $customerName);
    	}
    	return $flag;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getOrderDetails()
     */
   public function getOrderDetails($id){
    	try{
            $logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	       
    		$orderDetails = $this->orderRepositoryInterface->get($id);
            $logger->info('***** getCreatedAtFormatted *****'.$orderDetails->getCreatedAt());
            $orderDetails->setCreatedAt($this->getCreatedAtFormatted(1,$orderDetails));
            $logger->info('***** from order details *****'.$orderDetails->getCreatedAt());
            return $orderDetails;
    	}catch(InputException $e){
    		throw $e;
    	}catch(NoSuchEntityException $e){
    		throw $e;
    	}
    	
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getPONumberByCustomerId()
     */
    public function getPONumberByCustomerId($id)
    {
    	if (!$id) {
    		throw new InputException(__('Customer Id required'));
    	}
    	$items = array();
    	$poNumberResult = $this->poNumberResultFactory->create();
    	$poNumber = $this->poNumberFactory->create();
    	$poNumbers = $poNumber->getCollection()->addFieldToFilter('customer_id',array('eq'=> $id));
    	foreach ($poNumbers as $item){
    		$items[] = $item->getData();
    	}
    	$poNumberResult->setItems($items);
    	return $poNumberResult;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::savePONumber()
     */
    public function savePONumber($customerId,$ponumber,$comment,$poId=null)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	$logger->info('PO ID : '.$poId);
    	$poNumber = $this->poNumberFactory->create();
    	if(null != $poId){
    		$poNumber->load($poId);
    	}
    	try{
    		$poNumber->setData('customer_id',$customerId)
    		->setData('po_number',$ponumber)
    		->setData('comment',$comment)
    		->save();
    		$poId = $poNumber->getData('po_id');
    		$logger->info('PO Number ID -'.$poId);
    		return $poId;
    	}catch (\Exception $e) {
    		throw new CouldNotSaveException(__('Problem While saving PO Number'));
    	}
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getProjectContactByCustomerId()
     */
    public function getProjectContactByCustomerId($id)
    {
    	if (!$id) {
    		throw new InputException(__('Customer Id required'));
    	}
    	$items = array();
    	$projectContactResult = $this->productContactResultFactory->create();
    	$projectContact = $this->projectContactFactory->create();
    	$projectContacts = $projectContact->getCollection()->addFieldToFilter('customer_id',array('eq'=> $id));
    	foreach ($projectContacts as $item){
    		$items[] = $item->getData();
    	}
    	$projectContactResult->setItems($items);
    	return $projectContactResult;
    }
    
   /**
    * 
    * {@inheritDoc}
    * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::saveProjectContact()
    */
    public function saveProjectContact($customerId,$name,$email,$phone,$company,$contactId=null)
    {
    	$projectContact = $this->projectContactFactory->create();
    	if(null != $contactId){
    		$projectContact->load($contactId);
    	}
    	try{
    		$projectContact->setData('customer_id',$customerId)
    		->setData('name',$name)
    		->setData('email',$email)
    		->setData('phone',$phone)
    		->setData('company',$company)
    		->save();
    		return $projectContact->getData('contact_id');
    	}catch (\Exception $e) {
    		throw new CouldNotSaveException(__('Problem While saving Project Contact'));
    	}
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::revokeCustomerAccessToken()
     */
    public function revokeCustomerAccessToken($id)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	try{
    		$this->deleteCart($id);
    		return $this->customerTokenServiceInterface->revokeCustomerAccessToken($id);
    	}catch(LocalizedException $e){
    		$logger->info($e->getMessage());
    	}
    	
    }
    
    /**
     * This method will delete active cart for specified customer ID
     * 
     * @param int $customerId
     * @throws \Exception
     */
    public function deleteCart($customerId)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	
    	try{
    		$quote = $this->quoteRepository->getActiveForCustomer($customerId);
    		$quote->delete();
    	}catch(\Exception $e){
    		$logger->info($e->getMessage());
    	}
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::createCustomerAccessToken()
     */
    public function createCustomerAccessToken($username, $password)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	try{
    		$token = $this->customerTokenServiceInterface->createCustomerAccessToken($username, $password);
    		$customerId = $this->customerRepository->get($username)->getId();
    		$logger->info('Customer Id ********'. $customerId);
    		$this->deleteCart($customerId);
    		return $token;
    	}catch(AuthenticationException $e){
			$logger->info("CustomerRegistrationWebService | createCustomerAccessToken | In AuthenticationException block :::: ".$e->getMessage());
    		throw $e;
    	}catch(NoSuchEntityException $e){
			$logger->info("CustomerRegistrationWebService | createCustomerAccessToken | In NoSuchEntityException block :::: ".$e->getMessage());
    		throw $e;
    	}catch(LocalizedException $e){
			$logger->info("CustomerRegistrationWebService | createCustomerAccessToken | In LocalizedException block :::: ".$e->getMessage());
    		throw $e;
    	}
    	
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::deletePONumber()
     */
    public function deletePONumber($id)
    {
    	$success = true;
    	try{
    		$poNumber = $this->poNumberFactory->create();
    		$poNumber->load($id);
    		$poNumber->delete();
    	}catch(\Exception $e){
    		$success = false;
    	}
    	return $success;
    }
    
    public function deleteProjectContact($id)
    {
    	$success = true;
    	try{
    		$projectContact = $this->projectContactFactory->create();
    		$projectContact->load($id);
    		$projectContact->delete();
    	}catch(\Exception $e){
    		$success = false;
    	}
    	return $success;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getSetUpProfileDetails()
     */
    public function getSetUpProfileDetails($id)
    {
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
    	// for get current time according to time zone
    	$time = $this->timeZone->scopeTimeStamp();
    	
    	// you can use \Zend_Date to work with date like this :
    	$date = new \Zend_Date($time, \Zend_Date::TIMESTAMP);
    	$logger->info('Date -----'.$date);
    	$logger->info('Time-------'.$time);
    	$setUpProfileResult = $this->setUpProfileResultFactory->create();
    	try{
    		$setUpProfileResult->setPoNumber($this->getPONumberByCustomerId($id));
    		$setUpProfileResult->setProjectContact($this->getProjectContactByCustomerId($id));
    	}catch(\Magento\Framework\Exception\InputException $e){
    		$logger->info($e);
    	}
    	return $setUpProfileResult;
    }
    
    /**
    * Override method. //TODO: Clean Up
    */
    public function execute()
    {
        
    }
    
       /**
     * Get formatted order created date in store timezone
     *
     * @param   string $format date format type (short|medium|long|full)
     * @return  string
     */
    public function getCreatedAtFormatted($format,$orderDetails)
    {
        return $this->timeZone->formatDateTime(
            new \DateTime($orderDetails->getCreatedAt()),
            $format,
            $format,
            null,
            $this->timeZone->getConfigTimezone('store', $this->storeManager->getStore())
        );
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::searchProducts()
     */
    public function searchProducts(){
    	$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		$files = $this->requestIterface->getFiles();
		$logger->info('Files received ----'.$files->toString());
		$logger->info('Files received ----'.$files->get('testSearch.txt'));
    }
    
    /**
     * @param int $customerId
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function userProfileQuestionnaire($customerId){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$securityQuestions = $resource->getTableName('security_questions');
    	$securityCheckMapping = $resource->getTableName('security_check_user_mapping');
    	
    	$sql = "select * from ".$securityQuestions." ORDER BY set_id";
    	$questionsArray = $connection->query($sql)->fetchAll();
    	$securityQ = array();
    	$set1 = array();
    	$set2 = array();
    	$set3 = array();
    	$questionsSetGroup = array();
    	$count = 0;
    	if($questionsArray){
    		for($j=0; $j<sizeof($questionsArray); $j++){
    			$question = new \Custom\CustomerRegistrationWebService\Api\SecurityQuestions();
    			$question->setId($questionsArray[$j]['set_id']);
    			$question->setQId($questionsArray[$j]['q_id']);
    			$question->setQuestion($questionsArray[$j]['question']);
    			if($questionsArray[$j]['set_id'] == 1){
    				$set1[] = $question;
    			}
    			elseif ($questionsArray[$j]['set_id'] == 2){
    				$set2[] = $question;
    			}
    			elseif ($questionsArray[$j]['set_id'] == 3){
    				$set3[] = $question;
    			}
    		}
    	}
    	
    	$userProfileQuestionnaire = $this->userProfileQuestinnaireFactory->create();
    	$securityQuestionsData = $this->securityQuestionsDataInterfaceFactory->create();
    	$securityQuestionsData->setSet1($set1);
    	$securityQuestionsData->setSet2($set2);
    	$securityQuestionsData->setSet3($set3);
    	
    	$userProfileQuestionnaire->setQuestions($securityQuestionsData);
    	
    	$sql2 = "select sc.id,sc.user_id,sq.q_id,sq.set_id,sc.answer 
				 from ".$securityCheckMapping." sc join ".$securityQuestions." sq on 
				 sc.question_id=sq.q_id where sc.user_id='".$customerId."'";
    	$answersArray = $connection->query($sql2)->fetchAll();
    	$answers = array();
    	if($answersArray){
    		foreach ($answersArray as $answer){
    			$ans = new \Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMapping();
    			$ans->setId($answer['id']);
    			$ans->setQuestionId($answer['q_id']);
    			$ans->setSetId($answer['set_id']);
    			$ans->setUserId($answer['user_id']);
    			$ans->setAnswer($answer['answer']);
    			$answers[] = $ans->jsonSerialize();
    		}
    	}
    	
    	$userProfileQuestionnaire->setPreferences($answers);
    	return $userProfileQuestionnaire;
    }
    
    /**
     * To Save the Security Questions Submitted by Customer
     *
     * @param \Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[] $answers
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function saveProfileQuestionnaire($answers){
    	
    		$userId = null;
    		foreach ($answers as $answer){
    			$userId = $answer->getUserId();
    			try{
    				$securityCheckUserMappingFactory = $this->securityCheckUserMappingFactory->create();
    				$securityCheckUserMappingFactory->setData('user_id',$answer->getUserId())
    				->setData('question_id',$answer->getQuestionId())
    				->setData('answer',$answer->getAnswer())
    				->save();
    			}
    			catch (\Exception $e){
    				throw new CouldNotSaveException(__('Problem occured during saving Questions'));
    			}
    		}
    		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    		$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($userId);
    		$customer->setCustomAttribute('security_info_updated',1);
    		$objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->save($customer);
    		return $this->userProfileQuestionnaire($userId);
    }
    
    
    /**
     * To Update the Security Questions Submitted by Customer
     *
     * @param \Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface[] $answers
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function updateProfileQuestionnaire($answers){
    	
    	$userId = null;
    	foreach ($answers as $answer){
    		$userId = $answer->getUserId();
    		try{
    			$securityCheckUserMappingFactory = $this->securityCheckUserMappingFactory->create();
    			$securityCheckUserMappingFactory->load($answer->getId());
    			$securityCheckUserMappingFactory->setData('question_id',$answer->getQuestionId())
    			->setData('answer',$answer->getAnswer())
    			->save();
    		}
    		catch (\Exception $e){
    			throw new CouldNotSaveException(__('Problem occured during Updating Questions'));
    		}
    	}
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($userId);
    	$customer->setCustomAttribute('security_info_updated',1);
    	$objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->save($customer);
    	return $this->userProfileQuestionnaire($userId);
    }
    
    /**
     * @param string $emailId
     * @return bool
     */
    public function customerValidation($emailId){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$logger = $objectManager->get('\Psr\Log\LoggerInterface');
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$customerEntity = $resource->getTableName('customer_entity');
    	$customerAddressEntity = $resource->getTableName('customer_address_entity');
    	$sql = "SELECT entity_id FROM ".$customerEntity." where email='".$emailId."'";
    	$entityId = $connection->query($sql)->fetchColumn(0);
    	$logger->info("ENTITY ID: ".$entityId);
    	if($entityId){
    		return true;
    	}
    	else{
    		throw new NoSuchEntityException(__('Not a Registered Customer'));
    	}
    }
    
    /**
     *
     * @param string $emailId
     * @param string $fName
     * @param string $lName
     * @param string $postCode
     * @param mixed $questions
     * @return \Custom\CustomerRegistrationWebService\Api\ValidationResponseInterface
     */
    public function customerInfoValidation($emailId,$fName,$lName,$postCode,$questions){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$logger = $objectManager->get('\Psr\Log\LoggerInterface');
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$customerEntity = $resource->getTableName('customer_entity');
    	$customerAddressEntity = $resource->getTableName('customer_address_entity');
    	$securityCheckUserMapping = $resource->getTableName('security_check_user_mapping');
    	$sql = "select ce.entity_id,ce.firstname,ce.lastname,ce.email,cde.postcode,sm.question_id ,sm.answer from
				 ".$customerEntity." ce join ".$customerAddressEntity." cde on cde.parent_id=ce.entity_id
				join ".$securityCheckUserMapping." sm on sm.user_id=ce.entity_id
				WHERE ce.email='".$emailId."' ORDER BY sm.question_id ASC";
    	$customerInfo = $connection->query($sql)->fetchAll();
    	$check = 0;
    	if($customerInfo){
    		for($j=0; $j<sizeof($customerInfo); $j++){
    			if((strcasecmp($customerInfo[$j]['firstname'],$fName)==0)){
    				if((strcasecmp($customerInfo[$j]['lastname'],$lName)==0)){
    					if((strcasecmp($customerInfo[$j]['postcode'],$postCode)==0)){
    						if((strcasecmp($customerInfo[$j]['question_id'],$questions[$j]['id'])==0)){
    							if((strcasecmp($customerInfo[$j]['answer'],$questions[$j]['answer'])==0)){
    								$check = 1;
    							}
    							else{
    								$check = 0;
    								break;
    							}
    						}
    						else{
    							$check = 0;
    							break;
    						}
    					}
    					else{
    						$check = 0;
    						break;
    					}
    				}
    				else{
    					$check = 0;
    					break;
    				}
    			}
    			else{
    				$check = 0;
    				break;
    			}
    		}
    	}
    	if($check == 1){
    		$customer = $this->customerRepository->get($emailId);
    		$newPasswordToken = $this->mathRandom->getUniqueHash();
    		$test = $objectManager->get('\Magento\Customer\Model\AccountManagement')->changeResetPasswordLinkToken($customer, $newPasswordToken);
    		$validationResponse = $this->validationResponseInterface->create();
    		$validationResponse->setValid(true);
    		$validationResponse->setCustomerId($customerInfo[0]['entity_id']);
    		$validationResponse->setToken($newPasswordToken);
    		return $validationResponse;
    	}
    	else{
    		throw new NoSuchEntityException(__('Entered input is Invalid'));
    	}
    }
    
    /**
     * @param string $customerEmail
     *
     * @return \Custom\CustomerRegistrationWebService\Api\UserProfileQuestionnaireInterface
     */
    public function securityQuestionsInfo($customerEmail){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$securityQuestions = $resource->getTableName('security_questions');
    	$securityCheckMapping = $resource->getTableName('security_check_user_mapping');
    	$customer = $this->customerRepository->get($customerEmail);
    	$customerId = $customer->getId();
    	$sql = "select sq.q_id,sq.set_id,sq.question from ".$securityQuestions." sq join ".$securityCheckMapping."
				 sc on sq.q_id=sc.question_id where sc.user_id='".$customerId."' ORDER BY sq.set_id";
    	$questionsArray = $connection->query($sql)->fetchAll();
    	$securityQ = array();
    	$set1 = array();
    	$set2 = array();
    	$set3 = array();
    	$questionsSetGroup = array();
    	$count = 0;
    	if($questionsArray){
    		for($j=0; $j<sizeof($questionsArray); $j++){
    			$question = new \Custom\CustomerRegistrationWebService\Api\SecurityQuestions();
    			$question->setId($questionsArray[$j]['set_id']);
    			$question->setQId($questionsArray[$j]['q_id']);
    			$question->setQuestion($questionsArray[$j]['question']);
    			if($questionsArray[$j]['set_id'] == 1){
    				$set1[] = $question;
    			}
    			elseif ($questionsArray[$j]['set_id'] == 2){
    				$set2[] = $question;
    			}
    			elseif ($questionsArray[$j]['set_id'] == 3){
    				$set3[] = $question;
    			}
    		}
    	}
    	else{
    		throw new NoSuchEntityException(__('No Security Questions answered w.r.t. this User EmailID'));
    	}
    	
    	$userProfileQuestionnaire = $this->userProfileQuestinnaireFactory->create();
    	$securityQuestionsData = $this->securityQuestionsDataInterfaceFactory->create();
    	$securityQuestionsData->setSet1($set1);
    	$securityQuestionsData->setSet2($set2);
    	$securityQuestionsData->setSet3($set3);
    	
    	$userProfileQuestionnaire->setQuestions($securityQuestionsData);
    	return $userProfileQuestionnaire;
    }
    
    public function resetSuccessMail($customerEmail,$customerName){
    	$this->inlineTranslation->suspend();
    	$store = $this->storeManager->getStore()->getId();
    	$transport = $this->_transportBuilder->setTemplateIdentifier('reset_password_success_email')
    	->setTemplateOptions(['area' => 'frontend', 'store' => $store])
    	->setTemplateVars(['customerName' =>$customerName])
    	/* ->setTemplateVars(
    	 [
    	 'store' => $this->_storeManager->getStore(),
    	 ]
    	 ) */
    	->setFrom('general')
    	// you can config general email address in Store -> Configuration -> General -> Store Email Addresses
    	->addTo($customerEmail, 'Five Bars')
    	->getTransport();
    	$transport->sendMessage();
    }
    
    /**
     *
     * @return \Magento\Framework\App\ResourceConnection
     */
    public function getResourceConnection(){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\ResourceConnection');
    }
}