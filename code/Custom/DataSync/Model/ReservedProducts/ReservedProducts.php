<?php

namespace Custom\DataSync\Model\ReservedProducts;

use Custom\DataSync\Api\Data\ReservedProductsInterface;

class ReservedProducts extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,ReservedProductsInterface
{
    const CACHE_TAG = 'impex_batch';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
        )
    {
        $this->logger = $logger;
    }
    
    protected function _construct()
    {
        $this->_init('\Custom\DataSync\Model\ResourceModel\ReservedProducts');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        
        return $values;
    }
    
    public function getProducts($connection, $timeStamp)
    {
        $this->logger->info("ReservedProducts | getProducts");
        $sql = "SELECT * FROM magento.reserved_products WHERE reserved_date > '".$timeStamp."'";
        $this->logger->info("ReservedProducts | getProducts | ::: ".$sql);
        return $connection->fetchAll($sql);
    }
    
    public function getReservedProducts($connection, $transaction, $timeStamp)
    {
        $this->logger->info("ReservedProducts | getReservedProducts");
        return $this->getProducts($connection, $timeStamp);
    }
}