<?php
namespace Custom\CustomOrder\Model;

use Magento\Store\Api\StoreResolverInterface;

/**
 * Service contract, which manage scopes
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CustomStoreManager extends \Magento\Store\Model\StoreManager
{
    /**
     * @param \Magento\Store\Api\StoreRepositoryInterface $storeRepository
     * @param \Magento\Store\Api\GroupRepositoryInterface $groupRepository
     * @param \Magento\Store\Api\WebsiteRepositoryInterface $websiteRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param StoreResolverInterface $storeResolver
     * @param \Magento\Framework\Cache\FrontendInterface $cache
     * @param bool $isSingleStoreAllowed
     */
    public function __construct(
        \Magento\Store\Api\StoreRepositoryInterface $storeRepository,
        \Magento\Store\Api\GroupRepositoryInterface $groupRepository,
        \Magento\Store\Api\WebsiteRepositoryInterface $websiteRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        StoreResolverInterface $storeResolver,
        \Magento\Framework\Cache\FrontendInterface $cache,
        $isSingleStoreAllowed = true
    ) {
        $this->storeRepository = $storeRepository;
        $this->websiteRepository = $websiteRepository;
        $this->groupRepository = $groupRepository;
        $this->scopeConfig = $scopeConfig;
        $this->storeResolver = $storeResolver;
        $this->cache = $cache;
        $this->isSingleStoreAllowed = $isSingleStoreAllowed;
    }

	/**
     * {@inheritdoc}
     */
    public function getStore($storeId = null)
    {
        return parent::getStore(0);
    }
    
}
