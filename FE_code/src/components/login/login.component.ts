import { Component, OnInit, ViewChild } from '@angular/core';
import {Router} from '@angular/router';

import { CookieService } from 'ngx-cookie';
import { DataService } from '../../common/services/data.service';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';

@Component({
  selector: 'site-selex-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  eyeOpen:boolean = false;
  frgtPswrdSuccessVisible:boolean=false;
  hostValue:string;
  isLoginVisible: boolean = true;
  isForgotPasswordVisible: boolean = false;
  isNewUserVisible: boolean = false;
  isNewUserFormVisible: boolean = false;
  isNewUserSuccessVisible: boolean = false;
  isNewUserErrorVisible: boolean = false;
  loginErrorTrue:boolean = false;
  loginErrorText:string = '';
  resetErrorTrue:boolean = false;
  showPopUpMessage:string;  
  showPopUpTrue:boolean = false;
  showLoader:boolean = false;
  // captcha:any='';
  // showCaptcha = false;
  invalidUserCount:number = 0;
  captchaVal;
  
  @ViewChild('loginPassword') loginPassword;

  constructor(private router:Router, private dataservice:DataService, private _cookieService:CookieService) { }
  
  ngOnInit() {
    localStorage.clear();
    this._cookieService.removeAll();
    this.hostValue = this.dataservice.getHostName();
  }

  resolved(event) {
    console.log(event);
    this.captchaVal = event;
    this.loginErrorTrue = false;
    // console.log(this.captcha);
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  showHidePassword() {
    this.eyeOpen = !this.eyeOpen;
    if(this.eyeOpen) {
      this.loginPassword.nativeElement.type = "text";
    } else {
      this.loginPassword.nativeElement.type = "password";
    }
  }

  showLogin(){
    this.isLoginVisible = true;
    this.isForgotPasswordVisible = false;
    this.isNewUserVisible = false;
  }

  // showNewUser(){
  //   this.isLoginVisible = false;
  //   this.isForgotPasswordVisible = false;
  //   this.isNewUserVisible = true;
  //   this.isNewUserFormVisible = true
  // }
  
  showforgotPassword(){
    // this.loginErrorTrue = false;
    // this.resetErrorTrue = false;
    // this.isLoginVisible = false;
    // this.isForgotPasswordVisible = true;
    // this.isNewUserVisible = false;
    // this.frgtPswrdSuccessVisible = false;
    this.router.navigate(['/forgotpassword']);
  }

  // onResetPasswordSubmit(forgotPaswrdForm){
  //   let headerDict = {
  //     "Content-Type": "application/json"
  //   }
  //   let userData = {
  //     "email": forgotPaswrdForm.value.email,
  //     "template": "email_reset",
  //     "websiteId": 1
  //   }
  //   let url = this.hostValue+'/magento/index.php/rest/V1/customers/password';
  //   this.showLoader = true;
  //   this.dataservice.fetchPutData(url, userData, headerDict).subscribe((data)=>{
  //     if(data) {
  //       this.frgtPswrdSuccessVisible=true;
  //     } else {
  //       this.resetErrorTrue=true;
  //     }
  //     this.showLoader = false;
  //   }, (err)=>{
  //     if(err.target && err.target.status === 0) {
  //       this.showPopUpMessage = "Server connection refused. Please try after some time.";
  //       this.showPopUpTrue = true;
  //     } else if(err.target && err.target.status === 401) {
  //       this.showPopUpMessage = "You are not authorized to access this.";
  //       this.showPopUpTrue = true;
  //     } else {
  //       this.showLoader = false;
  //       this.resetErrorTrue=true;
  //     }
  //   })
  // }

  onLoginSubmit(loginForm){
    this.loginErrorTrue = false;
    if(this.invalidUserCount<2 || this.captchaVal) {
      let headerDict = {
        'Content-Type': 'application/json'
      }
      let data = {
        "username": loginForm.value.email,
        "password": loginForm.value.password
      }
      this.showLoader = true;
      let url = this.hostValue+'/magento/index.php/rest/V1/integration/customer/login';
      this.dataservice.fetchPostData(url, data, headerDict).subscribe((data: any)=>{
        this.showLoader = false;
        this._cookieService.put('5barsToken', data);
        this.router.navigate(['site/home']);
      }, (err)=>{
        this.invalidUserCount++;
        if(err.target && err.target.status === 0) {
          this.showPopUpMessage = "Server connection refused. Please try after some time.";
          this.showPopUpTrue = true;
        } else if(err.target && err.target.status === 401) {
          this.showPopUpMessage = "You are not authorized to access this.";
          this.showPopUpTrue = true;
        } else {
          this.showLoader = false;
          this.loginErrorText = 'You did not sign in correctly or your account is temporarily disabled.';
          this.loginErrorTrue = true;
        }
      })
    } else {
      this.loginErrorText = 'Please solve the captcha to continue.';
      this.loginErrorTrue = true;
    }
  }

  // onNewUserFormSubmit(newUserForm){
  //   console.log(newUserForm.value)
  //   let headerDict = {
  //     'Content-Type': 'application/json'
  //   }
  //   let userData = {
  //     "email" : newUserForm.value.email,
  //     "firstName" : newUserForm.value.firstName,
  //     "lastName" : newUserForm.value.lastName,
  //     "phoneNumber" : newUserForm.value.phone,
  //     "companyName" : newUserForm.value.company
  //   }
  //   this.showLoader = true;
  //   let url = this.hostValue+'/magento/index.php/rest/V1/customer/newuser';
  //   this.dataservice.fetchPostData(url, userData, headerDict).subscribe((data)=>{
  //     this.showLoader = false;
  //     console.log(data);
  //     this.isNewUserFormVisible = false;
  //     this.isNewUserSuccessVisible = true;
  //   }, (err)=>{
  //     this.showLoader = false;
  //     this.isNewUserFormVisible = false;
  //     this.isNewUserErrorVisible = true;
  //     console.log(err.message);
  //   })
    
  // }
}