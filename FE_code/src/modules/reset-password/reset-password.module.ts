import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ResetPasswordComponent } from '../../components/reset-password/reset-password.component';
import { ResetPasswordRouterModule } from './reset-password.module.routing';

@NgModule({
    imports: [SharedModule, ResetPasswordRouterModule],
    declarations: [ResetPasswordComponent],
    exports: [ResetPasswordComponent]
})

export class ResetPasswordModule {
    constructor (){}
}