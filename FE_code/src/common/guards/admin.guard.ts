import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class AdminGuard implements CanActivate {
    userData:any;

    constructor(
        private _cookieService:CookieService
    ) {}

    canActivate(): boolean {
        this.userData = JSON.parse(localStorage.getItem('fivebarsUserData'));
        let isAdmin = false;
        if(this.userData) {
            (this.userData.custom_attributes).forEach((element) => {
                if(element.attribute_code == 'is_admin') {
                    if(element.value == "1") {
                        isAdmin = true;
                    }
                }
            });
        }
        return isAdmin;
        // let emailSplitArray = this.userData.email.split("@");
        // emailSplitArray = emailSplitArray[emailSplitArray.length - 1].toLowerCase();
        // if (emailSplitArray == "xgcommunities.com" || emailSplitArray == "deloitte.com") {
        //     return true;
        // }
        // return false;
    }
}
