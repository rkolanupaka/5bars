<?php

namespace Custom\CustomerRegistrationWebService\Api;

use Custom\CustomerRegistrationWebService\Api\ValidationResponseInterface;

class ValidationResponse implements ValidationResponseInterface{
	
	/**
	 *
	 * @var bool
	 */
	protected $valid;
	
	/**
	 * @var int
	 */
	protected $customerId;
	
	/**
	 * @var string
	 */
	protected $token;
	
	/**
	 * @api
	 * @return bool
	 */
	public function getValid(){
		return $this->valid;
	}
	
	/**
	 * @api
	 * @param bool $valid
	 * @return void
	 */
	public function setValid($valid){
		$this->valid = $valid;
	}
	
	/**
	 * @api
	 * @return int
	 */
	public function getCustomerId(){
		return $this->customerId;
	}
	
	/**
	 * @api
	 * @param int $customerId
	 * @return void
	 */
	public function setCustomerId($customerId){
		$this->customerId = $customerId;
	}
	
	/**
	 * @api
	 * @return string
	 */
	public function getToken(){
		return $this->token;
	}
	
	/**
	 * @api
	 * @param string $token
	 * @return void
	 */
	public function setToken($token){
		$this->token = $token;
	}
}