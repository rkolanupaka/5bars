<?php
namespace Custom\CustomOrder\Model;

use Custom\CustomOrder\Api\SwapOrderItemInterface;

class SwapOrderItem implements SwapOrderItemInterface{
	
	/**
	 * @var \Magento\Sales\Api\OrderRepositoryInterfaceFactory
	 */
	protected $orderRepositoryInterfaceFactory;
	
	protected $logger;
	
	/**
	 *
	 * @param \Magento\Sales\Api\OrderRepositoryInterfaceFactory $orderRepositoryInterfaceFactory
	 */
	public function __construct(
			\Magento\Sales\Api\OrderRepositoryInterfaceFactory $orderRepositoryInterfaceFactory,
			\Psr\Log\LoggerInterface $logger
			){
		
				$this->orderRepositoryInterfaceFactory = $orderRepositoryInterfaceFactory;
				$this->logger = $logger;
	}
	
	/**
	 * Performs persist operations for a specified order.
	 *
	 * @param \Magento\Sales\Api\Data\OrderInterface $entity The order ID.
	 * @return \Magento\Sales\Api\Data\OrderInterface Order interface.
	 */
	public function save(\Magento\Sales\Api\Data\OrderInterface $entity){
		$this->logger->info("Into the SWAP Method");
		$orderRepositoryInterface = $this->orderRepositoryInterfaceFactory->create();
		return $orderRepositoryInterface->save($entity);
	}
}