import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { OrderHistoryComponent } from '../../components/order-history/order-history.component';
import { SwapSiteComponent } from '../../components/swap-site/swap-site.component';
import { SiteSwapConfirmationComponent } from '../../components/site-swap-confirmation/site-swap-confirmation.component';
import { OrdersRouterModule } from './orders.module.routing';

@NgModule({
    imports: [SharedModule, OrdersRouterModule],
    declarations: [OrderHistoryComponent, SwapSiteComponent, SiteSwapConfirmationComponent],
    exports: [OrderHistoryComponent, SwapSiteComponent, SiteSwapConfirmationComponent]
})

export class OrdersModule {
    constructor (){}
}