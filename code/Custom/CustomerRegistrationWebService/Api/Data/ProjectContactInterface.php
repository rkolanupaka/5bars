<?php
namespace Custom\CustomerRegistrationWebService\Api\Data;
interface ProjectContactInterface
{
	/**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
	const EMAIL = 'email';
	const NAME = 'name';
	const PHONE_NUMBER = 'phone';
	const CUSTOMER_ID = 'customer_id';
	const COMPANY = 'company';
	
	/**#@-*/
	
}