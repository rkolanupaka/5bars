import { Component, OnInit } from '@angular/core';
import {DataService} from '../../common/services/data.service';
import { CookieService } from 'ngx-cookie';
import { Router} from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-request-site',
  templateUrl: './request-site.component.html',
  styleUrls: ['./request-site.component.scss']
})
export class RequestSiteComponent implements OnInit {
  showPopUpTrue = false;
  siteFormModel:any;
  hostValue:string;
  showLoader:boolean = false;
  statelist = [];
  showPopUpMessage:string;
  citylist = [];
  requestedSites = [];
  requestedSitesSuccess = [];
  duplicateSitesSuccess = [];
  duplicateRequestsSuccess = [];
  requestSuccess:boolean = false;
  today;
  fiveBarsToken:string;
  otherPoleType:string = "";
  loggedUserData:any;

  constructor(
    private dataservice: DataService,
    private _cookieService:CookieService,
    private router: Router
  ) {
    this.hostValue = this.dataservice.getHostName();
    this.fiveBarsToken = this._cookieService.get('5barsToken');
    this.resetEditSiteForm();
  }

  receivedUserData($event) {
    this.loggedUserData = $event;
  }

  ngOnInit() {
    let url = this.hostValue+ '/magento/index.php/rest/V1/state/cities';
    let headerDect = {
      "Content-Type": "application/json",
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data:any)=>{
      this.showLoader = false; 
      this.statelist = (data.items).sort(function(a, b) {
        return a.state.localeCompare(b.state)
      });
    }, (err)=> {
      this.showLoader = false;   
      this.showPopUpMessage = "Something went wrong while loading states. Please try again later.";
      this.showPopUpTrue = true;
    })
    // let url = this.hostValue+'/magento/index.php/rest/V1/states';
    // let headerDict = {
    //   "Content-Type": "application/json"
    // }
    // this.showLoader = true;
    // this.dataservice.fetchData(url, headerDict).subscribe(
    //   (data)=>{
    //     this.statelist = data.items;
    //     this.showLoader = false;
    //   },(err)=>{
    //     this.showLoader = false;
    //     this.showPopUpMessage = "something went wrong. Please try again";
    //     this.showPopUpTrue = true;
    //   });
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }


  resetEditSiteForm() {
    this.siteFormModel = {
      state: "",
      city: "",
      // siteId : "",
      latitude : "",
      longitude: "",
      locType: "",
      poleType: ""
      // siteSelectionFee: 750,
      // rentLeaseMonthlyFee: 150,
      // siteSurveyLeaseFee: 3000
    }
  }

  stateChanged() {
    let stateObj = this.siteFormModel.state;
    this.siteFormModel.city = "";
    if(this.siteFormModel.state!="") {
      this.citylist = (stateObj.cities).sort(function(a, b) {
        return a.localeCompare(b)
      });
    } else {
      this.citylist = [];
    }
  }

  addRequestedSite(requestForm) {
    let poleType;
    if(this.siteFormModel.poleType == "Other") {
      poleType = this.otherPoleType;
    } else {
      poleType = this.siteFormModel.poleType;
    }
    let a = {
      customerId : this.loggedUserData.id,
      state: this.siteFormModel.state.state,
      city: this.siteFormModel.city,
      latitude: Number(this.siteFormModel.latitude),
      longitude: Number(this.siteFormModel.longitude),
      cityArea: this.siteFormModel.locType,
      poleClass: poleType,
      status: "open"
    }
    requestForm.resetForm();
    this.resetEditSiteForm();
    console.log(a);
    this.requestedSites.push(a)
  }

  removeSite(index) {
    this.requestedSites.splice(index, 1);
  }

  requestSites() {
    console.log(this.requestedSites);
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    let url = this.hostValue+'/magento/index.php/rest/V1/sitemanagement/siteReq';
    let data = {
      "sites" : this.requestedSites
    }
    this.showLoader = true;
    this.dataservice.fetchPostData(url, data, headerDict).subscribe((data:any)=>{
      this.showLoader = false;
      this.requestedSitesSuccess = data.requested_sites;
      this.duplicateSitesSuccess = data.duplicate_sites;
      this.duplicateRequestsSuccess = data.request_pending_sites;
      this.today = Date.now();
      this.requestSuccess = true;
    }, (err)=>{
      this.showLoader = false;
      // this.requestSuccess = true;
      if(err.statusText) {
        this.showPopUpMessage = err.statusText;
      } else {
        this.showPopUpMessage = "Something went wrong...";
      }
      this.showPopUpTrue = true;
    })
  }

  confirmationRequest() {
    if(this.requestedSitesSuccess.length>0) {
      this.requestedSites = this.requestedSitesSuccess;
      
    } else {
      this.showPopUpMessage = "Could not request any site due to some issue. Please try again.";
      this.showPopUpTrue = true;
      this.requestedSites = [];
    }
  }

  reserveSiteToSearch(site) {
    localStorage.setItem(
      "preStateCity",
      site.state + "***" + site.city
    );
    localStorage.setItem(
      "duplicateSku",
      site.sku
    );
    this.router.navigate(['site/site-search'], { 
      queryParams: {
        type: 'findSiteByInput'
      }
    });
  }
  
}