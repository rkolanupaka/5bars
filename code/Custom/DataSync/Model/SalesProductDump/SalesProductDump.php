<?php

namespace Custom\DataSync\Model\SalesProductDump;

use Custom\DataSync\Api\Data\SalesProductDumpInterface;

class SalesProductDump extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,SalesProductDumpInterface
{
    const CACHE_TAG = 'master_product_dump';
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
        ) 
    {
        $this->logger = $logger;
    }
    
    protected function _construct()
    {
        $this->_init('\Custom\DataSync\Model\ResourceModel\SalesProductDump');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        
        return $values;
    }
    
    public function createQuery($result, $transaction)
    {
        $this->logger->info("SalesProductDump | createQuery");
        $sql = "Insert into magento.master_product_dump "
            ."(sku, active, description, longitude, latitude, monthly_charges, site_selection_fee, site_survey_lease_fee, city_area, pole_class, city, state) "
            ."values (";
        $resultArray = array();
        $resultArray = $result;
        $str = implode(",", $resultArray) ;
        $values = explode(",", $str);
        $sql = $sql."'".$values[1]."','".$values[2]."','".$values[4]."','".$values[6]."','".$values[7]."','".$values[8]."','".$values[9]."','".$values[10]."','".$values[11]."','".$values[12]."','".$values[13]."','".$values[14]."')";
        $this->logger->info("SalesProductDump | createQuery | ::: ".$sql);
        return $sql;
    }
    
    public function saveSalesProduct($connection, $transaction, $results)
    {
        $this->logger->info("SalesProductDump | saveSalesProduct");
        try
        {
            foreach ($results as $result)
            {
                $sql = $this->createQuery($result, $transaction);
                $connection->query($sql);
            }
        }
        catch (\Exception $e)
        {
            $this->logger->info("SalesProductDump | saveSalesProduct | Exception While Inserting In Product Dump Table::: ".$e->getMessage());
        }
    }
    
    public function saveSalesProductDetails($connection, $transaction, $results)
    {
        $this->logger->info("SalesProductDump | saveSalesProductDetails");
        $this->saveSalesProduct($connection, $transaction, $results);
    }
}
