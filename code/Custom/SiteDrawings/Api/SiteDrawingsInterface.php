<?php

namespace Custom\SiteDrawings\Api;


interface SiteDrawingsInterface{

	/**
     * Create new gallery entry
     *
     * @param string $sku
     * @param \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[] $entrys
     * @return \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface[]
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
	public function saveDrawings($sku,$entrys);
	
	/**
	 * Create new gallery entry
	 *
	 * @param string $sku
	 * @param int $entryId
	 * @return bool
	 */
	public function removeDrawings($sku,$entryId);
}