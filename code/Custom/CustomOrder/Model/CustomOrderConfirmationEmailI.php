<?php

/**
 * Copyright 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomOrder\Model;

use Custom\CustomOrder\Api\CustomOrderConfirmationEmailInterface;

/**
 * Defines the implementation class of the CustomOrderConfirmationEmailInterface service contract.
 */
class CustomOrderConfirmationEmailI implements CustomOrderConfirmationEmailInterface
{
	/**
	* @var \Magento\Framework\Serialize\Serializer\Json
	*/
	protected $serializer;
	
	/**
	* @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
	*/
	public function __construct(
		\Magento\Framework\Serialize\Serializer\Json $serializer = null
	){
		$this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
	}
		
    /**
     * @api
     * @param string $orderID.
     * @param string $emailID.
     * @return void
     */
    public function send($orderID, $emailID) {
		$logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
		$logger->info("CustomOrderConfirmationEmailI.php :::");
		$logger->info("Order ID:::: " .$orderID);
		$logger->info("Email ID:::: " .$emailID);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('\Magento\Sales\Model\Order')->load($orderID);
		$order->setCustomerId($emailID);

		$emailSender = $objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
		$emailSender->send($order);
    }
}