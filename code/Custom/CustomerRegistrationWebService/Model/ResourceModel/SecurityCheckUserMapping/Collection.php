<?php
namespace Custom\CustomerRegistrationWebService\Model\ResourceModel\SecurityCheckUserMapping;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Custom\CustomerRegistrationWebService\Model\SecurityCheckUserMapping', 'Custom\CustomerRegistrationWebService\Model\ResourceModel\SecurityCheckUserMapping');
	}
	
}