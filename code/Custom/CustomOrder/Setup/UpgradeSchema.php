<?php

namespace Custom\CustomOrder\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        //$quote = 'quote';
        //$setup->getConnection()->addColumn(
        //    $setup->getTable($quote),
        //    'addon_aneservice_enabled',
        //    [
        //        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
        //        'length' => 255,
        //        'comment' =>'Addon AnEService Service Enabled'
        //    ]
        //);
		//$setup->getConnection()->addColumn(
        //    $setup->getTable($quote),
        //    'is_build_selected',
        //    [
        //        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
        //        'length' => 255,
        //        'comment' =>'Is Build Selected'
        //    ]
        //);
		//$setup->getConnection()->addColumn(
        //    $setup->getTable($quote),
        //    'build_type',
        //    [
        //        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
        //        'length' => 255,
        //        'comment' =>'Build Type'
        //    ]
        //);
			
		//Order table
        $orderTable = 'sales_order';
		$setup->getConnection()->addColumn(
            $setup->getTable($orderTable),
            'po_number',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' =>'PO Number'
            ]
        );
		$setup->getConnection()->addColumn(
            $setup->getTable($orderTable),
            'site_contact_name',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' =>'Site Contact Name'
            ]
        );
		$setup->getConnection()->addColumn(
            $setup->getTable($orderTable),
            'site_contact_email',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' =>'Site Contact Email'
            ]
        );
		$setup->getConnection()->addColumn(
            $setup->getTable($orderTable),
            'site_contact_phone',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' =>'Site Contact Phone Number'
            ]
        );
		$setup->getConnection()->addColumn(
				$setup->getTable($orderTable),
				'site_contact_company',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Site Contact Company'
				]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderTable),
		    'monthly_charges_total',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Monthly Charges Total'
		    ]
		);
		
		//Order item table
		$orderItemTable = 'sales_order_item';
		$setup->getConnection()->addColumn(
            $setup->getTable($orderItemTable),
            'monthly_charges',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 255,
                'comment' =>'Monthly Charges'
            ]
        );
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'cam_fee',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Cam Fee'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'city_area',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'City Area'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'one_time_fee',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'One Time Fee'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'pole_class',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Pole Class'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'rent_lease_fee',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Rent Lease Fee'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'site_selection_fee',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Site Selection Fee'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'site_survey_lease_fee',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Site Survey Lease Fee'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'latitude',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Latitude'
		    ]
		);
		$setup->getConnection()->addColumn(
		    $setup->getTable($orderItemTable),
		    'longitude',
		    [
		        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
		        'length' => 255,
		        'comment' =>'Longitude'
		    ]
		);
		$setup->getConnection()->addColumn(
				$setup->getTable($orderItemTable),
				'state',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'State'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($orderItemTable),
				'city',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'City'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable($orderItemTable),
				'reason_code',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'ReasonCode for Swap'
				]
				);
		
        $setup->endSetup();
    }
}