<?php

namespace Custom\CustomerRegistrationWebService\Model\ResourceModel;

class SecurityCheckUserMapping extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{
	
	
	/**
	 * constructor
	 *
	 * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
	 */
	public function __construct(
			\Magento\Framework\Model\ResourceModel\Db\Context $context
			)
	{
		parent::__construct($context);
	}
	
	
	/**
	 * Initialize resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('security_check_user_mapping','id');
	}
}