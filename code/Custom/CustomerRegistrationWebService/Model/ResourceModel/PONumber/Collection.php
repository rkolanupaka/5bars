<?php
namespace Custom\CustomerRegistrationWebService\Model\ResourceModel\PONumber;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Custom\CustomerRegistrationWebService\Model\PONumber', 'Custom\CustomerRegistrationWebService\Model\ResourceModel\PONumber');
    }

}