<?php

namespace Custom\CustomerRegistrationWebService\Api;

use Custom\CustomerRegistrationWebService\Api\SecurityQuestionsDataInterface;

class SecurityQuestionsData implements SecurityQuestionsDataInterface{
	
	/**
	 *
	 * @var Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]
	 */
	protected $set1;
	
	/**
	 *
	 * @var Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]
	 */
	protected $set2;
	
	/**
	 *
	 * @var Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]
	 */
	protected $set3;
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]|null
	 */
	public function getSet1(){
		return $this->set1;
	}
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[] $set1
	 * @return void
	 */
	public function setSet1($set1){
		$this->set1 = $set1;
	}
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]|null
	 */
	public function getSet2(){
		return $this->set2;
	}
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[] $set2
	 * @return void
	 */
	public function setSet2($set2){
		$this->set2 = $set2;
	}
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]|null
	 */
	public function getSet3(){
		return $this->set3;
	}
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[] $set3
	 * @return void
	 */
	public function setSet3($set3){
		$this->set3 = $set3;
	}
	
}