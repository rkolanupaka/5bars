import { Component, OnInit } from '@angular/core';

import { DataService } from '../../common/services/data.service';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'site-selex-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})

export class ReportsComponent implements OnInit {

  // endDate:Date;
  fiveBarsToken:string;
  hostValue:string;
  reportType = "";
  showPopUpMessage:string;
  // startDate:Date;
  showPopUpTrue:boolean = false;
  showLoader: boolean = false;
  // tempDate = new Date();

  date = new FormControl(new Date(new Date().setMonth(new Date().getMonth() - 1)));
  date1 = new FormControl(new Date());
  maxDate = new Date();

  // public myDatePickerOptions: IMyDpOptions = {
  //   dateFormat: 'yyyy-mm-dd',
  //   editableDateField: false,
  //   openSelectorOnInputClick: true,
  //   showTodayBtn: false,
  //   showClearDateBtn: false,
  //   disableSince: {year: this.tempDate.getFullYear(), month: this.tempDate.getMonth()+1, day: this.tempDate.getDate()+1}
  // };

  // public startDateModel: any = { date: { year: this.tempDate.getMonth()==0?this.tempDate.getFullYear()-1:this.tempDate.getFullYear(), month: this.tempDate.getMonth()==0?12:this.tempDate.getMonth(), day: this.tempDate.getDate() } };
  // public endDateModel: any = { date: { year: this.tempDate.getFullYear(), month: this.tempDate.getMonth()+1, day: this.tempDate.getDate() } };

  constructor(private dataservice: DataService) {}

  ngOnInit() {
    this.hostValue = this.dataservice.getHostName();
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  reportsFormSubmit(reportForm) {
    if(this.reportType == "" || this.date.invalid || this.date1.invalid) {
      this.showPopUpMessage = "All fileds are mandatory.";
      this.showPopUpTrue = true;
    } else {
      let startDate = this.date.value.getFullYear()+"-"+(this.date.value.getMonth()+1)+"-"+this.date.value.getDate();
      let endDate = this.date1.value.getFullYear()+"-"+(this.date1.value.getMonth()+1)+"-"+this.date1.value.getDate();
      let reportType = reportForm.value.reportType;
      let url = this.hostValue+'/magento/index.php/rest/V1/report/'+reportType+'/'+startDate+' 00:00:00/'+endDate+' 23:59:59';
      this.showLoader = true;
      let headerDect = {
        "Content-Type": "application/json"
      }
      this.dataservice.fetchPlainData(url, headerDect).subscribe(
        (data:any)=>{
          this.showLoader = false;
          if(data == '"false"') {
            this.showPopUpMessage = "No data available for this selection.";
            this.showPopUpTrue = true;
          } else {
            window.open(this.hostValue + '/magento/index.php/rest/V1/report/'+reportType+'/'+startDate+' 00:00:00/'+endDate+' 23:59:59', "_self");
          }
        },(err)=> {
          this.showLoader = false;
          if(err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please try again";
          }
          this.showPopUpTrue = true;
      });
    }
  }
}
