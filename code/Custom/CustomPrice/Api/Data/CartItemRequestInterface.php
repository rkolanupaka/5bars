<?php

namespace Custom\CustomPrice\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface CartItemRequestInterface
{
	/**
	 * Get Survey Lease Flag
	 *
	 * @return int
	 */
	public function getSurveyLeaseFlag();
	
	/**
	 * Set Survey Lease Flag
	 *
	 * @param int $surveyLeaseFlag
	 * @return $this
	 */
	public function setSurveyLeaseFlag($surveyLeaseFlag);
	
	/**
	 * Get Sku Id.
	 *
	 * @return string
	 */
	public function getSku();
	
	/**
	 * Set Sku Id.
	 *
	 * @param string
	 * @return $this
	 */
	public function setSku($sku);
	
}
