<?php 


namespace Custom\Reports\Model;
use Custom\CustomerRegistrationWebService\Api\CustomerInterface;
use Custom\Reports\Api\ReportsInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Psr\Log\LoggerInterface;

/**
 * Defines the implementation class of the Customer service contract.
 */
class Reports extends \Magento\Framework\App\Action\Action implements ReportsInterface
{
    
     /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    
    /**
     * 
     * @var \DateTime
     */
    protected $date;
    
    /**
     * 
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timeZone;
    
    /**
     * 
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @var \Magento\Framework\Data\CollectionFactory
     */
    protected $collectionFactory;
    
    
    /**
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;
    
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \DateTime $date;
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Data\CollectionFactory $collectionFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
    							\DateTime $date,
    							\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
    							\Psr\Log\LoggerInterface $logger,
    							\Magento\Framework\Data\CollectionFactory $collectionFactory,
    							\Magento\Framework\Json\Helper\Data $jsonHelper
    							) {
        $this->storeManager     = $storeManager;
        $this->date = $date;
        $this->timeZone = $timeZone;
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }
    
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getOrders()
     */
    public function getFinServReport($startDate,$endDate)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$salesOrder = $resource->getTableName('sales_order');
    	$salesOrderItem = $resource->getTableName('sales_order_item');
    	$salesOrderAddress = $resource->getTableName('sales_order_address');
    	$salesOrderPayment = $resource->getTableName('sales_order_payment');
    	$sql = "SELECT o.increment_id, GROUP_CONCAT(DISTINCT oi.sku) sku, o.site_contact_name,o.site_contact_phone,
			o.site_contact_email, concat(o.customer_firstname,' ',o.customer_lastname) customer_name,
			oa.telephone,o.customer_email, o.created_at,o.monthly_charges_total,sop.method,o.po_number,
			concat(oa.street,', ',oa.region,', ',oa.country_id,', ',oa.postcode) billing_address 
			FROM ".$salesOrder." as o JOIN ".$salesOrderItem." as oi ON o.entity_id=oi.order_id 
			join ".$salesOrderAddress." as oa ON o.billing_address_id=oa.entity_id 
			join ".$salesOrderPayment." as sop ON sop.parent_id = o.entity_id
			WHERE (o.created_at BETWEEN '".$startDate."' AND '".$endDate."') 
			and oa.address_type like 'billing' GROUP BY o.entity_id";
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size ------'.sizeof($array));
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	$fileName = "Orders". round(microtime(true) * 1000).".csv";
    	$this->prepareFileCsv($array,$this->createFinServReportHeading(),$fileName);
    }
    
    /**
     * 
     * @return \Magento\Framework\Phrase[]
     */
    protected function createFinServReportHeading(){
    	return [
    			__('Order Number'),
    			__('Sites Reserved'),
    			__('Project Contact Name'),
    			__('Project Contact Number'),
    			__('Project Contact eMail'),
    			__('Order Contact Name'),
    			__('Order Contact Number'),
    			__('Order Contact eMail'),
    			__('Date & Time'),
    			__('Total Monthly Fees'),
    			__('Payment Method'),
    			__('PO Number'),
    			__('Billing Address')
    	];
    }
    
    /**
     *
     * @param array $array
     */
    public function prepareFileCsv($array,$heading,$outputFile)
    {
    	$handle = fopen($outputFile, 'w');
    	fputcsv($handle, $heading);
    	
    	$row = array();
    	foreach ($array as $resultSet)
    	{
    		$row[0] = "\t".$resultSet['increment_id'];
    		$row[1] = $resultSet['sku'];
    		$row[2] = $resultSet['site_contact_name'];
    		$row[3] = $resultSet['site_contact_phone'];
    		$row[4] = $resultSet['site_contact_email'];
    		$row[5] = $resultSet['customer_name'];
    		$row[6] = $resultSet['telephone'];
    		$row[7] = $resultSet['customer_email'];
    		$row[8] = $resultSet['created_at'];
    		$row[9] = $resultSet['monthly_charges_total'];
    		$row[10] = $resultSet['method'];
    		$row[11] = $resultSet['po_number'];
    		$row[12] = $resultSet['billing_address'];
    		
    		fputcsv($handle,$row);
    	}
    	$this->downloadCsv($outputFile);
    }
    
    
    /**
     *
     * @return \Magento\Framework\App\ResourceConnection
     */
    public function getResourceConnection(){
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    	return $objectManager->get('Magento\Framework\App\ResourceConnection');
    }
    
    /**
     * 
     * @param array $array
     */
    public function prepareFile($array,$heading,$outputFile)
    {
    	$handle = fopen($outputFile, 'w');
    	fputcsv($handle, $heading);
    	
    	foreach ($array as $resultSet)
    	{
    		$row = array();
    		foreach ($resultSet as $result){
    			$row[] = $result;
    		}
    		
    		fputcsv($handle,$row);
    	}
    	$this->downloadCsv($outputFile);
    }
    
    public function downloadCsv($file)
    {
    	if (file_exists($file)) {
    		//set appropriate headers
    		header('Content-Description: File Transfer');
    		header('Content-Type: application/csv');
    		header('Content-Disposition: attachment; filename='.basename($file));
    		header('Expires: 0');
    		header('Cache-Control: must-revalidate');
    		header('Pragma: public');
    		header('Content-Length: ' . filesize($file));
    		ob_clean();
    		flush();
    		readfile($file);
    	}
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getEngineeringReport()
     */
    public function getEngineeringReport($startDate,$endDate)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$salesOrder = $resource->getTableName('sales_order');
    	$salesOrderItem = $resource->getTableName('sales_order_item');
    	$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    	$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    	$eavAttribute = $resource->getTableName('eav_attribute');
    	$eavEntityType = $resource->getTableName('eav_entity_type');
    	$sql = "select oi.sku,oi.latitude,oi.longitude,oi.pole_class,o.entity_id,o.created_at,v1.value as state,
				v2.value as city,o.site_contact_name,o.site_contact_phone,o.site_contact_email
				FROM ".$salesOrder." as o JOIN ".$salesOrderItem." as oi ON o.entity_id=oi.order_id 
				JOIN ".$catalogProductEntity." e ON oi.sku=e.sku 
				LEFT JOIN ".$catalogProductEntityVarchar." v1 ON e.entity_id = v1.entity_id 
				AND v1.store_id = 0 
				AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'state' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product')) 
				LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id 
				AND v2.store_id = 0 
				AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product')) 
				where (o.created_at BETWEEN '".$startDate."' AND '".$endDate."')";
    			
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size --------'.sizeof($array));
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	$fileName = "Products". round(microtime(true) * 1000).".csv";
    	$this->prepareFile($array,$this->createEnggReportHeading(),$fileName);
    }
    
    /**
     *
     * @return \Magento\Framework\Phrase[]
     */
    protected function createEnggReportHeading(){
    	return [
    			__('Site ID'),
    			__('Latitude'),
    			__('Longitude'),
    			__('Pole-Type'),
    			__('Order Number'),
    			__('Order Date'),
    			__('State'),
    			__('City'),
    			__('Project Contact Name'),
    			__('Project Contact Number'),
    			__('Project Contact eMail')
    	];
    }
    
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\CustomerRegistrationWebService\Api\CustomerInterface::getSiteLocationData()
     */
    public function getSiteLocationData($state,$city)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
    	$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    	$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    	$eavAttribute = $resource->getTableName('eav_attribute');
    	$eavEntityType = $resource->getTableName('eav_entity_type');
    	$sql = "select e.sku,v4.value as latitude,v5.value as longitude,v6.value as city_area,v7.value as pole_class,v8.value as monthly_charges FROM ".$catalogProductEntity." e LEFT JOIN ".$catalogProductEntityVarchar." v1 ON e.entity_id = v1.entity_id 
				AND v1.store_id = 0 
				AND v1.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'state' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityVarchar." v2 ON e.entity_id = v2.entity_id 
				AND v2.store_id = 0 
				AND v2.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
                LEFT JOIN ".$catalogProductEntityInt." v3 ON e.entity_id = v3.entity_id 
				AND v3.store_id = 0 
				AND v3.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'reserved' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v4 ON e.entity_id = v4.entity_id 
				AND v4.store_id = 0 
				AND v4.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'latitude' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				LEFT JOIN ".$catalogProductEntityVarchar." v5 ON e.entity_id = v5.entity_id 
				AND v5.store_id = 0 
				AND v5.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'longitude' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))
				
				LEFT JOIN ".$catalogProductEntityVarchar." v6 ON e.entity_id = v6.entity_id 
				AND v6.store_id = 0 
				AND v6.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'city_area' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))

				LEFT JOIN ".$catalogProductEntityVarchar." v7 ON e.entity_id = v7.entity_id 
				AND v7.store_id = 0 
				AND v7.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'pole_class' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))

				LEFT JOIN ".$catalogProductEntityVarchar." v8 ON e.entity_id = v8.entity_id 
				AND v8.store_id = 0 
				AND v8.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'monthly_charges' 
										AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType." 
																WHERE entity_type_code = 'catalog_product'))

                WHERE v1.value = '".$state."' AND v2.value = '".$city."' and v3.value = 0";
    	
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size --------'.sizeof($array));
    	
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	$fileName = $state."_".$city.".csv";
    	$this->prepareFile($array,$this->createSiteLocationReportHeading(),$fileName);
    }
    
    /**
     *
     * @return \Magento\Framework\Phrase[]
     */
    protected function createSiteLocationReportHeading(){
    	return [
    			__('ID'),
    			__('Latitude'),
    			__('Longitude'),
    			__('City Area'),
    			__('Pole Class'),
    			__('Monthly Rent')
    	];
    }
    
    
    /**
     *
     * {@inheritDoc}
     * @see \Custom\Reports\Api\ReportsInterface::getSiteLocationDataforMapView()
     */
    public function getSiteLocationDataforMapView($state,$city)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
    	$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    	$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    	//$catalogProductEntityDecimal = $resource->getTableName('catalog_product_entity_decimal');
    	$eavAttribute = $resource->getTableName('eav_attribute');
    	$eavEntityType = $resource->getTableName('eav_entity_type');
    	$sql= "select cpeIP1.value as state, cpeIP2.value as city, cpe.sku as sku,  cpeOP1.value as longitude, cpeOP2.value as latitude, cpeOP3.value as reserved,cpeOP4.value as status,cpeOP5.value as city_area,
				cpeOP6.value as pole_class,cpeOP7.value as rent_lease_fee,cpeOP8.value as site_selection_fee,cpeOP9.value as site_survey_lease_fee
    			
				from ".$catalogProductEntity." cpe
						
				join ".$catalogProductEntityVarchar." cpeIP1 on cpeIP1.store_id=0 AND cpeIP1.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrInput1 on evAttrInput1.attribute_id = cpeIP1.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeIP2 on cpeIP2.store_id=0 AND cpeIP2.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrInput2 on evAttrInput2.attribute_id = cpeIP2.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP1 on cpeOP1.store_id=0 AND cpeOP1.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput1 on evAttrOutput1.attribute_id = cpeOP1.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP2 on cpeOP2.store_id=0 AND cpeOP2.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput2 on evAttrOutput2.attribute_id = cpeOP2.attribute_id
						
				join ".$catalogProductEntityInt." cpeOP3 on cpeOP3.store_id=0 AND cpeOP3.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput3 on evAttrOutput3.attribute_id = cpeOP3.attribute_id
						
				join ".$catalogProductEntityInt." cpeOP4 on cpeOP4.store_id=0 AND cpeOP4.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput4 on evAttrOutput4.attribute_id = cpeOP4.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP5 on cpeOP5.store_id=0 AND cpeOP5.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput5 on evAttrOutput5.attribute_id = cpeOP5.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP6 on cpeOP6.store_id=0 AND cpeOP6.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput6 on evAttrOutput6.attribute_id = cpeOP6.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP7 on cpeOP7.store_id=0 AND cpeOP7.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput7 on evAttrOutput7.attribute_id = cpeOP7.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP8 on cpeOP8.store_id=0 AND cpeOP8.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput8 on evAttrOutput8.attribute_id = cpeOP8.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP9 on cpeOP9.store_id=0 AND cpeOP9.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput9 on evAttrOutput9.attribute_id = cpeOP9.attribute_id
						
						
				join ".$eavEntityType." eet
				              on  eet.entity_type_id = evAttrInput1.entity_type_id
				              and eet.entity_type_id = evAttrInput2.entity_type_id
				              and eet.entity_type_id = evAttrOutput1.entity_type_id
				              and eet.entity_type_id = evAttrOutput2.entity_type_id
				              and eet.entity_type_id = evAttrOutput3.entity_type_id
							  and eet.entity_type_id = evAttrOutput4.entity_type_id
							  and eet.entity_type_id = evAttrOutput5.entity_type_id
							  and eet.entity_type_id = evAttrOutput6.entity_type_id
							  and eet.entity_type_id = evAttrOutput7.entity_type_id
							  and eet.entity_type_id = evAttrOutput8.entity_type_id
							  and eet.entity_type_id = evAttrOutput9.entity_type_id
						
						
				where eet.entity_type_code = 'catalog_product'
				              and evAttrInput1.attribute_code = 'state'
				              and evAttrInput2.attribute_code = 'city'
				              and evAttrOutput1.attribute_code = 'longitude'
				              and evAttrOutput2.attribute_code = 'latitude'
				              and evAttrOutput3.attribute_code = 'reserved'
						      and evAttrOutput4.attribute_code = 'status'
							  and evAttrOutput5.attribute_code = 'city_area'
							  and evAttrOutput6.attribute_code = 'pole_class'
							  and evAttrOutput7.attribute_code = 'rent_lease_fee'
							  and evAttrOutput8.attribute_code = 'site_selection_fee'
							  and evAttrOutput9.attribute_code = 'site_survey_lease_fee'
				              and cpeIP1.value='".$state."'
				              and cpeIP2.value='".$city."' ORDER BY cpe.entity_id ASC";
    	
    	$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('Result Set Array Size --------'.sizeof($array));
    	
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	
    	
    	$tempArray = array();
    	foreach ($array as $item) {
    		$mapObject = new \Custom\Reports\Api\MapViewData();
    		
    		//$this->logger->info('--------------loop Start----------------');
    		//$this->logger->info('Item  data 1--------'.$item['sku']);
    		
    		if($item['status']==1){
    			if($item['reserved']==1){
    				$statusCheck="reserved";
    			}
    			else{
    				$statusCheck="available";
    			}
    		}
    		else {
    			$statusCheck="unavailable";
    		}
    		
    		$mapObject->setSku($item['sku']);
    		$mapObject->setRes($item['reserved']);
    		$mapObject->setLat(floatval($item['latitude']));
    		$mapObject->setLong(floatval($item['longitude']));   
    		$mapObject->setStat($statusCheck);
    		$mapObject->setSChk($item['status']);
    		$mapObject->setPCls($item['pole_class']);
    		$mapObject->setLType($item['city_area']);
    		$mapObject->setRlFee($item['rent_lease_fee']);
    		$mapObject->setSsFee($item['site_selection_fee']);
    		$mapObject->setSslFee($item['site_survey_lease_fee']);
    		//$mapObject->setLatitude($this->jsonHelper->jsonDecode($this->jsonHelper->jsonEncode($item['latitude'],JSON_NUMERIC_CHECK)));
    		//$mapObject->setLongitude($this->jsonHelper->jsonDecode($this->jsonHelper->jsonEncode($item['longitude'],JSON_NUMERIC_CHECK)));   
    		
    		
    		//$this->logger->info('Item  Size check --------'.sizeof($item));
			//$this->logger->info('--------------loop End----------------');
			$tempArray[] = $mapObject->jsonSerialize();
    	}
    	
    	$this->logger->info('Object Array Size check --------'.sizeof($tempArray));
    	
    	//$encodedData = $this->jsonHelper->jsonEncode($tempArray,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    	//return $this->jsonHelper->jsonDecode($encodedData,true);
		return $tempArray;
    	
    }
    
    
    /**
     *
     * {@inheritDoc}
     * @see \Custom\Reports\Api\ReportsInterface::getSiteLocationDataforMaps()
     */
    public function getSiteLocationDataforMaps($state,$city)
    {
    	$resource = $this->getResourceConnection();
    	$connection = $resource->getConnection();
    	$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
    	$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    	$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    	//$catalogProductEntityDecimal = $resource->getTableName('catalog_product_entity_decimal');
    	$eavAttribute = $resource->getTableName('eav_attribute');
    	$eavEntityType = $resource->getTableName('eav_entity_type');
    	$sql= "select cpeIP1.value as state, cpeIP2.value as city, cpe.sku as sku,  cpeOP1.value as longitude, cpeOP2.value as latitude,cpeOP3.value as reserved,cpeOP5.value as city_area,
				cpeOP6.value as pole_class,cpeOP4.value as status 
				
				from ".$catalogProductEntity." cpe
						
				join ".$catalogProductEntityVarchar." cpeIP1 on cpeIP1.store_id=0 AND cpeIP1.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrInput1 on evAttrInput1.attribute_id = cpeIP1.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeIP2 on cpeIP2.store_id=0 AND cpeIP2.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrInput2 on evAttrInput2.attribute_id = cpeIP2.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP1 on cpeOP1.store_id=0 AND cpeOP1.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput1 on evAttrOutput1.attribute_id = cpeOP1.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP2 on cpeOP2.store_id=0 AND cpeOP2.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput2 on evAttrOutput2.attribute_id = cpeOP2.attribute_id
						
				join ".$catalogProductEntityInt." cpeOP3 on cpeOP3.store_id=0 AND cpeOP3.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput3 on evAttrOutput3.attribute_id = cpeOP3.attribute_id
						
				join ".$catalogProductEntityInt." cpeOP4 on cpeOP4.store_id=0 AND cpeOP4.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput4 on evAttrOutput4.attribute_id = cpeOP4.attribute_id

				join ".$catalogProductEntityVarchar." cpeOP5 on cpeOP5.store_id=0 AND cpeOP5.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput5 on evAttrOutput5.attribute_id = cpeOP5.attribute_id
						
				join ".$catalogProductEntityVarchar." cpeOP6 on cpeOP6.store_id=0 AND cpeOP6.entity_id = cpe.entity_id
				join ".$eavAttribute." evAttrOutput6 on evAttrOutput6.attribute_id = cpeOP6.attribute_id
						
						
				join ".$eavEntityType." eet
				              on  eet.entity_type_id = evAttrInput1.entity_type_id
				              and eet.entity_type_id = evAttrInput2.entity_type_id
				              and eet.entity_type_id = evAttrOutput1.entity_type_id
				              and eet.entity_type_id = evAttrOutput2.entity_type_id
				              and eet.entity_type_id = evAttrOutput3.entity_type_id
							  and eet.entity_type_id = evAttrOutput4.entity_type_id
							  and eet.entity_type_id = evAttrOutput5.entity_type_id
							  and eet.entity_type_id = evAttrOutput6.entity_type_id
						
				where eet.entity_type_code = 'catalog_product'
				              and evAttrInput1.attribute_code = 'state'
				              and evAttrInput2.attribute_code = 'city'
				              and evAttrOutput1.attribute_code = 'longitude'
				              and evAttrOutput2.attribute_code = 'latitude'
				              and evAttrOutput3.attribute_code = 'reserved'
						      and evAttrOutput4.attribute_code = 'status'
							  and evAttrOutput5.attribute_code = 'city_area'
							  and evAttrOutput6.attribute_code = 'pole_class'
							  
				              and cpeIP1.value='".$state."'
				              and cpeIP2.value='".$city."' ORDER BY cpe.entity_id ASC";
    	
    	//$this->logger->info($sql);
    	$array = $connection->query($sql)->fetchAll();
    	$this->logger->info('getSiteLocationDataforMaps | Result Set Array Size --------'.sizeof($array));
    	
    	if(sizeof($array) < 1){
    		return "false";
    	}
    	
    	
    	$tempArray = array();
    	foreach ($array as $item) {
    		$mapObject = new \Custom\Reports\Api\MapViewSearchData();
    		
    		//$this->logger->info('--------------loop Start----------------');
    		//$this->logger->info('Item  data 1--------'.$item['sku']);
    		
    		if($item['status']==1){
    			if($item['reserved']==1){
    				$statusCheck="RS";
    			}
    			else{
    				$statusCheck="AV";
    			}
    		}
    		else {
    			$statusCheck="UA";
    		}
    		
    		$mapObject->setSku($item['sku']);
    		$mapObject->setLat(floatval($item['latitude']));
    		$mapObject->setLong(floatval($item['longitude']));
    		$mapObject->setsChk($statusCheck);
    		$mapObject->setpType($item['pole_class']);
    		$mapObject->setlType($item['city_area']);
    		//$mapObject->setLatitude($this->jsonHelper->jsonDecode($this->jsonHelper->jsonEncode($item['latitude'],JSON_NUMERIC_CHECK)));
    		//$mapObject->setLongitude($this->jsonHelper->jsonDecode($this->jsonHelper->jsonEncode($item['longitude'],JSON_NUMERIC_CHECK)));
    		
    		
    		//$this->logger->info('Item  Size check --------'.sizeof($item));
    		//$this->logger->info('--------------loop End----------------');
    		$tempArray[] = $mapObject->jsonSerialize();
    	}
    	
    	$this->logger->info('getSiteLocationDataforMaps | Object Array Size check --------'.sizeof($tempArray));
    	
    	//$encodedData = $this->jsonHelper->jsonEncode($tempArray,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    	//return $this->jsonHelper->jsonDecode($encodedData,true);
    	return $tempArray;
    	
    }
    
    /**
     * Override method.
     */
    public function execute()
    {
    	$this->logger->info('Reports Class being called');
    }
    
}