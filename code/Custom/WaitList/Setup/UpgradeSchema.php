<?php

namespace Custom\WaitList\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
	
	/**
	 * {@inheritdoc}
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){
		$installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('wishlist_item')){
			$table = $installer->getConnection()->newTable(
					$installer->getTable('wishlist_item')
					)->addColumn(
							'wishlist_item_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
							'Wishlist item ID'
							)->addColumn(
									'wishlist_id',
									\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
									null,
									['unsigned' => true, 'nullable' => false, 'default' => '0'],
									'Wishlist ID'
									)->addColumn(
											'product_id',
											\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
											null,
											['unsigned' => true, 'nullable' => false, 'default' => '0'],
											'Product ID'
											)->addColumn(
													'store_id',
													\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
													null,
													['unsigned' => true, 'nullable' => true],
													'Store ID'
													)->addColumn(
															'added_at',
															\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
															null,
															[],
															'Add date and time'
															)->addColumn(
																	'description',
																	\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
																	'64k',
																	[],
																	'Short description of wish list item'
																	)->addColumn(
																			'qty',
																			\Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
																			'12,4',
																			['nullable' => false],
																			'Qty'
																			)->addColumn(
																					'email_check',
																					\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
																					null,
																					[],
																					'Check Email sent to Customer or Not'
																					)->addIndex(
																							$installer->getIdxName('wishlist_item', 'wishlist_id'),
																							'wishlist_id'
																							)->addForeignKey(
																									$installer->getFkName('wishlist_item', 'wishlist_id', 'wishlist', 'wishlist_id'),
																									'wishlist_id',
																									$installer->getTable('wishlist'),
																									'wishlist_id',
																									\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
																									)->addIndex(
																											$installer->getIdxName('wishlist_item', 'product_id'),
																											'product_id'
																											)->addForeignKey(
																													$installer->getFkName('wishlist_item', 'product_id', 'catalog_product_entity', 'entity_id'),
																													'product_id',
																													$installer->getTable('catalog_product_entity'),
																													'entity_id',
																													\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
																													)->addIndex(
																															$installer->getIdxName('wishlist_item', 'store_id'),
																															'store_id'
																															)->addForeignKey(
																																	$installer->getFkName('wishlist_item', 'store_id', 'store', 'store_id'),
																																	'store_id',
																																	$installer->getTable('store'),
																																	'store_id',
																																	\Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL
																																	)->setComment(
																																			'Wishlist items'
																																			);
																																	$installer->getConnection()->createTable($table);
		}
		
		$setup->getConnection()->addColumn(
				$setup->getTable('wishlist_item'),
				'ready_to_reserve',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
						'length' => null,
						'comment' =>'Flag to Check for Ready to Reserve or Not'
				]
				);
		$setup->getConnection()->addColumn(
				$setup->getTable('wishlist_item'),
				'reserve_by_date',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
						'length' => null,
						'comment' =>'Date to store the Till date for reserving the Site'
				]
				);
	}
}