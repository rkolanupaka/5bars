<?php
namespace Custom\CustomerRegistrationWebService\Api;

interface ValidationResponseInterface{
	
	/**
	 * @api
	 * @return bool
	 */
	public function getValid();
	
	/**
	 * @api
	 * @param bool $valid
	 * @return void
	 */
	public function setValid($valid);
	
	/**
	 * @api
	 * @return int
	 */
	public function getCustomerId();
	
	/**
	 * @api
	 * @param int $customerId
	 * @return void
	 */
	public function setCustomerId($customerId);
	
	/**
	 * @api
	 * @return string
	 */
	public function getToken();
	
	/**
	 * @api
	 * @param string $token
	 * @return void
	 */
	public function setToken($token);
}