import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RequestSiteComponent } from '../../components/request-site/request-site.component';
import { MySiteRequestsComponent } from '../../components/my-site-requests/my-site-requests.component';
import { RequestSiteRouterModule } from './request-site.module.routing';

@NgModule({
    imports: [SharedModule, RequestSiteRouterModule],
    declarations: [RequestSiteComponent, MySiteRequestsComponent],
    exports: [RequestSiteComponent, MySiteRequestsComponent]
})

export class RequestSiteModule {
    constructor (){}
}