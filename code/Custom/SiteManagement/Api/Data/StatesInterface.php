<?php
namespace Custom\SiteManagement\Api\Data;
interface StatesInterface
{
	/**#@+
	 * Constants defined for keys of the data array. Identical to the name of the getter in snake case
	 */
	const STATE = 'state';
	const STATE_CODE = 'state_code';
	const ENABLED = 'enabled';
	
	/**#@-*/
	
	
}