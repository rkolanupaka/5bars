import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

import { CookieService } from 'ngx-cookie';
import { DataService } from '../../common/services/data.service';

@Component({
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  confirmationNumber:string;
  confirmationTime:string;
  fiveBarsToken:string;
  hostValue:string;
  orderDetails:any;
  paymentDetail:string="";
  projectContact:any;
  parseInt = parseInt;
  receivePaymentPageData;
  showLoader = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  sitedataInConfirmation = {};
  totalsitecountdata:number = 0;
  totalRentLeaseFee:number = 0;
  totalOneTimeFee:number = 0;
  
  //siteservicepriceinpayment = configfile.configdata.siteserviceprice;

  constructor(private dataservice:DataService, private _cookieService:CookieService, private router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    if(this._cookieService.get('5barsBookingId') != undefined) {
      this.hostValue = this.dataservice.getHostName();
      this.fiveBarsToken = this._cookieService.get('5barsToken');
      this.getConfirmationNumber();
      // removing data from local storage and cookies
      localStorage.removeItem('selectedSiteDataWdotCart');
      localStorage.removeItem('selectedSiteDataWidCart');
      this._cookieService.remove("5barsCartId");
      // this._cookieService.remove("5barsBookingId");
    } else {
      this.router.navigate(['site/payment']);
    }
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  getConfirmationNumber() {
    this.showLoader = true;
    let url = this.hostValue+'/magento/rest/default/V1/customer/orders/'+ this._cookieService.get('5barsBookingId');
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.dataservice.fetchData(url, headerDect).subscribe((data)=>{
      this.showLoader = false;
      this.orderDetails = data;
      this.getProjectContactDetails();
      this.getTimeStamp();
      this.getPaymentOptionDetails();
      this.getSiteDetails();
      this.confirmationNumber = this.orderDetails.increment_id;
    }, (err)=>{
      this.showLoader = false;
      console.log(err);
    })
  }

  getProjectContactDetails() {
    let projectContactData = this.orderDetails.extension_attributes;
    this.projectContact = {
      "name": projectContactData.site_contact_name!=undefined?projectContactData.site_contact_name:'',
      "email": projectContactData.site_contact_email!=undefined?projectContactData.site_contact_email:'',
      "company": projectContactData.site_contact_company!=undefined?projectContactData.site_contact_company:'',
      "phone": projectContactData.site_contact_phone!=undefined?projectContactData.site_contact_phone:'',
      "comment": projectContactData.site_contact_comments!=undefined?projectContactData.site_contact_comments:''
    }
  }

  getTimeStamp() {
    this.confirmationTime = this.orderDetails.created_at;
  }

  getPaymentOptionDetails() {
    let paymentMethod = this.orderDetails.payment.method;
    if(paymentMethod.toLowerCase() == 'purchaseorder') {
      this.paymentDetail = "Applied to the PO# <b>" + this.orderDetails.payment.po_number + "</b>";
    } else {
      this.paymentDetail = "Invoiced through the eprocurement";
    }
  }

  getSiteDetails() {
    this.sitedataInConfirmation = this.orderDetails.items;
    this.totalsitecountdata = this.orderDetails.items.length;
    this.totalRentLeaseFee =  this.orderDetails.extension_attributes.monthly_charges_total;
    this.totalOneTimeFee = this.orderDetails.grand_total;
  }

  print() {
    window.print();
  }

}
