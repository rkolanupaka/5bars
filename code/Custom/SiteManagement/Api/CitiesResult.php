<?php


namespace Custom\SiteManagement\Api;

use Magento\Framework\Api\AbstractSimpleObject;
use Custom\SiteManagement\Api\Data\CitiesResultInterface;

/**
 * SearchResults Service Data Object used for the search service requests
 */
class CitiesResult extends AbstractSimpleObject implements CitiesResultInterface
{
	const KEY_ITEMS = 'items';
	
	/**
	 * Get items
	 *
	 * @return \Magento\Framework\Api\AbstractExtensibleObject[]
	 */
	public function getItems()
	{
		return $this->_get(self::KEY_ITEMS) === null ? [] : $this->_get(self::KEY_ITEMS);
	}
	
	/**
	 * Set items
	 *
	 * @param \Magento\Framework\Api\AbstractExtensibleObject[] $items
	 * @return $this
	 */
	public function setItems(array $items)
	{
		return $this->setData(self::KEY_ITEMS, $items);
	}
	
}
