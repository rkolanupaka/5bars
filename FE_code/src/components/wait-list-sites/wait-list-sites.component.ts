import { Component, OnInit } from "@angular/core";
import { DataService } from "../../common/services/data.service";

import { CookieService } from "ngx-cookie";

@Component({
  selector: "app-wait-list-sites",
  templateUrl: "./wait-list-sites.component.html",
  styleUrls: ["./wait-list-sites.component.scss"]
})
export class WaitListSitesComponent implements OnInit {
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  showLoader: boolean = false;
  waitListSites: any = [];
  hostValue: string;
  fiveBarsToken: string;

  constructor(private dataservice: DataService, private _cookieService: CookieService) {}

  ngOnInit() {
    this.hostValue = this.dataservice.getHostName();
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    this.getWaitListSites();
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  getWaitListSites() {
    const URL = this.hostValue + "/magento/rest/V1/waitlist/items";
    const HEADERS = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchData(URL, HEADERS).subscribe(
      res => {
        this.showLoader = false;
        this.waitListSites = res;
      },
      err => {
        this.showLoader = false;
        console.log(err);
      }
    );
  }

  // remove site method starts
  removeSite(site) {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    const url = this.hostValue + "/magento/rest/V1/waitlist/delete/" + site.product.sku;
    this.dataservice.fetchDeleteData(url, headerDict).subscribe(
      data => {
        this.showLoader = false;
        this.waitListSites.splice(this.waitListSites.indexOf(site), 1);
        this.removeFromLocalStorage(site.product.sku);
      },
      err => {
        this.showLoader = false;
        if (err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "something went wrong..";
        }
        this.showPopUpTrue = true;
      }
    );
  }

  removeFromLocalStorage(siteId) {
    if (localStorage.getItem("selectedSiteDataWidCart")) {
      let withCartSitesTemp = JSON.parse(localStorage.getItem("selectedSiteDataWidCart"));
      let spliceIdex: number;
      withCartSitesTemp.forEach((element, index) => {
        if (element.uniqueId == siteId) {
          spliceIdex = index;
        }
      });
      if (spliceIdex != undefined) {
        withCartSitesTemp.splice(spliceIdex, 1);
        localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(withCartSitesTemp));
      }
    }
    if (localStorage.getItem("selectedSiteDataWdotCart")) {
      let withCartSitesTemp = JSON.parse(localStorage.getItem("selectedSiteDataWdotCart"));
      let spliceIdex: number;
      withCartSitesTemp.forEach((element, index) => {
        if (element.uniqueId == siteId) {
          spliceIdex = index;
        }
      });
      if (spliceIdex != undefined) {
        withCartSitesTemp.splice(spliceIdex, 1);
        localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(withCartSitesTemp));
      }
    }
  }
}
