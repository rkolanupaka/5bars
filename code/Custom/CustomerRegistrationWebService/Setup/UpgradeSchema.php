<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\CustomerRegistrationWebService\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
	/**
	 * {@inheritdoc}
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('customer_entity_ponumber')) {
			$table = $installer->getConnection()->newTable(
					$installer->getTable('customer_entity_ponumber')
					)
					->addColumn(
							'po_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[
									'identity' => true,
									'nullable' => false,
									'primary'  => true,
									'unsigned' => true,
							],
							'PO ID'
							)
							->addColumn(
									'customer_id',
									\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
									1,
									[],
									'Customer ID'
									)
									->addColumn(
											'po_number',
											\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
											255,
											['nullable => false'],
											'PO Number'
											)
											->addColumn(
													'comment',
													\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
													255,
													['nullable => false'],
													'Comment'
													)
													->setComment('PO Number Table');
													
													$installer->getConnection()->createTable($table);
													
													$installer->getConnection()->addIndex(
															$installer->getTable('customer_entity_ponumber'),
															$setup->getIdxName(
																	$installer->getTable('customer_entity_ponumber'),
																	['comment','po_number'],
																	\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
																	),
															['comment','po_number'],
															\Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
															);
		}
		
		$setup->getConnection()->addColumn(
				$setup->getTable('customer_entity_project_contact'),
				'company',
				[
						'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						'length' => 255,
						'comment' =>'Site Contact Company'
				]
				);
		
		
		if (!$installer->tableExists('security_questions')){
			$table = $installer->getConnection()->newTable(
					$installer->getTable('security_questions')
					)
					->addColumn(
							'q_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[
									'identity' => true,
									'nullable' => false,
									'primary'  => true,
									'unsigned' => true,
							],
							'Question ID'
							)
							->addColumn(
									'set_id',
									\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
									1,
									[],
									'Set ID'
									)
									->addColumn(
											'question',
											\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
											255,
											['nullable => false'],
											'Question'
											)
											
											->setComment('Security Questions Table');
											$installer->getConnection()->createTable($table);
		}
		
		
		if (!$installer->tableExists('security_check_user_mapping')){
			$table = $installer->getConnection()->newTable(
					$installer->getTable('security_check_user_mapping')
					)
					->addColumn(
							'id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[
									'identity' => true,
									'nullable' => false,
									'primary'  => true,
									'unsigned' => true,
							],
							'ID'
							)
							->addColumn(
									'user_id',
									\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
									null,
									[
											'unsigned' => true,
											'nullable' => false,
									],
									'User ID'
									)
									->addColumn(
											'question_id',
											\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
											null,
											[	
												'unsigned' => true,
												'nullable' => false,
													
											],
											'Question ID'
											)
											->addColumn(
													'answer',
													\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
													255,
													['nullable => false'],
													'Answer'
													)
													
															->setComment('Security Check User Mapping Table');
															
															$installer->getConnection()->createTable($table);
		}
		$setup->getConnection()->addForeignKey(
				$setup->getFkName('security_check_user_mapping','question_id','security_questions','q_id'),
				$setup->getTable('security_check_user_mapping'),
				'question_id',
				$setup->getTable('security_questions'),
				'q_id'
				);
		$setup->getConnection()->addForeignKey(
				$setup->getFkName('security_check_user_mapping','user_id','customer_entity','entity_id'),
				$setup->getTable('security_check_user_mapping'),
				'user_id',
				$setup->getTable('customer_entity'),
				'entity_id'
				);
		$installer->endSetup();
	}
}
