<?php

namespace Custom\SiteManagement\Api;

use Custom\SiteManagement\Api\SiteRequestsRespDataInterface;

class SiteRequestsRespData implements SiteRequestsRespDataInterface{
	
	/**
	 *
	 * @var Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	protected $requestedSites;
	
	
	/**
	 *
	 * @var Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	protected $duplicateSites;
	
	
	/**
	 *
	 * @var Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	protected $requestPendingSites;
	
	/**
	 * Method to save the Original Site Details requested by User
	 *
	 * @api
	 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $requestedSites
	 * @return void
	 */
	public function setRequestedSites($requestedSites){
		$this->requestedSites = $requestedSites;
	}
	
	
	/**
	 * @api
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getRequestedSites(){
		return $this->requestedSites;
	}
	
	
	/**
	 * Method to save the Duplicate Site Details requested by User
	 *
	 * @api
	 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $duplicateSites
	 * @return void
	 */
	public function setDuplicateSites($duplicateSites){
		$this->duplicateSites = $duplicateSites;
	}
	
	
	/**
	 * @api
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getDuplicateSites(){
		return $this->duplicateSites;
	}
	
	
	/**
	 *  Method to save the Request Pending Site Details requested by User
	 *
	 * @api
	 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $requestPendingSites
	 * @return void
	 */
	public function setRequestPendingSites($requestPendingSites){
		$this->requestPendingSites = $requestPendingSites;
	}
	
	
	/**
	 * @api
	 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
	 */
	public function getRequestPendingSites(){
		return $this->requestPendingSites;
	}
}