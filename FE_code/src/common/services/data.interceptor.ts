import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse }
  from '@angular/common/http';

import { Observable, of } from 'rxjs';

import {tap} from 'rxjs/operators';

@Injectable()
export class HttpDataInterceptor implements HttpInterceptor {

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(tap(res => {}, (err:any) => {
        if (err instanceof HttpErrorResponse) {
            throw (err.error);
        }
    }));

  }
}