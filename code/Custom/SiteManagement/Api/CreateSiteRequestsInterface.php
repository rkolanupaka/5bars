<?php

namespace Custom\SiteManagement\Api;

interface CreateSiteRequestsInterface{
	
	/**
	 * @api
	 * @param string $requestId
	 * @return void
	 */
	public function setRequestId($requestId);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRequestId();
	
	/**
	 * @api
	 * @param string $customerId
	 * @return void
	 */
	public function setCustomerId($customerId);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCustomerId();
	
	
	/**
	 * @api
	 * @param \Magento\Catalog\Api\Data\ProductInterface $product
	 * @return string|null
	 */
	public function setProduct($product);
	
	
	/**
	 * @api
	 * @return \Magento\Catalog\Api\Data\ProductInterface|null
	 */
	public function getProduct();
	
	/**
	 * @api
	 * @param string $rejectedReason
	 * @return void
	 */
	public function setRejectedReason($rejectedReason);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRejectedReason();
}