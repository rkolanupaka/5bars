<?php

namespace Custom\SiteManagement\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface StateCitiesInterface
{
	/**
	 * Get attribute.
	 *
	 * @return string
	 */
	public function getState();
	
	/**
	 * Set attribute.
	 *
	 * @param string $state
	 * @return $this
	 */
	public function setState($state);
	
	/**
	 * Get attribute.
	 *
	 * @return string
	 */
	public function getStateCode();
	
	/**
	 * Set attribute.
	 *
	 * @param string $stateCode
	 * @return $this
	 */
	public function setStateCode($stateCode);
	
	/**
	 * Get attribute.
	 *
	 * @return array
	 */
	public function getCities();
	
	/**
	 * Set attribute.
	 *
	 * @param array $cities
	 * @return $this
	 */
	public function setCities($cities);
	
	/**
	 * Get attribute.
	 *
	 * @return array
	 */
	public function getCitiesWithoutInventory();
	
	/**
	 * Set attribute.
	 *
	 * @param array $citiesWithoutInventory
	 * @return $this
	 */
	public function setCitiesWithoutInventory($citiesWithoutInventory);
	
}
