<?php
namespace Custom\CustomerRegistrationWebService\Api;

interface SecurityQuestionsDataInterface {
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]|null
	 */
	public function getSet1();
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[] $set1
	 * @return void
	 */
	public function setSet1($set1);
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]|null
	 */
	public function getSet2();
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[] $set2
	 * @return void
	 */
	public function setSet2($set2);
	
	/**
	 * @api
	 * @return Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[]|null
	 */
	public function getSet3();
	
	/**
	 * @api
	 * @param Custom\CustomerRegistrationWebService\Api\SecurityQuestionsInterface[] $set3
	 * @return void
	 */
	public function setSet3($set3);
}