import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { CookieService } from "ngx-cookie";
import { DataService } from "../../common/services/data.service";
import {FormControl} from '@angular/forms';

import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: "site-selex-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"]
})
export class PaymentComponent implements OnInit {
  fiveBarsToken: string;
  hostValue: string;
  isTermsAcceptedOne: boolean = false;
  isTermsAcceptedTwo: boolean = false;
  loggedUserData: any;
  parseInt = parseInt;
  // projectcontactdropdownInvalid: boolean = false;
  waitListStringArray: any = [];
  poNumberControl = new FormControl();
  // options: string[] = ['One', 'Two', 'Three'];
  paymentData: any = {
    paymentType: "procurement",
    isPoForAllSelected: true,
    ponumberForAllSites: ""
  };
  projectContactData: any = [];
  filteredponumberList: Observable<any[]>;
  ponumberlist: any = [];
  // subLicLink: string = "";
  subLicLinkArr:any = [];
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  showLoader: boolean = false;
  sitedataInPayment: any[] = [];
  selectedProjectedContact = "";
  showNewContactForm: boolean = false;
  totalsitecountdata = {
    reserveSiteCount: 0,
    buildSiteCount: 0,
    totalAmount: 0
  };
  totalRentLeaseFee: number = 0;
  totalOneTimeFee: number = 0;

  // options: string[] = ['One', 'Two', 'Three'];

  constructor(private dataservice: DataService, private _cookieService: CookieService, private router: Router) {
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    this.hostValue = this.dataservice.getHostName();
    this.poNumberControl.disable();
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    if (localStorage.getItem("selectedSiteDataWidCart") != undefined) {
      let tempSitedataInPayment = JSON.parse(localStorage.getItem("selectedSiteDataWidCart"));
      tempSitedataInPayment.forEach(element => {
        if(this.subLicLinkArr.indexOf(element.state+'_'+element.city) == -1) {
          this.subLicLinkArr.push(element.state+'_'+element.city)
        }
      });
      console.log(this.subLicLinkArr);
      this.sitedataInPayment = tempSitedataInPayment.filter(item => item.reserved == false);
      this.sitedataInPayment.forEach(element => {
        this.totalRentLeaseFee += parseInt(element.rent_lease_fee);
        this.totalOneTimeFee += parseInt(element.one_time_fee_price);
      });
      this.waitListStringArray = tempSitedataInPayment.filter(item => item.reserved == true);
      this.filteredponumberList = this.poNumberControl.valueChanges
      .pipe(
        startWith(''),
        map(value => value ? this._filterPo(value): this.ponumberlist.slice())
      );
    }
  }

  private _filterPo(value: string): any {
    const filterValue = value.toLowerCase();
    return this.ponumberlist.filter(option => option.po_number.toLowerCase().includes(filterValue));
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  receivedUserData($event) {
    this.loggedUserData = $event;
    this.loadPoAndPcDetails();
  }

  loadPoAndPcDetails() {
    let url = this.hostValue + "/magento/index.php/rest/default/V1/customer/setup/" + this.loggedUserData.id;
    let headerDect = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe(
      (data:any) => {
        this.showLoader = false;
        this.ponumberlist = data.po_number.items;
        this.projectContactData = data.project_contact.items;
      },
      err => {
        this.showLoader = false;
        if (err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage =
            "Something went wrong while loading your PO numbers and Project Contacts. Projact Contact is mandatory for booking. So, try reloading the page. If you see this error again for more than two times please login again.";
        }
        this.showPopUpTrue = true;
      }
    );
  }

  typeaheadListFormat(data: any): string {
    return data.po_number;
  }

  typeaheadValueFormat(data: any): string {
    return data.po_number;
  }

  // onPoSelectChange() {
  //   if (this.selectedProjectedContact == "") {
  //     this.projectcontactdropdownInvalid = true;
  //   } else {
  //     this.projectcontactdropdownInvalid = false;
  //   }
  // }

  reserveSite(reserveSiteForm) {
    // payment info form submit function
    let paymentOption = reserveSiteForm.paymentRadio;
    let paymentMethodObj = {};
    if (paymentOption == "ponumber") {
      paymentMethodObj = {
        method: "purchaseorder",
        po_number: this.poNumberControl.value.trim()
      };
    } else {
      paymentMethodObj = {
        method: "checkmo"
      };
    }
    //localStorage.setItem('paymentOptionDetails', JSON.stringify(paymentMethodObj));
    this.confirmationSucessFunc(paymentMethodObj, reserveSiteForm);
  }

  confirmationSucessFunc(paymentMethodObj, reserveSiteForm) {
    //final payment data submit api call
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    let data = {
      site_contact_name: reserveSiteForm.projectcontactdropdown.name,
      site_contact_email: reserveSiteForm.projectcontactdropdown.email,
      site_contact_company: reserveSiteForm.projectcontactdropdown.company,
      site_contact_phone: reserveSiteForm.projectcontactdropdown.phone,
      billing_address: {
        email: this.loggedUserData.email,
        region:
          this.loggedUserData.addresses[0].region.region != undefined
            ? this.loggedUserData.addresses[0].region.region
            : "",
        region_id: 
          this.loggedUserData.addresses[0].region.region_id != undefined
          ? this.loggedUserData.addresses[0].region.region_id
          : "",
        region_code:
          this.loggedUserData.addresses[0].region.region_code != undefined
            ? this.loggedUserData.addresses[0].region.region_code
            : "",
        country_id: "US",
        street: [
          this.loggedUserData.addresses[0].street[0] != undefined ? this.loggedUserData.addresses[0].street[0] : "",
          this.loggedUserData.addresses[0].street[1] != undefined ? this.loggedUserData.addresses[0].street[1] : ""
        ],
        postcode:
          this.loggedUserData.addresses[0].postcode != undefined ? this.loggedUserData.addresses[0].postcode : "",
        city: this.loggedUserData.addresses[0].city != undefined ? this.loggedUserData.addresses[0].city : "",
        telephone:
          this.loggedUserData.addresses[0].telephone != undefined ? this.loggedUserData.addresses[0].telephone : "",
        firstname: this.loggedUserData.firstname,
        lastname: this.loggedUserData.lastname
      },
      paymentMethod: paymentMethodObj
    };
    let url = this.hostValue + "/magento/rest/default/V1/carts/mine/payment-information";
    this.showLoader = true;
    this.dataservice.fetchPostData(url, data, headerDict).subscribe(
      data => {
        this.showLoader = false;
        document.cookie = "5barsBookingId=" + data;
        this.router.navigate(["site/confirmation"]);
      },
      err => {
        this.showLoader = false;
        if (err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "something went wrong...";
        }
        this.showPopUpTrue = true;
      }
    );
  }

  addNewContact(addNewContactForm) {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    let newContactData = {
      customerId: this.loggedUserData.id,
      name: addNewContactForm.newContactName,
      email: addNewContactForm.newContactEmail,
      company: addNewContactForm.newContactCompany,
      phone: addNewContactForm.newContactPhone
    };
    let url = this.hostValue + "/magento/index.php/rest/default/V1/customer/project/contact";
    this.showLoader = true;
    this.dataservice.fetchPostData(url, newContactData, headerDict).subscribe(
      data => {
        this.showLoader = false;
        this.projectContactData.push(newContactData);
        this.selectedProjectedContact = this.projectContactData[this.projectContactData.length - 1];
        // this.projectcontactdropdownInvalid = false;
      },
      err => {
        this.showLoader = false;
        if (err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "An error occured. Please try again.";
        }
        this.showPopUpTrue = true;
      }
    );
    this.showNewContactForm = false;
  }
}
