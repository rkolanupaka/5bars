<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\SiteManagement\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$installer = $setup;
    	$installer->startSetup();
    	
    	// Creation of master product dump table
    	
    	if (!$installer->tableExists('site_requests')) {
    		$table = $installer->getConnection()->newTable(
    				$installer->getTable('site_requests')
    				)
    				->addColumn(
    						'id',
    						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    						null,
    						[
    								'identity' => true,
    								'nullable' => false,
    								'primary'  => true,
    								'unsigned' => true,
    						],
    						'ID'
    				)
    				->addColumn(
    					'requestid',
    					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    					255,
    					['nullable => false'],
    					'Request ID'
    				)
    				->addColumn(
    						'longitude',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'Longitude'
    				)
    				->addColumn(
    						'latitude',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'Latitude'
    				)
    				->addColumn(
    						'city',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'City'
    				)
    				->addColumn(
    						'state',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'State'
    				)
    				->addColumn(
    						'city_area',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'City Area'
    				)
    				->addColumn(
    						'pole_class',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'Pole Class'
    				)
					->addColumn(
						'created_timestamp',
						\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
						'Created Timestamp'
					)
					->addColumn(
						'updated_timestamp',
						\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
						'Created Timestamp'
					)
					->addColumn(
							'status',
							\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
							45,
							['nullable => false'],
							'status'
							)
    				->addIndex(
    				    $setup->getIdxName(
    				        $installer->getTable('site_requests'),
    				        ['id', 'requestid'],
    				        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
    				        ),
    				    ['id', 'requestid'],
    				    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
    				)
    				->setComment('Site Requests');
    		
    				$installer->getConnection()->createTable($table);
    								
    	}
    	
    	$installer->endSetup();
    }
}
