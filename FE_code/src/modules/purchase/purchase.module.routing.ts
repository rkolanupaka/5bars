import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "../../components/home/home.component";
import { PaymentComponent } from "../../components/payment/payment.component";
import { ConfirmationComponent } from "../../components/confirmation/confirmation.component";
import { SiteSearchComponent } from "../../components/site-search/site-search.component";
import { WaitListSitesComponent } from "../../components/wait-list-sites/wait-list-sites.component";

const routes: Routes = [
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "payment",
    component: PaymentComponent
  },
  {
    path: "confirmation",
    component: ConfirmationComponent
  },
  {
    path: "site-search",
    component: SiteSearchComponent
  },
  {
    path: "wait-list",
    component: WaitListSitesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseRouterModule {}
