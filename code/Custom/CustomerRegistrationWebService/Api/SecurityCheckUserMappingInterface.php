<?php

namespace Custom\CustomerRegistrationWebService\Api;

interface SecurityCheckUserMappingInterface{
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getId();
	
	/**
	 * @api
	 * @param int $id
	 * @return void
	 */
	public function setId($id);
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getQuestionId();
	
	/**
	 * @api
	 * @param int $questionId
	 * @return void
	 */
	public function setQuestionId($questionId);
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getUserId();
	
	/**
	 * @api
	 * @param int $userId
	 * @return void
	 */
	public function setUserId($userId);
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSetId();
	
	/**
	 * @api
	 * @param int $setId
	 * @return void
	 */
	public function setSetId($setId);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getAnswer();
	
	/**
	 * @api
	 * @param string $answer
	 * @return void
	 */
	public function setAnswer($answer);
}