<?php

namespace Custom\SiteManagement\Api;
use Custom\SiteManagement\Api\SiteRequestsDataInterface;
/**
 * @since 100.0.2
 */

interface SiteRequestsInterface{
	
		/**
		 * Method to save the Site Details requested by User
		 * 
		 * @api
		 * @param Custom\SiteManagement\Api\SiteRequestsDataInterface[] $sites
		 * @return Custom\SiteManagement\Api\SiteRequestsRespDataInterface
		 */
	public function saveSiteRequets($sites);
	
		/**
		 * Method to get the details of the Sites Requested by User
		 * 
		 * @api
		 * @param string $startDate
		 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
		 */
	public function getSiteRequests($startDate);
	
		/**
		 * Method to generate Sku automatically basing on the State and City
		 * 
		 * @api
		 * @param string $city
		 * @param string $state
		 * @return string
		 */
	public function generateSku($state,$city);
	
		/**
		 * Method to call the AutoSku function and Saving the Product to Inventory and Sending Notification Mail after Admin Approval requested by Customer
		 *
		 * @api
		 * @param \Custom\SiteManagement\Api\CreateSiteRequestsInterface $siteRequestedInfo
		 * @return null
		 */
	public function approveSiteRequests($siteRequestedInfo);
	
	
		/**
		 * Method to Reject the Site Requested by Customer and send the Notification Mail to Customer
		 *
		 * @api
		 * @param \Custom\SiteManagement\Api\CreateSiteRequestsInterface $siteRequestedInfo
		 * @return null
		 */
	public function rejectSiteRequests($siteRequestedInfo);
	
	
	
		/**
		 * Method to get the siteDetails of the Sites requested w.r.t that customer
		 *
		 * @api
		 * @param int $customerId
		 * @return Custom\SiteManagement\Api\SiteRequestsDataInterface[]
		 */
	public function getRequestedSitesforCustomer($customerId);
	
}