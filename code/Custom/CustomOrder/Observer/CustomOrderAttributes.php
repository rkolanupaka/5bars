<?php
    namespace Custom\CustomOrder\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\ObjectManager;
    use Magento\Framework\App\ResourceConnection;
    	
    class CustomOrderAttributes implements ObserverInterface
    {
        /**
         * @var \Psr\Log\LoggerInterface
         */
        protected $logger;
        
        /**
         * @var OrderExtensionFactory
         */
        private $orderExtensionFactory;
        
        /**
         * @var OrderItemExtensionFactory
         */
        private $orderItemExtensionFactory;
        
        /**
         * @var ResourceConnection
         */
        private $resourceConnection;
        
        /**
         * Constructor
         * 
         * @param \Psr\Log\LoggerInterface LoggerInterface
         * @param \Magento\Sales\Api\Data\OrderExtensionFactory|null $orderExtensionFactory
         * @param \Magento\Sales\Api\Data\OrderItemExtensionFactory|null $orderItemExtensionFactory
         */
        public function __construct(
            \Psr\Log\LoggerInterface $logger,
            \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory = null,
            \Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory = null,
            \Magento\Framework\App\ResourceConnection $resourceConnection
            ) 
        {
            $this->logger = $logger;
            $this->orderExtensionFactory = $orderExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderExtensionFactory::class);
            $this->orderItemExtensionFactory = $orderItemExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderItemExtensionFactory::class);
            $this->resourceConnection = $resourceConnection;
        }
        
		public function execute(\Magento\Framework\Event\Observer $observer) 
		{
			$this->logger->info('Executing Custom Order Attributes');
			
		    $order  = $observer->getOrder();
            
            $extensionAttributes = $order->getExtensionAttributes();
			
			if ($extensionAttributes === null) {
			    $extensionAttributes = $this->orderExtensionFactory->create();
			}

			//set site contact details
			$siteContactName = $order->getData('site_contact_name');
			$extensionAttributes->setSiteContactName($siteContactName);
			
			$siteContactEmail = $order->getData('site_contact_email');
			$extensionAttributes->setSiteContactEmail($siteContactEmail);
			
			$siteContactPhone = $order->getData('site_contact_phone');
			$extensionAttributes->setSiteContactPhone($siteContactPhone);
			
			$siteContactCompany = $order->getData('site_contact_company');
			$extensionAttributes->setSiteContactCompany($siteContactCompany);
			
			$monthlyChargesTotal = $order->getData('monthly_charges_total');
			$extensionAttributes->setMonthlyChargesTotal($monthlyChargesTotal);
			
			$order->setExtensionAttributes($extensionAttributes);
			
			//set custom item attributes
			$items = $order->getItems();
			
			foreach ($items as $item)
			{
			    $itemExtensionAttributes = $item->getExtensionAttributes();
			    
			    if ($itemExtensionAttributes === null) {
			        $itemExtensionAttributes = $this->orderItemExtensionFactory->create();
			    }
			    
			    $itemExtensionAttributes->setMonthlyCharges($item->getMonthlyCharges());
			    $itemExtensionAttributes->setCamFee($item->getCamFee());
			    $itemExtensionAttributes->setSiteSelectionFee($item->getSiteSelectionFee());
			    $itemExtensionAttributes->setSiteSurveyLeaseFee($item->getSiteSurveyLeaseFee());
			    $itemExtensionAttributes->setRentLeaseFee($item->getRentLeaseFee());
			    $itemExtensionAttributes->setCityArea($item->getCityArea());
			    $itemExtensionAttributes->setPoleClass($item->getPoleClass());
			    $itemExtensionAttributes->setLatitude($item->getLatitude());
			    $itemExtensionAttributes->setLongitude($item->getLongitude());
			    $itemExtensionAttributes->setState($item->getState());
			    $itemExtensionAttributes->setCity($item->getCity());
			    if(($item->getReasonCode()) != null){
			    	$itemExtensionAttributes->setReasonCode($item->getReasonCode());
			    }
			    
			    $item->setExtensionAttributes($itemExtensionAttributes);
			    
			    $item->save();
			}
			
			$order->save();
			return $this;
        }
    }