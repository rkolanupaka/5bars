<?php 


namespace Custom\SiteManagement\Model;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Webapi\Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\App\Request\Http;
use Custom\SiteManagement\Api\SiteManagementInterface;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\State\InputMismatchException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Defines the implementation class of the Customer service contract.
 */
class SiteManagement extends \Magento\Framework\App\Action\Action implements SiteManagementInterface
{
    const CITY_ID = "city_id";
     /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;
    
    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $addressFactory;
    
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagementInterface;
    
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepositoryInterface;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Model\PONumberFactory
     */
    protected $poNumberFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Model\ProjectContactFactory
     */
    protected $projectContactFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterfaceFactory
     */
    protected $poNumberResultFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterfaceFactory
     */
    protected $projectContactResultFactory;
    
    /**
     * @var \Magento\Integration\Api\CustomerTokenServiceInterface
     */
    protected $customerTokenServiceInterface;
    
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;
    
    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagementInterface;
    
    /**
     * @var \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    
    /**
     * @var \Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterfaceFactory
     */
    protected $setUpProfileResultFactory;
    
    /**
     * 
     * @var \DateTime
     */
    protected $date;
    
    /**
     * 
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timeZone;
    
    /**
     * 
     * @var \Magento\Sales\Model\Order
     */
    protected $order;
    
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    
    /**
     * 
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $requestIterface;
    
    /**
     * @var \Custom\SiteManagement\Model\StatesFactory
     */
    protected $statesFactory;
    
    /**
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var \Custom\SiteManagement\Api\Data\StatesResultInterfaceFactory
     */
    protected $statesResultFactory;
    
    /**
     * @var \Custom\SiteManagement\Model\CitiesFactory
     */
    protected $citiesFactory;
    
    /**
     * @var \Custom\SiteManagement\Api\Data\CitiesResultInterfaceFactory
     */
    protected $citiesResultFactory;
    
    /**
     * @var \Custom\SiteManagement\Api\Data\StateWithCitiesResultInterfaceFactory
     */
    protected $stateWithCitiesResultInterfaceFactory;
    
    /**
     * @var \Custom\SiteManagement\Api\Data\StateCitiesInterfaceFactory
     */
    protected $stateCitiesInterfaceFactory;
    
    
    
    
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterfaceFactory
     */
    protected $productRepositoryInterfaceFactory;
    
    
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;
    
    
    /**
     * @param \Magento\Framework\App\Action\Context      $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Customer\Api\AccountManagementInterface $accountManagementInterface
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface
     * @param \Custom\CustomerRegistrationWebService\Model\PONumberFactory $poNumberFactory
     * @param \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterfaceFactory $poNumberResultFactory
     * @param \Custom\CustomerRegistrationWebService\Model\ProjectContactFactory $projectContactFactory
     * @param \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterfaceFactory $projectContactResultFactory
     * @param \Magento\Integration\Api\CustomerTokenServiceInterface
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagementInterface
     * @param \Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterfaceFactory $setUpProfileResultFactory
     * @param \DateTime $date;
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\App\RequestInterface $requestIterface
     * 
     * @param \Custom\SiteManagement\Model\StatesFactory $statesFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Custom\SiteManagement\Api\Data\StatesResultInterfaceFactory $statesResultFactory
     * @param \Custom\SiteManagement\Model\CitiesFactory $citiesFactory
     * @param \Custom\SiteManagement\Api\Data\CitiesResultInterfaceFactory $citiesResultFactory
     * @param \Custom\SiteManagement\Api\Data\StateWithCitiesResultInterfaceFactory $stateWithCitiesResultInterfaceFactory
     * @param \Custom\SiteManagement\Api\Data\StateCitiesInterfaceFactory $stateCitiesInterfaceFactory
     * @param \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
     * @param \Magento\Catalog\Model\Product $product
     */
    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
                                \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
                                \Magento\Customer\Model\AddressFactory $addressFactory,
    							\Magento\Customer\Api\AccountManagementInterface $accountManagementInterface,
    							\Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
    							\Custom\CustomerRegistrationWebService\Model\PONumberFactory $poNumberFactory,
    							\Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterfaceFactory $poNumberResultFactory,
    							\Custom\CustomerRegistrationWebService\Model\ProjectContactFactory $projectContactFactory,
    							\Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterfaceFactory $projectContactResultFactory,
    							\Magento\Integration\Api\CustomerTokenServiceInterface $customerTokenServiceInterface,
    							\Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
    							\Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
    							\Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory $searchResultsFactory,
    							\Custom\CustomerRegistrationWebService\Api\Data\SetUpProfileResultInterfaceFactory $setUpProfileResultFactory,
    							\DateTime $date,
    							\Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone,
    							\Magento\Sales\Model\Order $order,
    							\Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
					    		\Magento\Framework\App\Request\Http $httpRequest,
    							\Magento\Framework\App\RequestInterface $requestIterface,
    		
    		\Custom\SiteManagement\Model\StatesFactory $statesFactory,
    		\Psr\Log\LoggerInterface $logger,
    		\Custom\SiteManagement\Api\Data\StatesResultInterfaceFactory $statesResultFactory,
    		\Custom\SiteManagement\Model\CitiesFactory $citiesFactory,
    		\Custom\SiteManagement\Api\Data\CitiesResultInterfaceFactory $citiesResultFactory,
    		\Custom\SiteManagement\Api\Data\StateWithCitiesResultInterfaceFactory $stateWithCitiesResultInterfaceFactory,
    		\Custom\SiteManagement\Api\Data\StateCitiesInterfaceFactory $stateCitiesInterfaceFactory,
    		\Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
    		\Magento\Catalog\Model\Product $product
    							) {
        $this->storeManager     = $storeManager;
        $this->customerRepositoryInterface     = $customerRepositoryInterface;
        $this->customerRepository = $customerRepositoryFactory->create();
        $this->addressFactory = $addressFactory->create();
        $this->accountManagementInterface= $accountManagementInterface;
        $this->orderRepositoryInterface = $orderRepositoryInterface;
        $this->poNumberFactory = $poNumberFactory;
        $this->poNumberResultFactory = $poNumberResultFactory;
        $this->projectContactFactory = $projectContactFactory;
        $this->productContactResultFactory = $projectContactResultFactory;
        $this->customerTokenServiceInterface = $customerTokenServiceInterface;
        $this->quoteRepository = $quoteRepository;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->setUpProfileResultFactory = $setUpProfileResultFactory;
        $this->date = $date;
        $this->timeZone = $timeZone;
        $this->order = $order;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->requestIterface = $requestIterface;
        
        $this->statesFactory = $statesFactory;
        $this->logger = $logger;
        $this->statesResultFactory = $statesResultFactory;
        $this->citiesFactory = $citiesFactory;
        $this->citiesResultFactory = $citiesResultFactory;
        $this->stateWithCitiesResultInterfaceFactory = $stateWithCitiesResultInterfaceFactory;
        $this->stateCitiesInterfaceFactory = $stateCitiesInterfaceFactory;
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->product= $product;
        parent::__construct($context);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\SiteManagement\Api\SiteManagementInterface::getList()
     */
    public function getList()
    {
    	$items = array();
    	$states = $this->statesFactory->create();
    	$statesresult = $this->statesResultFactory->create();
    	$statesList = $states->getCollection();
    	foreach ($statesList as $item){
    		$items[] = $item->getData();
    	}
    	$statesresult->setItems($items);
    	return $statesresult;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\SiteManagement\Api\SiteManagementInterface::getList()
     */
    public function getCities($stateCode)
    {
    	$items = array();
    	$cities = $this->citiesFactory->create();
    	$citiesResult = $this->citiesResultFactory->create();
    	$citiesList = $cities->getCollection()->addFieldToFilter('state_code',array('eq'=> $stateCode));
    	foreach ($citiesList as $item){
    		$items[] = $item->getData();
    	}
    	$citiesResult->setItems($items);
    	return $citiesResult;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\SiteManagement\Api\SiteManagementInterface::getStateCities()
     */
    public function getStateCities()
    {
    	$this->logger->info('Entered state city method');
    	$items = array();
    	$reponse = $this->stateWithCitiesResultInterfaceFactory->create();
    	$states = $this->statesFactory->create();
    	$cities = $this->citiesFactory->create();
    	$statesList = $states->getCollection()->addFieldToFilter('enabled',array('eq' => 1));
    	foreach ($statesList as $state){
    		$stateCityResult = $this->stateCitiesInterfaceFactory->create();
    		$stateCode = $state->getData('state_code');
    		$stateCityResult->setState($state->getData('state'));
    		$stateCityResult->setStateCode($stateCode);
    		$activeCitiesList = $cities->getCollection()->addFieldToSelect('city')
    												  ->addFieldToFilter('state_code',array('eq' => $stateCode))
    												  ->addFieldToFilter('enabled', array('eq' => 1));
    		$comingSoonCitiesList = $cities->getCollection()->addFieldToSelect('city')
    															->addFieldToFilter('state_code',array('eq' => $stateCode))
    															->addFieldToFilter('coming_soon',array('eq' => 1));
    		$stateCityResult->setCities($this->getCitiesArray($activeCitiesList));
    		$stateCityResult->setCitiesWithoutInventory($this->getCitiesArray($comingSoonCitiesList));
    		$this->logger->info('state code'.$stateCityResult->getStateCode());
    		$items[] = $stateCityResult;
    	}
    	$reponse->setItems($items);
    	return $reponse;
    }
    
    protected function getCitiesArray($citiesList){
    	$items = array();
    	foreach ($citiesList as $city){
    		$items[] = $city->getData('city');
    	}
    	return $items;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \Custom\SiteManagement\Api\SiteManagementInterface::updateCity()
     */
    public function updateCity($cityId,$enabled,$comingSoon,$email){
    	$this->logger->info('Entered update city method');
    	$cityModel = $this->citiesFactory->create();
    	if(null != $cityId || !empty($cityId)){
    		$cityModel->load($cityId);
    		$cityModel->setData('enabled',$enabled)
    				  ->setData('coming_soon',$comingSoon)
    				  ->setData('admin_emailID',$email)
    				  ->save();
    	}
    	$this->updateState($cityModel->getData('state_code'),$cityModel);
    	return $cityModel->getData('city_id');
    }
    
    /*
     * This method will disable state in case no enabled cities found for it and vice versa. 
     */
    protected function updateState($stateCode,$cityModel){
    	$this->logger->info('Entered Update State Method with state code '.$stateCode);
    	$citiesList = $cityModel->getCollection()->addFieldToFilter('state_code',array('eq' => $stateCode))
    											 ->addFieldToFilter('enabled', array('eq' => 1));
    	$this->logger->info('Collection Size'.$citiesList->count());
    	if($citiesList->count() <=1){
    		$states = $this->statesFactory->create();
    		$states->load($stateCode);
    		$citiesList->count() == 0 ? $states->setData('enabled',0) : $states->setData('enabled',1);
    		$states->save();
    	}
    }
    
    public function execute()
    {
    	
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \Custom\SiteManagement\Api\SiteManagementInterface::saveSites()
     */
    public function saveSites(\Magento\Catalog\Api\Data\ProductInterface $product){
    	
    		$latitudeToCheck = $product->getCustomAttribute('latitude')->getValue();
    		$longitudeToCheck = $product->getCustomAttribute('longitude')->getValue();
    	
    		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
    		$connection = $resource->getConnection();
    		$tableName = $resource->getTableName('site_requests');
    		$catalogProductEntity = $resource->getTableName('catalog_product_entity');
    		$catalogProductEntityVarchar = $resource->getTableName('catalog_product_entity_varchar');
    		$eavAttribute = $resource->getTableName('eav_attribute');
    		$eavEntityType = $resource->getTableName('eav_entity_type');
    		$catalogProductEntityInt = $resource->getTableName('catalog_product_entity_int');
    		$catalogProductEntityDateTime = $resource->getTableName('catalog_product_entity_datetime');
    		
    		$InventoryCheckSql = "select e.sku,v4.value as latitude,v5.value as longitude FROM ".$catalogProductEntity." e 
								LEFT JOIN ".$catalogProductEntityVarchar." v4 ON e.entity_id = v4.entity_id
								AND v4.store_id = 0
								AND v4.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'latitude'
														AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																				WHERE entity_type_code = 'catalog_product'))
								LEFT JOIN ".$catalogProductEntityVarchar." v5 ON e.entity_id = v5.entity_id
								AND v5.store_id = 0
								AND v5.attribute_id = (SELECT attribute_id FROM ".$eavAttribute." WHERE attribute_code = 'longitude'
														AND entity_type_id = (SELECT entity_type_id FROM ".$eavEntityType."
																				WHERE entity_type_code = 'catalog_product'))
																
				                WHERE v4.value = '".$latitudeToCheck."' AND v5.value = '".$longitudeToCheck."'";
    		
    		$inventoryCheckArray = $connection->query($InventoryCheckSql)->fetch();
    		
    		if(is_null($inventoryCheckArray['sku'])){
    			
    			$siteRequestsCheckSql = "SELECT * from ".$tableName." sr WHERE sr.latitude='".$latitudeToCheck."' and sr.longitude='".$longitudeToCheck."' and sr.status='REQUESTED'";
    			$siteRequestsCheckArray = $connection->query($siteRequestsCheckSql)->fetch();
    			
    			if(is_null($siteRequestsCheckArray['latitude'])){
    			
		    		$productRepositoryInterface = $this->productRepositoryInterfaceFactory->create();
		    		$newSku = $objectManager->get('\Custom\SiteManagement\Model\SiteRequests')->generateSku($product->getCustomAttribute('state')->getValue(),$product->getCustomAttribute('city')->getValue());
		    		$product->setSku($newSku);
		    		$product->setName($newSku);
		    		$product->setAttributeSetId(4);
		    		$product->setPrice(0);
		    		$product->setStockData(array(
		    				'is_in_stock' => 1,
		    				'qty' => 1
		    		));
		    		$product->setTypeId($objectManager->get('Magento\Catalog\Model\Product\Type')::TYPE_VIRTUAL);
		    		return $productRepositoryInterface->save($product);
    			}
    			else{
    				throw new LocalizedException(__('Site with requested coordinates is requested by another customer and is under review by SiteSelex'));
    			}
    		}
    		else{
    			throw new LocalizedException(__('Product already exists with Site ID # '.$inventoryCheckArray['sku']));
    		}
    	
    }
    
    
    /**
     * Saving the Site to Inventory
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\State\InputMismatchException If the provided SKU is already exists
     */
    public function saveSitesToInventory(\Magento\Catalog\Api\Data\ProductInterface $product){
    	
    	if($this->product->getIdBySku($product->getSku())){
    		throw new InputMismatchException(__('product with specified SKU Exists'));
    	}
    	else{
    		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    		
    		$productRepositoryInterface = $this->productRepositoryInterfaceFactory->create();
    		$newSku = $objectManager->get('\Custom\SiteManagement\Model\SiteRequests')->generateSku($product->getCustomAttribute('state')->getValue(),$product->getCustomAttribute('city')->getValue());
    		$product->setSku($newSku);
    		$product->setName($newSku);
    		$product->setAttributeSetId(4);
    		$product->setPrice(0);
    		$product->setStockData(array(
    				'is_in_stock' => 1,
    				'qty' => 1
    		));
    		$product->setTypeId($objectManager->get('Magento\Catalog\Model\Product\Type')::TYPE_VIRTUAL);
    		return $productRepositoryInterface->save($product);
    	}
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \Custom\SiteManagement\Api\SiteManagementInterface::editSite()
     */
    public function editSite(\Magento\Catalog\Api\Data\ProductInterface $product){
    		
	    	//$productRepositoryInterface = $this->productRepositoryInterfaceFactory->create();
	    	//return $productRepositoryInterface->save($product);
    	
    		$productRepositoryInterface = $this->productRepositoryInterfaceFactory->create();
    		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    		$productInfo = $objectManager->get('\Magento\Catalog\Model\ProductRepository')->get($product->getSku());
    		$productInfo->setStoreId(0);
    		if($product->getStatus()){
    			$productInfo->setStatus($product->getStatus());
    		}
    		if($product->getCustomAttribute('reserved')){
    			$productInfo->setCustomAttribute('reserved', $product->getCustomAttribute('reserved')->getValue());
    			if($product->getCustomAttribute('reserved')->getValue()==1){
    				$productInfo->setStockData(array(
    						'is_in_stock' => 0,
    						'qty' => 0
    				));
    			}
    			elseif ($product->getCustomAttribute('reserved')->getValue()==0){
    				$productInfo->setStockData(array(
    						'is_in_stock' => 1,
    						'qty' => 1
    				));
    			}
    		}
    		if($product->getCustomAttribute('pole_class')){
    			$productInfo->setCustomAttribute('pole_class', $product->getCustomAttribute('pole_class')->getValue());
    		}
    		if($product->getCustomAttribute('city_area')){
    			$productInfo->setCustomAttribute('city_area', $product->getCustomAttribute('city_area')->getValue());
    		}
    		if($product->getCustomAttribute('site_survey_lease_fee')){
    			$productInfo->setCustomAttribute('site_survey_lease_fee', $product->getCustomAttribute('site_survey_lease_fee')->getValue());
    		}
    		if($product->getCustomAttribute('rent_lease_fee')){
    			$productInfo->setCustomAttribute('rent_lease_fee', $product->getCustomAttribute('rent_lease_fee')->getValue());
    		}
    		if($product->getCustomAttribute('site_selection_fee')){
    			$productInfo->setCustomAttribute('site_selection_fee', $product->getCustomAttribute('site_selection_fee')->getValue());
    		}
    		
    		$productInfo->save();
    		return $productInfo;
    	
    }
    
}