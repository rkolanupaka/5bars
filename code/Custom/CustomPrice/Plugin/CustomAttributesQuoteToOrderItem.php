<?php
namespace Custom\CustomPrice\Plugin;
 
class CustomAttributesQuoteToOrderItem
{
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);
        $orderItem->setSurveyLeaseSelected($item->getSurveyLeaseSelected());
        
        if($item->getHasConduitRequested() != null){
        	$orderItem->setHasConduitRequested($item->getHasConduitRequested());
        }
        return $orderItem;
    }
}