<?php

namespace Custom\DataSync\Model\ProductConflict;

use Custom\DataSync\Api\Data\ProductConflictInterface;

class ProductConflict extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,ProductConflictInterface
{
    const CACHE_TAG = 'impex_batch';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;
    
    /**
     * @var \Magento\Catalog\Api\Data\ProductInterfaceFactory
     */
    protected $productFactory;
    
    /**
     * Constructor
     *
     * @param \Psr\Log\LoggerInterface LoggerInterface
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory
        )
    {
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
    }
    
    protected function _construct()
    {
        $this->_init('\Custom\DataSync\Model\ResourceModel\ProductConflict');
    }
    
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    public function getDefaultValues()
    {
        $values = [];
        
        return $values;
    }
    
    public function createQuery($record, $transaction)
    {
        $this->logger->info("ProductConflict | createQuery");
        $sql = "Insert into magento.product_conflicts (sku, order_id) values (";
        $sql = $sql.$record.")";
        $this->logger->info("ProductConflict | createQuery | ::: ".$sql);
        return $sql;
    }
    
    public function saveSalesProduct($connection, $transaction, $conflictRecords)
    {
        $this->logger->info("ProductConflict | saveSalesProduct");
        try
        {
            for($count = 0; $count < sizeof($conflictRecords); $count++)
            {
                $sql = $this->createQuery($conflictRecords[$count], $transaction);
                $connection->query($sql);
                
                $product = $this->productFactory->create();
                $sku = substr($conflictRecords[$count],1,9);
                $product->load($product->getIdBySku($sku));
                $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
                $this->productRepository->save($product);
            }
        }
        catch (\Exception $e)
        {
            $this->logger->info("ProductConflict | saveSalesProduct | Exception While Inserting Product Conflicts::: ".$e->getMessage());
        }
    }
    
    public function saveConflictProductDetails($connection, $transaction, $conflictRecords)
    {
        $this->logger->info("ProductConflict | saveConflictProductDetails");
        $this->saveSalesProduct($connection, $transaction, $conflictRecords);
    }

}