import {
  Component,
  OnInit,
  Input,
  Output,
  OnDestroy,
  OnChanges,
  SimpleChanges,
  EventEmitter,
  HostListener
} from "@angular/core";
import { Router } from "@angular/router";

import { CookieService } from "ngx-cookie";
import { DataService } from "../../common/services/data.service";

@Component({
  selector: "app-header-new",
  templateUrl: "./header-new.component.html",
  styleUrls: ["./header-new.component.scss"]
})
export class HeaderNewComponent implements OnInit {
  @Output() loggedUserDetails = new EventEmitter<any>();
  @Input() sitesZeroCartCount: number;

  customerId: string;
  cartAvailable: boolean = false;
  fiveBarsToken: string;
  hostValue: string;
  idealTime: number = 0;
  // openNavdd = false;
  // openNavddMobile = false;
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  showLoader: boolean = false;
  // sideNavVisible = false;
  userName: string;
  validUserForReports: boolean = false;
  domainUser: string = "";

  private timer: any;

  openNav: boolean = false;

  constructor(private dataservice: DataService, private _cookieService: CookieService, private router: Router) {
    this.timer = setInterval(() => {
      this.timerIncrement();
    }, 1000 * 60);
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.cartAvailable = false;
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  ngOnInit() {
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    let userDetails = localStorage.getItem("fivebarsUserData");
    this.hostValue = this.dataservice.getHostName();
    // if(this.fiveBarsToken) {
    if (userDetails == undefined) {
      let url = this.hostValue + "/magento/index.php/rest/V1/customers/me";
      let headerDect = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      this.dataservice.fetchData(url, headerDect).subscribe(
        (data: any) => {
          let securityQueCheck = false;
          (data.custom_attributes).forEach((element) => {
            if(element.attribute_code == 'security_info_updated') {
              if(element.value == "1") {
                securityQueCheck = true;
              }
            }
          });

          if(data.addresses.length > 0) {
            localStorage.setItem("fivebarsUserData", JSON.stringify(data));
          }
          
          if (data.addresses.length > 0 && securityQueCheck) {
            this.getUserDetailsSuccess(data);
          } else {
            this.getUserDetailsSuccess(data);
            this.router.navigate(["profile"]);
          }
        },
        err => {
          if (err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please login again";
          }
          this.showPopUpTrue = true;
          setTimeout(() => {
            this.router.navigate([""]);
          }, 4000);
        }
      );
    } else {
      this.getUserDetailsSuccess(JSON.parse(localStorage.getItem("fivebarsUserData")));
    }

    if (
      localStorage.getItem("selectedSiteDataWidCart") &&
      JSON.parse(localStorage.getItem("selectedSiteDataWidCart")).length > 0
    ) {
      this.cartAvailable = true;
    }

    // } else {
    //   console.log('you are not logged in');
    //   this.router.navigate(['']);
    // }
  }

  @HostListener("document:mousemove", ["$event"])
  mousemove(event) {
    this.idealTime = 0;
  }

  @HostListener("document:keydown", ["$event"])
  keydown(event) {
    this.idealTime = 0;
  }

  timerIncrement() {
    // logout user after 15 mintutes of inactivity
    this.idealTime += 1;
    if (this.idealTime > 14) {
      this.showPopUpMessage = "Your session has expired. Please login again.";
      this.showPopUpTrue = true;
      setTimeout(() => {
        this.logout();
      }, 3000);
    }
  }

  getUserDetailsSuccess(data) {
    this.customerId = data.id;
    this.loggedUserDetails.emit(data);

    (data.custom_attributes).forEach((element) => {
      if(element.attribute_code == 'is_admin') {
        if(element.value == "1") {
          this.validUserForReports = true;
        }
      }
    });

    this.userName = data.firstname.substr(0, 1) + data.lastname.substr(0, 1);
    let emailSplitArray = data.email.split("@");
    emailSplitArray = emailSplitArray[emailSplitArray.length - 1].toLowerCase();
    // if (emailSplitArray == "xgcommunities.com" || emailSplitArray == "deloitte.com") {
    //   this.validUserForReports = true;
    // }
    this.domainUser = emailSplitArray.substring(0, emailSplitArray.indexOf("."));
  }

  logout() {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    let url = this.hostValue + "/magento/index.php/rest/default/V1/customer/revoke/" + this.customerId;
    this.showLoader = true;
    this.dataservice.fetchPostData(url, headerDict).subscribe(
      data => {
        this.showLoader = false;
        // if(data == true) {
        localStorage.clear();
        this._cookieService.removeAll();
        this.router.navigate([""]);
        // } else {
        //   alert("Could not Logout. Please try again.");
        // }
      },
      err => {
        this.showLoader = false;
        localStorage.clear();
        this._cookieService.removeAll();
        // this.showPopUpMessage = err.message;
        // this.showPopUpTrue = true;
        this.router.navigate([""]);
        // alert("Could not Logout. Please try again.");
      }
    );
  }

  navigateToPayment() {
    if (this.cartAvailable) {
      this.router.navigate(["site/payment"]);
    }
  }

  scrollTop() {
    window.scrollTo(0, 0);
  }
}
