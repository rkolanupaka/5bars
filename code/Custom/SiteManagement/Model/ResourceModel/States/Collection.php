<?php
namespace Custom\SiteManagement\Model\ResourceModel\States;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Custom\SiteManagement\Model\States', 'Custom\SiteManagement\Model\ResourceModel\States');
    }

}