<?php

namespace Custom\Reports\Api;

interface MapViewSearchDataInterface{
	/**
	 * @api
	 * @return string|null
	 */
	public function getSku();
	
	/**
	 * @api
	 * @param string $sku
	 * @return void
	 */
	public function setSku($sku);
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLat();
	
	/**
	 * @api
	 * @param float $lat
	 * @return void
	 */
	public function setLat($lat);
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLong();
	
	/**
	 * @api
	 * @param float $long
	 * @return void
	 */
	public function setLong($long);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getsChk();
	
	/**
	 * @api
	 * @param string $sChk
	 * @return void
	 */
	public function setsChk($sChk);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getpType();
	
	/**
	 * @api
	 * @param string $pType
	 * @return void
	 */
	public function setpType($pType);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getlType();
	
	/**
	 * @api
	 * @param string $lType
	 * @return void
	 */
	public function setlType($lType);
}