<?php

namespace Custom\CustomOrder\Model\Order\Email;

use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Email\Container\OrderIdentity;
use Magento\Sales\Model\Order\Email\Container\IdentityInterface;
use Magento\Sales\Model\Order\Email\Container\Template;
use Magento\Sales\Model\Order\Email\Sender;
use Magento\Sales\Model\ResourceModel\Order as OrderResource;
use Magento\Sales\Model\Order\Address\Renderer;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;

class CustomSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender{
	
	/**
	 * @var PaymentHelper
	 */
	protected $paymentHelper;
	
	/**
	 * @var OrderResource
	 */
	protected $orderResource;
	
	/**
	 * Global configuration storage.
	 *
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $globalConfig;
	
	/**
	 * @var Renderer
	 */
	protected $addressRenderer;
	
	/**
	 * Application Event Dispatcher
	 *
	 * @var ManagerInterface
	 */
	protected $eventManager;
	
	/**
	 * @var \Psr\Log\LoggerInterface
	 */
	protected $logger;
	
	/**
	 * @var Template
	 */
	protected $templateContainer;
	
	/**
	 * @var \Magento\Sales\Model\Order\Email\SenderBuilderFactory
	 */
	protected $senderBuilderFactory;
	
	/**
	 * @var TransportBuilder
	 */
	protected $transportBuilder;
	
	/**
	 * @var OrderItemExtensionFactory
	 */
	private $orderItemExtensionFactory;
	
	/**
	 * @param Template $templateContainer
	 * @param OrderResource $orderResource
	 * @param \Magento\Framework\App\Config\ScopeConfigInterface $globalConfig
	 * @param SenderBuilderFactory $senderBuilderFactory
	 * @param OrderIdentity $identityContainer
	 * @param PaymentHelper $paymentHelper
	 * @param Renderer $addressRenderer
	 * @param \Psr\Log\LoggerInterface $logger
	 * @param ManagerInterface $eventManager
	 * @param TransportBuilder $transportBuilder
     * @param \Magento\Sales\Api\Data\OrderItemExtensionFactory|null $orderItemExtensionFactory
	 */
	public function __construct(
			Template $templateContainer,
			OrderResource $orderResource,
			OrderIdentity $identityContainer,
			PaymentHelper $paymentHelper,
			Renderer $addressRenderer,
			\Magento\Framework\App\Config\ScopeConfigInterface $globalConfig,
			\Magento\Sales\Model\Order\Email\SenderBuilderFactory $senderBuilderFactory,
			ManagerInterface $eventManager,
			\Psr\Log\LoggerInterface $logger,
			TransportBuilder $transportBuilder,
			\Magento\Sales\Api\Data\OrderItemExtensionFactory $orderItemExtensionFactory = null
			) {
				$this->identityContainer = $identityContainer;
				$this->orderResource = $orderResource;
				$this->globalConfig = $globalConfig;
				$this->addressRenderer = $addressRenderer;
				$this->logger = $logger;
				$this->paymentHelper = $paymentHelper;
				$this->eventManager = $eventManager;
				$this->templateContainer = $templateContainer;
				$this->senderBuilderFactory = $senderBuilderFactory;
				$this->transportBuilder = $transportBuilder;
				$this->orderItemExtensionFactory = $orderItemExtensionFactory ?: ObjectManager::getInstance()->get(\Magento\Sales\Api\Data\OrderItemExtensionFactory::class);
	}
	
	/**
	 * @param Order $order
	 * @param bool $forceSyncMode
	 * @return bool
	 */
	public function send(Order $order, $forceSyncMode = false){
		$this->logger->info("CustomSender | send | start");
		$flag = null;
		$items = $order->getItems();
		$cityAdmins = array();
		if($items){
			foreach ($items as $item){
				$sku = $item->getSku();
				$orderId = $item->getOrderId();
				$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
				$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
				$connection = $resource->getConnection();
				$salesOrderItem = $resource->getTableName('sales_order_item');
				$cities = $resource->getTableName('cities');
				$states = $resource->getTableName('states');
				$sql = "select city,state from ".$salesOrderItem." where sku='".$sku."' and order_id='".$orderId."'";
				$orderItemArray = $connection->query($sql)->fetch();
				$city = $orderItemArray['city'];
				$state = $orderItemArray['state'];
				$sql1 = "select c.admin_emailID from ".$cities." c join ".$states." s on s.state_code=c.state_code where s.state='".$state."' and c.city='".$city."'";
				$cityArray = $connection->query($sql1)->fetch();
				if($cityArray){
					if(in_array($cityArray['admin_emailID'], $cityAdmins)){
					}
					elseif (($cityArray['admin_emailID'] != null) && !empty($cityArray['admin_emailID'])){
						$cityAdmins[] = $cityArray['admin_emailID'];
					}
				}
			}
		}
		$order->setSendEmail(true);
		if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode){
			$this->logger->info("Order Id:".$order->getId()."..Order State:".$order->getState()."..".$order->getStore()->getId());
			$this->identityContainer->setStore($order->getStore());
			if (!$this->identityContainer->isEnabled()) {
				$flag = false;
			}
			$this->prepareTemplate($order);
			/** @var SenderBuilder $sender */
			$sender = $this->getSender();
			try {
				//Email sending Logic
				
				$this->transportBuilder->setTemplateIdentifier($this->templateContainer->getTemplateId());
				$this->transportBuilder->setTemplateOptions($this->templateContainer->getTemplateOptions());
				$this->transportBuilder->setTemplateVars($this->templateContainer->getTemplateVars());
				$this->transportBuilder->setFrom($this->identityContainer->getEmailIdentity());
				
				$this->transportBuilder->addTo(
						$this->identityContainer->getCustomerEmail(),
						$this->identityContainer->getCustomerName()
						);
				if(!empty($cityAdmins)){
					foreach ($cityAdmins as $cityAdmin){
						$this->transportBuilder->addCc($cityAdmin);
					}
				}
				
				$copyTo = $this->identityContainer->getEmailCopyTo();
				
				if (!empty($copyTo) && $this->identityContainer->getCopyMethod() == 'bcc') {
					foreach ($copyTo as $email) {
						$this->transportBuilder->addBcc($email);
					}
				}
				$transport = $this->transportBuilder->getTransport();
				$transport->sendMessage();
				$sender->sendCopyTo();
			} catch (\Exception $e) {
				$this->logger->error($e->getMessage());
			}
			
			$flag = true;
			if($flag){
				$order->setEmailSent(true);
				$this->orderResource->saveAttribute($order, ['send_email', 'email_sent']);
				return true;
			}
		}else{
			$order->setEmailSent(null);
			$this->orderResource->saveAttribute($order, 'email_sent');
		}
		$this->orderResource->saveAttribute($order, 'send_email');
		
		return false;
	}
}