<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Custom\DataSync\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
    	$installer = $setup;
    	$installer->startSetup();
    	
    	// Creation of master product dump table
    	
    	if (!$installer->tableExists('master_product_dump')) {
    		$table = $installer->getConnection()->newTable(
    				$installer->getTable('master_product_dump')
    				)
    				->addColumn(
    						'id',
    						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    						null,
    						[
    								'identity' => true,
    								'nullable' => false,
    								'primary'  => true,
    								'unsigned' => true,
    						],
    						'ID'
    				)
	    			->addColumn(
	    					'sku',
	    					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
	    					255,
	    					['nullable => false'],
	    					'SKU'
	    			)
    				->addColumn(
    					'active',
    					\Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
    					null,
    					['nullable' => false, 'default' => 0],
    					'Active'
    				)
    				->addColumn(
    					'description',
    					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    					255,
    					['nullable => false'],
    					'Description'
    				)
    				->addColumn(
    						'longitude',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'Longitude'
    				)
    				->addColumn(
    						'latitude',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'Latitude'
    				)
    				->addColumn(
    						'monthly_charges',
    						\Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
    						'12,4',
    						['nullable => false', 'default' => '0.0000'],
    						'Monthly Charges'
    				)
    				->addColumn(
    						'site_selection_fee',
    						\Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
    						'12,4',
    						['nullable => false', 'default' => '0.0000'],
    						'Site Selection Fee'
    				)
    				->addColumn(
    						'site_survey_lease_fee',
    						\Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
    						'12,4',
    						['nullable => false', 'default' => '0.0000'],
    						'Site Survey Lease Fee'
    				)
    				->addColumn(
    						'city_area',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'City Area'
    				)
    				->addColumn(
    						'pole_class',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'Pole Class'
    				)
    				->addColumn(
    						'city',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'City'
    				)
    				->addColumn(
    						'state',
    						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
    						45,
    						['nullable => false'],
    						'State'
    				)
    				->addIndex(
    				    $setup->getIdxName(
    				        $installer->getTable('master_product_dump'),
    				        ['id', 'sku'],
    				        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
    				        ),
    				    ['id', 'sku'],
    				    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
    				)
    				->setComment('Master Product Dump');
    		
    				$installer->getConnection()->createTable($table);
    								
    	}
    	
    	// Creation of reserved products table
    	
    	if (!$installer->tableExists('reserved_products')) {
    		$table = $installer->getConnection()->newTable(
    				$installer->getTable('reserved_products')
    				)
    				->addColumn(
						'id',
						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
						null,
						[
							'identity' => true,
							'nullable' => false,
							'primary'  => true,
							'unsigned' => true,
						],
						'ID'
    				)
					->addColumn(
						'sku',
						\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
						45,
						['nullable => false'],
						'Sku'
					)
					->addColumn(
						'order_number',
						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
						null,
						['nullable' => false, 'default' => '0'],
						'Order Number'
					)
					->addColumn(
						'reserved_date',
						\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
						null,
						['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
						'Reserved Date'
				    )
			        ->addIndex(
			            $setup->getIdxName(
			                $installer->getTable('reserved_products'),
			                ['id', 'sku'],
			                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
			            ),
			            ['id', 'sku'],
			            ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
			        )
					->setComment('Reserved Products Table');
				$installer->getConnection()->createTable($table);
    	} 
								
		//Creation Of Product Conflict table
								
		if (!$installer->tableExists('product_conflicts')) {
		$table = $installer->getConnection()->newTable(
					$installer->getTable('product_conflicts')
					)
					->addColumn(
							'pc_id',
							\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
							null,
							[
								'identity' => true,
								'nullable' => false,
								'primary'  => true,
								'unsigned' => true,
							],
							'PC ID'
							)
								->addColumn(
								    'sku',
								    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
								    45,
								    ['nullable => false'],
								    'Sku'
								)
								->addColumn(
								'order_id',
									\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
									null,
									['nullable' => false, 'default' => '0'],
									'Order ID'
								)
								->addColumn(
									'created_timestamp',
									\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
									null,
									['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
									'Created Timestamp'
								)
								->setComment('Product Conflicts Table');
								$installer->getConnection()->createTable($table);
    	}
    	
    	// creation of impex batch table
    	
    	if (!$installer->tableExists('impex_batch')) {
    		$table = $installer->getConnection()->newTable(
    				$installer->getTable('impex_batch')
    				)
    				->addColumn(
    						'batch_id',
    						\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
    						null,
    						[
    								'identity' => true,
    								'nullable' => false,
    								'primary'  => true,
    								'unsigned' => true,
    						],
    						'Batch ID'
    						)
    							->addColumn(
    								'start_time',
    								\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
    								null,
    								['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
    								'Start Time'
    							)
    							->setComment('Impex Batch Table');
    							$installer->getConnection()->createTable($table);
    	}
    	
    	$installer->endSetup();
    }
}
