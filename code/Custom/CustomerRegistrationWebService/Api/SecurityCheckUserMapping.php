<?php

namespace Custom\CustomerRegistrationWebService\Api;

use Custom\CustomerRegistrationWebService\Api\SecurityCheckUserMappingInterface;

class SecurityCheckUserMapping implements SecurityCheckUserMappingInterface{
	
	/**
	 *
	 * @var int
	 */
	protected $id;
	
	/**
	 *
	 * @var int
	 */
	protected $userId;
	
	/**
	 *
	 * @var int
	 */
	protected $questionId;
	
	/**
	 *
	 * @var int
	 */
	protected $setId;
	
	/**
	 *
	 * @var string
	 */
	protected $answer;
	
	
	public function jsonSerialize(){
		return array(
				'id' => $this->id,
				'setId' => $this->setId,
				'questionId' => $this->questionId,
				'answer' => $this->answer,
				'userId' => $this->userId,
				
		);
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getId(){
		return $this->id;
	}
	
	/**
	 * @api
	 * @param int $id
	 * @return void
	 */
	public function setId($id){
		$this->id = $id;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getUserId(){
		return $this->userId;
	}
	
	/**
	 * @api
	 * @param int $userId
	 * @return void
	 */
	public function setUserId($userId){
		$this->userId = $userId;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getQuestionId(){
		return $this->questionId;
	}
	
	/**
	 * @api
	 * @param int $questionId
	 * @return void
	 */
	public function setQuestionId($questionId){
		$this->questionId = $questionId;
	}
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSetId(){
		return $this->setId;
	}
	
	/**
	 * @api
	 * @param int $setId
	 * @return void
	 */
	public function setSetId($setId){
		$this->setId = $setId;
	}
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getAnswer(){
		return $this->answer;
	}
	
	/**
	 * @api
	 * @param string $answer
	 * @return void
	 */
	public function setAnswer($answer){
		$this->answer = $answer;
	}
}