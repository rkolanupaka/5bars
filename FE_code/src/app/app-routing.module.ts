import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthenticationGuard } from '../common/guards/authentication.guard';
import { AdminGuard } from '../common/guards/admin.guard';

import { LoginComponent } from '../components/login/login.component';
import { ForgotpasswordComponent } from "../components/forgotpassword/forgotpassword.component";

const routes: Routes = [
    {
        path: 'site',
        loadChildren: '../modules/purchase/purchase.module#PurchaseModule',
        canActivate: [AuthenticationGuard]
    },

    {
        path: 'profile',
        loadChildren: '../modules/profile/profile.module#ProfileModule',
        canActivate: [AuthenticationGuard]
    },

    {
        path: 'orderhistory',
        loadChildren: '../modules/orders/orders.module#OrdersModule',
        canActivate: [AuthenticationGuard]
    },

    {
        path: 'admin',
        loadChildren: '../modules/admin/admin.module#AdminModule',
        canActivate: [AuthenticationGuard, AdminGuard]
    },

    {
        path: 'resetpassword/:id/:token',
        loadChildren: '../modules/reset-password/reset-password.module#ResetPasswordModule'
    },

    {
        path: 'contactus',
        loadChildren: '../modules/contact-us/contact-us.module#ContactUsModule',
        canActivate: [AuthenticationGuard]
    },

    {
        path: 'comingsoon',
        loadChildren: '../modules/coming-soon/coming-soon.module#ComingSoonModule',
        canActivate: [AuthenticationGuard]
    },

    {
        path: 'requestsite',
        loadChildren: '../modules/request-site/request-site.module#RequestSiteModule',
        canActivate: [AuthenticationGuard]
    },

    {
        path: 'forgotpassword',
        component: ForgotpasswordComponent
    },

    {
        path: 'login',
        component: LoginComponent
    },

    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },

    {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true, preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
})

export class AppRoutingModule { }
