<?php
namespace Custom\SiteManagement\Api;

interface SiteRequestsDataInterface{
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getId();
	
	/**
	 * @api
	 * @param int $id
	 * @return void
	 */
	public function setId($id);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getSku();
	
	/**
	 * @api
	 * @param string $sku
	 * @return void
	 */
	public function setSku($sku);

	/**
	 * @api
	 * @return int|null
	 */
	public function getCustomerId();
	
   /**
	* @api
	* @param int $customerId
	* @return void
	*/
	public function setCustomerId($customerId);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCustomerName();
	
	/**
	 * @api
	 * @param string $customerName
	 * @return void
	 */
	public function setCustomerName($customerName);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCustomerEmail();
	
	/**
	 * @api
	 * @param string $customerEmail
	 * @return void
	 */
	public function setCustomerEmail($customerEmail);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRequestId();
	
	/**
	 * @api
	 * @param string $requestId
	 * @return void
	 */
	public function setRequestId($requestId);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCityArea();
	
	/**
	 * @api
	 * @param string $cityArea
	 * @return void
	 */
	public function setCityArea($cityArea);
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLatitude();
	
	/**
	 * @api
	 * @param float $latitude
	 * @return void
	 */
	public function setLatitude($latitude);
	
	/**
	 * @api
	 * @return float|null
	 */
	public function getLongitude();
	
	/**
	 * @api
	 * @param float $longitude
	 * @return void
	 */
	public function setLongitude($longitude);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getPoleClass();
	
	/**
	 * @api
	 * @param string $poleClass
	 * @return void
	 */
	public function setPoleClass($poleClass);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getState();
	
	/**
	 * @api
	 * @param string $state
	 * @return void
	 */
	public function setState($state);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getCity();
	
	/**
	 * @api
	 * @param string $city
	 * @return void
	 */
	public function setCity($city);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getStatus();
	
	/**
	 * @api
	 * @param string $status
	 * @return void
	 */
	public function setStatus($status);
	
	
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getRejectedReason();
	
	/**
	 * @api
	 * @param string $rejectedReason
	 * @return void
	 */
	public function setRejectedReason($rejectedReason);
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSiteAssignedTo();
	
	/**
	 * @api
	 * @param int $siteCustomerId
	 * @return void
	 */
	public function setSiteAssignedTo($siteCustomerId);
	
	/**
	 * @api
	 * @return \DateTime|null
	 */
	public function getSiteAssignedDate();
	
	/**
	 * @api
	 * @param \DateTime $siteAssignedDate
	 * @return void
	 */
	public function setSiteAssignedDate($siteAssignedDate);
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSiteHoldDaysCount();
	
	/**
	 * @api
	 * @param int $siteHoldDaysCount
	 * @return void
	 */
	public function setSiteHoldDaysCount($siteHoldDaysCount);
	
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getSiteRequested();
	
	/**
	 * @api
	 * @param int $siteRequested
	 * @return void
	 */
	public function setSiteRequested($siteRequested);
	
}