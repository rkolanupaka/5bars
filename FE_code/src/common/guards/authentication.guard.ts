import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(
        private _cookieService:CookieService
    ) {}

    canActivate(): boolean {
        if(this._cookieService.get('5barsToken')) {
            return true;
        }
        return false;
    }
}
