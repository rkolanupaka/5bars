<?php 
namespace Custom\SiteManagement\Model;

class States extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface,
\Custom\SiteManagement\Api\Data\StatesInterface
{
    const CACHE_TAG = 'states';

    protected $_cacheTag = 'states';

    protected $_eventPrefix = 'states';

    protected function _construct()
    {
        $this->_init('Custom\SiteManagement\Model\ResourceModel\States');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
    
}