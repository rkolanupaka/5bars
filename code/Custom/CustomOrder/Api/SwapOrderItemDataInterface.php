<?php

namespace Custom\CustomOrder\Api;

interface SwapOrderItemDataInterface{
	
	/**
	 * @api
	 * @return int|null
	 */
	public function getOrderNumber();
	
	/**
	 * @api
	 * @param int $orderNumber
	 * @return void
	 */
	public function setOrderNumber($orderNumber);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getOldSku();
	
	/**
	 * @api
	 * @param string $oldSku
	 * @return void
	 */
	public function setOldSku($oldSku);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getNewSku();
	
	/**
	 * @api
	 * @param string $newSku
	 * @return void
	 */
	public function setNewSku($newSku);
	
	
	/**
	 * Get Survey Lease Flag
	 *
	 * @return int
	 */
	public function getSurveyLeaseFlag();
	
	/**
	 * Set Survey Lease Flag
	 *
	 * @param int $surveyLeaseFlag
	 * @return $this
	 */
	public function setSurveyLeaseFlag($surveyLeaseFlag);
	
	
	/**
	 * Get HasConduitRequested Flag
	 *
	 * @return int
	 */
	public function getHasConduitRequested();
	
	/**
	 * Set HasConduitRequested Flag
	 *
	 * @param int $hasConduitRequested
	 * @return $this
	 */
	public function setHasConduitRequested($hasConduitRequested);
	
	/**
	 * @api
	 * @return string|null
	 */
	public function getReasonCode();
	
	/**
	 * @api
	 * @param string $reasonCode
	 * @return void
	 */
	public function setReasonCode($reasonCode);
	
}