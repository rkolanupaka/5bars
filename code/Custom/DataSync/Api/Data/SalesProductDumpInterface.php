<?php
namespace Custom\DataSync\Api\Data;

/**
 * Interface SalesProductDumpInterface.php
 * @api
 */
interface SalesProductDumpInterface
{
    /**
     * Saves product details of the last batch
     *
     * @return void
     */
    public function saveSalesProductDetails($connection, $transaction, $results);
    
}
