<?php

namespace Custom\SiteManagement\Model;

class SiteRequestsTable extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface{
	
	const CACHE_TAG = 'site_requests';
	
	protected $_cacheTag = 'site_requests';
	
	protected $_eventPrefix = 'site_requests';
	
	protected function _construct()
	{
		$this->_init('Custom\SiteManagement\Model\ResourceModel\SiteRequestsTable');
	}
	
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	
	public function getDefaultValues()
	{
		$values = [];
		
		return $values;
	}
	
}