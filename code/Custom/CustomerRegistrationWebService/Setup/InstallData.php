<?php
/**
 * @author		Sashas
 * @category    Sashas
 * @package     Sashas_CustomerAttribute
 * @copyright   Copyright (c) 2015 Sashas IT Support Inc. (http://www.extensions.sashas.org) 
 */
namespace Custom\CustomerRegistrationWebService\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    
    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
    }
 
    
    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        
        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        
        $customerSetup->addAttribute(Customer::ENTITY, 'company_name', [
            'type' => 'varchar',
            'label' => 'Company Name',
            'input' => 'text',
            'required' => false,
            'visible' => true,
            'user_defined' => true,
            'sort_order' => 1000,
            'position' => 1000,
            'system' => 0,
        ]);
        
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'company_name')
        ->addData([
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer'],
        ]);
        
        $attribute->save();
        
        $customerSetup->addAttribute(Customer::ENTITY, 'job_title', [
        		'type' => 'varchar',
        		'label' => 'Job Title',
        		'input' => 'text',
        		'required' => false,
        		'visible' => true,
        		'user_defined' => true,
        		'sort_order' => 1000,
        		'position' => 1000,
        		'system' => 0,
        ]);
        
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'job_title')
        ->addData([
        		'attribute_set_id' => $attributeSetId,
        		'attribute_group_id' => $attributeGroupId,
        		'used_in_forms' => ['adminhtml_customer'],
        ]);
        
        $attribute->save();
       
        $customerSetup->addAttribute(Customer::ENTITY, 'security_info_updated', [
        		'type' => 'int',
        		'label' => 'Security Info Updated',
        		'input' => 'boolean',
        		'value' => 0,
        		'required' => false,
        		'visible' => true,
        		'user_defined' => true,
        		'sort_order' => 1000,
        		'position' => 1000,
        		'system' => 0,
        ]);
        
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'security_info_updated')
        ->addData([
        		'attribute_set_id' => $attributeSetId,
        		'attribute_group_id' => $attributeGroupId,
        		'used_in_forms' => ['adminhtml_customer'],
        ]);
        
        $attribute->save();
        
        //Customer Custom attribute to check whether the Logged in user is Admin or not
        
        $customerSetup->addAttribute(Customer::ENTITY, 'is_admin', [
        		'type' => 'int',
        		'label' => 'Is Admin',
        		'input' => 'boolean',
        		'value' => 0,
        		'required' => false,
        		'visible' => true,
        		'user_defined' => true,
        		'sort_order' => 1000,
        		'position' => 1000,
        		'system' => 0,
        ]);
        
        $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'is_admin')
        ->addData([
        		'attribute_set_id' => $attributeSetId,
        		'attribute_group_id' => $attributeGroupId,
        		'used_in_forms' => ['adminhtml_customer','customer_account_create','customer_account_edit'],
        ]);
        
        $attribute->save();
        
    }
}