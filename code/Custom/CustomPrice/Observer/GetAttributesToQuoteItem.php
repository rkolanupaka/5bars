<?php
    namespace Custom\CustomPrice\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\RequestInterface;
		
    class GetAttributesToQuoteItem implements ObserverInterface
    {
		/**
		* @var \Magento\Framework\App\RequestInterface
		*/
		protected $_request;
		
		/**
		* @var \Magento\Catalog\Model\ProductRepository
		*
		*/
		protected $_productRepo;
		
		/**
		* @var \Magento\Framework\Serialize\Serializer\Json
		*/
		protected $serializer;
		
		/**
		* @var Magento\Quote\Api\Data\CartItemExtensionFactory
		*/		
		private $cartItemExtensionFactory;
		
		/**
		* @param RequestInterface $request
		* @param \Magento\Catalog\Model\ProductRepository $productRepo
		* @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
		* @param \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
		*/
		public function __construct(
			\Magento\Framework\App\RequestInterface $request,
			\Magento\Catalog\Model\ProductRepository $productRepo,
			\Magento\Framework\Serialize\Serializer\Json $serializer = null,
			\Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
		){
			$this->_request = $request;
			$this->_productRepo = $productRepo;
			$this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
			$this->cartItemExtensionFactory = $cartItemExtensionFactory ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Quote\Api\Data\CartItemExtensionFactory::class);
		} 
		
        public function execute(\Magento\Framework\Event\Observer $observer) 
		{
            $logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
			$logger->info("GetAttributesToQuoteItem | execute | entered");
			//$quote_item = $observer->getQuoteItem();
			$quote_item = $observer->getItem();
			$logger->info("GetAttributesToQuoteItem | execute | quote_item found");
			$product = $quote_item->getProduct();
			//$productId = $quote_item->getProduct()->getId();
			
			if($product)
			{
				//get all the params from request
				
				
				$params = [];
				$content = $this->_request->getContent();
				if ($content) {
					$params = $this->serializer->unserialize($content);
				}
				
				$itemExtensionAttributes = $quote_item->getExtensionAttributes();
				
				if ($itemExtensionAttributes === null) {
			        $itemExtensionAttributes = $this->cartItemExtensionFactory->create();
			    }
				
				$logger->info("GetAttributesToQuoteItem | execute | after getting itemExtensionAttributes");
				
			    $logger->info("GetAttributesToQuoteItem | execute | getMonthlyCharges :".$itemExtensionAttributes->getMonthlyCharges());
			    $logger->info("GetAttributesToQuoteItem | execute | getCamFee :".$itemExtensionAttributes->getCamFee());
			    $logger->info("GetAttributesToQuoteItem | execute | getSiteSelectionFee :".$itemExtensionAttributes->getSiteSelectionFee());
			    $logger->info("GetAttributesToQuoteItem | execute | getSiteSurveyLeaseFee :".$itemExtensionAttributes->getSiteSurveyLeaseFee());
			    $logger->info("GetAttributesToQuoteItem | execute | getRentLeaseFee :".$itemExtensionAttributes->getRentLeaseFee());
			    $logger->info("GetAttributesToQuoteItem | execute | getCityArea :".$itemExtensionAttributes->getCityArea());
			    $logger->info("GetAttributesToQuoteItem | execute | getPoleClass :".$itemExtensionAttributes->getPoleClass());
			    $logger->info("GetAttributesToQuoteItem | execute | getLatitude :".$itemExtensionAttributes->getLatitude());
			    $logger->info("GetAttributesToQuoteItem | execute | getLongitude :".$itemExtensionAttributes->getLongitude());
			    $logger->info("GetAttributesToQuoteItem | execute | getState :".$itemExtensionAttributes->getState());
			    $logger->info("GetAttributesToQuoteItem | execute | getCity :".$itemExtensionAttributes->getCity());
			    
			    
				$logger->info("GetAttributesToQuoteItem | execute | after getting itemExtensionAttributes");				
				$logger->info("GetAttributesToQuoteItem | execute | leaving with set survey_lease_selected as :".$quote_item->getSurveyLeaseSelected());
				$logger->info("GetAttributesToQuoteItem | execute | leaving with set has_conduit_requested as :".$quote_item->getHasConduitRequested());
				
				$logger->info("GetAttributesToQuoteItem | execute | getSiteSurveyLeaseFee from itemExtensionAttributes :".$itemExtensionAttributes->getSiteSurveyLeaseFee());
			}
			$logger->info("GetAttributesToQuoteItem | execute | leaving");
        }
    }