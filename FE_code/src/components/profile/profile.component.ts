import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';

import { CookieService } from 'ngx-cookie';
import {DataService} from '../../common/services/data.service';

@Component({
  selector: 'site-selex-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  addNewPoTrue:boolean = false;
  addNewPcTrue:boolean = false;
  editItem:any;
  editIndex:string;
  fiveBarsToken:string;
  hostValue:string;
  isShowSaveSuccess:boolean = false;
  isEditEnabled:boolean = false;
  loggedUserData:any;
  newPoNumber:string = "";
  newPoComment:string = "";
  newPcName:string = "";
  newPcEmail:string = "";
  newPcPhone:string = "";
  newPcCompany:string = "";
  profileTabShow:boolean = true;
  ponumberTabShow:boolean = false;
  pcDetailsTabShow:boolean = false;
  changePassTabShow:boolean = false;
  securityQuesTabShow:boolean = false;
  ponumberdata:any = [];
  pcdata:any = [];  
  poAddFlag:boolean = true;
  pcAddFlag:boolean = true;
  showLoader:boolean = false;
  showPopUpMessage:string;
  showPopUpTrue:boolean = false;
  stateList:any = [];
  stateSelected = '';
  resetState = '';
  
  profileFormData = {
    "firstName" : "",
    "lastName" : "",
    "company" : "",
    "jobTitle" : "",
    "email" : "",
    "phone" : "",
    "addLine1" : "",
    "addLine2" : "",
    "city" : "",
    "state" : "",
    "countryCode" : "US",
    "zip" : ""
  };
  profileFromDataReset:any = {}

  @ViewChild('newPoNumberInp') newPoNumberInp;
  @ViewChild('newPcNumberInp') newPcNumberInp;

  constructor(private dataservice: DataService, private _cookieService:CookieService, private _eRef: ElementRef) { 
    this.fiveBarsToken = this._cookieService.get('5barsToken');
    this.hostValue = this.dataservice.getHostName();
  }

  ngOnInit() {
    this.getState();
  }

  getState() {
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    let url = this.hostValue+'/magento/index.php/rest/V1/directory/countries/US';
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDict).subscribe((data:any)=>{
      this.showLoader = false;
      console.log(data);
      this.stateList = data.available_regions;
    }, (err)=>{
      this.showLoader = false;
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "There was some issue fetching the states. Please try again.";
      }
      this.showPopUpTrue = true;
    })
  }

  stateChangeEvent(event) {
    // console.log(this.stateList[event.target.selectedIndex-1]);
    if(event.name != '') {
      // this.profileFormData.state = this.stateList[event.target.selectedIndex-1];
      (this.profileFormData.state as any) = {
        region: event.name,
        region_code: event.code,
        region_id: event.id
      }
    } else {
      this.profileFormData.state = '';
    }
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  showProfileTab() {
    this.ponumberTabShow = false;
    this.pcDetailsTabShow = false;
    this.changePassTabShow = false;
    this.securityQuesTabShow = false;
    this.profileTabShow = true;
    this.isShowSaveSuccess = false;
  }

  showPonumbersTab() {
    this.profileTabShow = false;
    this.pcDetailsTabShow = false;
    this.changePassTabShow = false;
    this.securityQuesTabShow = false;
    this.ponumberTabShow = true;
  }

  showProjectContactsTab() {
    this.profileTabShow = false;
    this.ponumberTabShow = false;
    this.changePassTabShow = false;
    this.securityQuesTabShow = false;
    this.pcDetailsTabShow = true;
  }

  showChangePassTab() {
    this.profileTabShow = false;
    this.ponumberTabShow = false;
    this.pcDetailsTabShow = false;
    this.securityQuesTabShow = false;
    this.changePassTabShow = true;
    this.isShowSaveSuccess = false;
  }

  showSecurityQueTab() {
    this.profileTabShow = false;
    this.ponumberTabShow = false;
    this.pcDetailsTabShow = false;
    this.changePassTabShow = false;
    this.securityQuesTabShow = true;
    if(this.securitySet1.length==0) {
      this.getSecurityQue();
    }
  }

  receivedUserData($event) {
    this.loggedUserData = $event;
    if(this.loggedUserData.addresses.length == 0) {
      this.isEditEnabled = true;
      this.showPopUpMessage = "Your profile is incomplete. Request you to save pending information so that you can book sites.";
      this.showPopUpTrue = true;
    } else {
      this.checkSecurityExist(this.loggedUserData);
    }
    
    this.profileFormData = {
      "firstName" : this.loggedUserData.firstname,
      "lastName" : this.loggedUserData.lastname,
      "company" : "",
      "jobTitle" : "",
      "email" : this.loggedUserData.email,
      "phone" : this.loggedUserData.addresses[0]!=undefined ? this.loggedUserData.addresses[0].telephone : "",
      "addLine1" : this.loggedUserData.addresses[0]!=undefined ? this.loggedUserData.addresses[0].street[0] : "",
      "addLine2" : this.loggedUserData.addresses[0]!=undefined ? this.loggedUserData.addresses[0].street[1] : "",
      "city" : this.loggedUserData.addresses[0]!=undefined ? this.loggedUserData.addresses[0].city : "",
      "state" : this.loggedUserData.addresses[0]!=undefined ? this.loggedUserData.addresses[0].region : "",
      "countryCode" : "US",
      "zip" : this.loggedUserData.addresses[0]!=undefined ? this.loggedUserData.addresses[0].postcode : ""
    }
    if(this.loggedUserData.custom_attributes) {
      (this.loggedUserData.custom_attributes).forEach(element => {
        if(element.attribute_code == 'company_name') {
          this.profileFormData.company = element.value;
        } else if(element.attribute_code == 'job_title') {
          this.profileFormData.jobTitle = element.value;
        }
      });
    }
    this.resetState = (this.profileFormData.state as any).region;
    this.profileFromDataReset = Object.assign({}, this.profileFormData);
    this.loadPoAndPcDetails();
  }

  // edit profile data method starts
  editUserProfileDetails(profileformdata){
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    let data = {
      "customer": {
        "id": this.loggedUserData.id,
        "email": this.loggedUserData.email,
        "firstname": profileformdata.value.firstName,
        "lastname": profileformdata.value.lastName,
        "store_id": this.loggedUserData.store_id,
        "website_id": this.loggedUserData.website_id,
        "addresses": [
            {
                "customer_id": this.loggedUserData.id,
                "region": {
                    "region": (this.profileFormData.state as any).region,
                    "region_id": (this.profileFormData.state as any).region_id
                },
                "country_id": "US",
                "street": [
                  profileformdata.value.addLine1,
                  profileformdata.value.addLine2
                ],
                "telephone": profileformdata.value.phone,
                "postcode": profileformdata.value.zip,
                "firstname": profileformdata.value.firstName,
                "lastname": profileformdata.value.lastName,
                "city": profileformdata.value.city,
                "default_shipping": true,
                "default_billing": true
            }
        ], "custom_attributes": [
          {
              "attribute_code": "job_title",
              "value":profileformdata.value.jobTitle
          },
          {
              "attribute_code": "company_name",
              "value": profileformdata.value.company
          }
  
        ]
      }
    }
    let url = this.hostValue+'/magento/index.php/rest/V1/customers/me';
    this.showLoader = true;
    this.dataservice.fetchPutData(url, data, headerDict).subscribe((data)=>{
      this.showLoader = false;
      localStorage.setItem("fivebarsUserData", JSON.stringify(data));
      this.isShowSaveSuccess = true;
      this.profileFromDataReset = Object.assign({}, this.profileFormData);
      this.resetState = (this.profileFormData.state as any).region;
      window.scroll(0,0);
      this.checkSecurityExist(data);
    }, (err)=>{
      this.showLoader = false;
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "Something went wrong...";
      }
      this.showPopUpTrue = true;
    })
    this.isEditEnabled=false; 
  }
  // edit profile data method ends

  checkSecurityExist(data) {
    if(data.custom_attributes) {
      let securityQueCheck = false;
      (this.loggedUserData.custom_attributes).forEach((element) => {
        if(element.attribute_code == 'security_info_updated') {
          if(element.value == "1") {
            securityQueCheck = true;
          }
        }
      });
      if(!securityQueCheck) {
        this.profileTabShow = false;
        this.securityQuesTabShow = true;
        this.getSecurityQue();
        this.isSecQueEnabled = true;
        this.showPopUpMessage = "Security questions and answers are mandatory for protection of your account.";
        this.showPopUpTrue = true;
      }
    }
  }

  resetFrofileForm() {
    this.profileFormData = Object.assign({}, this.profileFromDataReset);
    console.log(this.resetState);
    (this.profileFormData.state as any).region = this.resetState;
    this.isEditEnabled = false;
  }

  // load po numbers and project contacts on page load starts
  loadPoAndPcDetails(){
    let url = this.hostValue + "/magento/index.php/rest/default/V1/customer/setup/"+this.loggedUserData.id;
    let headerDect = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe((data:any)=>{
      this.showLoader = false;
      this.ponumberdata = data.po_number.items;
      this.pcdata = data.project_contact.items;
    }, (err)=> {
      this.showLoader = false;      
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "Something went wrong while loading your PO numbers and Project Contacts.";
      }
      this.showPopUpTrue = true;
    })
  }
  // load po numbers and project contacts on page load ends

  // save new or edit po number method starts
  savePoNo(poForm) {
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    let url = this.hostValue+'/magento/index.php/rest/default/V1/customer/ponumber';
    let data:any = {
      "customerId" : this.loggedUserData.id,
      "ponumber" : poForm.value.newPoNumber,
      "comment" : poForm.value.newPoComment
    }
    this.showLoader = true;
    if(this.poAddFlag == true) {
      this.dataservice.fetchPostData(url, data, headerDict).subscribe((data)=>{
        let newPoId = data;
        this.ponumberdata.push({
          "po_id" : newPoId,
          "po_number" : poForm.value.newPoNumber,
          "comment" : poForm.value.newPoComment
        })
        this.showLoader = false;
        this.addNewPoTrue = false;
      }, (error)=>{
        this.poSaveErrorFunc(error);
      });
    } else {
      data.poId = this.editItem.po_id;
      this.dataservice.fetchPutData(url,data, headerDict).subscribe((data)=>{
        this.showLoader = false;
        this.editItem.po_number = poForm.value.newPoNumber;
        this.editItem.comment = poForm.value.newPoComment;
        this.addNewPoTrue = false;
      }, (error)=>{
        this.poSaveErrorFunc(error);
      });
    }
    
  }
  // save new or edit po number method ends

  poSaveErrorFunc(err) {
    try {
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "There was an error. Please try again.";
      }
    } 
    catch (err) {
      this.showPopUpMessage = "There was an error. Please try again.";
    }
    this.showPopUpTrue = true;
    this.showPopUpTrue = true;
    this.showLoader = false;
    this.addNewPoTrue = false;
  }

  // save new or edit pc details method starts
  savePcData(pcSaveForm) {
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    let url = this.hostValue+'/magento/index.php/rest/default/V1/customer/project/contact';
    let data:any = {
      "customerId" : this.loggedUserData.id,
      "name" : pcSaveForm.value.newPcName,
      "email" : pcSaveForm.value.newPcEmail,
      "company" : pcSaveForm.value.newPcCompany,
      "phone" : pcSaveForm.value.newPcPhone
    };
    this.showLoader = true;
    if(this.pcAddFlag == true) {   
      this.dataservice.fetchPostData(url, data, headerDict).subscribe((data)=>{
        this.pcdata.push({
          "contact_id" : data,
          "name" : pcSaveForm.value.newPcName,
          "email" : pcSaveForm.value.newPcEmail,
          "company" : pcSaveForm.value.newPcCompany,
          "phone" : pcSaveForm.value.newPcPhone
        });
        this.showLoader = false;
        this.addNewPcTrue = false;
      }, (error)=>{
        this.pcSaveErrorFunc(error);
      });
    } else {
      data.contactId = this.editItem.contact_id;
      this.dataservice.fetchPutData(url, data, headerDict).subscribe((data)=>{
        this.editItem.name = pcSaveForm.value.newPcName;
        this.editItem.email = pcSaveForm.value.newPcEmail;
        this.editItem.company = pcSaveForm.value.newPcCompany;
        this.editItem.phone = pcSaveForm.value.newPcPhone;
        this.showLoader = false;
        this.addNewPcTrue = false;
      }, (error)=>{
        this.pcSaveErrorFunc(error);
      });
    }
  }
  // save new or edit pc details method ends

  pcSaveErrorFunc(err) {
    if(err.message) {
      this.showPopUpMessage = err.message;
    } else {
      this.showPopUpMessage = "There was an error. Please try again.";
    }
    this.showPopUpTrue = true;
    this.showLoader = false;
    this.addNewPcTrue = false;
  }

  // delete po number method starts
  deletePo(itemId, index) {
    let url = this.hostValue+'/magento/index.php/rest/default/V1/customer/ponumber/'+itemId;
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchDeleteData(url, headerDict).subscribe((data)=>{
      this.showLoader = false;
      this.ponumberdata.splice(index, 1);
    }, (error)=>{
      this.showLoader = false;
      this.showPopUpMessage = "There was some issue deleting Purchase Order. Kindly try again.";
      this.showPopUpTrue = true;
    });
  }
  // delete po number method ends

  // delete pc detail method starts
  deletePc(itemId, index) {
    let url = this.hostValue+'/magento/index.php/rest/default/V1/customer/project/contact/'+itemId;
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    };
    this.showLoader = true;
    this.dataservice.fetchDeleteData(url, headerDict).subscribe((data)=>{
      this.showLoader = false;
      this.pcdata.splice(index, 1);
    }, (error)=>{
      this.showLoader = false;
      this.showPopUpMessage = "There was some issue deleting Project Contact. Kindly try again.";
      this.showPopUpTrue = true;
    });
  }
  // delete pc detail method ends

  addPo() {
    this.addNewPoTrue=true;
    this.poAddFlag=true;
    this.newPoNumber = "";
    this.newPoComment = "";
    this.editItem = "";
    this.editIndex = "";
    
    setTimeout(()=> {
      this.newPoNumberInp.nativeElement.focus();
    }, 1);
  }

  // edit po number method starts
  editPo(item, index) {
    this.addNewPoTrue = true;
    this.poAddFlag = false;
    this.newPoNumber = item.po_number;
    this.newPoComment = item.comment;
    this.editItem = item;
    this.editIndex = index;
    setTimeout(()=> {
      this.newPoNumberInp.nativeElement.focus();
    }, 1);
  }
  // edit po number method ends

  addPc() {
    this.addNewPcTrue=true;
    this.pcAddFlag=true;
    this.newPcName = "";
    this.newPcEmail = "";
    this.newPcPhone = "";
    this.newPcCompany = "";
    this.editItem = "";
    this.editIndex = "";
    setTimeout(()=> {
      this.newPcNumberInp.nativeElement.focus();
    }, 1);
  }

  // edit pc number method starts
  editPc(item, index) {
    this.pcAddFlag = false;
    this.addNewPcTrue = true;
    this.newPcName = item.name;
    this.newPcEmail = item.email;
    this.newPcPhone = item.phone;
    this.newPcCompany = item.company;
    this.editItem = item;
    this.editIndex = index;
    setTimeout(()=> {
      this.newPcNumberInp.nativeElement.focus();
    }, 1);
  }
  // edit pc number method ends

  changePassword(passForm) {
    this.isShowSaveSuccess = false;
    if(passForm.newPassword === passForm.conNewPassword) {
      let url = this.hostValue+'/magento/index.php/rest/default/V1/customers/me/password';
      let headerDict = {
        "Content-Type": "application/json",
        "authorization": "Bearer " + this.fiveBarsToken
      };
      let data = {
        "currentPassword": passForm.curPassword,
        "newPassword": passForm.newPassword
      }
      this.showLoader = true;
      this.dataservice.fetchPutData(url,data, headerDict).subscribe((data)=>{
        this.showLoader = false;
        this.isShowSaveSuccess = true;
      }, (error)=>{
        this.showLoader = false;
        if(error.message) {
          this.showPopUpMessage = error.message;
        } else {
          this.showPopUpMessage = "There was some issue changing password. Kindly try again.";
        }
        this.showPopUpTrue = true;
      });
    } else {
      this.showPopUpMessage = "Confirm password does not match new password.";
      this.showPopUpTrue = true;
    }
  }

  securitySet1 = [];
  securitySet2 = [];
  securitySet3 = [];
  set1Ques:number;
  set2Ques:number;
  set3Ques:number;
  set1Id;
  set2Id;
  set3Id;
  set1Ans:string = '';
  set2Ans:string = '';
  set3Ans:string = '';
  isSecQueEnabled = false;
  tempSecFormData:any;

  getSecurityQue() {
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    let url = this.hostValue+'/magento/index.php/rest/V1/ProfileManagement/questionnaire/'+this.loggedUserData.id;
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDict).subscribe((data:any)=>{
      this.showLoader = false;
      this.tempSecFormData = data.preferences;
      this.securitySet1 = data.questions.set1;
      this.securitySet2 = data.questions.set2;
      this.securitySet3 = data.questions.set3;
      if(data.preferences.length>0) {
        this.resetSecForm(data.preferences);
      } else {
        this.isSecQueEnabled = true;
      }
      console.log(data);
    }, (err)=>{
      this.showLoader = false;
      if(err.message) {
        this.showPopUpMessage = err.message;
      } else {
        this.showPopUpMessage = "There was some issue fetching the security questions. Please try again.";
      }
      this.showPopUpTrue = true;
    })
  }

  resetSecForm(data) {
    (data).forEach(element => {
        if(element.setId == "1") {
        this.set1Ques = Number(element.questionId);
        this.set1Ans = element.answer;
        this.set1Id = element.id;
      } else if(element.setId == "2") {
        this.set2Ques = Number(element.questionId);
        this.set2Ans = element.answer;
        this.set2Id = element.id;
      } else if(element.setId == "3") {
        this.set3Ques = Number(element.questionId);
        this.set3Ans = element.answer;
        this.set3Id = element.id;
      }
    });
  }

  updateSecurityQues() {
    let headerDict = {
      "Content-Type": "application/json",
      "authorization": "Bearer " + this.fiveBarsToken
    }
    let url = this.hostValue+'/magento/index.php/rest/V1/ProfileManagement/questionnaire';
    let data = {
      "answers":[
        {
          "id": this.set1Id,
          "userId": this.loggedUserData.id,
          "questionId": this.set1Ques,
          "answer": this.set1Ans
        },
        {
          "id": this.set2Id,
          "userId": this.loggedUserData.id,
          "questionId": this.set2Ques,
          "answer": this.set2Ans
        },
        {
          "id": this.set3Id,
          "userId": this.loggedUserData.id,
          "questionId": this.set3Ques,
          "answer": this.set3Ans
        }
      ]
    }
    
    this.showLoader = true;
    
    if(this.set1Id || this.set2Id || this.set3Id) {
      this.dataservice.fetchPutData(url, data, headerDict).subscribe((data:any)=>{
        this.showLoader = false;
        this.isSecQueEnabled = false;
        this.tempSecFormData = data.preferences;
        this.resetSecForm(data.preferences);
      }, (err)=>{
        this.showLoader = false;
        this.isSecQueEnabled = false;
        if(err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "There was some updating the security questions. Please try again.";
        }
        this.showPopUpTrue = true;
      })
    } else {
      this.dataservice.fetchPostData(url, data, headerDict).subscribe((data:any)=>{
        this.showLoader = false;
        this.isSecQueEnabled = false;
        this.tempSecFormData = data.preferences;
        this.resetSecForm(data.preferences);
      }, (err)=>{
        this.showLoader = false;
        this.isSecQueEnabled = false;
        if(err.message) {
          this.showPopUpMessage = err.message;
        } else {
          this.showPopUpMessage = "There was some updating the security questions. Please try again.";
        }
        this.showPopUpTrue = true;
      })
    }
    
  }
}