import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
// import { AgmMap, AgmDataLayer } from "@agm/core";
import { Router, ActivatedRoute } from "@angular/router";

import { CookieService } from "ngx-cookie";
import { DataService } from "../../common/services/data.service";
import { FormatSitesResponse } from "../../common/services/format-sites-response.service";
// import { searchFilterPipe } from "../../common/pipes/order-filter.pipe";
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';

@Component({
  selector: "app-site-search",
  templateUrl: "./site-search.component.html",
  styleUrls: ["./site-search.component.scss"]
})
export class SiteSearchComponent implements OnInit {

  selectedSiteId = new FormControl('', [
    Validators.required,
    Validators.pattern('^[a-zA-Z0-9]{9}(,[a-zA-Z0-9]{9})*'),
    Validators.maxLength(99)
  ]);

  allSitesOptionalAdd: boolean = false;
  citySelected = "";
  citylist = [];
  citywdoutinvtrylist = [];
  chosenSites = [];
  chosenSitesTemp = [];
  disableFileUpload: boolean = true;
  fiveBarsToken: string;
  fiveBarsCartId: string;
  hostValue: string;
  // baseLocalHost: string;
  // infiniteScrollCount: number = 10;
  parseInt = parseInt;
  sitesZeroCartCount: number = 0;
  stateSelected = "";
  siteIdError: boolean = false;
  siteIdErrorText: boolean = false;
  showPopUpMessage: string;
  showPopUpTrue: boolean = false;
  // selectedSiteId: string;
  selectedStateText: string;
  selectedCityText: string;
  statecitylist = [];
  // statecitydataurl = "./assets/data/search_statecity.json";
  siteIdData = [];
  showLoader: boolean = false;
  selectedStateValue;
  showUploadedAvailableSiteDet: boolean = false;
  showUploadedNonAvailableSiteDet: boolean = false;
  showUploadedWaitlistSiteDet: boolean = false;
  searchValue: string = "";
  searchValueFilter: string = "";
  searchValueCounter: number = 0;
  searchByType = "Site ID";
  uploadedSiteIds: string = "";
  uploadedFileName: string = "";
  uploadedFileFlag: boolean = false;
  uploadSiteIdError: boolean = false;
  uploadSiteIdErrorText: boolean = false;
  uploadedSitesInvalid = [];
  validSiteError: boolean = true;
  chosenSitesAvailableFiltered: any = [];
  chosenSitesWaitFiltered: any = [];
  mySiteMap:any;

  @ViewChild("fileUploadInput") fileUploadInput;

  @ViewChild("agmMapNested") myMap;

  @HostListener("window:resize")
  onWindowResize() {
    if (this.myMap) {
      this.myMap.triggerResize().then(() => {
        console.log("resized");
        (this.myMap as any)._mapsWrapper.setCenter({
          lat: this.markerInfoWindowObj.markerInfoWindowLat,
          lng: this.markerInfoWindowObj.markerInfoWindowLng
        });
      });
    }
  }

  activeScreen: string = "findSiteByInput";

  openNav: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private dataservice: DataService,
    private _cookieService: CookieService,
    private router: Router,
    private formatSitesResponse: FormatSitesResponse
  ) {
    this.route.queryParams.subscribe(params => {
      this.activeScreen = params.type;
      // if(params.sku) {
      //   this.stateSelected = params.state;
      //   this.citySelected = params.city;
      //   this.selectedSiteId = params.sku;
      // }
    });
  }

  receivedClosePopup() {
    this.showPopUpTrue = false;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    this.hostValue = this.dataservice.getHostName();
    // this.baseLocalHost = this.dataservice.getLocalHostName();
    this.fiveBarsToken = this._cookieService.get("5barsToken");
    this.loadStateAndCities();
    this.loadSitesFromLocalWdoutCart();
  }

  loadStateAndCities() {
    let url = this.hostValue + "/magento/index.php/rest/V1/state/cities";
    let headerDect = {
      "Content-Type": "application/json"
    };
    this.showLoader = true;
    this.dataservice.fetchData(url, headerDect).subscribe(
      (data:any) => {
        this.showLoader = false;
        this.statecitylist = data.items.sort(function(a, b) {
          return a.state.localeCompare(b.state);
        });
        if (localStorage.getItem("preStateCity") != undefined) {
          this.preselectStateCity();
        }
        if(!this._cookieService.get('5barsCartId')) {
          this.createCartId();
        }
      },
      err => {
        this.showLoader = false;
        this.showPopUpMessage = "Something went wrong while loading states. Please try again later.";
        this.showPopUpTrue = true;
      }
    );
  }

  loadSitesFromLocalWdoutCart() {
    if (localStorage.getItem("selectedSiteDataWdotCart")) {
      this.chosenSites = JSON.parse(localStorage.getItem("selectedSiteDataWdotCart"));
    }
  }

  preselectStateCity() {
    let stateCityPreData = localStorage.getItem("preStateCity").split("***");
    this.stateSelected = stateCityPreData[0];
    this.onStateChange(stateCityPreData[0]);
    this.citySelected = stateCityPreData[1];
    this.selectedCityText = stateCityPreData[1];
    if (this.activeScreen == "findSiteByInput" && this.selectedCityText != "Select City") {
      this.loadGoogleMaps();
    }
  }

  createCartId() {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    let data = {};
    let url = this.hostValue + "/magento/rest/default/V1/carts/mine";
    this.dataservice.fetchPostData(url, data, headerDict).subscribe(
      (data:any) => {
        this._cookieService.put("5barsCartId", data);
        this.fiveBarsCartId = data;
        // this.route.queryParams.subscribe(params => {
          if(localStorage.getItem('duplicateSku')) {
            this.selectedSiteId.setValue(localStorage.getItem('duplicateSku'));
            localStorage.removeItem('duplicateSku');
            this.addChosenSite();
          }
        // });
      },
      err => {
        this.showPopUpMessage = "There was some issue creating your Cart ID. Request you to login again.";
        this.showPopUpTrue = true;
        setTimeout(() => {
          this.router.navigate([""]);
        }, 4000);
      }
    );
  }

  onStateChange(state) {
    if (state.value != undefined) {
      this.selectedStateText = state.value;
    } else {
      this.selectedStateText = state;
    }

    var stateCode = this.stateSelected;
    if (stateCode == "") {
      this.citylist = [];
      this.citywdoutinvtrylist = [];
      this.citySelected = "";
      this.selectedCityText = "Select City";
    } else {
      for (let a of this.statecitylist) {
        if (a.state == stateCode) {
          this.citylist = a.cities.sort(function(a, b) {
            return a.localeCompare(b);
          });
          this.citywdoutinvtrylist = a.cities_without_inventory.sort(function(a, b) {
            return a.localeCompare(b);
          });
        }
      }
      this.citySelected = "";
    }
  }

  onCityChange(city) {
    this.resetAllErrors();
    this.selectedCityText = city.value;
    this.selectedSiteId.setValue("");
    localStorage.setItem(
      "preStateCity",
      this.stateSelected + "***" + this.citySelected
    );
    if (this.activeScreen == "findSiteByInput" && this.selectedCityText != "Select City") {
      this.loadGoogleMaps();
    }
  }

  // validateSiteField() {
  //   let enteredSitesArray = this.selectedSiteId.value.replace(/ /g, "").split(",");
  //   this.resetAllErrors();
  //   enteredSitesArray.forEach((site, index) => {
  //     if (site.trim().length != 9) {
  //       this.siteIdError = true;
  //       this.siteIdErrorText = true;
  //     }
  //   });
  //   if (enteredSitesArray.length > 10) {
  //     this.siteIdError = true;
  //     this.siteIdErrorText = false;
  //   } else if (this.selectedSiteId.value.replace(/ /g, "").indexOf(",,") > -1) {
  //     this.siteIdError = true;
  //     this.siteIdErrorText = true;
  //   }
  // }

  resetAllErrors() {
    this.siteIdError = false;
    this.uploadSiteIdError = false;
    this.uploadSiteIdError = false;
  }

  fileUpload(event) {
    let self = this;
    let target = event.target || event.srcElement;
    if (target.files.length != 0) {
      this.commonFileUpload(target.files[0]);
    }
  }

  removeUploadedSitesFile() {
    this.allSitesOptionalAdd = false;
    this.uploadedFileName = "";
    this.uploadedSiteIds = "";
    this.fileUploadInput.nativeElement.value = "";
    this.uploadedFileFlag = false;
    this.chosenSitesTemp = [];
  }

  onFileChangeInDirective(evt) {
    this.commonFileUpload(evt[0]);
  }

  commonFileUpload(fileDetail) {
    this.resetAllErrors();
    if (this.citySelected == "" || this.stateSelected == "") {
      this.fileUploadInput.nativeElement.value = "";
      this.uploadedSiteIds = "";
      this.uploadSiteIdError = true;
      this.uploadSiteIdErrorText = false;
      return;
    }
    this.siteIdError = false;
    let reader = new FileReader();
    let self = this;
    self.selectedSiteId.setValue("");
    reader.readAsText(fileDetail);
    reader.onload = function() {
      self.uploadedFileName = fileDetail.name;
      self.uploadedSiteIds = reader.result
        .trim()
        .replace(/ /g, "")
        .replace(/\r?\n|\r/g, "")
        .replace(/,/g, ", ")
        .toUpperCase();
      self.addChosenSite();
    };
  }

  // download kmz method starts
  downloadKmz() {
    if (
      this.selectedCityText == undefined ||
      this.selectedStateText == undefined ||
      this.selectedStateText == "Select State" ||
      this.selectedCityText == "Select City"
    ) {
      this.showPopUpMessage = "Please select state and city";
      this.showPopUpTrue = true;
    } else {
      this.showLoader = true;
      let headerDect = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      let url =
        this.hostValue +
        "/magento/index.php/rest/V1/report/site/location/" +
        this.selectedStateText +
        "/" +
        this.selectedCityText;
      this.dataservice.fetchPlainData(url, headerDect).subscribe(
        (data:any) => {
          this.showLoader = false;
          if (data == '"false"') {
            this.showPopUpMessage = "No data available for this selection.";
            this.showPopUpTrue = true;
          } else {
            window.open(
              this.hostValue +
                "/magento/index.php/rest/V1/report/site/location/" +
                this.selectedStateText +
                "/" +
                this.selectedCityText,
              "_self"
            );
          }
        },
        err => {
          this.showLoader = false;
          if (err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong. Please try again";
          }
          this.showPopUpTrue = true;
        }
      );
    }
  }
  // download kmz method ends

  // add enetered sites method starts
  addChosenSite() {
    if (this.uploadedFileFlag) {
      this.chosenSites = this.chosenSites.concat(this.chosenSitesTemp);
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      this.removeUploadedSitesFile();
      return;
    }
    this.uploadedSitesInvalid = [];
    let enteredSitesString =
      this.uploadedSiteIds != "" ? this.uploadedSiteIds.replace(/ /g, "") : this.selectedSiteId.value.replace(/ /g, "");
    let enteredSitesArray =
      this.uploadedSiteIds != ""
        ? this.uploadedSiteIds.replace(/ /g, "").split(",")
        : this.selectedSiteId.value.replace(/ /g, "").split(",");
    let enteredSitesAlreadyArray = [];
    let allSitesValid = true;
    let enteredSitesArrayTemp = enteredSitesArray.map(x => x);
    let alreadyAddedCounter = 0;
    let inValidorUnavailableSites = [];
    enteredSitesArray.forEach((site, index) => {
      if (site.trim().length != 9) {
        allSitesValid = false;
      }
      let isAlreadyAdded = this.checkIfAlreadyAdded(site.trim());
      if (isAlreadyAdded) {
        enteredSitesArrayTemp.splice(index - alreadyAddedCounter, 1);
        enteredSitesAlreadyArray.push(site.trim());
        enteredSitesString = enteredSitesString.replace(site.trim(), "");
        alreadyAddedCounter++;
      }
    });

    if (enteredSitesString.replace(/[,]/, "") == "") {
      if (enteredSitesArray.length > 1) {
        this.showPopUpMessage = "Sites you are trying to add are already added and cannot be added again.";
      } else {
        this.showPopUpMessage = "Site you are trying to add is already added and cannot be added again.";
      }
      this.showPopUpTrue = true;
      return false;
    }

    if (allSitesValid) {
      this.selectedSiteId.setValue("");
      this.showLoader = true;
      var url =
        this.hostValue +
        "/magento/index.php/rest/V1/products?" +
        "searchCriteria[filter_groups][0][filters][0][field]=sku&" +
        "searchCriteria[filter_groups][0][filters][0][value]=" +
        enteredSitesString +
        "&" +
        "searchCriteria[filter_groups][0][filters][0][condition_type]=in&" +
        "searchCriteria[filter_groups][1][filters][0][field]=state&" +
        "searchCriteria[filter_groups][1][filters][0][value]=" +
        this.selectedStateText +
        "&" +
        "searchCriteria[filter_groups][1][filters][0][condition_type]=like&" +
        "searchCriteria[filter_groups][2][filters][0][field]=city&" +
        "searchCriteria[filter_groups][2][filters][0][value]=" +
        this.selectedCityText +
        "&" +
        "searchCriteria[filter_groups][2][filters][0][condition_type]=like&fields=items[sku,name,status,custom_attributes,price],total_count";

      const headerDict = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + this.fiveBarsToken
      };
      this.dataservice.fetchData(url, headerDict).subscribe(
        (data:any) => {
          let tempResponseSites = data.items;
          if (tempResponseSites != null && tempResponseSites.length > 0) {
            let formattedSites = this.formatSitesResponse.getSitesFormatted(tempResponseSites, "all");
            if (this.uploadedSiteIds == "") {
              this.chosenSites = this.chosenSites.concat(formattedSites);
            } else {
              this.chosenSitesTemp = this.chosenSitesTemp.concat(formattedSites);
            }

            if (this.uploadedSiteIds == "") {
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
            } else {
              if (this.chosenSitesTemp.length > 0) {
                this.chosenSitesAvailableFiltered = this.chosenSitesTemp.filter(item => item.reserved == false);
                this.chosenSitesWaitFiltered = this.chosenSitesTemp.filter(item => item.reserved == true);
                this.showUploadedAvailableSiteDet = false;
                this.showUploadedNonAvailableSiteDet = false;
                this.showUploadedWaitlistSiteDet = false;
                this.uploadedFileFlag = true;
              }
            }

            console.log(this.chosenSitesTemp);
          }
          this.showLoader = false;

          //error handling for site search starts
          let errorChosenSitesTemp = [];
          if (this.uploadedSiteIds == "") {
            errorChosenSitesTemp = this.chosenSites;
          } else {
            errorChosenSitesTemp = this.chosenSitesTemp;
          }
          enteredSitesArrayTemp.forEach((site, index) => {
            let added = false;
            errorChosenSitesTemp.forEach(element => {
              if (element.uniqueId.toLowerCase() == site.toLowerCase()) {
                added = true;
              }
            });
            if (added == false) {
              inValidorUnavailableSites.push(site);
            }
          });

          let fullAlreadyAddedSiteString: string;
          if (enteredSitesAlreadyArray.length > 0) {
            fullAlreadyAddedSiteString = enteredSitesAlreadyArray[0];
            if (enteredSitesAlreadyArray.length > 1) {
              enteredSitesAlreadyArray.forEach((site, index) => {
                if (index != 0) {
                  fullAlreadyAddedSiteString += ", " + site;
                }
              });
              fullAlreadyAddedSiteString =
                "Sites " + fullAlreadyAddedSiteString + " are already added and cannot be added again.";
            } else {
              fullAlreadyAddedSiteString =
                "Site " + fullAlreadyAddedSiteString + " is already added and cannot be added again.";
            }
          }

          let fullInvalidSIteString: string;
          if (inValidorUnavailableSites.length > 0) {
            fullInvalidSIteString = inValidorUnavailableSites[0];
            if (inValidorUnavailableSites.length > 1) {
              inValidorUnavailableSites.forEach((site, index) => {
                if (index != 0) {
                  fullInvalidSIteString += ", " + site;
                }
              });
              fullInvalidSIteString = "Sites " + fullInvalidSIteString + " are either invalid or not available.";
            } else {
              fullInvalidSIteString = "Site " + fullInvalidSIteString + " is either invalid or not available.";
            }
          }

          if (this.uploadedSiteIds != "") {
            this.uploadedSitesInvalid = inValidorUnavailableSites.concat(enteredSitesAlreadyArray);
            if (this.uploadedSitesInvalid.length > 0) {
              this.showUploadedAvailableSiteDet = false;
              this.showUploadedNonAvailableSiteDet = false;
              this.showUploadedWaitlistSiteDet = false;
              this.uploadedFileFlag = true;
            }
          } else {
            if (fullInvalidSIteString != undefined && fullAlreadyAddedSiteString != undefined) {
              this.showPopUpMessage = fullInvalidSIteString + "<br>" + fullAlreadyAddedSiteString;
              this.showPopUpTrue = true;
            } else if (fullInvalidSIteString != undefined) {
              this.showPopUpMessage = fullInvalidSIteString;
              this.showPopUpTrue = true;
            } else if (fullAlreadyAddedSiteString != undefined) {
              this.showPopUpMessage = fullAlreadyAddedSiteString;
              this.showPopUpTrue = true;
            }
          }
          //error handling for site search ends
          window.scrollTo(0, document.body.scrollHeight);
        },
        err => {
          this.showPopUpMessage = "something went wrong while fetching the Sites. Please try again.";
          this.showPopUpTrue = true;
          this.selectedSiteId.setValue("");
          this.showLoader = false;
        }
      );
    } else {
      if (this.uploadedSiteIds != "") {
        this.uploadSiteIdError = true;
        this.uploadSiteIdErrorText = true;
        this.uploadedSiteIds = "";
        this.fileUploadInput.nativeElement.value = "";
      } else {
        this.siteIdError = true;
        this.siteIdErrorText = true;
      }
    }
  }
  // add enetered sites method ends

  checkIfAlreadyAdded(siteId) {
    let added = false;
    this.chosenSites.forEach(element => {
      if (element.uniqueId.toLowerCase() == siteId.toLowerCase()) {
        added = true;
      }
    });
    return added;
  }

  // remove site method starts
  removeSite(site) {
    if (site.item_id) {
      let headerDict = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      this.showLoader = true;
      let url = "";
      if (site.reserved) {
        url = this.hostValue + "/magento/rest/V1/waitlist/delete/" + site.uniqueId;
      } else {
        url = this.hostValue + "/magento/rest/V1/carts/mine/items/" + site.item_id;
      }
      this.dataservice.fetchDeleteData(url, headerDict).subscribe(
        data => {
          this.showLoader = false;
          this.chosenSites.splice(this.chosenSites.indexOf(site), 1);
          localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
          if (localStorage.getItem("selectedSiteDataWidCart")) {
            let withCartSitesTemp = JSON.parse(localStorage.getItem("selectedSiteDataWidCart"));
            let spliceIdex: number;
            withCartSitesTemp.forEach((element, index) => {
              if (element.uniqueId == site.uniqueId) {
                spliceIdex = index;
              }
            });
            if (spliceIdex != undefined) {
              withCartSitesTemp.splice(spliceIdex, 1);
              localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(withCartSitesTemp));
            }
          }
          if (this.chosenSites.length == 0) {
            this.sitesZeroCartCount++;
          }
          this.searchValueCounter++;
        },
        err => {
          this.showLoader = false;
          if (err.message) {
            this.showPopUpMessage = err.message;
          } else {
            this.showPopUpMessage = "something went wrong..";
          }
          this.showPopUpTrue = true;
        }
      );
    } else {
      this.chosenSites.splice(this.chosenSites.indexOf(site), 1);
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      if (this.chosenSites.length == 0) {
        this.sitesZeroCartCount++;
      }
      this.searchValueCounter++;
    }
  }
  // remove site method ends

  changeOptionalFeeStatus(index, event) {
    let site = this.chosenSites[index];
    let tempEvent = event;
    if (site.item_id) {
      let surveyFlag = 0;
      let conduitFlag = 0;

      if(event.name.indexOf('optionalFeeStatus') > -1) {
        if (site.optionalFeeStatus) {
          surveyFlag = 0;
        } else {
          surveyFlag = 1;
        }
        conduitFlag = site.optionalConduitStatus?1:0;
      } else {
        if(site.optionalConduitStatus) {
          conduitFlag = 0;
        } else {
          conduitFlag = 1;
        }
        surveyFlag = site.optionalFeeStatus?1:0;
      }
      
      let headerDict = {
        "Content-Type": "application/json",
        authorization: "Bearer " + this.fiveBarsToken
      };
      let url = this.hostValue + "/magento/rest/default/V1/custom/carts/mine/items";
      let siteData = {
        surveyLeaseFlag: surveyFlag,
        hasConduitRequested: conduitFlag,
        cart_item: {
          item_id: site.item_id,
          quote_id: this._cookieService.get('5barsCartId'),
          sku: site.uniqueId,
          qty: 1
        }
      };
      this.showLoader = true;
      this.dataservice.fetchPutData(url, siteData, headerDict).subscribe(
        data => {
          let withCartSitesTemp = JSON.parse(localStorage.getItem("selectedSiteDataWidCart"));
          withCartSitesTemp.forEach((element, index) => {
            if (element.uniqueId == site.uniqueId) {
              if(event.name.indexOf('optionalFeeStatus') > -1) {
                element.optionalFeeStatus = !element.optionalFeeStatus;
              } else {
                element.optionalConduitStatus = !element.optionalConduitStatus;
              }
            }
            localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(withCartSitesTemp));
          });
          this.changeOptionalFeeStatusCommon(index, event);
          this.showLoader = false;
        },
        error => {
          this.showLoader = false;
          this.showPopUpMessage = "something went wrong. Please try again";
          this.showPopUpTrue = true;
          tempEvent.checked = !tempEvent.checked;
        }
      );
    } else {
      this.changeOptionalFeeStatusCommon(index, event);
    }
  }

  changeOptionalFeeStatusCommon(index, event) {
    let site = this.chosenSites[index];
    if(event.name.indexOf('optionalFeeStatus') > -1) {
      site.optionalFeeStatus = !site.optionalFeeStatus;
      setTimeout(() => {
        if (event.checked) {
          site.one_time_fee_price = Number(site.site_selection_fee) + Number(site.site_survey_lease_fee);
        } else {
          site.one_time_fee_price = Number(site.site_selection_fee);
        }
        localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      }, 100);
    } else {
      site.optionalConduitStatus = !site.optionalConduitStatus; 
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
    } 
  }

  changeAllOptionalFeeStatus(event) {
    if (event.checked) {
      this.chosenSitesTemp.forEach(element => {
        element.optionalFeeStatus = true;
        element.one_time_fee_price = Number(element.site_selection_fee) + Number(element.site_survey_lease_fee);
      });
    } else {
      this.chosenSitesTemp.forEach(element => {
        element.optionalFeeStatus = false;
        element.one_time_fee_price = Number(element.site_selection_fee);
      });
    }
  }

  // changeOptionalConduitStatus() {

  // }

  checkAllSitesReservedFlag() {
    let temp = this.chosenSites.filter(item => item.reserved == false);
    if (temp.length > 0) {
      return true;
    }
    return false;
  }

  allSitesAddedToWaitListFlag = false;
  addAllSitesToWaitList() {
    let counter = 0;
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    this.chosenSites.forEach(element => {
      if (element.item_id == undefined) {
        let url = this.hostValue + "/magento/rest/V1/waitlist/add/" + element.uniqueId;
        this.showLoader = true;
        this.dataservice.fetchPostData(url, {}, headerDict).subscribe(
          data => {
            this.showLoader = false;
            this.chosenSites.forEach(element2 => {
              if (element2.uniqueId == element.uniqueId) {
                element2.item_id = counter + 1;
              }
            });
            counter++;
            if (counter == this.chosenSites.length) {
              this.showLoader = false;
              this.chosenSites = [];
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
              localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
              this.allSitesAddedToWaitListFlag = true;
            }
          },
          error => {
            this.showPopUpMessage = "something went wrong...";
            this.showPopUpTrue = true;
            this.showLoader = false;
          }
        );
      } else {
        counter++;
      }
    });
    if (counter == this.chosenSites.length) {
      this.showLoader = false;
      this.chosenSites = [];
      localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
      localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
      this.allSitesAddedToWaitListFlag = true;
    }
  }

  reserveSites(e) {
    let counter = 0;
    let errorOccured: boolean = false;
    this.chosenSites.forEach(element => {
      if (element.item_id == undefined) {
        let headerDict = {
          "Content-Type": "application/json",
          authorization: "Bearer " + this.fiveBarsToken
        };
        let data = {};
        if (element.reserved) {
          let url = this.hostValue + "/magento/rest/V1/waitlist/add/" + element.uniqueId;
          this.showLoader = true;
          this.dataservice.fetchPostData(url, data, headerDict).subscribe(
            data => {
              this.chosenSites.forEach(element2 => {
                if (element2.uniqueId == element.uniqueId) {
                  element2.item_id = counter + 1;
                }
              });
              counter++;
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
              if (counter == this.chosenSites.length) {
                this.showLoader = false;
                localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
                this.router.navigate(["site/payment"]);
              }
              if (errorOccured) {
                this.showLoader = false;
              }
            },
            error => {
              errorOccured = true;
              if (error.message != undefined) {
                if (error.message.match(/"([^"]+)"/) != undefined) {
                  this.showPopUpMessage =
                    "Sorry " +
                    JSON.parse(error.responseText).message.match(/"([^"]+)"/)[1] +
                    " was reserved by someone just now and not available for reservation";
                } else {
                  this.showPopUpMessage = error.message;
                }
              } else {
                this.showPopUpMessage = "something went wrong...";
              }
              this.showPopUpTrue = true;
              this.showLoader = false;
              return false;
            }
          );
        } else {
          let url = this.hostValue + "/magento/rest/V1/carts/mine/items";
          let tempStatus = element.optionalFeeStatus + "";
          let tempConduitStatus = element.optionalConduitStatus + "";
          let siteData = {
            survey_lease_selected: tempStatus,
            has_conduit_requested: tempConduitStatus,
            cart_item: {
              quote_id: this._cookieService.get('5barsCartId'),
              sku: element.uniqueId,
              qty: 1
            }
          };
          this.showLoader = true;
          this.dataservice.fetchPostData(url, siteData, headerDict).subscribe(
            (data:any) => {
              this.chosenSites.forEach(element => {
                if (element.uniqueId == data.sku) {
                  element.item_id = data.item_id;
                }
              });
              counter++;
              localStorage.setItem("selectedSiteDataWdotCart", JSON.stringify(this.chosenSites));
              if (counter == this.chosenSites.length) {
                this.showLoader = false;
                localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
                this.router.navigate(["site/payment"]);
              }
              if (errorOccured) {
                this.showLoader = false;
              }
            },
            error => {
              errorOccured = true;
              if (error.message != undefined) {
                if (error.message.match(/"([^"]+)"/) != undefined) {
                  this.showPopUpMessage =
                    "Sorry " +
                    JSON.parse(error.responseText).message.match(/"([^"]+)"/)[1] +
                    " was reserved by someone just now and not available for reservation";
                } else {
                  this.showPopUpMessage = error.message;
                }
              } else {
                this.showPopUpMessage = "something went wrong...";
              }
              this.showPopUpTrue = true;
              this.showLoader = false;
              return false;
            }
          );
        }
      } else {
        counter++;
      }
    });
    if (counter == this.chosenSites.length) {
      this.showLoader = false;
      localStorage.setItem("selectedSiteDataWidCart", JSON.stringify(this.chosenSites));
      this.router.navigate(["site/payment"]);
    }
  }

  markers: any = [];
  // google maps zoom level
  zoom: number = 15;

  // initial center position for the map
  lat: number = 38.79930767201779;
  lng: number = -77.12911152370515;
  infoWindowIsOpen: boolean = false;
  infoWindowZoom: number = 15;
  mapLoading: boolean = false;
  markerInfoWindowObj: any = {
    markerInfoWindowLat: 0,
    markerInfoWindowLng: 0,
    markerInfoWindowSku: "",
    markerInfoWindowPoleType: "",
    markerInfoWindowLocationType: "",
    markerInfoWindowElectical: "",
    markerInfoWindowStatus: ""
  };

  loadGoogleMaps() {
    let headerDict = {
      "Content-Type": "application/json",
      authorization: "Bearer " + this.fiveBarsToken
    };
    
    let url =  "../assets/search-data/" 
    + this.selectedStateText + "_" + this.selectedCityText+".txt";
    
    // url = this.hostValue + "/magento/rest/V1/report/site/location/maps/" 
    //   + this.selectedStateText + "/" + this.selectedCityText;

    this.mapLoading = true;

    // if(this.baseLocalHost === 'local') {
    //   url = "./assets/data/"+this.selectedStateText+"_"+this.selectedCityText+".json"
    // } else {
    //   url = this.hostValue + "/assets/data/"+this.selectedStateText+"_"+this.selectedCityText+".json"
    // }

    // this.dataservice.fetchData(url, headerDict).subscribe(
    this.dataservice.fetchData(url).subscribe(
      data => {
        this.zoom = 15;
        this.mapLoading = false;
        this.markers = data;
        this.lat = this.markers[0].lat;
        this.lng = this.markers[0].long;
        console.log("success");
      },
      err => {
        console.log(err);
        this.mapLoading = false;
      }
    );
  }

  boundsChange(event) {
    console.log(event);
  }

  centerChange(event) {
    console.log(event);
  }

  resizeCounter = 0;
  markerInfoWindowSkuIsOpen = false;
  markerClicked(siteData) {
    console.log(siteData);
    this.infoWindowZoom = 15;
    this.markerInfoWindowObj.markerInfoWindowSku = siteData.sku;
    this.markerInfoWindowObj.markerInfoWindowLng = siteData.long;
    this.markerInfoWindowObj.markerInfoWindowLat = siteData.lat;
    this.markerInfoWindowObj.markerInfoWindowStatus = siteData.sChk;
    this.markerInfoWindowObj.markerInfoWindowPoleType = siteData.pCls;
    this.markerInfoWindowObj.markerInfoWindowLocationType = siteData.cArea;
    this.infoWindowIsOpen = true;
    let resizeTimeOut;
    if (this.resizeCounter == 0) {
      resizeTimeOut = setTimeout(() => {
        let event = document.createEvent("Event");
        if (typeof Event === "function") {
          event = new Event("resize");
        } else {
          event = document.createEvent("Event");
          event.initEvent("resize", true, true);
        }
        window.dispatchEvent(event);
        this.markerInfoWindowSkuIsOpen = true;
      }, 100);
    } else if (this.resizeCounter == 1) {
      clearTimeout(resizeTimeOut);
    }
    this.resizeCounter++;
  }

  infoWindowClose(event) {
    // console.log(event);
    this.infoWindowIsOpen = false;
  }

  mapReady(map) {
    this.mySiteMap = map;
  }

  mapReset() {
    this.mySiteMap.setCenter({ lat: this.lat, lng: this.lng });
    this.mySiteMap.setZoom(this.zoom);
  }
  // maps css ends

  addSIteFromMap() {
    this.selectedSiteId.setValue(this.markerInfoWindowObj.markerInfoWindowSku);
    this.addChosenSite();
    this.infoWindowIsOpen = false;
  }
}


// export validateSiteField(control: FormControl) { 
//   let enteredSitesArray = control.value.replace(/ /g, "").split(",");
//   this.resetAllErrors();
//   enteredSitesArray.forEach((site, index) => {
//     if (site.trim().length != 9) {
//       // this.siteIdError = true;
//       // this.siteIdErrorText = true;
//       return false;
//     }
//   });
// }