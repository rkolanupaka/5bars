<?php
    namespace Custom\CustomPrice\Observer;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\RequestInterface;
		
    class SetAttributesToQuoteItem implements ObserverInterface
    {
		/**
		* @var \Magento\Framework\App\RequestInterface
		*/
		protected $_request;
		
		/**
		* @var \Magento\Catalog\Model\ProductRepository
		*
		*/
		protected $_productRepo;
		
		/**
		* @var \Magento\Framework\Serialize\Serializer\Json
		*/
		protected $serializer;
		
		/**
		* @var Magento\Quote\Api\Data\CartItemExtensionFactory
		*/		
		private $cartItemExtensionFactory;
		
		/**
		* @param RequestInterface $request
		* @param \Magento\Catalog\Model\ProductRepository $productRepo
		* @param \Magento\Framework\Serialize\Serializer\Json|null $serializer
		* @param \Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
		*/
		public function __construct(
			\Magento\Framework\App\RequestInterface $request,
			\Magento\Catalog\Model\ProductRepository $productRepo,
			\Magento\Framework\Serialize\Serializer\Json $serializer = null,
			\Magento\Quote\Api\Data\CartItemExtensionFactory $cartItemExtensionFactory
		){
			$this->_request = $request;
			$this->_productRepo = $productRepo;
			$this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Framework\Serialize\Serializer\Json::class);
			$this->cartItemExtensionFactory = $cartItemExtensionFactory ?: \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\Quote\Api\Data\CartItemExtensionFactory::class);
		} 
		
        public function execute(\Magento\Framework\Event\Observer $observer) 
		{
            $logger = \Magento\Framework\App\ObjectManager::getInstance()->get('\Psr\Log\LoggerInterface');
			$logger->info("SetAttributesToQuoteItem | execute | entered");
			//$quote_item = $observer->getQuoteItem();
			$quote_item = $observer->getItem();
			$logger->info("SetAttributesToQuoteItem | execute | quote_item found");
			$product = $quote_item->getProduct();
			//$productId = $quote_item->getProduct()->getId();
			
			if($product)
			{
				//get all the params from request
				
				
				$params = [];
				$content = $this->_request->getContent();
				if ($content) {
					$params = $this->serializer->unserialize($content);
				}
				
				$itemExtensionAttributes = $quote_item->getExtensionAttributes();
				
				if ($itemExtensionAttributes === null) {
			        $itemExtensionAttributes = $this->cartItemExtensionFactory->create();
			    }
			    $logger->info("SetAttributesToQuoteItem | execute | before setting itemExtensionAttributes");
			    $logger->info("SetAttributesToQuoteItem | execute | before setting itemExtensionAttributes & ProdCity: ".$product->getCity()." ProdSKU: ".$product->getSku());
			    $itemExtensionAttributes->setMonthlyCharges($product->getMonthlyCharges());
			    $itemExtensionAttributes->setCamFee($product->getCamFee());
			    $itemExtensionAttributes->setSiteSelectionFee($product->getSiteSelectionFee());
			    $itemExtensionAttributes->setSiteSurveyLeaseFee($product->getSiteSurveyLeaseFee());
			    $itemExtensionAttributes->setRentLeaseFee($product->getRentLeaseFee());
			    $itemExtensionAttributes->setCityArea($product->getCityArea());
			    $itemExtensionAttributes->setPoleClass($product->getPoleClass());
			    $itemExtensionAttributes->setLatitude($product->getLatitude());
			    $itemExtensionAttributes->setLongitude($product->getLongitude());
			    $itemExtensionAttributes->setState($product->getState());
			    $itemExtensionAttributes->setCity($product->getCity());
				
				$quote_item->setExtensionAttributes($itemExtensionAttributes);
			    
			    $quote_item->setMonthlyCharges($product->getMonthlyCharges());
			    $quote_item->setCamFee($product->getCamFee());
			    $quote_item->setSiteSelectionFee($product->getSiteSelectionFee());
			    $quote_item->setSiteSurveyLeaseFee($product->getSiteSurveyLeaseFee());
			    $quote_item->setRentLeaseFee($product->getRentLeaseFee());
			    $quote_item->setCityArea($product->getCityArea());
			    $quote_item->setPoleClass($product->getPoleClass());
			    $quote_item->setLatitude($product->getLatitude());
			    $quote_item->setLongitude($product->getLongitude());
			    $quote_item->setState($product->getState());
			    $quote_item->setCity($product->getCity());
			    
			    $logger->info("SetAttributesToQuoteItem | execute | MonltyCharges: ".$quote_item->getMonthlyCharges());
			    
			   // $quote_item->save();
			    
				$logger->info("SetAttributesToQuoteItem | execute | after setting itemExtensionAttributes");				
				$logger->info("SetAttributesToQuoteItem | execute | leaving with set survey_lease_selected as :".$quote_item->getSurveyLeaseSelected());
				$logger->info("SetAttributesToQuoteItem | execute | leaving with set has_conduit_requested as :".$quote_item->getHasConduitRequested());
				
				//$logger->info("SetAttributesToQuoteItem | execute | getSiteSurveyLeaseFee from itemExtensionAttributes :".$itemExtensionAttributes->getSiteSurveyLeaseFee());	
			}
			$logger->info("SetAttributesToQuoteItem | execute | leaving");
        }
    }