<?php

namespace Custom\CustomerRegistrationWebService\Api\Data;

/**
 * @api
 * @since 100.0.2
 */
interface SetUpProfileResultInterface
{
    /**
     * Get attribute.
     *
     * @return \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterface
     */
    public function getProjectContact();
    
    /**
     * Set attribute.
     *
     * @param \Custom\CustomerRegistrationWebService\Api\Data\ProjectContactSearchResultInterface $projectContact
     * @return $this
     */
    public function setProjectContact($projectContact);
    
    /**
     * Get attribute.
     *
     * @return \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterface
     */
    public function getPoNumber();
    
    /**
     * Set attribute.
     *
     * @param \Custom\CustomerRegistrationWebService\Api\Data\PONumberSearchResultInterface $poNumber
     * @return $this
     */
    public function setPoNumber($poNumber);

}
