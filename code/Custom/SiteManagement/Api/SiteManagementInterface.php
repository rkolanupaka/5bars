<?php



namespace Custom\SiteManagement\Api;

use Custom\SiteManagement\Api\Data\StatesInterface;
use Magento\Catalog\Api\Data\ProductInterface;



/**
 * Defines Sevice Contract for Site Management class
 */
interface SiteManagementInterface
{
    
	/**
	 * This method will return all the states available irrespective of their status
	 * @return \Custom\SiteManagement\Api\Data\StatesResultInterface
	 */
    public function getList();
    
   /**
    * This method will retrun all the cities asociatyed with the given state
    * 
    * @param string $stateCode
    * @return \Custom\SiteManagement\Api\Data\CitiesResultInterface
    */
    public function getCities($stateCode);
    
    /**
     * This method will retrun all active states along with the active and inactive cities
     *
     * @return \Custom\SiteManagement\Api\Data\StateWithCitiesResultInterface
     */
    public function getStateCities();
    
    /**
     * This method will update city information.
     * It will also make a call whether to update state's status depending upon all the cities for the repective state.
     * 
     * @param int $cityId
     * @param int $enabled
     * @param int $comingSoon
     * @param string $email
     * @return int
     */
    public function updateCity($cityId,$enabled,$comingSoon,$email);
    
    
    /**
     * Create new Sites
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\State\InputMismatchException If the provided SKU is already exists
     */
    public function saveSites(\Magento\Catalog\Api\Data\ProductInterface $product);
    
    /**
     * Edit Existing Site
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function editSite(\Magento\Catalog\Api\Data\ProductInterface $product);
    
}